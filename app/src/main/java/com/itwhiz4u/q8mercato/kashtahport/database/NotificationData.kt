package com.itwhiz4u.q8mercato.kashtahport.database
import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import android.os.Parcel
import android.os.Parcelable

@Entity(tableName = "notification")
class NotificationData() :Parcelable {
    @PrimaryKey(autoGenerate = false)
    @ColumnInfo(name = "id")
    var id:String = ""

    @ColumnInfo(name = "type")
    var type:String = ""

    @ColumnInfo(name = "read")
    var read:Int = 0

    constructor(parcel: Parcel) : this() {
        id = parcel.readString()
        type = parcel.readString()
        read = parcel.readInt()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(id)
        parcel.writeString(type)
        parcel.writeInt(read)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<NotificationData> {
        override fun createFromParcel(parcel: Parcel): NotificationData {
            return NotificationData(parcel)
        }

        override fun newArray(size: Int): Array<NotificationData?> {
            return arrayOfNulls(size)
        }
    }


}


/* var id :String?= null
    var name :String?= null
    var packagePrice :String?= null
    var packageType :String?= null
    var description :String?= null
    var status :String?= null
    var createdAt :String?= null
    var updatedAt :String?= null
    var amountPayable :String?= null
    var modelId :String?= null
    var brandId :String?= null
    var isExpand:Boolean = false*/