package com.itwhiz4u.q8mercato.kashtahport.helper;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by bodacious-dev-8 on 26/6/17.
 */

public class MyPreferenceManager {


    public static String KEY_INVITE_APP_LINK = "KEY_INVITE_APP_LINK";
    private Context _context;
    private SharedPreferences pref;
    private SharedPreferences.Editor editor;
    private static final String PREF_NAME = "KASHTAPortPreferences";





    public MyPreferenceManager(Context context) {
        this._context = context;
        pref = _context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
        editor = pref.edit();
    }

    public SharedPreferences.Editor getEditor() {
        return editor;
    }

    public SharedPreferences getPref() {
        return pref;
    }


    public void clear() {

        String lang = pref.getString(Constant.KEY_LAN,"en");

        editor.clear();
        editor.commit();

        setString(Constant.KEY_LAN,lang);
    }


    public int getInt(String key) {

        return pref.getInt(key,0);
    }

    public void setInt(String key, int value) {

        editor.putInt(key,value);
        editor.commit();
    }



    public String getString(String key) {

        return pref.getString(key,"");
    }

    public void setString(String key, String value) {

        editor.putString(key,value);
        editor.commit();
    }


    public Boolean getBoolean(String key) {

        return pref.getBoolean(key,false);
    }


    public void setBoolean(String key, Boolean value) {

        editor.putBoolean(key,value);
        editor.commit();
    }


}