package com.itwhiz4u.q8mercato.kashtahport.adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.itwhiz4u.q8mercato.kashtahport.R
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import kotlinx.android.synthetic.main.list_item_property_image.view.*

class ListImageAdapter(private var context: Context, val list: ArrayList<String>, val listener: OnItemClick) : RecyclerView.Adapter<ListImageAdapter.ViewHolder>() {

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

    var isVideo = false
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.list_item_property_image, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        downloadImage(list[position],holder)


        holder.itemView.image.setOnClickListener {

            listener.onItemClick(position)
        }

        holder.itemView.ll_reload.setOnClickListener {

            listener.onItemClick(position)
        }

    }

    private fun downloadImage(url: String, holder: ViewHolder) {

        Glide.with(context)
                .asBitmap()
                .load(url)
                .apply(RequestOptions().override(250))
                .into(holder.itemView.image)
    }

    override fun getItemCount(): Int {
        return list.size
    }


    interface OnItemClick {
        fun onItemClick(position: Int);
    }
}