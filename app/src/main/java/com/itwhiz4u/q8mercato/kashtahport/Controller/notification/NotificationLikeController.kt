package com.itwhiz4u.q8mercato.kashtahport.Controller.notification

import android.content.Context
import com.itwhiz4u.q8mercato.kashtahport.adapter.NotificationListAdapter
import com.itwhiz4u.q8mercato.kashtahport.model.NotificationModel
import kotlinx.android.synthetic.main.list_item_notification_listing.view.*

class NotificationLikeController(val context: Context, val  adapter: NotificationListAdapter, val holder: NotificationListAdapter.MyListingViewHolder, val  model: NotificationModel, val listener: NotificationListAdapter.OnItemClick, val position: Int) {
    fun control(){
        /*if(position%2 == 0){
            holder.itemView.back1.visibility = View.GONE
            holder.itemView.back2.visibility = View.VISIBLE
        }else{
            holder.itemView.back1.visibility = View.VISIBLE
            holder.itemView.back2.visibility = View.GONE
        }*/
        adapter.viewBinderHelper.bind(holder.itemView.swipe_revel,""+position)
        holder.itemView.noti_content.text = model.message
        val ind = model.message.indexOf("@")
        if(ind != -1){
            val name = model.message.substring(0,ind).split(" ")
            val content = model.message.substring(ind+1)

            //Log.e("NotificationFe", " "+name.size+" "+content+" "+name[0])
            holder.itemView.noti_heading.text = name[0]
            holder.itemView.noti_content.text = content
        }else{
            holder.itemView.noti_content.text = model.message
        }


        // holder.itemView.time.text = ConstantFunction.getOnlyDate(model.created_at.toLong(),Constant.DEFAULT_DATE_FORMAT2)
        holder.itemView.time.text = adapter.daysCount.printDifference(System.currentTimeMillis(),model.created_at.toLong())

        holder.itemView!!.ll_des.setOnClickListener {

            listener.onItemClick(position)
        }

        holder.itemView!!.frame_delete.setOnClickListener {

            listener.onDelete(position)
        }
    }
}