package com.itwhiz4u.q8mercato.kashtahport.model

import android.os.Parcel
import android.os.Parcelable

class AmenitiesModel() :Parcelable {

    var id = ""
    var name = ""
    var status = false

    constructor(parcel: Parcel) : this() {
        id = parcel.readString()
        name = parcel.readString()
        status = parcel.readByte() != 0.toByte()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(id)
        parcel.writeString(name)
        parcel.writeByte(if (status) 1 else 0)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<AmenitiesModel> {
        override fun createFromParcel(parcel: Parcel): AmenitiesModel {
            return AmenitiesModel(parcel)
        }

        override fun newArray(size: Int): Array<AmenitiesModel?> {
            return arrayOfNulls(size)
        }
    }
}