package com.itwhiz4u.q8mercato.kashtahport.Retrofit.Model.Response

import com.google.gson.annotations.SerializedName

data class UserDataResponse(
        @SerializedName("error")
        var error: Boolean,

        @SerializedName("message")
        var message: String,

        @SerializedName("userData")
        var userData: UserData? = null

)

class UserData {
    @SerializedName("_id")
    var id = ""

    @SerializedName("name")
    var name = ""

    @SerializedName("email")
    var email = ""

    @SerializedName("password")
    var password = ""

    @SerializedName("phone")
    var phone = ""

    @SerializedName("type")
    var type = ""

    @SerializedName("profile_image_url")
    var profileImageUrl = ""

    @SerializedName("device_id")
    var deviceId = ""

    @SerializedName("notification_token")
    var notificationToken = ""

    @SerializedName("access_token")
    var access_token = ""

    @SerializedName("online_status")
    var online_status: Boolean = false

    @SerializedName("can_sale")
    var can_sale: Boolean = false


    @SerializedName("last_login")
    var lastLogin = ""

    @SerializedName("created_at")
    var createdAt = ""

    @SerializedName("is_blocked")
    var isBlocked = ""

    @SerializedName("country_code")
    var countryCode = ""

    @SerializedName("facebook")
    var facebook = ""

    @SerializedName("description")
    var description = ""

    @SerializedName("profession")
    var profession = ""

    @SerializedName("favourites")
    var favList = ArrayList<String>()

    @SerializedName("IBAN")
    var IBAN: UserIban? = null
}

class UserIban {
    @SerializedName("IBAN")
    var IBAN = ""

    @SerializedName("country_code")
    var countryCode = ""

    @SerializedName("bank_name")
    var bank_name = ""

    @SerializedName("phone")
    var phone = ""

}
