package com.itwhiz4u.q8mercato.kashtahport.model

import android.os.Parcel
import android.os.Parcelable
import com.itwhiz4u.q8mercato.kashtahport.Api.ApiUrl

class TapPaymentModel() : Parcelable {

    var chargeid : String = ""
    var crd : String = ""
    var crdtype : String = ""
    var hash : String = ""
    var payid : String = ""
    var ref : String = ""
    var result : String = ""
    var trackid : String = ""
    var amount : String = "10"
    var orderNumber : String = "ORD-1001"
    var currency : String = "KWD"
    var redirect : String = ApiUrl.getPaymetLoader
    var description : String = "Test Transaction"
    var firstName : String = "Name"
    var receiptSms : String = "SMS"
    var receiptEmail : String = "test@gmail.com"
    var statementDescriptor : String = "Test Txn 001"
    var capture : Boolean = true

    constructor(parcel: Parcel) : this() {
        chargeid = parcel.readString()
        crd = parcel.readString()
        crdtype = parcel.readString()
        hash = parcel.readString()
        payid = parcel.readString()
        ref = parcel.readString()
        result = parcel.readString()
        trackid = parcel.readString()
        amount = parcel.readString()
        orderNumber = parcel.readString()
        currency = parcel.readString()
        redirect = parcel.readString()
        description = parcel.readString()
        firstName = parcel.readString()
        receiptSms = parcel.readString()
        receiptEmail = parcel.readString()
        statementDescriptor = parcel.readString()
        capture = parcel.readByte() != 0.toByte()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(chargeid)
        parcel.writeString(crd)
        parcel.writeString(crdtype)
        parcel.writeString(hash)
        parcel.writeString(payid)
        parcel.writeString(ref)
        parcel.writeString(result)
        parcel.writeString(trackid)
        parcel.writeString(amount)
        parcel.writeString(orderNumber)
        parcel.writeString(currency)
        parcel.writeString(redirect)
        parcel.writeString(description)
        parcel.writeString(firstName)
        parcel.writeString(receiptSms)
        parcel.writeString(receiptEmail)
        parcel.writeString(statementDescriptor)
        parcel.writeByte(if (capture) 1 else 0)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<TapPaymentModel> {
        override fun createFromParcel(parcel: Parcel): TapPaymentModel {
            return TapPaymentModel(parcel)
        }

        override fun newArray(size: Int): Array<TapPaymentModel?> {
            return arrayOfNulls(size)
        }
    }


}