package com.itwhiz4u.q8mercato.kashtahport.activities

import android.os.Bundle
import com.itwhiz4u.q8mercato.kashtahport.R

class ProfileActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_profile)
    }
}
