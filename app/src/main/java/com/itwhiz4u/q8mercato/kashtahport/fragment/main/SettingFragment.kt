package com.itwhiz4u.q8mercato.kashtahport.fragment.main


import android.app.Dialog
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.*
import com.itwhiz4u.q8mercato.kashtahport.Api.ApiResponseListener
import com.itwhiz4u.q8mercato.kashtahport.Api.ServiceRequest

import com.itwhiz4u.q8mercato.kashtahport.R
import com.itwhiz4u.q8mercato.kashtahport.activities.*
import com.itwhiz4u.q8mercato.kashtahport.database.AppDatabase
import com.itwhiz4u.q8mercato.kashtahport.helper.Constant
import com.itwhiz4u.q8mercato.kashtahport.helper.MyApplication
import com.itwhiz4u.q8mercato.kashtahport.model.EventModel
import com.facebook.accountkit.AccountKit
import com.facebook.login.LoginManager
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.dialog_alert.*
import kotlinx.android.synthetic.main.fragment_setting.view.*
import kotlinx.android.synthetic.main.view_header_toolbar.view.*
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread
import org.json.JSONObject
import java.lang.Exception

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 *
 */
class SettingFragment : Fragment() {

    var fragPosition= -1

    lateinit var fragView :View

     var mainActivity: MainActivity?= null

    fun setActivity(mainActivity: MainActivity){
        this.mainActivity = mainActivity
    }

    var dialog: Dialog ?= null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        fragView = inflater.inflate(R.layout.fragment_setting, container, false)

        fragView.header_name.text = getString(R.string.frag_sett_hint_header_name)

        try {
            dialog = Dialog(mainActivity!!)
        }catch (e: Exception){

        }

        initUI()

        initLogoutDailog()

        return fragView
    }

    private fun initLogoutDailog() {

        dialog!!.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog!!.setContentView(R.layout.dialog_alert)
        dialog!!.window.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

        val window = dialog!!.window
        window.setWindowAnimations(R.style.Animation)

        val wlp = window.getAttributes()
        wlp.width = WindowManager.LayoutParams.MATCH_PARENT
        wlp.height = WindowManager.LayoutParams.WRAP_CONTENT
        wlp.gravity = Gravity.BOTTOM
        window.setAttributes(wlp)

        dialog!!.alt_content.setText("Are you sure want to logout?")

        dialog!!.alt_content.visibility = View.VISIBLE

        dialog!!.alt_ok.setOnClickListener {

            logoutUser()

            dialog!!.cancel()
        }

        dialog!!.alt_cancel.setOnClickListener {

            dialog!!.cancel()
        }
    }

    override fun onResume() {
        EventBus.getDefault().register(this)
        mainActivity?.progress?.dismiss()
        super.onResume()
    }

    override fun onPause() {
        EventBus.getDefault().unregister(this)
        super.onPause()
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onMessageEvent(event: EventModel) {
        when(event.type){
            Constant.EVENT_UPDATE_MAIN ->{

                if(mainActivity?.view_pager!!.currentItem == Constant.SETTING_POSITION){
                   initUI()
                }

            }
        }
    }

    private fun initUI() {

        try {
            fragView.switch_notificarion.isChecked =   mainActivity!!.prefManager?.pref.getBoolean(Constant.KEY_NOTIFICATION_SOUND,true)

        }catch (e: Exception){

        }

        fragView.contact_us.setOnClickListener {
            val intent = Intent(activity!!,SupportActivity::class.java)
            startActivity(intent)
        }

        fragView.frame_book_request.setOnClickListener {

            val intent = Intent(activity!!,BookingListActivity::class.java)

            startActivity(intent)
        }

        fragView.frame_property.setOnClickListener {

            val intent = Intent(activity!!,PropertyListActivity::class.java)
            startActivity(intent)
        }

        fragView.frame_profile.setOnClickListener {
            mainActivity!!.view_pager.setCurrentItem(3)
        }

        fragView.view_switch_noti.setOnClickListener {

            updateNotificationStatus()
        }

        fragView.about.setOnClickListener {
            startActivity(Intent(activity!!,AboutUsctivity::class.java))
        }

        fragView.privacy.setOnClickListener {
            startActivity(Intent(activity!!,PrivacyPolicyActivity::class.java))
        }

        fragView.frame_iban.setOnClickListener {
            startActivity(Intent(activity!!,IBANActivity::class.java))
        }

        fragView.frame_transaction.setOnClickListener {
            startActivity(Intent(activity!!,TransactionListActivity::class.java))
        }

        fragView.frame_language.setOnClickListener {
           startActivity(Intent(activity!!,LanguageActivity::class.java))
        }

        fragView.frame_login.setOnClickListener {
            if(MyApplication.instance.prefManager.getBoolean(Constant.KEY_USER_LOGIN_STATUS)){

                dialog!!.show()

            }else{
               LoginActivity.startLogin(activity!!,true,"")
            }
        }

        if(MyApplication.instance.prefManager.getBoolean(Constant.KEY_USER_LOGIN_STATUS)){
            fragView.frame_login.text = getString(R.string.hint_logout)

            fragView.ll_request.visibility = View.VISIBLE
            fragView.ll_notification.visibility = View.VISIBLE
            fragView.ll_tran.visibility = View.VISIBLE
        }else{
            fragView.frame_login.text = getString(R.string.hint_login)

            fragView.ll_request.visibility = View.GONE
            fragView.ll_notification.visibility = View.GONE
            fragView.ll_tran.visibility = View.GONE
        }
    }

    private fun updateNotificationStatus() {

        val param = JSONObject()
        param.put("api_key", Constant.API_KEY)
        param.put("enable", !fragView.switch_notificarion.isChecked)
        param.put("user_id", mainActivity?.prefManager?.getString(Constant.KEY_USER_ID))

        mainActivity?.progress?.show()

        ServiceRequest(object :ApiResponseListener{
            override fun onCompleted(`object`: Any) {

                val flag =  !fragView.switch_notificarion.isChecked
                fragView.switch_notificarion.isChecked = flag
                mainActivity?.prefManager?.setBoolean(Constant.KEY_NOTIFICATION_SOUND,flag)
                mainActivity?.progress?.dismiss()

            }

            override fun onError(errorMessage: String) {
                mainActivity?.errorDialog?.show(getString(R.string.hint_error),errorMessage)
                mainActivity?.progress?.dismiss()
            }

        }).updateNotificationStatus(param)
    }


    private fun logoutUser() {
        mainActivity!!.progress?.show()

        val email =  mainActivity!!.prefManager.getString(Constant.KEY_USER_EMAIL)
        val psd =  mainActivity!!.prefManager.getString(Constant.KEY_USER_PASSWORD)
        //val cnf_psd = fragView.et_cnf_password.text.toString().trim()

        val param = JSONObject()
        param.put("api_key",Constant.API_KEY)
        param.put("email",email)
        param.put("password",psd)

        ServiceRequest(object : ApiResponseListener {
            override fun onCompleted(`object`: Any) {
                mainActivity!!.progress?.dismiss()
                MyApplication.instance.prefManager.clear()
                MyApplication.instance.prefManager.setBoolean(Constant.KEY_USER_LOGIN_STATUS,false)
                mainActivity!!.view_pager.currentItem = 0
                doAsync {
                    AppDatabase.getAppDatabase().favPostDao().deleteAllFavPost()
                    AppDatabase.getAppDatabase().notificationDao().deleteAllNotification()
                    AppDatabase.getAppDatabase().filterDao().deleteAllFilter()
                    uiThread {

                        LoginActivity.startLogin(activity!!,true,"")

                    }
                }
                LoginManager.getInstance().logOut()
                AccountKit.logOut();
            }

            override fun onError(errorMessage: String) {
                mainActivity?.errorDialog?.show("Error!!","$errorMessage")
                mainActivity!!.progress?.dismiss()
            }

        }).logoutRequest(param)

    }
}
