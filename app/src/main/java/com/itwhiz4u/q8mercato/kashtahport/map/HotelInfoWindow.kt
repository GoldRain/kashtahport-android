package com.itwhiz4u.q8mercato.kashtahport.map

import android.content.Context
import android.view.View
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.model.Marker

class HotelInfoWindow(val context: Context, val view: View):GoogleMap.InfoWindowAdapter {

    override fun getInfoContents(p0: Marker?): View? {

        return null
    }

    override fun getInfoWindow(p0: Marker?): View? {

     //   map_relative_layout.setMarkerWithInfoWindow(p0, view);

        return view
    }

    fun getPixelsFromDp(context: Context, dp: Float): Int {
        val scale = context.resources.displayMetrics.density
        return (dp * scale + 0.5f).toInt()
    }
}