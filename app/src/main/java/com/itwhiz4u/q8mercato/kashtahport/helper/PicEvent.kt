package com.mazadlive.helper

import android.content.Intent

/**
 * Created by bodacious on 25/10/18.
 */
open class PicEvent() {

    var req: Int = 0
    var res: Int = 0
    var intent: Intent? = null
    var result = 0

    constructor(req:Int,res:Int,intent:Intent):this(){
        this.req = req
        this.res = res
        this.intent = intent
    }
}