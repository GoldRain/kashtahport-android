package com.itwhiz4u.q8mercato.kashtahport.Notification

import android.app.*
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.media.AudioAttributes
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.support.annotation.RequiresApi
import android.support.v4.app.NotificationCompat
import android.support.v4.content.ContextCompat
import android.util.Log
import com.itwhiz4u.q8mercato.kashtahport.Api.ApiResponseListener
import com.itwhiz4u.q8mercato.kashtahport.Api.ServiceRequest
import com.itwhiz4u.q8mercato.kashtahport.R
import com.itwhiz4u.q8mercato.kashtahport.activities.SplashActivity
import com.itwhiz4u.q8mercato.kashtahport.helper.Constant
import com.itwhiz4u.q8mercato.kashtahport.helper.MyApplication
import com.itwhiz4u.q8mercato.kashtahport.model.EventModel
import org.greenrobot.eventbus.EventBus
import org.json.JSONObject


/* Glide.with(this)
                .load("http://api.androidhive.info/images/sample.jpg")
                .asBitmap()
                .into(object : SimpleTarget<Bitmap>(100, 100) {
                    override fun onResourceReady(resource: Bitmap?, glideAnimation: GlideAnimation<in Bitmap>?) {
                       // NotificationUtils.showNotification(this@PreciousChatActivity, SignupActivity::class.java, "App Name", "message", "", R.drawable.ic_notification, 1,resource)

                    }
                })*/
class NotificationUtils {
    private val notificationId: Int = 0

    companion object {
        //  public var message:Message?= null
        private val foregroud: Boolean = false
        private val alt: AlertDialog.Builder? = null


        fun showNotification(context: Context, title: String, message: String, param: String, type:String,image: Bitmap? = null) {



            Log.e("NotificationUtils"," "+message+" "+MyApplication.instance.prefManager.getBoolean(Constant.KEY_USER_LOGIN_STATUS))
            if (!MyApplication.instance.prefManager.getBoolean(Constant.KEY_USER_LOGIN_STATUS)) {
                return
            }

            val params= JSONObject()
            params.put("api_key", Constant.API_KEY)
            params.put("user_id", MyApplication.instance.prefManager!!.getString(Constant.KEY_USER_ID))

            ServiceRequest(object :ApiResponseListener{
                override fun onCompleted(`object`: Any) {
                    Handler().postDelayed(object:Runnable{
                        override fun run() {
                            EventBus.getDefault().post(EventModel(Constant.EVENT_NOTIFICATION))
                        }

                    },100)
                }

                override fun onError(errorMessage: String) {

                }

            }).getNotifications(params)



            val sound = MyApplication.instance.prefManager.getBoolean(Constant.KEY_NOTIFICATION_SOUND)
            if(!sound){

                return
            }


            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {

                Log.e("NotificationUtils"," orio ")
                showOrioNotification(context,title,message,param,type,image)

                return
            }
            val bundle = Bundle()
            val intent = Intent(context, SplashActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
            intent.putExtra("message", message)
            intent.putExtra(Constant.KEY_NOTIFICATION, true)
            intent.putExtra(Constant.KEY_NOTIFICATION_PARAM, param)
            intent.putExtra(Constant.KEY_NOTIFICATION_TYPE, type)

            val nId = getNotificationId(type)
            val pendingIntent = PendingIntent.getActivity(context, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT)


            val notificationBuilder: NotificationCompat.Builder

            notificationBuilder = NotificationCompat.Builder(context, "my_channel_01")
                    .setSmallIcon(R.drawable.ic_notification_logo)
                    .setContentTitle(title)
                    .setContentText(message)
                    .setAutoCancel(true)
                    .setContentIntent(pendingIntent)
                    .setPriority(NotificationCompat.PRIORITY_HIGH)
                    .setColor(ContextCompat.getColor(context, R.color.colorPrimary))



            if (image != null) {
                notificationBuilder.setLargeIcon(image)
                        .setStyle(NotificationCompat.BigPictureStyle().bigPicture(image))
            }

            val notificationManager = context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

            var nCount = MyApplication.instance.prefManager.getInt(Constant.KEY_NOTIFICATION_COUNT)
            nCount++
            MyApplication.instance.prefManager.setInt(Constant.KEY_NOTIFICATION_COUNT,nCount)

            notificationManager.notify(nId, notificationBuilder.build())
        }

        private fun getNotificationId(type:String): Int {
            val rand = (Math.random()*1000).toInt()
            return  rand
        }

        @RequiresApi(Build.VERSION_CODES.O)
      private  fun  showOrioNotification(context: Context, title: String, message: String, param: String, type:String,image: Bitmap? = null){
            val intent = Intent(context, SplashActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
            intent.putExtra("message", message)
            intent.putExtra(Constant.KEY_NOTIFICATION, true)
            intent.putExtra(Constant.KEY_NOTIFICATION_PARAM, param)
            intent.putExtra(Constant.KEY_NOTIFICATION_TYPE, type)

            val pendingIntent = PendingIntent.getActivity(context, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT)

            val sound = MyApplication.instance.prefManager.getBoolean(Constant.KEY_NOTIFICATION_SOUND)

            val notificationBuilder: NotificationCompat.Builder

            notificationBuilder = NotificationCompat.Builder(context, "my_channel_01")
                    .setSmallIcon(R.drawable.ic_notification_logo)
                    .setContentTitle(title)
                    .setContentText(message)
                    .setAutoCancel(true)
                    .setChannelId("my_channel_01")
                    .setContentIntent(pendingIntent)
                    .setPriority(NotificationCompat.PRIORITY_HIGH)
                    .setColor(ContextCompat.getColor(context, R.color.colorPrimary))



            if (image != null) {
                notificationBuilder.setLargeIcon(image)
                        .setStyle(NotificationCompat.BigPictureStyle().bigPicture(image))
            }

            val notificationManager = context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager


                val uri = Uri.parse("android.resource://" + context.packageName + "/")

                val attributes = AudioAttributes.Builder()
                        .setUsage(AudioAttributes.USAGE_NOTIFICATION)
                        .build()

                val mChannel = NotificationChannel("my_channel_01", "my_channel", NotificationManager.IMPORTANCE_HIGH)

                mChannel.setDescription(context.getString(R.string.app_name));
                mChannel.enableLights(true);
                mChannel.enableVibration(true);
                //mChannel.importance(NotificationManager.IMPORTANCE_HIGH);
            mChannel.lockscreenVisibility= 1

                if (notificationManager != null) {
                    val channelList = notificationManager.getNotificationChannels()

                    var i = 0
                    while (channelList != null && i < channelList!!.size) {
                        notificationManager.deleteNotificationChannel(channelList!!.get(i).getId())
                        i++
                    }

                    notificationManager.createNotificationChannel(mChannel)
                }

            val nId = getNotificationId(type)
            notificationManager.notify(nId, notificationBuilder.build())
            Log.e("NotificationUtils"," notify ")
        }
    }
}
