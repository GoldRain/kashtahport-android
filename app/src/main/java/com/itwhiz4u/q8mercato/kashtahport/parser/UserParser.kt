package com.itwhiz4u.q8mercato.kashtahport.parser

import android.util.Log
import com.itwhiz4u.q8mercato.kashtahport.model.UserModel
import org.json.JSONArray
import org.json.JSONObject

class UserParser {

    /*"data": {
        "name": "Ray",
        "email": "chand@gmail.com",
        "password": "123456d",
        "phone": "9074452230",
        "type": "",
        "profile_image_url": "www.asdf.com",
        "device_id": "",
        "device_type": "",
        "notification_token": "",
        "access_token": "",
        "socket_id": "",
        "online_status": true,
        "is_login": true,
        "is_blocked": false,
        "created_at": "",
        "last_login": "",
        "favourites": [],
        "can_sale": false,
        "_id": "5cca8f67cde675186a9d67f9",
        "__v": 0
    }*/
    fun parse(jsonObject: JSONObject):UserModel{

        val userModel = UserModel()

       try {
           if(jsonObject.has("_id")){
               userModel.id = jsonObject.getString("_id")
           }

           if(jsonObject.has("name")){
               userModel.name = jsonObject.getString("name")
           }

           if(jsonObject.has("email")){
               userModel.email = jsonObject.getString("email")
           }

           if(jsonObject.has("password")){
               userModel.password = jsonObject.getString("password")
           }

           if(jsonObject.has("phone")){
               userModel.phone = jsonObject.getString("phone")
           }

           if(jsonObject.has("type")){
               userModel.type = jsonObject.getString("type")
           }
           if(jsonObject.has("profile_image_url")){
               userModel.imageUrl = jsonObject.getString("profile_image_url")
           }

           if(jsonObject.has("device_id")){
               userModel.device_id = jsonObject.getString("device_id")
           }

           if(jsonObject.has("notification_token")){
               userModel.notification_token = jsonObject.getString("notification_token")
           }

           if(jsonObject.has("access_token")){
               userModel.access_token = jsonObject.getString("access_token")
           }

           if(jsonObject.has("online_status")){
               userModel.online_status = jsonObject.getBoolean("online_status")
           }

           if(jsonObject.has("can_sale")){
               userModel.can_sale = jsonObject.getBoolean("can_sale")
           }

           if(jsonObject.has("last_login")){
               userModel.last_login = jsonObject.getString("last_login")
           }
           if(jsonObject.has("created_at")){
               userModel.created_at = jsonObject.getString("created_at")
           }

           if(jsonObject.has("is_blocked")){
               userModel.is_blocked = jsonObject.getString("is_blocked")
           }
           if(jsonObject.has("country_code")){
               userModel.countryCode = jsonObject.getString("country_code")
           }

           if(jsonObject.has("facebook")){
               userModel.faceBookId = jsonObject.getString("facebook")
           }

           if(jsonObject.has("description")){
               userModel.description = jsonObject.getString("description")
           }

           if(jsonObject.has("profession")){
               userModel.profession= jsonObject.getString("profession")
           }

           if(jsonObject.has("IBAN")){
               var ibanObj = jsonObject.get("IBAN")

               if(ibanObj is JSONArray){
                   val ar = jsonObject.getJSONArray("IBAN")
                   ibanObj = ar.getJSONObject(0)
               }

               val obj = ibanObj as JSONObject
               if(obj.has("country_code")){
                   userModel.ibanCountry = obj.getString("country_code")
               }
               if(obj.has("IBAN")){
                   userModel.ibanNumber = obj.getString("IBAN")
               }

               if(obj.has("bank_name")){
                   userModel.ibankName = obj.getString("bank_name")
               }

               if(obj.has("phone")){
                   userModel.ibanPhone = obj.getString("phone")
               }

           }
       }catch (e:Exception){
           Log.e("TAG_USER", " ********** "+e.message)
           e.printStackTrace()
       }finally {
           return  userModel
       }

    }
}