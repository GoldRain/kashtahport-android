package com.itwhiz4u.q8mercato.kashtahport.parser

import com.itwhiz4u.q8mercato.kashtahport.model.FeedBackModel
import org.json.JSONObject

class FeedbackParser {


    /*
/*"_id":"5ce69192e678895a47a3aafe",
"user_id":"5cdfd9fed1ea2a45c037cbe1",
"hotel_id":"5ce6398e54d8b8359ac45671",
"communication":2,
"price":5,
"delivery":4,
"proficiency":3,
"created_at":"1558614231352",
"comment":"Hahahhahah ",
"__v":0*/*/
    fun parse(jsonObject: JSONObject):FeedBackModel{

        val feedBackModel = FeedBackModel()

        if(jsonObject.has("_id")){
            feedBackModel.id = jsonObject.getString("_id")

        }

        if(jsonObject.has("user_id")){
            feedBackModel.user_id = jsonObject.getString("user_id")

        }
        if(jsonObject.has("booking_id")){
            feedBackModel.bookingId = jsonObject.getString("booking_id")

        }
        if(jsonObject.has("hotel_id")){
            feedBackModel.hotel_id = jsonObject.getString("hotel_id")
        }

        if(jsonObject.has("communication")){
            feedBackModel.communication = jsonObject.get("communication").toString()
        }
        if(jsonObject.has("price")){
            feedBackModel.price = jsonObject.get("price").toString()
        }

        if(jsonObject.has("delivery")){
            feedBackModel.delivery = jsonObject.get("delivery").toString()
        }

        if(jsonObject.has("proficiency")){
            feedBackModel.proficiency = jsonObject.get("proficiency").toString()
        }

        if(jsonObject.has("created_at")){
            feedBackModel.created_at = jsonObject.getString("created_at")
        }


        if(jsonObject.has("comment")){
            feedBackModel.comment = jsonObject.getString("comment")
        }
        return  feedBackModel

    }
}