package com.itwhiz4u.q8mercato.kashtahport.activities

import android.os.Bundle
import android.support.v4.widget.SwipeRefreshLayout
import android.view.View
import com.itwhiz4u.q8mercato.kashtahport.customDialog.CustomAlertBottom
import com.itwhiz4u.q8mercato.kashtahport.customDialog.CustomErrorBottom
import com.itwhiz4u.q8mercato.kashtahport.customDialog.CustomProgress
import com.itwhiz4u.q8mercato.kashtahport.helper.AbstractAppPauseActivity
import com.itwhiz4u.q8mercato.kashtahport.helper.MyApplication
import java.lang.Exception

open class BaseActivity : AbstractAppPauseActivity() {

    var progress: CustomProgress?= null
    var alert: CustomAlertBottom?= null
    var errorDialog: CustomErrorBottom?= null
    val prefManager = MyApplication.instance.prefManager
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        progress = CustomProgress(this)
        alert = CustomAlertBottom(this)
        errorDialog = CustomErrorBottom(this)
    }

    fun showProgress(view:View?){
        runOnUiThread {
            view?.visibility = View.VISIBLE
        }
    }

    fun hideProgress(view:View?,swipe: SwipeRefreshLayout?= null){
        try {
            runOnUiThread {
                view?.visibility = View.GONE
                if(swipe != null){
                    swipe?.isRefreshing = false
                }
            }
        }catch (e:Exception){
            e.printStackTrace()
        }
    }

    fun showEmptyView(view:View?,flag:Boolean= false){
       if(flag){
           runOnUiThread {
               view?.visibility = View.VISIBLE
           }
       }else{
           runOnUiThread {
               view?.visibility = View.GONE
           }
       }
    }

    fun hideEmptyView(view:View?){
        runOnUiThread {
            view?.visibility = View.GONE
        }
    }

    open fun showNotificationCount(count:Int){}


}
