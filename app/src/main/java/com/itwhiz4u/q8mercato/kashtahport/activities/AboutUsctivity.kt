package com.itwhiz4u.q8mercato.kashtahport.activities

import android.graphics.Bitmap
import android.os.Build
import android.os.Bundle
import android.support.annotation.RequiresApi
import android.text.TextUtils
import android.util.Log
import android.view.View
import android.webkit.WebResourceRequest
import android.webkit.WebView
import android.webkit.WebViewClient
import com.itwhiz4u.q8mercato.kashtahport.Api.ApiUrl
import com.itwhiz4u.q8mercato.kashtahport.R
import com.itwhiz4u.q8mercato.kashtahport.helper.Constant
import kotlinx.android.synthetic.main.activity_about_us_activity.*
import kotlinx.android.synthetic.main.view_header_toolbar.*
import java.util.*

class AboutUsctivity : BaseActivity() {

    var lang  ="en"
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setLocale()
        setContentView(R.layout.activity_about_us_activity)
        supportActionBar?.hide()

        header_name.setText(getString(R.string.hint_aboutUs))
        icon_back.visibility = View.VISIBLE
        icon_back.setOnClickListener {
            super.onBackPressed()
        }

        Log.e("ABOUT_US", ApiUrl.ABOUT_US_WEB_VIEW)
        web_view.settings.javaScriptEnabled = true
        web_view.webViewClient = MyClient()

        if(lang.equals("en")){
            web_view.loadUrl(ApiUrl.ABOUT_US_WEB_VIEW)
        }else{
            web_view.loadUrl(ApiUrl.ABOUT_US_WEB_VIEW_AR)
        }
    }

    private fun setLocale() {
        var lan  = prefManager?.getString(Constant.KEY_LAN)
        if(TextUtils.isEmpty(lan)){
            lan = "en"
        }
        lang = lan
        Log.e("LANGUAGE","$lan   *** ")
        val myLocale = Locale(lan)
        Locale.setDefault(myLocale)
        val res = resources
        val dm = res.displayMetrics
        val conf = res.configuration
        conf.locale = myLocale
        baseContext.resources.updateConfiguration(conf, dm)
        prefManager?.setString(Constant.KEY_LAN,lan)

    }
    inner class MyClient : WebViewClient() {

        override fun onPageStarted(view: WebView?, url: String?, favicon: Bitmap?) {
            Log.e("ABOUT_US", " onPageStarted")
            runOnUiThread {
                frame_loader.visibility = View.VISIBLE
            }
        }

        override fun onPageFinished(view: WebView?, url: String?) {
            Log.e("ABOUT_US", " onPageFinished ")
            runOnUiThread {
                frame_loader.visibility = View.GONE
            }
        }


        @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
        override fun shouldOverrideUrlLoading(view: WebView?, request: WebResourceRequest?): Boolean {
            Log.e("ABOUT_US", " shouldOverrideUrlLoading ")
            view!!.loadUrl(request!!.url.toString())
            return super.shouldOverrideUrlLoading(view, request)
        }
    }
}
