package com.itwhiz4u.q8mercato.kashtahport.listeners

import android.view.View
import com.google.android.gms.maps.model.Marker

interface InfoClickListener {
    fun onClick(view:View,marker:Marker)
}