package com.itwhiz4u.q8mercato.kashtahport.activities

import android.Manifest
import android.app.NotificationManager
import android.arch.lifecycle.ViewModelProvider
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.support.v4.print.PrintHelper
import android.support.v4.view.ViewPager
import android.text.TextUtils
import android.util.Log
import android.view.View
import com.itwhiz4u.q8mercato.kashtahport.Api.ApiResponseListener
import com.itwhiz4u.q8mercato.kashtahport.Api.ServiceRequest
import com.itwhiz4u.q8mercato.kashtahport.LiveData.MainViewModel
import com.itwhiz4u.q8mercato.kashtahport.R
import com.itwhiz4u.q8mercato.kashtahport.adapter.ViewPagerAdapter
import com.itwhiz4u.q8mercato.kashtahport.database.AppDatabase
import com.itwhiz4u.q8mercato.kashtahport.fragment.main.*
import com.itwhiz4u.q8mercato.kashtahport.helper.Constant
import com.itwhiz4u.q8mercato.kashtahport.helper.MyApplication
import com.itwhiz4u.q8mercato.kashtahport.model.CountryModel
import com.itwhiz4u.q8mercato.kashtahport.model.EventModel
import com.itwhiz4u.q8mercato.kashtahport.parser.CountryParser
import com.itwhiz4u.q8mercato.kashtahport.parser.HotelParser
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.view_main_bottom.*
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode
import org.jetbrains.anko.doAsync
import org.json.JSONObject

class MainActivity : BaseActivity() {

    private lateinit var viewPagerAdater: ViewPagerAdapter

    private lateinit var homeFragment: HomeFragment
    private lateinit var favFragment: FavFragment
    private lateinit var notificationFragment: NotificationFragment
    private lateinit var profileFragment: ProfileFragment
    private lateinit var settingFragment: SettingFragment


    private val LOCATION_PERMISSION = 111
    private val str: String? = null
    val countryList = ArrayList<CountryModel>()
    private var openFor: String? = null

    var linkType = ""
    var linkTPostId = ""

    private lateinit var model: MainViewModel
    companion object {

        var ins: MainActivity? = null

        fun getInstance(): MainActivity? {

            return ins

        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        supportActionBar?.hide()

        Log.e("SaveInstanceState"," onCreate ")
        ins = this

// Get the ViewModel.
      //  model = ViewModelProvider.of(this).get(NameViewModel::class.java)

        if (prefManager.getBoolean(Constant.KEY_USER_LOGIN_STATUS)) {
            com.onesignal.OneSignal.idsAvailable { userId, registrationId ->
                prefManager.setString(Constant.KEY_USER_NOTIFICATION_TOKEN, userId)
                MyApplication.NOTI_TOKEN = userId
                ServiceRequest.updateNotificationToken()
            }
        }

        getCountries()

        // NotificationUtils.showNotification(this,"App Name"," sdhshds","",null)
        viewPagerAdater = ViewPagerAdapter(supportFragmentManager)

        //checkPermissions()
        initViewPager()

        initBottomView()

        val notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        notificationManager.cancelAll()

        Log.e(TAG, " Link handleInviteLink onCreate " )
        handleInviteLink()
    }


    override fun onNewIntent(intent: Intent?) {
        super.onNewIntent(intent)
        Log.e(TAG, " Link handleInviteLink onNewIntent " )
        handleInviteLink()
    }

    private fun handleInviteLink() {
        Log.e(TAG, " Link handleInviteLink " + Constant.LINK_URL)

        if(!TextUtils.isEmpty(Constant.LINK_URL)){
            var link = Constant.LINK_URL
            Constant.LINK_URL = ""
            val uri = Uri.parse(link)
            val linkTPostId = uri.getQueryParameter("post_id")

            val param = JSONObject()
            param.put("api_key",Constant.API_KEY)
            param.put("hotel_id",linkTPostId)

            progress?.show()
            ServiceRequest(object :ApiResponseListener{
                override fun onCompleted(`object`: Any) {
                    progress?.dismiss()
                    try {
                        val json = JSONObject(`object`.toString())
                        if(!json.getBoolean("error")){
                            if(json.has("data")){
                                val array = json.getJSONArray("data")
                                for(i in 0 until array.length()){
                                    val obj  = array.getJSONObject(i)
                                    val hotel = HotelParser().parse(obj)

                                    val intent = Intent(this@MainActivity,HotelDetailsActivity::class.java)
                                    intent.putExtra(Constant.KEY_HOTEL, hotel)
                                    startActivity(intent)
                                }
                            }
                        }else{
                            errorDialog?.content = json.getString("message")
                            errorDialog?.title = getString(R.string.hint_error)
                            errorDialog?.show()
                        }
                    }catch (e:java.lang.Exception){
                        errorDialog?.content = e.message!!
                        errorDialog?.title = getString(R.string.hint_error)
                        errorDialog?.show()
                    }
                }

                override fun onError(errorMessage: String) {
                    progress?.dismiss()
                    errorDialog?.content = errorMessage
                    errorDialog?.title = getString(R.string.hint_error)
                    errorDialog?.show()
                }

            }).getProperty(param)
        }
    }

    val TAG = "MainActivity"
    override fun onDestroy() {
        Log.e("onDestroyMain", "onDestroy")
        prefManager.setBoolean(Constant.KEY_FILTER_ENABLE, false)
        doAsync {
            AppDatabase.getAppDatabase().filterDao().deleteAllFilter()
        }
        super.onDestroy()
    }

    override fun onStart() {

        super.onStart()

    }

    override fun onResume() {

        // view_pager.currentItem = view_pager.currentItem

        EventBus.getDefault().register(this)

        getAllActivityCount()

        super.onResume()
    }

    override fun onPause() {
        EventBus.getDefault().unregister(this)
        super.onPause()

    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onMessageEvent(event: EventModel) {
        when (event.type) {
            Constant.EVENT_NOTIFICATION -> {
                updateNotificationCount()
            }
        }
    }

    override fun showNotificationCount(count: Int) {

        Log.e("ShowNotification", " " + count)
        //updateNotificationCount()
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)

        if (requestCode == LOCATION_PERMISSION) {
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED &&
                    ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {

                initViewPager()
            }
        }

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
    }


    private fun updateNotificationCount(flag: Boolean = false) {

        doAsync {
            try {

                val count = AppDatabase.getAppDatabase().notificationDao().getTotalNotificationCount()
                val list = AppDatabase.getAppDatabase().notificationDao().getTotalUnreadNotification()
                Log.e("updateNotificationCount", " ** " + count + " " + list.size)
                runOnUiThread {
                    if (count == 0) {
                        noti_btm_count.visibility = View.GONE
                    } else {
                        noti_btm_count.visibility = View.VISIBLE

                        if (count >= 100) {
                            noti_btm_count.setText("99+")
                        } else noti_btm_count.setText("" + count)
                    }
                }
            } catch (e: java.lang.Exception) {
                e.printStackTrace()
                Log.e("updateNotificationCount", " ** " + e.message)
            }
        }
    }

    private fun getCountries() {

        ServiceRequest(object : ApiResponseListener {
            override fun onCompleted(`object`: Any) {
                try {
                    val json = JSONObject(`object`.toString())
                    if (json.has("data")) {
                        val objArray = json.getJSONArray("data")
                        val parser = CountryParser()
                        for (i in 0 until objArray.length()) {
                            val obj = objArray.getJSONObject(i)
                            val country = parser.parse(obj)
                            countryList.add(country)
                        }
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

            override fun onError(errorMessage: String) {
                // errorDialog?.show("Error!!","Failed $errorMessage")
            }

        }).getCountries()
    }

    private fun forceCrash(view: View) {
        // throw RuntimeException("This is a crash")

    }

    private fun checkPermissions(): Boolean {


        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED ||
                ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(this,
                    arrayOf(Manifest.permission.ACCESS_FINE_LOCATION,
                            Manifest.permission.ACCESS_COARSE_LOCATION), LOCATION_PERMISSION)
            return false
        }

        return true
    }

    private fun initViewPager(flag: Boolean = false) {

        homeFragment = HomeFragment()
        homeFragment.setActivity(this)
        homeFragment.fragPosition = Constant.HOME_POSITION

        favFragment = FavFragment()
        favFragment.setActivity(this)
        favFragment.fragPosition = Constant.FAV_POSITION

        notificationFragment = NotificationFragment()
        notificationFragment.setActivity(this)
        notificationFragment.fragPosition = Constant.NOTIFICATION_POSITION

        profileFragment = ProfileFragment()
        profileFragment.setActivity(this)
        profileFragment.fragPosition = Constant.PROFILE_POSITION

        settingFragment = SettingFragment()
        settingFragment.setActivity(this)
        settingFragment.fragPosition = Constant.SETTING_POSITION

        /*  if(flag){
              viewPagerAdater.addFragment(hotelMapListFragment,"")
          }else{
              viewPagerAdater.addFragment(homeFragment,"")
          }*/
        viewPagerAdater.addFragment(homeFragment, "")
        viewPagerAdater.addFragment(favFragment, "")
        viewPagerAdater.addFragment(notificationFragment, "")
        viewPagerAdater.addFragment(profileFragment, "")
        viewPagerAdater.addFragment(settingFragment, "")

        view_pager.adapter = viewPagerAdater
        tab_layout.setupWithViewPager(view_pager)

        updateBottomTabs(0)

        view_pager.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrollStateChanged(p0: Int) {

            }

            override fun onPageScrolled(p0: Int, p1: Float, p2: Int) {

            }

            override fun onPageSelected(p0: Int) {

                EventBus.getDefault().post(EventModel(Constant.EVENT_UPDATE_MAIN))
                updateBottomTabs(p0)


            }

        })


        //  viewPagerAdater.notifyDataSetChanged()

        if (intent.getBooleanExtra("fromLan", false)) {
            view_pager.setCurrentItem(Constant.SETTING_POSITION, true)

            Handler().postDelayed(object : Runnable {
                override fun run() {
                    EventBus.getDefault().post(EventModel(Constant.EVENT_UPDATE_MAIN))
                }

            }, 100)
            updateBottomTabs(Constant.SETTING_POSITION)
        }

        view_pager.setPagingEnabled(false)
        openFor = intent.getStringExtra(Constant.KEY_OPEN_FOR)
        if (!TextUtils.isEmpty(openFor)) {

            when (openFor) {
                Constant.OPEN_FOR_ADD_PROPERTY -> {
                    val intent = Intent(this, SellerAddPropertyActivity::class.java)
                    startActivity(intent)
                }

                /* Constant.OPEN_FOR_BOOK_HOTEL ->{
                     val intent =Intent(this,HotelDetailsActivity::class.java)
                     intent.putExtra(Constant.KEY_CAN_BOOK,true)
                     startActivity(intent)
                 }*/

                Constant.OPEN_FOR_PROFILE -> {
                    view_pager.setCurrentItem(Constant.PROFILE_POSITION, true)

                    Handler().postDelayed(object : Runnable {
                        override fun run() {
                            EventBus.getDefault().post(EventModel(Constant.EVENT_UPDATE_MAIN))
                        }

                    }, 100)
                    updateBottomTabs(Constant.PROFILE_POSITION)


                }

                Constant.OPEN_FOR_FAV_HOTEL -> {
                    view_pager.setCurrentItem(Constant.FAV_POSITION, true)

                    Handler().postDelayed(object : Runnable {
                        override fun run() {
                            EventBus.getDefault().post(EventModel(Constant.EVENT_UPDATE_MAIN))
                        }

                    }, 100)
                    updateBottomTabs(Constant.FAV_POSITION)


                }

                Constant.OPEN_FOR_NOTIFICATION -> {
                    view_pager.setCurrentItem(Constant.NOTIFICATION_POSITION, true)

                    Handler().postDelayed(object : Runnable {
                        override fun run() {
                            EventBus.getDefault().post(EventModel(Constant.EVENT_UPDATE_MAIN))
                        }

                    }, 100)
                    updateBottomTabs(Constant.NOTIFICATION_POSITION)


                }

                Constant.OPEN_FOR_BOOKING_LIST -> {
                    val intent = Intent(this, BookingListActivity::class.java)
                    startActivity(intent)
                }
            }

            openFor = ""
        }


    }

    private fun initBottomView() {
        frame_btm_home.setOnClickListener {
            if (view_pager.currentItem != 0) {
                view_pager.setCurrentItem(0)
            }
        }

        frame_btm_fav.setOnClickListener {
            if (view_pager.currentItem != 1) {
                view_pager.setCurrentItem(1)
            }
        }

        frame_btm_notification.setOnClickListener {
            if (view_pager.currentItem != 2) {
                view_pager.setCurrentItem(2)
            }

            doAsync {
                AppDatabase.getAppDatabase().notificationDao().updateNotificationRead()
                updateNotificationCount()
            }
        }

        frame_btm_profile.setOnClickListener {
            if (view_pager.currentItem != 3) {
                view_pager.setCurrentItem(3)
            }

        }

        frame_btm_setting.setOnClickListener {
            if (view_pager.currentItem != 4) {
                view_pager.setCurrentItem(4)
            }
        }
    }

    private fun updateBottomView(position: Int) {
        when (position) {
            0 -> {
                im_btm_home.setImageResource(R.drawable.ic_home_blue)
                im_btm_fav.setImageResource(R.drawable.like_gray)
                im_btm_notification.setImageResource(R.drawable.noti_black)
                im_btm_profile.setImageResource(R.drawable.user)
                im_btm_setting.setImageResource(R.drawable.settings_black)

            }

            1 -> {
                im_btm_home.setImageResource(R.drawable.ic_home_black)
                im_btm_fav.setImageResource(R.drawable.like_blue)
                im_btm_notification.setImageResource(R.drawable.noti_black)
                im_btm_profile.setImageResource(R.drawable.user)
                im_btm_setting.setImageResource(R.drawable.settings_black)
            }

            2 -> {
                im_btm_home.setImageResource(R.drawable.ic_home_black)
                im_btm_fav.setImageResource(R.drawable.like_gray)
                im_btm_notification.setImageResource(R.drawable.noti_blue)
                im_btm_profile.setImageResource(R.drawable.user)
                im_btm_setting.setImageResource(R.drawable.settings_black)
            }

            3 -> {

                im_btm_home.setImageResource(R.drawable.ic_home_black)
                im_btm_fav.setImageResource(R.drawable.like_gray)
                im_btm_notification.setImageResource(R.drawable.noti_black)
                im_btm_profile.setImageResource(R.drawable.user_blue)
                im_btm_setting.setImageResource(R.drawable.settings_black)
            }

            4 -> {
                im_btm_home.setImageResource(R.drawable.ic_home_black)
                im_btm_fav.setImageResource(R.drawable.like_gray)
                im_btm_notification.setImageResource(R.drawable.noti_black)
                im_btm_profile.setImageResource(R.drawable.user)
                im_btm_setting.setImageResource(R.drawable.settings_blue)
            }
        }
    }

    private fun updateBottomTabs(position: Int) {

        updateBottomView(position)

        when (position) {
            0 -> {
                tab_layout.getTabAt(0)?.setIcon(R.drawable.ic_home_blue)
                tab_layout.getTabAt(1)?.setIcon(R.drawable.like_gray)
                tab_layout.getTabAt(2)?.setIcon(R.drawable.noti_black)
                tab_layout.getTabAt(3)?.setIcon(R.drawable.user)
                tab_layout.getTabAt(4)?.setIcon(R.drawable.settings_black)
            }

            1 -> {
                tab_layout.getTabAt(0)?.setIcon(R.drawable.ic_home_black)
                tab_layout.getTabAt(1)?.setIcon(R.drawable.like_blue)
                tab_layout.getTabAt(2)?.setIcon(R.drawable.noti_black)
                tab_layout.getTabAt(3)?.setIcon(R.drawable.user)
                tab_layout.getTabAt(4)?.setIcon(R.drawable.settings_black)
            }

            2 -> {
                tab_layout.getTabAt(0)?.setIcon(R.drawable.ic_home_black)
                tab_layout.getTabAt(1)?.setIcon(R.drawable.like_gray)
                tab_layout.getTabAt(2)?.setIcon(R.drawable.noti_blue)
                tab_layout.getTabAt(3)?.setIcon(R.drawable.user)
                tab_layout.getTabAt(4)?.setIcon(R.drawable.settings_black)
            }

            3 -> {

                tab_layout.getTabAt(0)?.setIcon(R.drawable.ic_home_black)
                tab_layout.getTabAt(1)?.setIcon(R.drawable.like_gray)
                tab_layout.getTabAt(2)?.setIcon(R.drawable.noti_black)
                tab_layout.getTabAt(3)?.setIcon(R.drawable.user_blue)
                tab_layout.getTabAt(4)?.setIcon(R.drawable.settings_black)
            }

            4 -> {
                tab_layout.getTabAt(0)?.setIcon(R.drawable.ic_home_black)
                tab_layout.getTabAt(1)?.setIcon(R.drawable.like_gray)
                tab_layout.getTabAt(2)?.setIcon(R.drawable.noti_black)
                tab_layout.getTabAt(3)?.setIcon(R.drawable.user)
                tab_layout.getTabAt(4)?.setIcon(R.drawable.settings_blue)
            }
        }
    }

    private fun setMapFragment(flag: Boolean = true) {
        viewPagerAdater.fragmentList.clear()
        viewPagerAdater.fragmentListTitels.clear()
        viewPagerAdater.notifyDataSetChanged()
        view_pager.adapter = null
        initViewPager(flag)
    }

    private fun getAllActivityCount() {
        val param = JSONObject()
        param.put("api_key", Constant.API_KEY)
        param.put("user_id", prefManager!!.getString(Constant.KEY_USER_ID))
        //  param.put("hotel_id", hotelList[position].id)

        ServiceRequest(object : ApiResponseListener {
            override fun onCompleted(`object`: Any) {
                Handler().postDelayed(object : Runnable {
                    override fun run() {
                        EventBus.getDefault().post(EventModel(Constant.EVENT_NOTIFICATION))
                    }

                }, 100)
            }

            override fun onError(errorMessage: String) {

            }

        }).getNotifications(param)
    }
}
