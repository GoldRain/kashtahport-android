package com.itwhiz4u.q8mercato.kashtahport.parser

import com.itwhiz4u.q8mercato.kashtahport.model.CityModel
import com.itwhiz4u.q8mercato.kashtahport.model.CountryModel
import org.json.JSONObject

class CountryParser {

    /*"data": {
        "name": "Ray",
        "email": "chand@gmail.com",
        "password": "123456d",
        "phone": "9074452230",
        "type": "",
        "profile_image_url": "www.asdf.com",
        "device_id": "",
        "device_type": "",
        "notification_token": "",
        "access_token": "",
        "socket_id": "",
        "online_status": true,
        "is_login": true,
        "is_blocked": false,
        "created_at": "",
        "last_login": "",
        "favourites": [],
        "can_sale": false,
        "_id": "5cca8f67cde675186a9d67f9",
        "__v": 0
    }*/
    fun parse(jsonObject: JSONObject):CountryModel{

        val countyry = CountryModel()

        if(jsonObject.has("country")){
            countyry.id = jsonObject.getString("country")
            countyry.name = jsonObject.getString("country")
        }

        if(jsonObject.has("city")){
            val array = jsonObject.getJSONArray("city")
            val list = ArrayList<CityModel>()

            for (i in 0 until array.length()){
                val city = CityModel()
                city.id = array.get(i).toString()
                city.name = array.get(i).toString()

                list.add(city)
            }

            countyry.cityList.addAll(list)
        }


        return  countyry

    }
}