package com.itwhiz4u.q8mercato.kashtahport.adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.itwhiz4u.q8mercato.kashtahport.Controller.TransactionController
import com.itwhiz4u.q8mercato.kashtahport.R
import com.itwhiz4u.q8mercato.kashtahport.model.TransactionModel


/**
 * Created by bodacious on 25/3/19.
 */

class TransactionListAdapter(internal var context: Context, internal var mList: List<TransactionModel>, internal var listener: OnItemClick) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    inner class MyBookViewHolder(itemView: View): RecyclerView.ViewHolder(itemView)  {}
    inner class MyMessageViewHolder(itemView: View): RecyclerView.ViewHolder(itemView)  {}
    inner class MyFeedBackViewHolder(itemView: View): RecyclerView.ViewHolder(itemView)  {}
    inner class MyOfferViewHolder(itemView: View): RecyclerView.ViewHolder(itemView)  {}

    override fun getItemViewType(position: Int): Int {
        return position
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {


        val view = LayoutInflater.from(context).inflate(R.layout.list_item_transaction, parent, false)
        return MyBookViewHolder(view)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {

        TransactionController(context,this,holder as MyBookViewHolder,mList[position],listener,position).control()

    }

    override fun getItemCount(): Int {
        return mList.size
    }

    abstract class OnItemClick {
        abstract fun onItemClick(position: Int)
    }

}
