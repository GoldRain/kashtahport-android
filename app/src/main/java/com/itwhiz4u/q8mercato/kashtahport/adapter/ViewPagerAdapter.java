package com.itwhiz4u.q8mercato.kashtahport.adapter;

import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by bodacious on 6/12/18.
 */

public class ViewPagerAdapter extends FragmentPagerAdapter {

     public List<Fragment> fragmentList = new ArrayList<>();

    public List<String> fragmentListTitels = new ArrayList<>();

    @Override
    public int getItemPosition(@NonNull Object object) {
        return POSITION_NONE;
    }

    public ViewPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {

        return fragmentList.get(position);
    }

    @Override
    public int getCount() {

        return fragmentListTitels.size();
    }

    public void addFragment(Fragment fragment, String title){

        fragmentList.add(fragment);
        fragmentListTitels.add(title);
    }

    public void addFragment(Fragment fragment, String title,int position){

        fragmentList.remove(position);
        fragmentListTitels.remove(position);

        fragmentList.add(position,fragment);
        fragmentListTitels.add(position,title);
    }

    @Override
    public CharSequence getPageTitle(int position) {

        return fragmentListTitels.get(position);
    }
}
