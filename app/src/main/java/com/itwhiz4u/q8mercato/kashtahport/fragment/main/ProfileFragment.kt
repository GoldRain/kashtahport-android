package com.itwhiz4u.q8mercato.kashtahport.fragment.main


import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.text.TextUtils
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.itwhiz4u.q8mercato.kashtahport.Api.ApiResponseListener
import com.itwhiz4u.q8mercato.kashtahport.Api.ServiceRequest
import com.itwhiz4u.q8mercato.kashtahport.R
import com.itwhiz4u.q8mercato.kashtahport.activities.*
import com.itwhiz4u.q8mercato.kashtahport.adapter.HotelMyBookingAdapter
import com.itwhiz4u.q8mercato.kashtahport.customDialog.DialogListener
import com.itwhiz4u.q8mercato.kashtahport.helper.BookingStatus
import com.itwhiz4u.q8mercato.kashtahport.helper.Constant
import com.itwhiz4u.q8mercato.kashtahport.helper.ConstantFunction
import com.itwhiz4u.q8mercato.kashtahport.helper.MyApplication
import com.itwhiz4u.q8mercato.kashtahport.model.EventModel
import com.itwhiz4u.q8mercato.kashtahport.model.HotelBookingModel
import com.itwhiz4u.q8mercato.kashtahport.parser.BookingParser
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.dialog_register_warning.*
import kotlinx.android.synthetic.main.fragment_profile.*
import kotlinx.android.synthetic.main.view_header_toolbar.*
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode
import org.json.JSONObject

// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 *
 */
class ProfileFragment : Fragment() {

    var fragPosition = -1
    lateinit var fragView :View

    lateinit var mainActivity: MainActivity
    lateinit var adapter: HotelMyBookingAdapter

    var hotelList = ArrayList<HotelBookingModel>()


    fun setActivity(mainActivity: MainActivity){
        this.mainActivity = mainActivity
    }

    var currentPageIndex = 0
    val manager = MyApplication.instance.prefManager

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
       
        return inflater.inflate(R.layout.fragment_profile, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        adapter = HotelMyBookingAdapter(activity!!,hotelList,object : HotelMyBookingAdapter.OnItemClick() {
            override fun onUpdateBooking(position: Int, status: String) {
                updateStatus(position,status)
            }

            override fun onItemClick(position: Int) {

                getBookingDetails(position, 0)
            }

            override fun onLoadMore(position: Int) {

                getMyBooking()
            }
        })

        swipe_to_refresh.setOnRefreshListener {
            if(swipe_to_refresh.isRefreshing){
                currentPageIndex = 0
                getMyBooking()
            }
        }

    }

    override fun onResume() {
        EventBus.getDefault().register(this)
        initUI()

        super.onResume()
    }

    override fun onPause() {
        EventBus.getDefault().unregister(this)
        super.onPause()
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onMessageEvent(event: EventModel) {
        when(event.type){
            Constant.EVENT_UPDATE_MAIN ->{
                Log.e("getMyBooking"," "+mainActivity.view_pager.currentItem)

                if(mainActivity.view_pager.currentItem == Constant.PROFILE_POSITION){
                    currentPageIndex = 0
                    getMyBooking()

                }
            }
        }
    }



    private fun initUI() {


        recycler_view.layoutManager = LinearLayoutManager(activity!!)
        //    recycler_view.addItemDecoration(EqualSpaceItemDecoration(16))
        recycler_view.adapter = adapter

        if(manager.getBoolean(Constant.KEY_USER_LOGIN_STATUS)){
            header_edit.visibility = View.VISIBLE
            card_login.visibility = View.GONE
            setData()
        }else{
            header_edit.visibility = View.GONE
            card_login.visibility = View.VISIBLE
            hotelList.clear()
            adapter.notifyDataSetChanged()
        }


        header_name.setText(getString(R.string.hint_profile))

        icon_back.setOnClickListener {
            activity!!.onBackPressed()
        }

        header_edit.setOnClickListener {
            val intent = Intent(activity!!,EditProfileActivity::class.java)
            startActivity(intent)
        }

        frame_register.setOnClickListener {
            LoginActivity.startLogin(activity!!,true,Constant.OPEN_FOR_PROFILE)
        }

        card_login.setOnTouchListener { v, event ->
            true
        }
        adapter.notifyDataSetChanged()

    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if(requestCode == Constant.PAYMENT_REQUEST && resultCode == Activity.RESULT_OK){
            currentPageIndex = 0
            getMyBooking()
        }

        if(requestCode == Constant.HOTEL_DETAIL_REQUEST && resultCode == Activity.RESULT_OK){
            currentPageIndex = 0
            getMyBooking()
        }
    }

    private fun setData() {
        username.text = manager.getString(Constant.KEY_USER_NAME)
        user_des.text = manager.getString(Constant.KEY_USER_DESCRIPTION)
        user_role.text = manager.getString(Constant.KEY_USER_PROFESSION)

        val imageUrl = manager.getString(Constant.KEY_USER_IMAGE_URL)

        Log.e("setDatasetData"," "+imageUrl)
        if(!TextUtils.isEmpty(imageUrl)){
            Glide.with(activity!!)
                    .load(imageUrl)
                    .apply(RequestOptions().placeholder(R.drawable.user_default_pic).override(200).error(R.drawable.user_default_pic))
                    .into(profile)
        }else{
            profile.setImageResource(R.drawable.user_default_pic)
        }
    }

    fun getMyBooking() {

        Log.e("getMyBooking"," "+mainActivity?.prefManager!!.getBoolean(Constant.KEY_USER_LOGIN_STATUS))
        if(!mainActivity?.prefManager!!.getBoolean(Constant.KEY_USER_LOGIN_STATUS)){
            return
        }

        val param = JSONObject()
        param.put("api_key", Constant.API_KEY)
        param.put("user_id", mainActivity?.prefManager.getString(Constant.KEY_USER_ID))
        param.put("number", "$currentPageIndex")
        param.put("type", "buyer")

        if(currentPageIndex == 0){
            hotelList.clear()
            if(!swipe_to_refresh.isRefreshing){
                mainActivity.showProgress(frame_loader)
            }
        }

        adapter.nextItemList.clear()
        ServiceRequest(object : ApiResponseListener {
            override fun onCompleted(`object`: Any) {
                mainActivity?.progress?.dismiss()

                mainActivity.hideProgress(frame_loader,swipe_to_refresh)

                try {
                    val json = JSONObject(`object`.toString())
                    if(json.has("data")){
                        val array  = json.getJSONArray("data")
                        val parser  = BookingParser()


                        for(i in 0 until array.length()){
                            val bookingModel = parser.parse(array.getJSONObject(i))
                            adapter.nextItemList.add(bookingModel)

                        }
                        if(adapter.nextItemList.size >0){
                            currentPageIndex++
                            adapter.SKIP_DATA += 5
                        }

                        hotelList.addAll(adapter.nextItemList)
                     //   Collections.reverse(hotelList)
                        adapter.notifyDataSetChanged()


                    }
                }catch (e: Exception){
                    e.printStackTrace()
                }finally {
                    mainActivity.showEmptyView(frame_empty,(hotelList.size == 0))
                }
            }

            override fun onError(errorMessage: String) {

               activity!!.runOnUiThread {
                   if(currentPageIndex >0){
                       adapter.notifyItemChanged(hotelList.size-1)
                   }
                   mainActivity.hideProgress(frame_loader,swipe_to_refresh)
                   mainActivity!!.showEmptyView(frame_empty,(hotelList.size == 0))

                   if(!MyApplication.isNetworkAvailable(activity!!)){
                       mainActivity!!.errorDialog?.show("Error!!","$errorMessage")
                   }
               }
            }

        }).getMyBookingList(param)
    }

    private fun updateStatus(position: Int, status: String) {
        when(ConstantFunction.capsFirstLetter(status)){

            BookingStatus.PENDING ->{

                mainActivity?.alert?.title = ""
                mainActivity?.alert?.content = getString(R.string.bd_hint_warn_cancel_book)
                mainActivity?.alert?.listenr = object : DialogListener(){
                    override fun okClick() {
                        getBookingDetails(position, 1)

                    }
                }

                mainActivity?.alert?.show()
            }

            BookingStatus.CONFIRM ->{

                getBookingDetails(position,2)
            }

            BookingStatus.ACCEPT ->{
                getBookingDetails(position,2)
            }
        }

    }

    private fun changeStatus(position: Int, status: String) {

        if(hotelList[position].hotelModel!!.isDelete){
            mainActivity!!.errorDialog?.show(getString(R.string.hint_error),getString(R.string.propety_deleted))
            return
        }

        val param = JSONObject()
        param.put("api_key", Constant.API_KEY)
        param.put("booking_id", hotelList[position].id)
        param.put("status", status)
        mainActivity.progress?.show()

        ServiceRequest(object : ApiResponseListener {
            override fun onCompleted(`object`: Any) {
                mainActivity!!.progress?.dismiss()

                try {
                    Toast.makeText(activity!!,getString(R.string.bd_hint_req_cancel), Toast.LENGTH_SHORT).show()
                    hotelList[position].status = status
                    adapter.notifyItemChanged(position)
                }catch (e: Exception){
                    e.printStackTrace()
                }
            }

            override fun onError(errorMessage: String) {
                mainActivity!!.progress?.dismiss()
                mainActivity?.errorDialog?.show(getString(R.string.hint_error),"$errorMessage")
            }

        }).updateBooking(param)
    }


    private fun getBookingDetails(position: Int, action: Int=0) {
        activity!!.runOnUiThread {
            mainActivity?.progress?.show()
        }

        val param = JSONObject()
        param.put("api_key", Constant.API_KEY)
        // param.put("user_id", mainActivity?.prefManager!!.getString(Constant.KEY_USER_ID))
        param.put("booking_id", hotelList[position].id)

        ServiceRequest(object :ApiResponseListener{
            override fun onCompleted(`object`: Any) {
                mainActivity?.progress?.dismiss()
                try {
                    val json = JSONObject(`object`.toString())
                    if(json.has("data")){
                        val array  = json.getJSONArray("data")
                        val parser  = BookingParser()
                        for(i in 0 until array.length()){
                            val bookingModel = parser.parse(array.getJSONObject(i))

                            Log.e("bookingModel"," *** "+bookingModel!!.hotelModel!!.isDelete)
                            if(!bookingModel!!.hotelModel!!.isDelete ){
                                when(action){
                                    0 ->{
                                        val intent = Intent(activity!!, BookingDetailsActivity::class.java)
                                        intent.putExtra(Constant.KEY_BOOKING,bookingModel)
                                        intent.putExtra(Constant.KEY_BOOK_CAN,true)
                                        startActivity(intent)
                                    }

                                    1 ->{
                                        changeStatus(position,"Cancel")
                                    }

                                    2 ->{
                                        val intent = Intent(activity!!,PaymentActivity::class.java)
                                        intent.putExtra(Constant.KEY_BOOKING,hotelList[position])
                                        startActivityForResult(intent,Constant.PAYMENT_REQUEST)
                                    }

                                    else ->{
                                        val intent = Intent(activity!!, BookingDetailsActivity::class.java)
                                        intent.putExtra(Constant.KEY_BOOKING,bookingModel)
                                        intent.putExtra(Constant.KEY_BOOK_CAN,true)
                                        startActivity(intent)
                                    }
                                }

                            }else{
                                mainActivity?.errorDialog!!.show("Error!!",getString(R.string.propety_deleted))
                            }
                            break
                        }
                    }
                }catch (e: Exception){
                    e.printStackTrace()
                }
            }

            override fun onError(errorMessage: String) {
               activity!!.runOnUiThread {
                   mainActivity!!.progress?.dismiss()
                   mainActivity!!.errorDialog?.show(getString(R.string.hint_error),"$errorMessage")
               }
            }

        }).getBookingDetails(param)

    }

}
