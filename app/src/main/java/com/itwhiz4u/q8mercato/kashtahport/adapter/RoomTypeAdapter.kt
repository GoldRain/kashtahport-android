package com.itwhiz4u.q8mercato.kashtahport.adapter

import android.app.Activity
import android.content.Context
import android.support.v4.view.PagerAdapter
import android.view.View
import android.support.v4.view.ViewPager
import android.view.ViewGroup
import com.itwhiz4u.q8mercato.kashtahport.R
import com.itwhiz4u.q8mercato.kashtahport.model.RoomTypeModel
import kotlinx.android.synthetic.main.view_room_type.view.*


class RoomTypeAdapter(val context: Context, val list:ArrayList<RoomTypeModel>) :PagerAdapter(){
    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        (container as ViewPager).removeView(`object` as View)
    }

    override fun isViewFromObject(view: View, `object`: Any): Boolean {
        return view === `object` as View
    }


    override fun getCount(): Int {
       return list.size
    }

    override fun instantiateItem(container: ViewGroup, position: Int): Any {

        val room = list[position]
        val viewItem = (context as Activity).layoutInflater.inflate(R.layout.view_room_type,container,false)
        (container as ViewPager).addView(viewItem)
        viewItem.adult_count.setText(room.person+" Adult")
        viewItem.room_count.setText(room.rooms+" Room")
        viewItem.children_count.setText(room.children+" Children")
        viewItem.wc_count.setText(room.wc+" Wc")
        return viewItem
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return ""
    }
}