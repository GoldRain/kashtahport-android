package com.itwhiz4u.q8mercato.kashtahport.Controller

import android.content.Context
import com.itwhiz4u.q8mercato.kashtahport.adapter.HotelListAdapter
import com.itwhiz4u.q8mercato.kashtahport.model.HotelModel

class HotelListMapController(val context: Context, val hotelListAdapter: HotelListAdapter, val holder:HotelListAdapter.MyHotelMapViewHolder, hotelModel: HotelModel, val listener: HotelListAdapter.OnItemClick, val position: Int) {

    fun controll(){

        holder.itemView!!.setOnClickListener {

            listener.onItemClick(position)
        }
    }
}