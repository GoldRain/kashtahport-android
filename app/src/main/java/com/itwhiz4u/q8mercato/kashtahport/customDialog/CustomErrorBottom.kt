package com.itwhiz4u.q8mercato.kashtahport.customDialog

import android.app.Dialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.text.TextUtils
import android.view.*
import com.itwhiz4u.q8mercato.kashtahport.R
import kotlinx.android.synthetic.main.dialog_bottom.*


class CustomErrorBottom(private val context: Context) {
    private var dialog: Dialog
    var title = ""
    var content =""
    var listenr :DialogListener?= null
    init {
        dialog = Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.window.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.setContentView(R.layout.dialog_bottom)

      dialog.window.setWindowAnimations(R.style.Animation)
        val window = dialog.getWindow()
        val wlp = window.getAttributes()
        wlp.width = WindowManager.LayoutParams.MATCH_PARENT
        wlp.height = WindowManager.LayoutParams.WRAP_CONTENT
        wlp.gravity = Gravity.BOTTOM
        window.setAttributes(wlp)

        dialog.alt_ok.setOnClickListener {view ->
            dialog.cancel()

            if(listenr != null){
                listenr?.okClick()
                listenr = null
            }

        }
    }

    fun show(title:String=context.getString(R.string.hint_error), content:String = context.getString(R.string.hint_failed)) {
        if(TextUtils.isEmpty(title)){
            dialog.alt_title.setText(title)
            dialog.alt_title.visibility = View.GONE
        }else{
            dialog.alt_title.setText(title)
            dialog.alt_title.visibility = View.VISIBLE
        }
        dialog.alt_title.setText(title)
        dialog.alt_content.setText(content)

        dialog.show()

    }

    fun show() {
        dialog.alt_title.setText(title)
        dialog.alt_content.setText(content)

        dialog.show()

    }


    fun dismiss() {
        dialog.cancel()
    }

}
