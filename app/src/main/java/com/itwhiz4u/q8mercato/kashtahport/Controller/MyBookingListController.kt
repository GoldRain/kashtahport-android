package com.itwhiz4u.q8mercato.kashtahport.Controller

import android.content.Context
import android.graphics.Bitmap
import android.support.v4.content.ContextCompat
import android.view.View
import com.itwhiz4u.q8mercato.kashtahport.R
import com.itwhiz4u.q8mercato.kashtahport.adapter.HotelMyBookingAdapter
import com.itwhiz4u.q8mercato.kashtahport.helper.BookingStatus
import com.itwhiz4u.q8mercato.kashtahport.helper.ConstantFunction
import com.itwhiz4u.q8mercato.kashtahport.model.HotelBookingModel
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.RequestOptions
import com.bumptech.glide.request.target.Target
import kotlinx.android.synthetic.main.list_item_latest_booking.view.*

class MyBookingListController(val context: Context, val hotelListAdapter: HotelMyBookingAdapter, val holder: HotelMyBookingAdapter.MyHotelViewHolder, val bookingModel: HotelBookingModel, val listener: HotelMyBookingAdapter.OnItemClick, val position: Int) {

    fun control() {
        if(position%2 == 0){
            holder.itemView.back1.visibility = View.GONE
            holder.itemView.back2.visibility = View.VISIBLE
        }else{
            holder.itemView.back1.visibility = View.VISIBLE
            holder.itemView.back2.visibility = View.GONE
        }
        val hotelModel = bookingModel?.hotelModel

        if(hotelModel != null){
            holder.itemView.hotel_name.setText(hotelModel?.name)
            holder.itemView.hotel_des.text = hotelModel?.description

        //    holder.itemView.rent.text = "$"+bookingModel?.price
            ConstantFunction.setPrice(context,hotelModel.currency,bookingModel.price,holder.itemView.rent)

            holder.itemView.booking_status.text = ConstantFunction.capsFirstLetter(bookingModel?.status)
            holder.itemView.rating_bar.rating = hotelModel?.rating!!.toFloat()

            holder.itemView.time.setText(ConstantFunction.getDateAndTime(bookingModel.created_at.toLong()))
            holder.itemView!!.im_cancel.visibility = View.GONE

            holder.itemView.room_count.text = bookingModel.hotelModel?.rooms+" "+context.getString(R.string.hint_room)
            holder.itemView.adult_count.text = bookingModel.hotelModel?.person+" "+context.getString(R.string.hint_adult)
            holder.itemView.children_count.text = bookingModel.hotelModel?.children+" "+context.getString(R.string.hint_child)

            when(ConstantFunction.capsFirstLetter(bookingModel?.status)){
                BookingStatus.PENDING ->{
                    holder.itemView.booking_status.text = context.getString(R.string.bk_status_cancel)
                    holder.itemView.booking_status.setBackgroundResource(R.drawable.back_corner_redd)
                    holder.itemView.booking_status.setTextColor(ContextCompat.getColor(context,R.color.colorWhite))

                }

                BookingStatus.CONFIRM ->{
                    holder.itemView.booking_status.text = context.getString(R.string.hint_pay_now)
                    holder.itemView.booking_status.setBackgroundResource(R.drawable.back_corner_pri)
                    holder.itemView.booking_status.setTextColor(ContextCompat.getColor(context,R.color.colorWhite))

                }

                BookingStatus.ACCEPT ->{
                    holder.itemView.booking_status.text = context.getString(R.string.hint_pay_now)
                    holder.itemView.booking_status.setTextColor(ContextCompat.getColor(context,R.color.colorWhite))
                    holder.itemView.booking_status.setBackgroundResource(R.drawable.back_corner_pri)
                }

                BookingStatus.REJECT ->{
                    holder.itemView.booking_status.text = context.getString(R.string.bk_status_rejected)
                    holder.itemView.booking_status.setTextColor(ContextCompat.getColor(context,R.color.color_status_red))
                    holder.itemView.booking_status.setBackgroundResource(0)
                }

                BookingStatus.CANCEL ->{
                    holder.itemView.booking_status.text = context.getString(R.string.bk_status_canceled)
                    holder.itemView.booking_status.setBackgroundResource(0)
                    holder.itemView.booking_status.setTextColor(ContextCompat.getColor(context,R.color.color_status_red))
                }

                BookingStatus.PAID ->{
                    holder.itemView.booking_status.text = context.getString(R.string.bk_status_paid)
                    holder.itemView.booking_status.setBackgroundResource(R.drawable.back_corner_green)
                    holder.itemView.booking_status.setTextColor(ContextCompat.getColor(context,R.color.colorWhite))
                }


                else ->{
                    holder.itemView.booking_status.text = ConstantFunction.capsFirstLetter(bookingModel?.status)
                    holder.itemView.booking_status.setBackgroundResource(0)
                    holder.itemView.booking_status.setTextColor(ContextCompat.getColor(context,R.color.color_status_red))
                }
            }

            if (hotelModel.imageList.size > 0) {
                //Log.d("HotelListController", " "+hotelModel.name+ " "+hotelModel.imageList[0].url)
                Glide.with(context)
                        .asBitmap()
                        .load(hotelModel.imageList[0].url)
                        .apply(RequestOptions().override(200))
                        .listener(object : RequestListener<Bitmap> {
                            override fun onLoadFailed(e: GlideException?, model: Any?, target: Target<Bitmap>?, isFirstResource: Boolean): Boolean {

                                return false
                            }

                            override fun onResourceReady(resource: Bitmap?, model: Any?, target: Target<Bitmap>?, dataSource: DataSource?, isFirstResource: Boolean): Boolean {
                                return false
                            }

                        })
                        .into(holder.itemView.hotel_image)
            }



            holder.itemView!!.setOnClickListener {

                listener.onItemClick(position)
            }
            holder.itemView!!.booking_status.setOnClickListener {
                listener.onUpdateBooking(position,bookingModel.status)
            }

            if(hotelListAdapter.nextItemList.size >0){
                if(hotelListAdapter.mList.size >=5 && position == hotelListAdapter.mList.size-1){
                    holder.itemView.card_progress.visibility = View.VISIBLE
                    listener.onLoadMore(position)
                }else{
                    holder.itemView.card_progress.visibility = View.GONE
                }

            }else{
                holder.itemView.card_progress.visibility = View.GONE
            }
        }else{

        }
    }
}