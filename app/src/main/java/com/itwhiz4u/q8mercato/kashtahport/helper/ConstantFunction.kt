package com.itwhiz4u.q8mercato.kashtahport.helper

import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.Environment
import android.text.TextUtils
import android.util.Log
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.TextView
import com.itwhiz4u.q8mercato.kashtahport.R
import com.itwhiz4u.q8mercato.kashtahport.Retrofit.Model.Response.UserData
import com.itwhiz4u.q8mercato.kashtahport.database.FilterData
import com.itwhiz4u.q8mercato.kashtahport.model.UserModel
import org.json.JSONArray
import org.json.JSONObject
import java.io.File
import java.io.FileInputStream
import java.io.FileOutputStream
import java.text.SimpleDateFormat
import java.util.*

class ConstantFunction {

    companion object {
        fun getTempLocation(): String {


            return Constant.LOCAL_BASE_PATH + ".Temp/"
        }


        fun hideKeyboard(context: Context,view: View){
            val imm = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(view.windowToken, 0)
        }

        fun capsFirstLetter(str: String): String {
            var rStr = ""
            if (str.length > 1) {
                rStr = str.get(0).toUpperCase() + str.substring(1)
            } else {
                rStr = str.toUpperCase()
            }

            return rStr
        }

        fun getTime(milliSeconds: Long): String {
            var dateSend = ""
            val calendar = Calendar.getInstance()
            calendar.timeInMillis = milliSeconds
            val dateFormat2 = SimpleDateFormat(Constant.DEFAULT_TIME_FORMAT)
            dateSend = dateFormat2.format(calendar.time)
            return dateSend
        }

        fun getOnlyDate(milliSeconds: Long): String {
            var dateSend = ""
            val calendar = Calendar.getInstance()
            calendar.timeInMillis = milliSeconds
            val dateFormat2 = SimpleDateFormat(Constant.DEFAULT_DATE_FORMAT)
            dateSend = dateFormat2.format(calendar.time)
            return dateSend
        }

        fun getOnlyDate(milliSeconds: Long, dateFormat: String): String {
            var dateSend = ""
            val calendar = Calendar.getInstance()
            calendar.timeInMillis = milliSeconds
            val dateFormat2 = SimpleDateFormat(dateFormat)
            dateSend = dateFormat2.format(calendar.time)
            return dateSend
        }

        fun getDateAndTime(milliSeconds: Long): String {
            var dateSend = ""
            val calendar = Calendar.getInstance()
            calendar.timeInMillis = milliSeconds

            val dateFormat2 = SimpleDateFormat(Constant.DEFAULT_DATE_TIME_FORMAT)

            dateSend = dateFormat2.format(calendar.time)
            return dateSend
        }

        fun getDateAndTimeMS(milliSeconds: Long): String {
            var dateSend = ""
            val calendar = Calendar.getInstance()
            calendar.timeInMillis = milliSeconds

            val dateFormat2 = SimpleDateFormat(Constant.DEFAULT_DATE_TIME_FORMAT)

            dateSend = dateFormat2.format(calendar.time)
            return dateSend
        }

        fun saveUserDataToPref(userModel: UserModel) {
            val manager = MyApplication.instance.prefManager

            manager.setString(Constant.KEY_USER_ID, userModel.id)
            manager.setString(Constant.KEY_USER_NAME, userModel.name)
            manager.setString(Constant.KEY_USER_TYPE, userModel.type)
            manager.setString(Constant.KEY_USER_EMAIL, userModel.email)
            manager.setString(Constant.KEY_USER_PASSWORD, userModel.password)
            manager.setString(Constant.KEY_USER_PHONE, userModel.phone)
            manager.setString(Constant.KEY_USER_CODE, userModel.countryCode)
            manager.setString(Constant.KEY_USER_ACCESS_TOKEN, userModel.access_token)
            manager.setString(Constant.KEY_USER_DESCRIPTION, userModel.description)
            manager.setString(Constant.KEY_USER_PROFESSION, userModel.profession)
            manager.setString(Constant.KEY_USER_IMAGE_URL, userModel.imageUrl)
            manager.setString(Constant.KEY_USER_LAST_LOGIN, userModel.last_login)
            manager.setString(Constant.KEY_USER_CREATED, userModel.created_at)
            manager.setString(Constant.KEY_USER_NOTIFICATION_TOKEN, userModel.notification_token)

            manager.setBoolean(Constant.KEY_USER_CAN_SELL, userModel.can_sale)
            manager.setString(Constant.KEY_USER_DEVICE_ID, userModel.device_id)
            manager.setString(Constant.KEY_USER_DEVICE_TYPE, userModel.device_type)
            manager.setString(Constant.KEY_USER_FACE_BOOK_ID, userModel.faceBookId)

            manager.setString(Constant.KEY_IBAN_COUNTRY, userModel.ibanCountry)
            manager.setString(Constant.KEY_IBAN_BAN_NUMBER, userModel.ibanNumber)
            manager.setString(Constant.KEY_IBAN_BANK_NAME, userModel.ibankName)
            manager.setString(Constant.KEY_IBAN_PHONE, userModel.ibanPhone)

        }

        fun saveUserDataToPref(userModel: UserData) {
            val manager = MyApplication.instance.prefManager

            manager.setString(Constant.KEY_USER_ID, ""+userModel.id)
            manager.setString(Constant.KEY_USER_NAME, ""+userModel.name)
            manager.setString(Constant.KEY_USER_TYPE, ""+userModel.type)
            manager.setString(Constant.KEY_USER_EMAIL, ""+userModel.email)
            manager.setString(Constant.KEY_USER_PASSWORD, ""+userModel.password)
            manager.setString(Constant.KEY_USER_PHONE, ""+userModel.phone)
            manager.setString(Constant.KEY_USER_CODE, ""+userModel.countryCode)
            manager.setString(Constant.KEY_USER_ACCESS_TOKEN, ""+userModel.access_token)
            manager.setString(Constant.KEY_USER_DESCRIPTION, ""+userModel.description)
            manager.setString(Constant.KEY_USER_PROFESSION, ""+userModel.profession)
            manager.setString(Constant.KEY_USER_IMAGE_URL, ""+userModel.profileImageUrl)
            manager.setString(Constant.KEY_USER_LAST_LOGIN, ""+userModel.lastLogin)
            manager.setString(Constant.KEY_USER_CREATED, ""+userModel.createdAt)
            manager.setString(Constant.KEY_USER_NOTIFICATION_TOKEN, ""+userModel.notificationToken)

            manager.setBoolean(Constant.KEY_USER_CAN_SELL, userModel.can_sale)

            manager.setString(Constant.KEY_USER_DEVICE_ID, ""+userModel.deviceId)

            manager.setString(Constant.KEY_USER_FACE_BOOK_ID, ""+userModel.facebook)

            manager.setString(Constant.KEY_IBAN_COUNTRY, ""+userModel.IBAN?.countryCode)
            manager.setString(Constant.KEY_IBAN_BAN_NUMBER, ""+userModel.IBAN?.IBAN)
            manager.setString(Constant.KEY_IBAN_BANK_NAME, ""+userModel.IBAN?.bank_name)
            manager.setString(Constant.KEY_IBAN_PHONE, ""+userModel.IBAN?.phone)

        }

        fun getResizedBitmap(image: Bitmap, maxSize: Int): Bitmap {
            var width = image.width
            var height = image.height

            val bitmapRatio = width.toFloat() / height.toFloat()
            if (bitmapRatio > 1) {
                width = maxSize
                height = (width / bitmapRatio).toInt()
            } else {
                height = maxSize
                width = (height * bitmapRatio).toInt()
            }
            return Bitmap.createScaledBitmap(image, width, height, true)
        }

        fun getBitmapFromPath(path: String): Bitmap? {
            var bitmap: Bitmap? = null
            try {

                val f = File(path)
                val options = BitmapFactory.Options()
                options.inPreferredConfig = Bitmap.Config.ARGB_8888

                bitmap = BitmapFactory.decodeStream(FileInputStream(f), null, options)

            } catch (e: Exception) {
                e.printStackTrace()

            } finally {
                return bitmap
            }
        }

        fun saveHotelImage(bitmap: Bitmap, name: String=""): File? {
            val filename: File
            var imageName = name
            if (TextUtils.isEmpty(imageName)) {
                imageName = "IMG_" + System.currentTimeMillis().toString()
            }
            try {
                //val path = Environment.getExternalStorageDirectory().toString()
                val file = File(Constant.HOTEL_IMAGE_PATH)
                file.mkdirs()
                filename = File("${Constant.HOTEL_IMAGE_PATH}$imageName.jpg")
                val out = FileOutputStream(filename)
                bitmap.compress(Bitmap.CompressFormat.JPEG, 90, out)
                out.flush()
                out.close()
                return filename
            } catch (e: Exception) {
                e.printStackTrace()
                return null
            }

        }

        fun saveUserImage(bitmap: Bitmap): File? {
            val filename: File
            val imageName = System.currentTimeMillis().toString() + ""
            try {
                val path = Environment.getExternalStorageDirectory().toString()
                val file = File(Constant.USER_IMAGE_PATH)
                file.mkdirs()
                filename = File("${Constant.USER_IMAGE_PATH}IMG_$imageName.jpg")
                val out = FileOutputStream(filename)
                bitmap.compress(Bitmap.CompressFormat.JPEG, 90, out)
                out.flush()
                out.close()
                return filename
            } catch (e: Exception) {
                e.printStackTrace()
                return null
            }


        }

        fun deleteUserImage() {
            try {
                val path = Environment.getExternalStorageDirectory().toString()
                val file = File(Constant.USER_IMAGE_PATH)
                if (file.isDirectory) {
                    file.listFiles().forEach {
                        if (it.exists()) {
                            it.delete()
                        }
                    }
                }

            } catch (e: Exception) {
                e.printStackTrace()
            }
        }

        fun deleteHotelImage() {
            try {
                val path = Environment.getExternalStorageDirectory().toString()
                val file = File(Constant.HOTEL_IMAGE_PATH)
                if (file.isDirectory) {
                    file.listFiles().forEach {
                        if (it.exists()) {
                            it.delete()
                        }
                    }
                }

            } catch (e: Exception) {
                e.printStackTrace()
            }


        }

        fun getDynamicLink(id:String):String{
            var link = ""
            val str = MyApplication.instance.prefManager.getString(Constant.KEY_DYNEMIC_LINK)
            var jsArray = JSONArray()

            if(!TextUtils.isEmpty(str)){
                jsArray = JSONArray(str)

                for (i in 0 until jsArray.length()){
                    val json = jsArray.getJSONObject(i)
                    if(json.has(id)){
                        link = json.getString(id)
                        break
                    }
                }
            }

            return link
        }

        fun saveDynamicLink(id:String, link:String){
            try {

                val str = MyApplication.instance.prefManager.getString(Constant.KEY_DYNEMIC_LINK)
                var jsArray = JSONArray()
                if(!TextUtils.isEmpty(str)){
                    jsArray = JSONArray(str)
                }

                val json = JSONObject()
                json.put(id, link)
                jsArray.put(json)

                MyApplication.instance.prefManager.setString(Constant.KEY_DYNEMIC_LINK,jsArray.toString())

            }catch (e:java.lang.Exception){
                e.printStackTrace()
            }
        }
        fun getFilterParam(filterData: FilterData):JSONObject{
            val param = JSONObject()
            param.put("api_key",Constant.API_KEY)
            param.put("type",filterData.type)
            param.put("city",filterData.city)

            if(!TextUtils.isEmpty(filterData.rooms) && filterData.rooms != "0"){
                param.put("room",filterData.rooms)
            }

            if(!TextUtils.isEmpty(filterData.country) ){
                param.put("country",filterData.country)
            }

            if(!TextUtils.isEmpty(filterData.person) && filterData.person != "0"){
                param.put("person",filterData.person)
            }

            if(!TextUtils.isEmpty(filterData.wc) && filterData.wc != "0"){
                param.put("wc",filterData.wc)
            }

            if(!TextUtils.isEmpty(filterData.child) && filterData.child != "0"){
                param.put("child",filterData.child)
            }



            if(!TextUtils.isEmpty(filterData.startDate) && filterData.startDate != "0"){
                Log.d("FilterParam", ConstantFunction.getDateAndTime(filterData.startDate.toLong())+" "+ ConstantFunction.getDateAndTime(filterData.endDate.toLong()))
                param.put("date_from",filterData.startDate)
            }

            if(!TextUtils.isEmpty(filterData.endDate) && filterData.endDate != "0"){
                param.put("date_to",filterData.endDate)
            }
            return param
        }


        fun setPrice(context: Context,currency:String,price:String, view:TextView){
            if(currency.toUpperCase() == context.getString(R.string.hint_usd)){
                view.text = "$ ${price}"
            }else{
                view.text = "KWD ${price}"
            }
        }

        fun getCurrencySign(type:String):String{
            if(type.toUpperCase() == "USD"){
                return "$"
            }else{
                return "KWD"
            }
        }
    }
}