package com.itwhiz4u.q8mercato.kashtahport.customView;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.support.v7.widget.AppCompatTextView;
import android.util.AttributeSet;
import com.itwhiz4u.q8mercato.kashtahport.R;

public class CustomFontText extends AppCompatTextView {

    String customFont;

    public CustomFontText(Context context) {
        super(context);
    }

    public CustomFontText(Context context, AttributeSet attrs) {
        super(context, attrs);
        style(context,attrs);
    }

    public CustomFontText(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        style(context,attrs);
    }

    private void style(Context context, AttributeSet attrs){
        TypedArray a= context.obtainStyledAttributes(attrs,R.styleable.CustomFontText);

        int cf = a.getInteger(R.styleable.CustomFontText_fontName,0);

        switch (cf){
            case 0 :{
                customFont = "Poppins-Light.ttf";
                break;
            }

            case 1 :{
                customFont = "Roboto-Bold.ttf";
                break;
            }
        }

        Typeface typeface = Typeface.createFromAsset(context.getAssets(),"fonts/"+customFont);
        setTypeface(typeface);
        a.recycle();

    }
}
