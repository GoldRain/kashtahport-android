package com.itwhiz4u.q8mercato.kashtahport.model

import android.os.Parcel
import android.os.Parcelable

class HotelBookingModel() :Parcelable {


    var id = ""
    var ownerId  = ""
    var buyerId = ""
    var hotelId = ""
    var image = ""
    var childrens= ""
    var person= ""
    var rooms= ""
    var status= ""
    var price= ""
    var dateFrom= ""
    var dateTo= ""
    var created_at= "0"
    var hotelModel:HotelModel?= null
    var userModel:UserModel?= null

    constructor(parcel: Parcel) : this() {
        id = parcel.readString()
        ownerId = parcel.readString()
        buyerId = parcel.readString()
        hotelId = parcel.readString()
        image = parcel.readString()
        childrens = parcel.readString()
        person = parcel.readString()
        rooms = parcel.readString()
        status = parcel.readString()
        price = parcel.readString()
        dateFrom = parcel.readString()
        dateTo = parcel.readString()
        created_at = parcel.readString()
        hotelModel = parcel.readParcelable(HotelModel::class.java.classLoader)
        userModel = parcel.readParcelable(UserModel::class.java.classLoader)
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(id)
        parcel.writeString(ownerId)
        parcel.writeString(buyerId)
        parcel.writeString(hotelId)
        parcel.writeString(image)
        parcel.writeString(childrens)
        parcel.writeString(person)
        parcel.writeString(rooms)
        parcel.writeString(status)
        parcel.writeString(price)
        parcel.writeString(dateFrom)
        parcel.writeString(dateTo)
        parcel.writeString(created_at)
        parcel.writeParcelable(hotelModel, flags)
        parcel.writeParcelable(userModel, flags)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<HotelBookingModel> {
        override fun createFromParcel(parcel: Parcel): HotelBookingModel {
            return HotelBookingModel(parcel)
        }

        override fun newArray(size: Int): Array<HotelBookingModel?> {
            return arrayOfNulls(size)
        }
    }


}