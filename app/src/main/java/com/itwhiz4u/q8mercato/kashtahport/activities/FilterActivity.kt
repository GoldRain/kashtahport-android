package com.itwhiz4u.q8mercato.kashtahport.activities

import android.app.Activity
import android.app.Dialog
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.os.Handler
import android.support.v7.widget.AppCompatSpinner
import android.text.TextUtils
import android.util.Log
import android.view.Gravity
import android.view.View
import android.view.Window
import android.view.WindowManager
import android.widget.AdapterView
import android.widget.TextView
import com.itwhiz4u.q8mercato.kashtahport.Api.ApiResponseListener
import com.itwhiz4u.q8mercato.kashtahport.Api.ServiceRequest
import com.itwhiz4u.q8mercato.kashtahport.R
import com.itwhiz4u.q8mercato.kashtahport.adapter.SpinnerListAdapter
import com.itwhiz4u.q8mercato.kashtahport.database.AppDatabase
import com.itwhiz4u.q8mercato.kashtahport.database.FilterData
import com.itwhiz4u.q8mercato.kashtahport.helper.Constant
import com.itwhiz4u.q8mercato.kashtahport.helper.ConstantFunction
import com.itwhiz4u.q8mercato.kashtahport.model.CityModel
import com.itwhiz4u.q8mercato.kashtahport.parser.CountryParser
import kotlinx.android.synthetic.main.activity_filter.*
import kotlinx.android.synthetic.main.dialog_calender.*
import kotlinx.android.synthetic.main.view_header_toolbar.*
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread
import org.json.JSONObject
import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.HashMap

class FilterActivity : BaseActivity() {

    var properTyList: List<String> = ArrayList()
    var childrenList: List<String> = ArrayList()
    var personList: List<String> = ArrayList()
    var roomList: List<String> = ArrayList()
    var wcList: List<String> = ArrayList()
    var countryList = ArrayList<String>()
    var cityList = ArrayList<String>()

    val cityHash = HashMap<String, ArrayList<CityModel>>()
    lateinit var dialogCalendar: Dialog

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_filter)
        supportActionBar?.hide()

        initView()

        header_name.setText(getString(R.string.fl_header_name))
        if (MainActivity.getInstance()!!.countryList.size == 0) {
            getCountries()
        }

        icon_back.visibility = View.VISIBLE
        header_reset.visibility = View.VISIBLE

        icon_back.setOnClickListener {

            onBackPressed()
        }

        date_start.setOnClickListener {
            isStart = true
            showCalenderDialog(date_start)
        }

        date_end.setOnClickListener {
            isStart = false
            showCalenderDialog(date_end)
        }

        header_reset.setOnClickListener {
            prefManager?.setBoolean(Constant.KEY_FILTER_ENABLE, false)
            doAsync {
                AppDatabase.getAppDatabase().filterDao().deleteAllFilter()
                uiThread {
                    setResult(Activity.RESULT_OK)
                    finish()
                }
            }
        }

        frame_btn.setOnClickListener {
            createFilter()
        }
    }

    private fun initView() {

        initList()
        initSpinner()

        if (prefManager?.getBoolean(Constant.KEY_FILTER_ENABLE)) {
            setSpinnerValue()
        }
    }

    /*"country":SearchHotel.country,
                  //"room":SearchHotel.room,
                  //"child":SearchHotel.child,
                  //"person":SearchHotel.person,
                  //"wc":SearchHotel.wc,
                  "date_from":SearchHotel.startDate,
                  "date_to":SearchHotel.endDate]*/
    fun createFilter() {
        var count = 0
        val filterData = FilterData()

        filterData.id = spinner_type.selectedItem.toString()

        if (spinner_type.selectedItemPosition > 0) {
            count++
            val type = spinner_type.selectedItem.toString()
            when (type) {
                getString(R.string.hint_camp) -> {
                    filterData.type = "Camp"
                }

                getString(R.string.hint_farm) -> {
                    filterData.type = "Farm"
                }

                getString(R.string.hint_chalet) -> {
                    filterData.type = "Chalet"
                }
            }
        }

        if (spinner_room.selectedItemPosition > 0) {
            count++
            filterData.rooms = spinner_room.selectedItem.toString()
        }

        if (spinner_children.selectedItemPosition > 0) {
            count++
            filterData.child = spinner_children.selectedItem.toString()
        }

        if (spinner_person.selectedItemPosition > 0) {
            count++
            filterData.person = spinner_person.selectedItem.toString()
        }

        if (spinner_wc.selectedItemPosition > 0) {
            count++
            filterData.wc = spinner_wc.selectedItem.toString()
        }

        if (startMs != "0") {
            count++
            filterData.startDate = (startMs.toLong() + TimeZone.getDefault().rawOffset).toString()

            if (endMs != "0") {
                filterData.endDate = (endMs.toLong() + TimeZone.getDefault().rawOffset).toString()
            }
        }

        if (spinner_city.selectedItemPosition > 0) {
            count++
            filterData.city = spinner_city.selectedItem.toString()
        }

        if (spinner_country.selectedItemPosition > 0) {
            count++
            filterData.country = spinner_country.selectedItem.toString()
        }

        filterData.filterCount = count
        val param = ConstantFunction.getFilterParam(filterData)

        Log.e("FILTER_DATA", " ${param.toString()}")

        prefManager?.setBoolean(Constant.KEY_FILTER_ENABLE, true)

        doAsync {
            AppDatabase.getAppDatabase().filterDao().deleteAllFilter()
            AppDatabase.getAppDatabase().filterDao().insert(filterData)
            uiThread {
                val intent = Intent()
                intent.putExtra(Constant.KEY_FILTER_DATA, filterData)
                setResult(Activity.RESULT_OK, intent)
                finish()
            }
        }
    }

    private fun initSpinner() {


        val adapterProperty = SpinnerListAdapter(this, properTyList)

        spinner_type.adapter = adapterProperty
        spinner_type.setSelection(0)


        val adapterPerson = SpinnerListAdapter(this, personList)

        spinner_person.adapter = adapterPerson
        spinner_person.setSelection(0)

        val adapterChild = SpinnerListAdapter(this, childrenList)

        spinner_children.adapter = adapterChild
        spinner_children.setSelection(0)

        val adapterRoom = SpinnerListAdapter(this, roomList)

        spinner_room.adapter = adapterRoom
        spinner_room.setSelection(0)

        val adapterWc = SpinnerListAdapter(this, wcList)

        spinner_wc.adapter = adapterWc
        spinner_wc.setSelection(0)


        //  (countryList as ArrayList<String>).add("Select Country")
        val adapterCountry = SpinnerListAdapter(this, countryList)

        spinner_country.adapter = adapterCountry
        spinner_country.setSelection(0)

        spinner_country.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {

            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {

                cityList.clear()
                cityList.add(getString(R.string.fl_hint_select_city))
                spinner_city.setSelection(0)
                if (position > 0) {
                    cityHash.get(countryList[position])!!.forEach {
                        cityList.add(it.name)
                    }

                }

                Log.e("MainActivity", " ** onItemSelected" + cityHash.size + " " + position)
            }

        }

        val adapterCity = SpinnerListAdapter(this, cityList)

        spinner_city.adapter = adapterCity
        spinner_city.setSelection(0)

    }

    fun setSpinnerValue() {
        doAsync {
            val filter = AppDatabase.getAppDatabase().filterDao().getFilter()
            if (filter != null) {
                uiThread {

                    setValueInSpinner(spinner_type, properTyList, filter.type)
                    setValueInSpinner(spinner_country, countryList, filter.country)

                    setValueInSpinner(spinner_room, roomList, filter.rooms)
                    setValueInSpinner(spinner_person, personList, filter.person)
                    setValueInSpinner(spinner_wc, wcList, filter.wc)
                    setValueInSpinner(spinner_children, childrenList, filter.child)

                    if (!TextUtils.isEmpty(filter.startDate)) {
                        date_start.setText(ConstantFunction.getOnlyDate(filter.startDate.toLong()))

                        startMs = filter.startDate
                    }

                    if (!TextUtils.isEmpty(filter.endDate)) {
                        date_end.setText(ConstantFunction.getOnlyDate(filter.endDate.toLong()))
                        endMs = filter.endDate
                    }


                    Handler().postDelayed(object : Runnable {
                        override fun run() {
                            Log.e("MainActivity", " ** " + cityHash.size + " ")
                            cityList.clear()
                            cityList.add(getString(R.string.fl_hint_select_city))
                            spinner_city.setSelection(0)
                            if (!TextUtils.isEmpty(filter.country)) {
                                cityHash.get(filter.country)!!.forEach {
                                    cityList.add(it.name)
                                }
                            }

                            setValueInSpinner(spinner_city, cityList, filter.city)
                        }
                    }, 200)


                }

            }
        }
    }

    private fun setValueInSpinner(spinner_type: AppCompatSpinner?, list: List<String>, modelValue: String?) {
        for (i in 0 until list.size) {
            val lV = list[i]
            if (modelValue.equals(lV)) {
                spinner_type?.setSelection(i)
            }
        }
    }

    private fun initList() {

        var arrayProperty = resources.getStringArray(R.array.property_type2)
        properTyList = arrayProperty.asList()


        var arrayPerson = resources.getStringArray(R.array.person_array2)
        personList = arrayPerson.asList()

        var arrayChild = resources.getStringArray(R.array.child_array2)
        childrenList = arrayChild.asList()

        var arrayRoom = resources.getStringArray(R.array.room_array2)
        roomList = arrayRoom.asList()

        var arrayWc = resources.getStringArray(R.array.wc_array2)
        wcList = arrayWc.asList()

        /* var countryArray = resources.getStringArray(R.array.country_array)
         countryList = countryArray.asList()

         var cityArray = resources.getStringArray(R.array.city_array)
         cityList = cityArray.asList()*/

        countryList.add(getString(R.string.fl_hint_select_country))
        cityList.add(getString(R.string.fl_hint_select_city))
        if (MainActivity!!.getInstance() != null) {

            MainActivity.getInstance()?.countryList!!.forEach {
                countryList.add(it.name)
                cityHash.put(it.name, it.cityList)
            }
        }
    }


    var startMs = "0"
    var endMs = "0"
    var isStart = false

    private fun showCalenderDialog(dateTextView: TextView) {
        progress?.show()
        Handler().postDelayed(object : Runnable {
            override fun run() {
                progress?.dismiss()
                dialogCalendar = Dialog(this@FilterActivity)
                dialogCalendar.requestWindowFeature(Window.FEATURE_NO_TITLE)
                dialogCalendar.window.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
                dialogCalendar.setContentView(R.layout.dialog_calender)
                val window = dialogCalendar.getWindow()
                val wlp = window.getAttributes()
                wlp.width = WindowManager.LayoutParams.MATCH_PARENT
                wlp.height = WindowManager.LayoutParams.WRAP_CONTENT
                wlp.gravity = Gravity.CENTER_VERTICAL
                window.setAttributes(wlp)

                val cal = Calendar.getInstance()
                cal.timeInMillis = System.currentTimeMillis()
                dialogCalendar.calender.setDate(cal)

                if (!isStart && startMs != "0") {
                    cal.timeInMillis = startMs.toLong()
                }

                dialogCalendar.calender.setMinimumDate(cal)

                dialogCalendar.calender.setOnDayClickListener {
                    val cal = it.calendar
                    val date = ConstantFunction.getOnlyDate(cal.timeInMillis)
                    dateTextView.setText(date)
                    dialogCalendar.dismiss()

                    if (isStart) {
                        endMs = "0"
                        date_end.setText(getString(R.string.fl_hint_to))

                        startMs = cal.timeInMillis.toString()
                    } else {
                        endMs = cal.timeInMillis.toString()
                    }
                }

                dialogCalendar.show()
            }

        }, 200)
    }

    private fun getCountries() {

        ServiceRequest(object : ApiResponseListener {
            override fun onCompleted(`object`: Any) {
                try {
                    val json = JSONObject(`object`.toString())
                    if (json.has("data")) {
                        val objArray = json.getJSONArray("data")
                        val parser = CountryParser()
                        MainActivity.getInstance()!!.countryList.clear()
                        for (i in 0 until objArray.length()) {
                            val obj = objArray.getJSONObject(i)
                            val country = parser.parse(obj)

                            MainActivity.getInstance()!!.countryList.add(country)
                            initView()
                        }
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

            override fun onError(errorMessage: String) {
                // errorDialog?.show("Error!!","Failed $errorMessage")
            }

        }).getCountries()
    }

}
