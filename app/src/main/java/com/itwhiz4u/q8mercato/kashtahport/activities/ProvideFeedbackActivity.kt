package com.itwhiz4u.q8mercato.kashtahport.activities

import android.content.Intent
import android.graphics.Bitmap
import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import android.view.MotionEvent
import android.view.View
import android.widget.Toast
import com.itwhiz4u.q8mercato.kashtahport.Api.ApiResponseListener
import com.itwhiz4u.q8mercato.kashtahport.Api.ServiceRequest
import com.itwhiz4u.q8mercato.kashtahport.R
import com.itwhiz4u.q8mercato.kashtahport.helper.Constant
import com.itwhiz4u.q8mercato.kashtahport.model.HotelBookingModel
import com.itwhiz4u.q8mercato.kashtahport.model.HotelModel
import com.itwhiz4u.q8mercato.kashtahport.parser.FeedbackParser
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.RequestOptions
import com.bumptech.glide.request.target.Target
import kotlinx.android.synthetic.main.activity_provide_feedback.*
import kotlinx.android.synthetic.main.view_header_toolbar.*
import org.json.JSONObject

class ProvideFeedbackActivity : BaseActivity() {

    var hotelModel: HotelModel? = null
     var bookingModel: HotelBookingModel? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_provide_feedback)
        supportActionBar?.hide()

         bookingModel = intent.getParcelableExtra<HotelBookingModel>(Constant.KEY_BOOKING)

        if (bookingModel != null) {
            hotelModel = bookingModel!!.hotelModel
        }
        icon_back.visibility = View.VISIBLE
        header_name.setText(getString(R.string.profe_hint_header_name))

        icon_back.setOnClickListener {
            onBackPressed()
        }

        frame_btn.setOnClickListener {
            if (validation()) {

                applyFeedback()
            }
        }

        getFeedback()
        try {
            if (hotelModel?.imageList!!.size > 0) {
                //  Log.d("HotelListController", " "+hotelModel?.name+ " "+hotelModel.imageList[0].url)
                Glide.with(this)
                        .asBitmap()
                        .load(hotelModel!!.imageList[0]!!.url)
                        .apply(RequestOptions().override(200))
                        .listener(object : RequestListener<Bitmap> {
                            override fun onLoadFailed(e: GlideException?, model: Any?, target: Target<Bitmap>?, isFirstResource: Boolean): Boolean {

                                return false
                            }

                            override fun onResourceReady(resource: Bitmap?, model: Any?, target: Target<Bitmap>?, dataSource: DataSource?, isFirstResource: Boolean): Boolean {
                                return false
                            }

                        })
                        .into(hotel_image)
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }

        et_des.setOnTouchListener { v, event ->
            v.getParent().requestDisallowInterceptTouchEvent(true);
            when (event.getAction()) {

                MotionEvent.ACTION_SCROLL -> {
                    v.getParent().requestDisallowInterceptTouchEvent(false);
                    true;
                }

            }
            false;
        }
    }

    /*user_id:5cc2d758b290fb2fbae2148c
api_key:T0BAnEzf6CaWwIMulImtA05nT6fDVjXKHoJM136S
hotel_id:5cc2e914add41c37c2f5799e
communication:5
price:1
delivery:3
proficiency:2*/
    private fun applyFeedback() {

        val param = JSONObject()
        param.put("user_id", prefManager.getString(Constant.KEY_USER_ID))
        param.put("api_key", Constant.API_KEY)
        param.put("hotel_id", hotelModel?.id)
        param.put("booking_id", bookingModel?.id)
        param.put("communication", rat_communication.rating.toString())
        param.put("price", rating_price.rating.toString())
        param.put("delivery", rating_delivery.rating.toString())
        param.put("proficiency", rating_proficiency.rating.toString())
        param.put("comment", et_des.text.toString().trim())

        progress?.show()
        ServiceRequest(object : ApiResponseListener {
            override fun onCompleted(`object`: Any) {
                progress?.dismiss()
                startActivity(Intent(this@ProvideFeedbackActivity, AllDoneActivity::class.java))
                finish()
            }

            override fun onError(errorMessage: String) {
                progress?.dismiss()
                Toast.makeText(this@ProvideFeedbackActivity, "$errorMessage", Toast.LENGTH_SHORT).show()
            }

        }).provideFeedback(param)
    }

    private fun validation(): Boolean {

        return true
    }

    private fun getFeedback() {

        val param = JSONObject()
        param.put("api_key", Constant.API_KEY)
        param.put("hotel_id", hotelModel?.id)


        frame_load_rat.visibility = View.VISIBLE
        ServiceRequest(object : ApiResponseListener {
            override fun onCompleted(`object`: Any) {
                try {
                       frame_load_rat.visibility = View.GONE
                    val json = JSONObject(`object`.toString())
                    if (json.has("data")) {
                        val array = json.getJSONArray("data")
                        for (i in 0 until  array.length()){
                            var obj = JSONObject()
                            if(array.get(i) is JSONObject){
                                obj = array.getJSONObject(i)
                                if(obj.has("feedbackData")){
                                    val feedArray = obj.getJSONArray("feedbackData")
                                    val parser = FeedbackParser()
                                    for (j in 0 until feedArray.length()){
                                        val feedObj = feedArray.getJSONObject(j)
                                        val model = parser.parse(feedObj)

                                        if(model.user_id.equals(prefManager?.getString(Constant.KEY_USER_ID))){
                                            //hint_feedback.setText()
                                            Log.e("ServiceRequest",bookingModel?.id+" "+model.bookingId)
                                            if(!TextUtils.isEmpty(model.bookingId) && bookingModel!!.id.equals(model.bookingId)){
                                                runOnUiThread {
                                                    frame_btn.visibility = View.GONE
                                                    rating_price.rating = model.price.toFloat()
                                                    rat_communication.rating = model.communication.toFloat()
                                                    rating_delivery.rating = model.delivery.toFloat()
                                                    rating_proficiency.rating = model.proficiency.toFloat()
                                                    et_des.setText(model!!.comment)

                                                    rating_delivery.setIsIndicator(true)
                                                    rating_price.setIsIndicator(true)
                                                    rat_communication.setIsIndicator(true)
                                                    rating_proficiency.setIsIndicator(true)
                                                    et_des.isFocusable = false
                                                    et_des.isFocusableInTouchMode = false
                                                    et_des.isEnabled = false
                                                }
                                                break
                                            }

                                        }

                                    }
                                }
                            }
                        }

                        if (array.length() <= 0) {
                            rating_price.rating = 1F
                            rat_communication.rating = 1F
                            rating_delivery.rating = 1F
                            rating_proficiency.rating = 1F
                        }
                    }
                } catch (e: java.lang.Exception) {
                    e.printStackTrace()
                }
            }

            override fun onError(errorMessage: String) {
                 frame_load_rat.visibility = View.GONE
                errorDialog?.show(getString(R.string.hint_error), getString(R.string.bd_hint_failed) + " $errorMessage")
            }

        }).getHotelFeedbackFeedback(param)
    }

}
