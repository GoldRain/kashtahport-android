package com.itwhiz4u.q8mercato.kashtahport.model

import android.os.Parcel
import android.os.Parcelable


class HotelPropertyModel() :Parcelable {

    var id = ""
    var ownerId = ""
    var name  = ""
    var description = "Here will be hotel description"
    var image = ""
    var latitude= "0"
    var longitude= "0"
    var rate= ""
    var dates= ""
    var person= ""
    var wc_number= ""
    var rooms= ""
    var country= ""
    var city= ""
    var type= ""
    var conditions= ""
    var children= ""
    var created_at= "0"
    var rating= "1"
    var phoneNumber= ""
    var fullAddress= ""
    var currency= "usd"

    var isEnable= false
    var isApprove= false
    var status= false
    var isFav= false
    var isDelete= false

    var availableDates = ArrayList<HotelDateModel>()
    var imageList = ArrayList<HotelImageModel>()
    var amenitiesList = ArrayList<AmenitiesModel>()

    constructor(parcel: Parcel) : this() {
        id = parcel.readString()
        ownerId = parcel.readString()
        name = parcel.readString()
        description = parcel.readString()
        image = parcel.readString()
        latitude = parcel.readString()
        longitude = parcel.readString()
        rate = parcel.readString()
        dates = parcel.readString()
        person = parcel.readString()
        wc_number = parcel.readString()
        rooms = parcel.readString()
        country = parcel.readString()
        city = parcel.readString()
        type = parcel.readString()
        conditions = parcel.readString()
        children = parcel.readString()
        created_at = parcel.readString()
        rating = parcel.readString()
        phoneNumber = parcel.readString()
        fullAddress = parcel.readString()
        currency = parcel.readString()

        isEnable = parcel.readByte() != 0.toByte()
        isApprove = parcel.readByte() != 0.toByte()
        status = parcel.readByte() != 0.toByte()
        isFav = parcel.readByte() != 0.toByte()
        isDelete = parcel.readByte() != 0.toByte()


        availableDates = parcel.createTypedArrayList(HotelDateModel.CREATOR)
        imageList = parcel.createTypedArrayList(HotelImageModel.CREATOR)
        amenitiesList = parcel.createTypedArrayList(AmenitiesModel.CREATOR)
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(id)
        parcel.writeString(ownerId)
        parcel.writeString(name)
        parcel.writeString(description)
        parcel.writeString(image)
        parcel.writeString(latitude)
        parcel.writeString(longitude)
        parcel.writeString(rate)
        parcel.writeString(dates)
        parcel.writeString(person)
        parcel.writeString(wc_number)
        parcel.writeString(rooms)
        parcel.writeString(country)
        parcel.writeString(city)
        parcel.writeString(type)
        parcel.writeString(conditions)
        parcel.writeString(children)
        parcel.writeString(created_at)
        parcel.writeString(rating)
        parcel.writeString(phoneNumber)
        parcel.writeString(fullAddress)
        parcel.writeString(currency)

        parcel.writeByte(if (isEnable) 1 else 0)
        parcel.writeByte(if (isApprove) 1 else 0)
        parcel.writeByte(if (status) 1 else 0)
        parcel.writeByte(if (isFav) 1 else 0)
        parcel.writeByte(if (isDelete) 1 else 0)

        parcel.writeTypedList(availableDates)
        parcel.writeTypedList(imageList)
        parcel.writeTypedList(amenitiesList)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<HotelPropertyModel> {
        override fun createFromParcel(parcel: Parcel): HotelPropertyModel {
            return HotelPropertyModel(parcel)
        }

        override fun newArray(size: Int): Array<HotelPropertyModel?> {
            return arrayOfNulls(size)
        }
    }


}