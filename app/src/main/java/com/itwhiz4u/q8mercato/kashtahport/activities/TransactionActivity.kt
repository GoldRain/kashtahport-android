package com.itwhiz4u.q8mercato.kashtahport.activities

import android.content.Intent
import android.graphics.Bitmap
import android.os.Bundle
import android.util.Log
import android.view.View
import android.view.animation.AnimationUtils
import com.itwhiz4u.q8mercato.kashtahport.R
import com.itwhiz4u.q8mercato.kashtahport.helper.Constant
import com.itwhiz4u.q8mercato.kashtahport.helper.ConstantFunction
import com.itwhiz4u.q8mercato.kashtahport.model.HotelBookingModel
import com.itwhiz4u.q8mercato.kashtahport.model.TransactionModel
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.RequestOptions
import com.bumptech.glide.request.target.Target
import kotlinx.android.synthetic.main.activity_transaction.*
import kotlinx.android.synthetic.main.view_header_toolbar.*

class TransactionActivity : BaseActivity() {

    var model:TransactionModel?= null
    var bookingModel:HotelBookingModel?= null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_transaction)
        supportActionBar?.hide()

        header_name.setText(getString(R.string.hint_trans_details))
       // header_name.setTextColor(ContextCompat.getColor(this,R.color.colorWhite))
        icon_back.visibility = View.VISIBLE
        icon_back.setOnClickListener {
            super.onBackPressed()
        }

        model = intent.getParcelableExtra(Constant.KEY_TRANSACTION)
        bookingModel = intent.getParcelableExtra(Constant.KEY_BOOKING)

        setData()

        frame_btn.setOnClickListener {
            val intent= Intent(this,ProvideFeedbackActivity::class.java)
            intent.putExtra(Constant.KEY_BOOKING,bookingModel)
            startActivity(intent)
        }

        val anim = AnimationUtils.loadAnimation(this,R.anim.scale_done)
        im_done.startAnimation(anim)
    }

    private fun setData() {

        checkForCheckOut()

        transaction_id.text = ""+model!!.txn_id
       // amount.text = "$"+model!!.amount
        ConstantFunction.setPrice(this,bookingModel!!.hotelModel!!.currency,model!!.amount,amount)

        hotel_name.setText(bookingModel!!.hotelModel!!.name)
        hotel_des.setText(bookingModel!!.hotelModel!!.description)

        hotel_city.setText(bookingModel!!.hotelModel!!.city+" , "+bookingModel?.hotelModel!!.country)

        Log.e("transaction" ," "+model!!.card_type)

        val type = ""+model!!.card_type.toLowerCase()

        if(type.contains("master")){
            im_card.setImageResource(R.drawable.card_ms)
        }else{
            if(type.contains("visa")){
                im_card.setImageResource(R.drawable.card_visa)
            }else{
                if(type.contains("american")){
                    im_card.setImageResource(R.drawable.card_ae)
                }
            }
        }
        try {
            time.text = ConstantFunction.getOnlyDate(model!!.created_at.toLong())
            rating_bar.rating = bookingModel!!.hotelModel!!.rating.toFloat()
        }catch (e:Exception){
            e.printStackTrace()
        }

        Glide.with(this)
                .asBitmap()
                .load(bookingModel!!.hotelModel!!.imageList[0].url)
                .apply(RequestOptions().override(200))
                .listener(object : RequestListener<Bitmap> {
                    override fun onLoadFailed(e: GlideException?, model: Any?, target: Target<Bitmap>?, isFirstResource: Boolean): Boolean {
                       frame_load_image.visibility = View.GONE
                        return false
                    }

                    override fun onResourceReady(resource: Bitmap?, model: Any?, target: Target<Bitmap>?, dataSource: DataSource?, isFirstResource: Boolean): Boolean {
                       frame_load_image.visibility = View.GONE
                        return false
                    }

                })
                .into(hotel_image)
    }

    private fun checkForCheckOut() {

        val endDate =bookingModel!!.dateTo.toLong()

        val currentDate = System.currentTimeMillis()

        if(endDate < currentDate){
            frame_btn.visibility = View.VISIBLE
        }else{
            frame_btn.visibility = View.GONE
        }
    }



}
