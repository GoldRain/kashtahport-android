package com.itwhiz4u.q8mercato.kashtahport.activities

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v4.view.ViewPager
import android.util.Log
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import com.itwhiz4u.q8mercato.kashtahport.Api.ApiResponseListener
import com.itwhiz4u.q8mercato.kashtahport.Api.ServiceRequest
import com.itwhiz4u.q8mercato.kashtahport.R
import com.itwhiz4u.q8mercato.kashtahport.adapter.HotelMapListAdapter
import com.itwhiz4u.q8mercato.kashtahport.customView.MapWrapperLayout
import com.itwhiz4u.q8mercato.kashtahport.database.AppDatabase
import com.itwhiz4u.q8mercato.kashtahport.database.FavPost
import com.itwhiz4u.q8mercato.kashtahport.helper.Constant
import com.itwhiz4u.q8mercato.kashtahport.helper.ConstantFunction
import com.itwhiz4u.q8mercato.kashtahport.helper.OnInfoWindowElemTouchListener
import com.itwhiz4u.q8mercato.kashtahport.listeners.InfoClickListener
import com.itwhiz4u.q8mercato.kashtahport.model.HotelModel
import com.itwhiz4u.q8mercato.kashtahport.parser.HotelParser
import com.google.android.gms.maps.*
import com.google.android.gms.maps.model.*
import kotlinx.android.synthetic.main.activity_map_new.*
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread
import org.json.JSONObject
import java.util.*
import kotlin.collections.HashMap
import kotlinx.android.synthetic.main.view_marker_info.view.*


class MapNewActivity : BaseActivity(), OnMapReadyCallback {

    val TAG = "MapNewActivity"
    lateinit var adapter: HotelMapListAdapter
    var hotelList = ArrayList<HotelModel>()

    var map: GoogleMap? = null

    val hashMapHotel = HashMap<String, HotelModel>()
    var hotelModel: HotelModel? = null

    lateinit var mapFrag:SupportMapFragment
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_map_new)
        supportActionBar?.hide()

        hotelModel = intent.getParcelableExtra(Constant.KEY_HOTEL)

        if (hotelModel != null) {
           // hotelList.add(hotelModel!!)
        }
        adapter = HotelMapListAdapter(this, hotelList, object : HotelMapListAdapter.OnItemClick() {
            override fun onItemClick(position: Int) {
               /* val intent =  Intent(this@MapNewActivity, HotelDetailsActivity::class.java)
                intent.putExtra(Constant.KEY_HOTEL,hotelList[position])
                startActivity(intent)*/

                val model = hotelList[position]
                val lat = LatLng(model.latitude.toDouble(), model.longitude.toDouble())
                setPosition(lat, model)
            }

            override fun onAddFav(position: Int) {

                if(hotelList[position].isFav){
                    removeToFav(position)
                }else{
                    addToFav(position)
                }
            }
        })

        view_pager.adapter = adapter


        /*  recycler_view.layoutManager = LinearLayoutManager(this,LinearLayoutManager.HORIZONTAL,false)
          recycler_view.addItemDecoration(EqualSpaceItemDecoration(16))
          recycler_view.adapter = adapter*/

//        adapter.notifyDataSetChanged()

        //map_view.onCreate(savedInstanceState)
       // map_view.onResume()

        try {
           // MapsInitializer.initialize(this?.applicationContext)


        } catch (e: Exception) {
            e.printStackTrace()
        }

     //   map_view!!.getMapAsync(this)

        mapFrag = getSupportFragmentManager().findFragmentById(R.id.mapFrag) as  SupportMapFragment;

        mapFrag.getMapAsync(this)


        view_pager.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrollStateChanged(p0: Int) {

            }

            override fun onPageScrolled(p0: Int, p1: Float, p2: Int) {

            }

            override fun onPageSelected(p0: Int) {
                /*val model = hotelList[p0]
                val lat = LatLng(model.latitude.toDouble(), model.longitude.toDouble())
                setPosition(lat, model)*/
            }

        })
    }


    private var infoWindow: ViewGroup? = null
    private var infoTitle: TextView? = null
    private var infoSnippet: TextView? = null
    private var infoButton1: Button? = null
    private var infoButton2:Button? = null
    private var infoButtonListener: OnInfoWindowElemTouchListener? = null


    private fun initMapWraper(googleMap: GoogleMap?, hotel: HotelModel?) {
        mapFrag = getSupportFragmentManager().findFragmentById(R.id.mapFrag) as  SupportMapFragment;

        val mapFragment = supportFragmentManager.findFragmentById(R.id.mapFrag) as SupportMapFragment?
        val mapWrapperLayout = findViewById<View>(R.id.mapWrapperLayout) as MapWrapperLayout
        mapWrapperLayout.init(googleMap, getPixelsFromDp(this, (39 + 20).toFloat()))

        this.infoWindow = layoutInflater.inflate(R.layout.view_marker_info, null) as ViewGroup


        infoButton1 = infoWindow!!.findViewById<Button>(R.id.book_now)
        infoWindow!!.hotel_name.text = hotel!!.name
        infoWindow!!.hotel_des.text = hotel!!.description

        ConstantFunction.setPrice(this@MapNewActivity,hotel!!.currency,hotel!!.rate,infoWindow!!.rent)
       // infoWindow!!.rent.text = "$"+hotel!!.rate
        infoWindow!!.rating_bar.rating = hotel.rating.toFloat()
        // Setting custom OnTouchListener which deals with the pressed state
        // so it shows up

        val clickListener= object :InfoClickListener{
            override fun onClick(view: View, marker: Marker) {
                Log.e(TAG,"onClickConfirmed ")
                //Toast.makeText(this@MapNewActivity, "click on button 1", Toast.LENGTH_SHORT).show()
            }

        }
        this.infoButtonListener = object : OnInfoWindowElemTouchListener(infoButton1, resources.getDrawable(R.drawable.back_corner_pri), resources.getDrawable(R.drawable.back_corner_pri)) {
            override fun onClickConfirmed(v: View?, marker: Marker?) {

                Log.e(TAG,"onClickConfirmed ")
              //  Toast.makeText(this@MapNewActivity, "click on button 1", Toast.LENGTH_SHORT).show()
                val intent =  Intent(this@MapNewActivity, HotelDetailsActivity::class.java)
                intent.putExtra(Constant.KEY_HOTEL,hotel)
                startActivity(intent)
            }
        }

        this.infoButton1!!.setOnTouchListener(infoButtonListener)


        googleMap!!.setInfoWindowAdapter(object : GoogleMap.InfoWindowAdapter {
            override fun getInfoWindow(marker: Marker): View? {
                infoButtonListener!!.setMarker(marker)

                // We must call this to set the current marker and infoWindow references
                // to the MapWrapperLayout
                mapWrapperLayout.setMarkerWithInfoWindow(marker, infoWindow)
                return infoWindow!!

            }

            override fun getInfoContents(marker: Marker): View? {
                // Setting up the infoWindow with current's marker info


                return null
            }
        })

        // Let's add a couple of markers
      /*  googleMap!!.addMarker(MarkerOptions()
                .position(latlng1)
                .title("Source")
                .snippet("Comapny Name")
                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN)))
        googleMap!!.animateCamera(CameraUpdateFactory.newLatLngZoom(latlng1, 10f))
*/
    }

    override fun onMapReady(googleMap: GoogleMap?) {

        map = googleMap
        //initMapWraper(googleMap)

        // For showing a move to my location button
       /* if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            map!!.setMyLocationEnabled(true)
        }
*/


        var sydney = LatLng(-34.0, 151.0)
        try {
            if (hotelModel != null) {
                sydney = LatLng(hotelModel!!.latitude.toDouble(), hotelModel!!.longitude.toDouble())
            }
        } catch (e: java.lang.Exception) {
            e.printStackTrace()
        }
        setPosition(sydney, hotelModel!!)

        getHotelList()

        map!!.setOnMarkerClickListener(object : GoogleMap.OnMarkerClickListener {
            override fun onMarkerClick(p0: Marker?): Boolean {
                // Log.e(TAG," onMarkerClick "+p0!!.snippet)
                val hotel = hashMapHotel.get(p0!!.snippet)
                initMapWraper(googleMap,hotel)
                p0!!.showInfoWindow()
            /*    if (hotel != null) {
                    val cusInfo = MapInfoWindow(this@MapNewActivity, hotel!!,map)
                    map!!.setInfoWindowAdapter(cusInfo)
                    p0!!.showInfoWindow()
                }
*/
                return false
            }

        })

    }

    fun getPixelsFromDp(context: Context, dp: Float): Int {
        val scale = context.resources.displayMetrics.density
        return (dp * scale + 0.5f).toInt()
    }

    private fun setPosition(sydney: LatLng, hotel: HotelModel) {


    //    val bitmap = getMarkerBitmapFromView(this)

        val markerOption = MarkerOptions().position(sydney).title("").snippet(hotel.id)
        val marker = map!!.addMarker(markerOption)

     //  marker.setIcon(BitmapDescriptorFactory.fromBitmap(bitmap))

   //     val cusInfo = MapInfoWindow(this, hotel!!,map)
      //  map!!.setInfoWindowAdapter(cusInfo)
        // For zooming automatically to the location of the marker
        val cameraPosition = CameraPosition.Builder().target(sydney).zoom(14f).build()
        map!!.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition))

       /* map!!.setOnInfoWindowClickListener(object : OnInfoWindowClickListener {
            override fun onInfoWindowClick(p0: Marker?) {
                val intent =  Intent(this@MapNewActivity, HotelDetailsActivity::class.java)
                intent.putExtra(Constant.KEY_HOTEL,hotel)
                startActivity(intent)

            }
        })*/
    }

    override fun onResume() {
        super.onResume()
       // map_view.onResume()
    }

    override fun onPause() {
        super.onPause()
       // map_view.onPause()
    }

    override fun onDestroy() {
        super.onDestroy()
      //  map_view.onDestroy()
    }

    override fun onLowMemory() {
        super.onLowMemory()
       // map_view.onLowMemory()
    }


    fun getHotelList() {
        val param = JSONObject()
        param.put("api_key", Constant.API_KEY)
        param.put("latitude", hotelModel!!.latitude)
        param.put("longitude", hotelModel!!.longitude)

        //progress?.show()
        hotelList.clear()
       showProgress(frame_loader)
        ServiceRequest(object : ApiResponseListener {
            override fun onCompleted(`object`: Any) {
                //progress?.dismiss()
                hideProgress(frame_loader)

                try {
                    val json = JSONObject(`object`.toString())
                    Log.e(TAG, " ss " + hotelList.size)
                    if (json.has("data")) {
                        val array = json.getJSONArray("data")
                        val parser = HotelParser()

                   //     adapter.notifyDataSetChanged()

                        var cp =0
                        for (i in 0 until array.length()) {
                            val hotel = parser.parse(array.getJSONObject(i))
                            hotelList.add(hotel)
                            hashMapHotel.put(hotel.id, hotel)
                            val lat = LatLng(hotel.latitude.toDouble(), hotel.longitude.toDouble())
                            setPosition(lat, hotel)
                            Log.e(TAG, " $i ** OBJ ${hotel.id} ** ${hotelModel?.id}")
                            if(hotel.id.equals(hotelModel?.id)){
                                cp = i
                            }
                        }

                      if(hotelList.size >1){
                          total_result.text = "${hotelList.size} "+getString(R.string.ma_new_hint_result)
                      }else{
                          total_result.text = "${hotelList.size} "+getString(R.string.ma_new_hint_results)
                      }
                        Collections.reverse(hotelList)

                        Log.e(TAG, " ss " + hotelList.size)
                        adapter.notifyDataSetChanged()
                        view_pager.setCurrentItem((hotelList.size-1)-cp)

                        showEmptyView(frame_empty,hotelList.size ==0 )
                    }
                } catch (e: java.lang.Exception) {
                    Log.e(TAG, " ss " + e.message)
                    e.printStackTrace()
                }
            }

            override fun onError(errorMessage: String) {
                hideProgress(frame_loader)
                showEmptyView(frame_empty,hotelList.size ==0 )
            }

        }).searchNearByHotel(param)


    }

    private fun addToFav(position: Int) {
        val hotel = hotelList[position]
        val param = JSONObject()
        param.put("api_key",Constant.API_KEY)
        param.put("hotel_id",hotel?.id)
        param.put("user_id",prefManager.getString(Constant.KEY_USER_ID))

        progress?.show()

        ServiceRequest(object : ApiResponseListener {
            override fun onCompleted(`object`: Any) {
                progress?.dismiss()

                doAsync {
                    hotel.isFav = true
                    val fav = FavPost()
                    fav.id = hotel?.id!!
                    fav.post_id = hotel?.id!!
                    AppDatabase.getAppDatabase().favPostDao().insert(fav)

                    uiThread {
                        adapter.notifyDataSetChanged()
                    }
                }
            }

            override fun onError(errorMessage: String) {
                progress?.dismiss()
            }

        }).addToFav(param)
    }

    private fun removeToFav(position: Int) {

        val hotel = hotelList[position]
        val param = JSONObject()
        param.put("api_key",Constant.API_KEY)
        param.put("hotel_id",hotel?.id)
        param.put("user_id",prefManager.getString(Constant.KEY_USER_ID))

        progress?.show()

        ServiceRequest(object : ApiResponseListener {
            override fun onCompleted(`object`: Any) {
                progress?.dismiss()


                doAsync {
                    hotel.isFav = false
                    val fav = FavPost()
                    fav.id = hotel?.id!!
                    fav.post_id = hotel?.id!!
                    AppDatabase.getAppDatabase().favPostDao().deleteFavPost(fav.id)

                    uiThread {
                        adapter.notifyDataSetChanged()
                    }
                }
            }

            override fun onError(errorMessage: String) {
                progress?.dismiss()
            }

        }).removeToFav(param)
    }

    /*fun getMarkerBitmapFromView(context: Context): Bitmap {

        val customMarkerView = (context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater).inflate(R.layout.view_custom_marker_info, null)
        // TextView address = customMarkerView.findViewById(R.id.address);
        //  address.setText(addressText);
        customMarkerView.book_now.setOnClickListener {
            Log.e(TAG,"customMarkerView  fkgjfgf")
        }
        customMarkerView.card_details.visibility = View.VISIBLE
        customMarkerView.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED)
        customMarkerView.layout(0, 0, customMarkerView.measuredWidth, customMarkerView.measuredHeight)
        customMarkerView.buildDrawingCache()
        val returnedBitmap = Bitmap.createBitmap(customMarkerView.measuredWidth, customMarkerView.measuredHeight,
                Bitmap.Config.ARGB_8888)
        val canvas = Canvas(returnedBitmap)
        canvas.drawColor(Color.WHITE, PorterDuff.Mode.SRC_IN)
        val drawable = customMarkerView.background
        drawable?.draw(canvas)
        customMarkerView.draw(canvas)


        return returnedBitmap
    }*/
}
