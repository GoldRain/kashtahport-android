package com.itwhiz4u.q8mercato.kashtahport.sort

import com.itwhiz4u.q8mercato.kashtahport.model.HotelBookingModel

class BookingSort:Comparator<HotelBookingModel> {
    override fun compare(o1: HotelBookingModel?, o2: HotelBookingModel?): Int {
        return if(o1!!.created_at.toLong() > o2!!.created_at.toLong()) 1 else 0
    }
}