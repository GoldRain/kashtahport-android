package com.itwhiz4u.q8mercato.kashtahport.activities

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Color
import android.location.Geocoder
import android.os.AsyncTask
import android.os.Bundle
import android.os.Handler
import android.os.SystemClock
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.text.TextUtils
import android.util.Log
import android.view.View
import android.view.animation.LinearInterpolator
import android.widget.Toast
import com.itwhiz4u.q8mercato.kashtahport.R
import com.itwhiz4u.q8mercato.kashtahport.helper.Constant
import com.itwhiz4u.q8mercato.kashtahport.helper.DaysCount
import com.itwhiz4u.q8mercato.kashtahport.helper.GpsTracker
import com.itwhiz4u.q8mercato.kashtahport.model.MarkerModel
import com.itwhiz4u.q8mercato.kashtahport.model.PlaceModel
import com.google.android.gms.common.api.Status
import com.google.android.gms.location.*
import com.google.android.gms.maps.*
import com.google.android.gms.maps.model.*
import com.google.android.libraries.places.api.Places
import com.google.android.libraries.places.api.model.Place
import com.google.android.libraries.places.widget.AutocompleteSupportFragment
import java.util.*
import com.google.android.libraries.places.widget.listener.PlaceSelectionListener
import kotlinx.android.synthetic.main.activity_map_location.*
import kotlinx.android.synthetic.main.view_marker_place_info.*
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import kotlin.collections.ArrayList


class MapLocationActivity : BaseActivity(), OnMapReadyCallback {

    var map: GoogleMap? = null


    var placeName = ""
    var placeAddress = ""
    var gpsTracker: GpsTracker? = null

    var selectLatlng: LatLng? = null
    var placeModel: PlaceModel? = null

    // lateinit var mapFrag:SupportMapFragment
    lateinit var mapView: MapView

    private var locationRequest:LocationRequest?= null
    private  var locationCallback: LocationCallback?= null
    private var fusedLocationClient:FusedLocationProviderClient?= null

    override fun onResume() {
        super.onResume()
        mapView.onResume()
    }

    override fun onPause() {
        super.onPause()
        mapView.onPause()
    }

    override fun onDestroy() {
        super.onDestroy()
        mapView.onDestroy()
    }

    override fun onLowMemory() {
        super.onLowMemory()
        mapView.onLowMemory()
    }

    var showCurrentPalce = false
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_map_location)
        supportActionBar?.hide()


        difineLocationLocation()

        placeModel = intent.getParcelableExtra(Constant.KEY_PLACE)

        if(placeModel == null){
            placeModel = PlaceModel()
            showCurrentPalce = true
        }

        gpsTracker = GpsTracker(this)
        gpsTracker?.currentLocation

        if (!Places.isInitialized()) {
            Places.initialize(getApplicationContext(), getString(R.string.googleMap_apiKey));
        }

        // mapFrag = supportFragmentManager.findFragmentById(R.id.map) as SupportMapFragment
        //  mapView = mapFrag.view
//   mapFrag.getMapAsync(this)

        mapView = findViewById(R.id.map_view)
        mapView.onCreate(savedInstanceState)
        mapView.onResume()
        mapView.getMapAsync(this)

        val autocompleteFragment =
                getSupportFragmentManager().findFragmentById(R.id.autocomplete_fragment) as AutocompleteSupportFragment
        autocompleteFragment.setPlaceFields(Arrays.asList(Place.Field.ID, Place.Field.NAME, Place.Field.ADDRESS, Place.Field.LAT_LNG));

        autocompleteFragment.setOnPlaceSelectedListener(object : PlaceSelectionListener {
            override fun onPlaceSelected(place: Place) {

                Log.e(TAG, "Place: " + place.name + ", " + place.address + " , " + place.latLng)
                placeName = place.name!!
                placeAddress = place.address!!

                pointToPositon(place.latLng!!)
            }

            override fun onError(status: Status) {
                Toast.makeText(this@MapLocationActivity, getString(R.string.m_loc_hint_invalid_place), Toast.LENGTH_SHORT).show()
                Log.e("autocompleteFragment", "An error occurred: $status")
            }
        })


        im_close_address.setOnClickListener {
            ll_info.visibility = View.GONE
        }

        ll_select_location.setOnClickListener {
            val intent = Intent()
            intent.putExtra(Constant.KEY_ADDRESS, placeModel)
            setResult(Activity.RESULT_OK, intent)
            finish()
        }

        card_point.setOnClickListener { view ->
            val location = gpsTracker?.currentLocation
            location?.let {
                val latLng = LatLng(it.latitude, it.longitude)
                pointToPositon(latLng)
            }

        }

        getUserLocation()
    }

    private fun getUserLocation() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return
        }

        // Create the location request to start receiving updates
        locationRequest = LocationRequest.create()
        locationRequest!!.interval = 20000
        locationRequest?.fastestInterval= 1000
        locationRequest?.priority = LocationRequest.PRIORITY_HIGH_ACCURACY

        // Create LocationSettingsRequest object using location request
        val builder = LocationSettingsRequest.Builder()
        builder.addLocationRequest(locationRequest!!)
        val locationSettRequest = builder.build()

        val settingsClient = LocationServices.getSettingsClient(this)

      /*
        val task = settingsClient.checkLocationSettings(locationSettRequest)

        task.addOnCompleteListener {

            try {
                fusedLocationClient = LocationServices.getFusedLocationProviderClient(this)
            }catch (exception:ApiException){
                when(exception.statusCode){
                    LocationSettingsStatusCodes.RESOLUTION_REQUIRED -> {
                        // Location settings are not satisfied. But could be fixed by showing the
                        // user a dialog.
                        try {
                            // Cast to a resolvable exception.
                            val resolvable = exception as ResolvableApiException
                            // Show the dialog by calling startResolutionForResult(),
                            // and check the result in onActivityResult().
                            resolvable.startResolutionForResult(this!!, 1001
                            )
                        } catch (e: IntentSender.SendIntentException) {
                            // Ignore the error.
                        } catch (e: ClassCastException) {
                            // Ignore, should be an impossible error.
                        }
                    }
                    LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE -> {
                        // Location settings are not satisfied. However, we have no way to fix the
                        // settings so we won't show the dialog.
                    }
                }
            }
        }*/
       /* val lastLocation = fusedLocationClient?.lastLocation
        lastLocation?.addOnSuccessListener {loc->
            loc?.let {
                val latLngs = LatLng(it.latitude,it.longitude);
                pointToPositon(latLngs)

                if(!showCurrentPalce){
                    val placeLat = LatLng(placeModel!!.lat,placeModel!!.lon)
                    pointToPositon(placeLat)
                }else{
                    pointToPositon(latLngs)
                }
            }

        }*/
    }


    val TAG = "LocationActivity"
    override fun onMapReady(googleMap: GoogleMap?) {


        Log.e(TAG, " onMapReady $googleMap")
        map = googleMap
        // For showing a move to my location button
        if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            /* map!!.setMyLocationEnabled(true)
             val  locationButton =  ((mapView.findViewById<View>(Integer.parseInt("1")).getParent() as View).findViewById<View>(Integer.parseInt("2")));
             val  rlp = locationButton . getLayoutParams () as  (RelativeLayout.LayoutParams);

             rlp.addRule(RelativeLayout.ALIGN_PARENT_TOP, RelativeLayout.TRUE);
             rlp.setMargins(0, 60, 60, 0);
             locationButton.layoutParams = rlp*/
        }


        var sydney = LatLng(-34.0, 151.0)
        val location = gpsTracker?.currentLocation

        location?.let {
            sydney = LatLng(it.latitude, it.longitude)
        }

        Log.e(TAG," onMapReady ** "+placeModel +" "+(placeModel?.lat != 0.0 && placeModel?.lon != 0.0))

      /*  if(!showCurrentPalce){
            val placeLat = LatLng(placeModel!!.lat,placeModel!!.lon)
            pointToPositon(placeLat)
        }else{
            pointToPositon(sydney)
        }*/

        pointToPositon(sydney)
        addmapListeners()
    }


    private fun addmapListeners() {

        map?.setOnCameraMoveStartedListener(object : GoogleMap.OnCameraMoveStartedListener {
            override fun onCameraMoveStarted(p0: Int) {
                ll_info.visibility = View.GONE
            }

        })

        map?.setOnCameraIdleListener(object : GoogleMap.OnCameraIdleListener {
            override fun onCameraIdle() {
                val latLng = map?.cameraPosition?.target
                setPlace(latLng!!)
            }

        })

        map?.setOnCameraMoveCanceledListener(object : GoogleMap.OnCameraMoveCanceledListener {
            override fun onCameraMoveCanceled() {

            }
        })

        map?.setOnMapClickListener(object : GoogleMap.OnMapClickListener {
            override fun onMapClick(p0: LatLng?) {
                //  val latLng = map?.cameraPosition?.target
                  pointToPositon(p0!!)

             /*   if (marker == null) {

                    origin = p0!!
                    marker = map?.addMarker(MarkerOptions().position(p0!!).flat(true).snippet("").title("").icon(BitmapDescriptorFactory.fromResource(R.drawable.power_station)))
                } else {
                    dest = p0
                    //  animateMarker(marker!!,p0!!,false)

                    val url = getDirectionsUrl(origin!!, dest!!)


                    //  val down = DownloadTask()
                    //down.execute(url)

                    ServiceRequest(object : ApiResponseListener {
                        override fun onError(errorMessage: String) {

                        }

                        override fun onCompleted(`object`: Any) {
                            val parserTask = ParserTask()

                            // Invokes the thread for parsing the JSON data
                            parserTask.execute(`object`.toString())
                            Log.e("onCompleted", "  ** " + `object`.toString())
                        }

                    }).fetchRoutes(url)
                }*/
            }

        })

    }

    var origin: LatLng? = null
    var dest: LatLng? = null
    var marker: Marker? = null

    fun animateMarker(marker: Marker, toPosition: LatLng, hideMarker: Boolean) {
        val handler = Handler()
        val start = SystemClock.uptimeMillis()
        val proj = map?.getProjection()
        val startPoint = proj!!.toScreenLocation(marker.position)
        val startLatLng = proj!!.fromScreenLocation(startPoint)
        val duration: Long = 1000

        val interpolator = LinearInterpolator()

        handler.post(object : Runnable {
            override fun run() {
                val elapsed = SystemClock.uptimeMillis() - start
                val t = interpolator.getInterpolation(elapsed.toFloat() / duration)
                val lng = t * toPosition.longitude + (1 - t) * startLatLng.longitude
                val lat = t * toPosition.latitude + (1 - t) * startLatLng.latitude
                marker.setPosition(LatLng(lat, lng))

                if (t < 1.0) {
                    // Post again 16ms later.

                    handler.postDelayed(this, 16)
                } else {
                    if (endLat != null) {

                        val cameraPosition = CameraPosition.Builder().target(toPosition).zoom(18f).build()
                        map?.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition))

                    }

                    moveMarker(0)

                    marker.isVisible = !hideMarker
                }
            }
        })
    }

    private fun pointToPositon(sydney: LatLng) {
        Log.e(TAG, "onMapReady $map")
        map?.clear()

        val cameraPosition = CameraPosition.Builder().target(sydney).zoom(16f).build()
        map?.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition))

        setPlace(sydney)
    }

    private fun setPlace(latLng: LatLng) {

        doAsync {
            val gcd = Geocoder(this@MapLocationActivity, Locale.getDefault());
            val addresses = gcd.getFromLocation(latLng.latitude, latLng.longitude, 1);
            if (addresses.size > 0) {
                val ad = addresses[0]
                val country = ad.countryName
                val city = ad.locality

                placeAddress = ad.getAddressLine(0)
                placeName = ad.featureName

                placeModel!!.name = placeName
                placeModel!!.address = placeAddress
                placeModel?.lat = latLng.latitude
                placeModel?.lon = latLng.longitude

                placeModel?.country = country

                if (!TextUtils.isEmpty(city)) {
                    placeModel?.city = city
                } else {
                    placeModel?.city = ""
                }
                uiThread {
                    place_address.text = placeAddress
                    place_name.text = getString(R.string.m_loc_hint_address)
                    ll_info.visibility = View.VISIBLE
                }
            }

            selectLatlng = latLng
        }


        /* val url = ApiUrl.GET_ADDRESS_FROM_MAP+"latlng=${latLng.latitude},${latLng.longitude}&key=${getString(R.string.googleMap_apiKey)}"
         ServiceRequest(object :ApiResponseListener{
             override fun onCompleted(`object`: Any) {

             }

             override fun onError(errorMessage: String) {

             }

         })*/
    }


    private fun difineLocationLocation() {
        locationCallback = object : LocationCallback() {
            override fun onLocationResult(p0: LocationResult?) {

            }
        }
    }

    private fun getDirectionsUrl(origin: LatLng, dest: LatLng): String {

        // Origin of route
        val str_origin = "origin=" + origin.latitude + "," + origin.longitude

        // Destination of route
        val str_dest = "destination=" + dest.latitude + "," + dest.longitude

        // Sensor enabled
        val sensor = "sensor=false"

        // Building the parameters to the web service
        val parameters = "$str_origin&$str_dest&$sensor"

        // Output format
        val output = "json"

        // Building the url to the web service

        return "https://maps.googleapis.com/maps/api/directions/$output?$parameters&key=" + getString(R.string.googleMap_apiKey)

        //"https://maps.googleapis.com/maps/api/directions/json?str_origin&$str_dest&sensor=false&key=" + getString(R.string.googleMap_apiKey)
    }

    inner class ParserTask : AsyncTask<String, Int, List<List<HashMap<String, String>>>>() {

        // Parsing the data in non-ui thread
        override fun doInBackground(vararg jsonData: String): List<List<HashMap<String, String>>>? {

            val jObject: JSONObject
            var routes: List<List<HashMap<String, String>>>? = null

            try {
                jObject = JSONObject(jsonData[0])
                val parser = DirectionsJSONParser()

                // Starts parsing data
                routes = parser.parse(jObject)
            } catch (e: Exception) {
                e.printStackTrace()
            }

            return routes
        }


        // Executes in UI thread, after the parsing process
        override fun onPostExecute(result: List<List<HashMap<String, String>>>) {
            var points: ArrayList<LatLng>? = null
            var lineOptions: PolylineOptions? = null
            val markerOptions = MarkerOptions()

            // Traversing through all the routes
            for (i in result.indices) {
                points = ArrayList()
                lineOptions = PolylineOptions()

                // Fetching i-th route
                val path = result[i]

                listRoutes.clear()
                // Fetching all the points in i-th route
                for (j in path.indices) {
                    val point = path[j]

                    val lat = java.lang.Double.parseDouble(point["lat"]!!)
                    val lng = java.lang.Double.parseDouble(point["lng"]!!)
                    val position = LatLng(lat, lng)

                    listRoutes.add(position)
                    points.add(position)
                }

                // Adding all the points in the route to LineOptions
                lineOptions.addAll(points)
                lineOptions.width(10f)
                lineOptions.color(Color.RED)
            }

            Log.e("addPolyline",""+" **  "+listStatLat.size)

            map?.addPolyline(lineOptions);

            moveMarker(0)
        }
    }

    var listRoutes = ArrayList<LatLng>()
    var startLat: LatLng? = null
    var endLat: LatLng? = null
    var listStatLat = ArrayList<MarkerModel>()
    val dayCount = DaysCount()

    private fun moveMarker(i: Int) {
        if (listRoutes.size == 0) {

            return
        }

        val place = listRoutes[0]

        if (startLat == null) {
            startLat = place
            endLat = place

            rotateMarker(marker!!,bearingBetweenLocations(place!!,listRoutes[listRoutes.size-1]).toFloat())
        } else {
            if (endLat != null) {
                startLat = endLat
            }
            endLat = place

            rotateMarker(marker!!,bearingBetweenLocations(startLat!!,endLat!!).toFloat())
        }


        listRoutes.removeAt(0)


        marker!!.position = place
        /*Handler().postDelayed(object :Runnable{
            override fun run() {
                moveMarker(0)
            }

        },1000)*/
        animateMarker(marker!!, place, false)

    }


    private fun bearingBetweenLocations(latLng1: LatLng, latLng2: LatLng): Double {

        val PI = 3.14159;
        val lat1 = latLng1.latitude * PI / 180;
        val long1 = latLng1.longitude * PI / 180;
        val lat2 = latLng2.latitude * PI / 180;
        val long2 = latLng2.longitude * PI / 180;

        val dLon = (long2 - long1);

        val y = Math.sin(dLon) * Math.cos(lat2);
        val x = Math.cos(lat1) * Math.sin(lat2) - Math.sin(lat1) * Math.cos(lat2) * Math.cos(dLon);

        var brng = Math.atan2(y, x);

        brng = Math.toDegrees(brng);
        brng = (brng + 360) % 360;

        return brng;
    }


    var isMarkerRotating = false
    private fun rotateMarker(marker: Marker, toRotation: Float) {
        if (!isMarkerRotating) {
            val handler = Handler();
            val start = SystemClock.uptimeMillis();
            val startRotation = marker.getRotation();
            val duration = 1000;

            val interpolator = LinearInterpolator();

            handler.post(object : Runnable {
                override fun run() {

                    isMarkerRotating = true;

                    val elapsed = SystemClock.uptimeMillis() - start;
                    val t = interpolator.getInterpolation((elapsed / duration).toFloat());

                    val rot = t * toRotation + (1 - t) * startRotation;

                    val bearing = if (rot > 180) rot / 2 else rot

                    marker.setRotation(bearing);

                    if (t < 1.0) {
                        // Post again 16ms later.
                        handler.postDelayed(this, 16);
                    } else {

                        isMarkerRotating = false;
                    }
                }
            });
        }
    }


    inner class DirectionsJSONParser {

        fun parse(jObject: JSONObject): List<List<HashMap<String, String>>> {

            val routes = java.util.ArrayList<List<HashMap<String, String>>>()
            var jRoutes: JSONArray? = null
            var jLegs: JSONArray? = null
            var jSteps: JSONArray? = null

            try {

                jRoutes = jObject.getJSONArray("routes")

                /** Traversing all routes  */
                for (i in 0 until jRoutes!!.length()) {
                    jLegs = (jRoutes.get(i) as JSONObject).getJSONArray("legs")
                    val path = java.util.ArrayList<HashMap<String, String>>()

                    /** Traversing all legs  */
                    for (j in 0 until jLegs!!.length()) {
                        jSteps = (jLegs.get(j) as JSONObject).getJSONArray("steps")

                        /** Traversing all steps  */
                        for (k in 0 until jSteps!!.length()) {
                            var polyline = ""
                            polyline = ((jSteps.get(k) as JSONObject).get("polyline") as JSONObject).get("points") as String
                            val list = decodePoly(polyline)

                            /** Traversing all points  */
                            for (l in list.indices) {
                                val hm = HashMap<String, String>()
                                hm["lat"] = java.lang.Double.toString(list[l].latitude)
                                hm["lng"] = java.lang.Double.toString(list[l].longitude)
                                path.add(hm)
                            }


                            val startObj = (jSteps.get(k) as JSONObject).get("start_location") as JSONObject
                            val endObj = (jSteps.get(k) as JSONObject).get("end_location") as JSONObject

                            val mMarkerModel = MarkerModel()

                            val sLat = startObj.get("lat").toString().toDouble()
                            val sLon = startObj.get("lng").toString().toDouble()
                            val eLat = endObj.get("lat").toString().toDouble()
                            val eLon = endObj.get("lng").toString().toDouble()

                            val sLatlng = LatLng(sLat, sLon)
                            val eLatlng = LatLng(eLat, eLon)

                            mMarkerModel.startLat = sLatlng
                            mMarkerModel.endLat = eLatlng

                            listStatLat.add(mMarkerModel)

                        }
                        routes.add(path)
                    }
                }

            } catch (e: JSONException) {
                e.printStackTrace()
            } catch (e: Exception) {
            }

            return routes
        }

        /**
         * Method to decode polyline points
         * Courtesy : http://jeffreysambells.com/2010/05/27/decoding-polylines-from-google-maps-direction-api-with-java
         */
        private fun decodePoly(encoded: String): List<LatLng> {

            val poly = java.util.ArrayList<LatLng>()
            var index = 0
            val len = encoded.length
            var lat = 0
            var lng = 0

            while (index < len) {
                var b: Int
                var shift = 0
                var result = 0
                do {
                    b = encoded[index++].toInt() - 63
                    result = result or (b and 0x1f shl shift)
                    shift += 5
                } while (b >= 0x20)
                val dlat = if (result and 1 != 0) (result shr 1).inv() else result shr 1
                lat += dlat

                shift = 0
                result = 0
                do {
                    b = encoded[index++].toInt() - 63
                    result = result or (b and 0x1f shl shift)
                    shift += 5
                } while (b >= 0x20)
                val dlng = if (result and 1 != 0) (result shr 1).inv() else result shr 1
                lng += dlng

                val p = LatLng(lat.toDouble() / 1E5,
                        lng.toDouble() / 1E5)
                poly.add(p)
            }

            return poly
        }

    }
}
