package com.itwhiz4u.q8mercato.kashtahport.receiver

import android.content.Context
import android.content.Intent
import android.text.TextUtils
import android.util.Log
import com.itwhiz4u.q8mercato.kashtahport.Notification.NotificationUtils
import com.itwhiz4u.q8mercato.kashtahport.R
import com.onesignal.GcmBroadcastReceiver
import org.json.JSONObject


/**
 * Created by bodacious on 12/10/18.
 */
public class OneSignalReceiver: GcmBroadcastReceiver(){


    lateinit var context: Context
    override fun onReceive(context: Context?, intent: Intent?) {
        this.context = context!!
        val bundle = intent!!.extras

        if(bundle != null){
            var str =  bundle.getString("custom")
            Log.e("OneSignalReceiver","$str ****")

            if(TextUtils.isEmpty(str)){
                str = "You have some new message!!"
            }
            handleNotification(context,intent,str)
        }else{
            var msg = "You have some new notification"
            NotificationUtils.showNotification(context,context.getString(R.string.app_name),msg,"","",null)
        }

    }


    private fun handleNotification(context: Context, intent: Intent, jsonStr: String?) {

        val data1 = JSONObject(jsonStr)
        var data: JSONObject? = data1
        if (data1.has("a")) {
            data = data1.getJSONObject("a")
        }
        var msg = "You have some new notification"
        var type = ""

        if(data!!.has("type")){
            type = data!!.getString("type")
        }
        if(data!!.has("message")){
           val obj = data.getJSONObject("message")
            msg = obj.getString("message")

            if(obj.has("type")){
                type = obj.getString("type")
            }
        }

        NotificationUtils.showNotification(context,context.getString(R.string.app_name),msg,"",type,null)
    }


}