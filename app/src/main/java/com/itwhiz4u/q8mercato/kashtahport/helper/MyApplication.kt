package com.itwhiz4u.q8mercato.kashtahport.helper

import android.app.ActivityManager
import android.app.Service
import android.content.Context
import android.net.ConnectivityManager
import android.os.StrictMode
import android.support.multidex.MultiDex
import android.util.Log
import android.webkit.WebView
import com.androidnetworking.AndroidNetworking
import com.itwhiz4u.q8mercato.kashtahport.Notification.ExampleNotificationOpenedHandler
import com.itwhiz4u.q8mercato.kashtahport.Notification.ExampleNotificationReceivedHandler
import com.crashlytics.android.Crashlytics
import com.onesignal.OneSignal
import io.fabric.sdk.android.Fabric
import okhttp3.OkHttpClient
import java.util.concurrent.TimeUnit

/**
 * Created by bodacious-dev-8 on 26/6/17.
 */

class MyApplication : AbstractAppPauseApplication() {
    private var pref: MyPreferenceManager? = null

    val prefManager: MyPreferenceManager
        get() {
            if (pref == null) {
                pref = MyPreferenceManager(this)
            }
            return pref!!
        }

    override fun attachBaseContext(base: Context) {
        super.attachBaseContext(base)
        MultiDex.install(this)
    }

    override fun onCreate() {
        super.onCreate()

        Log.e("MY_APPLICATION", "onCreate")

        instance = this

        val okHttpClient = OkHttpClient().newBuilder()
                .connectTimeout(60, TimeUnit.SECONDS)
                .readTimeout(120, TimeUnit.SECONDS)
                .writeTimeout(120, TimeUnit.SECONDS)
                .build()
        AndroidNetworking.initialize(applicationContext, okHttpClient)


        OneSignal.startInit(instance)
                .autoPromptLocation(false)
                .setNotificationReceivedHandler(ExampleNotificationReceivedHandler())
                .setNotificationOpenedHandler(ExampleNotificationOpenedHandler())
                .inFocusDisplaying(OneSignal.OSInFocusDisplayOption.Notification)
                .unsubscribeWhenNotificationsAreDisabled(true)
                .filterOtherGCMReceivers(true)
                .init()



        OneSignal.setEmail("abc@gmail.com")

        com.onesignal.OneSignal.idsAvailable { userId, registrationId ->
            prefManager.setString(Constant.KEY_USER_NOTIFICATION_TOKEN,userId)

            NOTI_TOKEN = userId
            Log.e("OneSignal", "userId=$userId registrationId=$registrationId  ${OneSignal.VERSION}  ${OneSignal.sdkType}")
        }

        val builder = StrictMode.VmPolicy.Builder()
        StrictMode.setVmPolicy(builder.build())

        Fabric.with(this, Crashlytics())

        WebView(this).destroy();

        //shuffles();
    }


    //7c60953c-91ed-4c0d-b7d9-a1d8520932bd //6306e09b-9f8a-469c-8a22-fe9466ae8a26
    var isPause = true

    override fun onAppPause() {
        Log.e("MY_APPLICATION", "onAppPause")//{"message":"Nk Tiwari added Nav","type":"alert"}****
        isPause = true

    }


    fun setOnline(flag: Boolean, isRegister: Boolean = false) {


    }

    override fun onAppResume() {

        isPause = false
    }

    companion object {

        var NOTI_TOKEN = ""
        @get:Synchronized
        lateinit var instance: MyApplication
            private set
        private val TAG = MyApplication::class.java.simpleName

        fun isNetworkAvailable(context: Context): Boolean {
            try {
                val connectivityManager = context.getSystemService(Context.CONNECTIVITY_SERVICE)

                return connectivityManager != null && (connectivityManager as ConnectivityManager).activeNetworkInfo != null && (connectivityManager as ConnectivityManager).activeNetworkInfo.isConnected
            } catch (e: Exception) {
                e.printStackTrace()
                return false
            }
        }


    }

    fun isNetworkAvailable(): Boolean {
        val cm = getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val activeNetwork = cm.activeNetworkInfo
        return activeNetwork != null && activeNetwork.isConnectedOrConnecting
    }


    fun isMyServiceRunning(serviceClass: Service): Boolean {
        val manager = getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager

        Log.e("MyServiceRunning"," ${serviceClass.javaClass.simpleName}")
        manager.getRunningServices(Integer.MAX_VALUE).forEach {
            Log.e("MyServiceRunning","***  ${it.javaClass.simpleName}")
            if (serviceClass.javaClass.simpleName.equals(it.javaClass.simpleName)) {

                return true;
            }
        }

        return false;
    }

}
