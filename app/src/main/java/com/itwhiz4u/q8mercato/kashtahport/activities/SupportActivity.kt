package com.itwhiz4u.q8mercato.kashtahport.activities

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.text.TextUtils
import android.view.View
import com.itwhiz4u.q8mercato.kashtahport.R
import kotlinx.android.synthetic.main.activity_support.*
import kotlinx.android.synthetic.main.view_header_toolbar.*

class SupportActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_support)
        supportActionBar?.hide()

        header_name.setText(getString(R.string.hint_contact_us))

        icon_back.visibility = View.VISIBLE
        icon_back.setOnClickListener {
            super.onBackPressed()
        }

        ll_btn.setOnClickListener {
            if(validation()){
                sendMail()
            }

        }
    }

    private fun validation(): Boolean {
        if(TextUtils.isEmpty(et_des.text.toString().trim())){

            errorDialog?.show(getString(R.string.hint_error),getString(R.string.hint_describe_problem))
            return false
        }

        return  true
    }

    private fun sendMail() {
        try {
            val intent = Intent(Intent.ACTION_SENDTO, Uri.parse("mailto:${resources.getString(R.string.support_email)}"))
            intent.putExtra(Intent.EXTRA_EMAIL, resources.getString(R.string.support_email))
            intent.putExtra(Intent.EXTRA_SUBJECT, "Report Problem")
            intent.putExtra(Intent.EXTRA_TEXT, et_des.text.toString())
            startActivity(Intent.createChooser(intent, "Select"))
        } catch (e: Exception) {
            e.printStackTrace()
            errorDialog?.show(getString(R.string.hint_error),getString(R.string.hint_no_apps_installed))
        }
    }
}
