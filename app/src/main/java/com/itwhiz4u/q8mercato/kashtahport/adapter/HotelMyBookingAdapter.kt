package com.itwhiz4u.q8mercato.kashtahport.adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.itwhiz4u.q8mercato.kashtahport.Controller.MyBookingListController
import com.itwhiz4u.q8mercato.kashtahport.R
import com.itwhiz4u.q8mercato.kashtahport.model.HotelBookingModel


/**
 * Created by bodacious on 25/3/19.
 */

class HotelMyBookingAdapter(internal var context: Context, internal var mList: List<HotelBookingModel>, internal var listener: OnItemClick) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    var hotelListType = TYPE_LIST
    var nextItemList = ArrayList<HotelBookingModel>()
    var SKIP_DATA = 0
    companion object {
        val TYPE_MAP_LIST = 111
        val TYPE_LIST = 112
    }
    inner class MyHotelViewHolder(itemView: View): RecyclerView.ViewHolder(itemView)  {}

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.list_item_latest_booking, parent, false)
        return MyHotelViewHolder(view)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {

        val model = mList[position]

        MyBookingListController(context,this,holder as MyHotelViewHolder,model,listener,position).control()
    }

    override fun getItemCount(): Int {
        return mList.size
    }

    abstract class OnItemClick {
        abstract fun onItemClick(position: Int)
        abstract fun onUpdateBooking(position: Int,status:String)
        abstract fun onLoadMore(position: Int)
    }

}
