package com.itwhiz4u.q8mercato.kashtahport.Controller

import android.content.Context
import com.itwhiz4u.q8mercato.kashtahport.adapter.TransactionListAdapter
import com.itwhiz4u.q8mercato.kashtahport.helper.ConstantFunction
import com.itwhiz4u.q8mercato.kashtahport.helper.DaysCount
import com.itwhiz4u.q8mercato.kashtahport.model.TransactionModel
import kotlinx.android.synthetic.main.list_item_transaction.view.*

class TransactionController(val context: Context, val notificationListAdapter: TransactionListAdapter, val holder: TransactionListAdapter.MyBookViewHolder, val model: TransactionModel, val listener: TransactionListAdapter.OnItemClick, val position: Int) {
    fun control() {



        val times = "" + DaysCount().printDifference(System.currentTimeMillis(), model.created_at.toLong())
        holder.itemView.times.setText(""+times)
        holder.itemView.tv_title.text = model.title
      //  holder.itemView.amount.text = "$" + model.amount

        ConstantFunction.setPrice(context,model.currency,model.amount,holder.itemView.amount)
        holder.itemView!!.setOnClickListener {
            listener.onItemClick(position)
        }
    }
}