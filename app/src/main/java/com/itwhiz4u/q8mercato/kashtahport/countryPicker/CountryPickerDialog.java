package com.itwhiz4u.q8mercato.kashtahport.countryPicker;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewCompat;
import android.support.v7.app.AppCompatDialog;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ListView;

import com.itwhiz4u.q8mercato.kashtahport.R;

import java.text.Collator;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Locale;

/**
 * Created by GODARD Tuatini on 07/05/15.
 */
public class CountryPickerDialog extends Dialog {

    private List<Country> countries;
    private List<Country> countriesMain;
    private CountryPickerCallbacks callbacks;
    private ListView listview;
    private ImageView search, clearAll;
    private EditText et_search;
    private FrameLayout frameNormal;
    private FrameLayout frameSearch;

    private String headingCountryCode;
    private boolean showDialingCode;

    CountryListAdapter adapter;
    Context  context;
    public CountryPickerDialog(Context context, CountryPickerCallbacks callbacks) {

        this(context, callbacks, null, true);

    }

    public CountryPickerDialog(Context context, CountryPickerCallbacks callbacks, @Nullable String headingCountryCode) {
        this(context, callbacks, headingCountryCode, true);
    }

    /**
     * You can set the heading country in headingCountryCode to show
     * your favorite country as the head of the list
     *
     * @param context
     * @param callbacks
     * @param headingCountryCode
     */
    public CountryPickerDialog(Context context, CountryPickerCallbacks callbacks,
                               @Nullable String headingCountryCode, boolean showDialingCode) {
        super(context,R.style.DialogTheme);

        this.context = context;
        this.callbacks = callbacks;
        this.headingCountryCode = headingCountryCode;
        this.showDialingCode = showDialingCode;
        countriesMain = new ArrayList<>();
        countries = Utils.parseCountries(Utils.getCountriesJSON(this.getContext()));
        sortCountry();

        countriesMain.addAll(countries);
    }

    private void sortCountry() {

        Collections.sort(countries, new Comparator<Country>() {
            @Override
            public int compare(Country country1, Country country2) {
                final Locale locale = getContext().getResources().getConfiguration().locale;
                final Collator collator = Collator.getInstance(locale);
                collator.setStrength(Collator.PRIMARY);
                return collator.compare(
                        new Locale(locale.getLanguage(), country1.getIsoCode()).getDisplayCountry(),
                        new Locale(locale.getLanguage(), country2.getIsoCode()).getDisplayCountry());
            }
        });
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_country_picker);
        WindowManager.LayoutParams  param = getWindow().getAttributes();
        param.width = WindowManager.LayoutParams.MATCH_PARENT;
        param.height = WindowManager.LayoutParams.MATCH_PARENT;
        param.horizontalMargin = 0;
        param.verticalMargin = 0;

        getWindow().setAttributes(param);

        ViewCompat.setElevation(getWindow().getDecorView(), 3);
        listview = (ListView) findViewById(R.id.country_picker_listview);
        et_search = (EditText) findViewById(R.id.et_c_name);
        search = (ImageView) findViewById(R.id.c_im_search);
        clearAll = (ImageView) findViewById(R.id.c_im_clear);
        frameNormal = (FrameLayout) findViewById(R.id.frame_c_normal);
        frameSearch = (FrameLayout) findViewById(R.id.frame_c_search);

        adapter = new CountryListAdapter(this.getContext(), countries, showDialingCode);
        listview.setAdapter(adapter);
        listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                hide();
                Country country = countries.get(position);
                callbacks.onCountrySelected(country, Utils.getMipmapResId(getContext(),
                        country.getIsoCode().toLowerCase(Locale.ENGLISH) + "_flag"));
            }
        });

        search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                frameNormal.setVisibility(View.GONE);
                frameSearch.setVisibility(View.VISIBLE);
            }
        });

        clearAll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                et_search.setText("");
                frameNormal.setVisibility(View.VISIBLE);
                frameSearch.setVisibility(View.GONE);
            }
        });

        et_search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if(s.length() >0){
                    searchQuery(s+"");
                }else {
                    if(TextUtils.isEmpty(s)){
                        countries.clear();
                        countries.addAll(countriesMain);
                        adapter.notifyDataSetChanged();
                    }
                }
            }
        });

        scrollToHeadingCountry();

    }

    private void searchQuery(String s) {
        countries.clear();
        for (int i =0 ; i< countriesMain.size(); i++){
            Country country = countriesMain.get(i);
            String str = new Locale(context.getResources().getConfiguration().locale.getLanguage(), country.getIsoCode()).getDisplayCountry();

            Log.e("searchQuery","   "+str.toLowerCase());
            if(str.toLowerCase().contains(s.toLowerCase())){
                countries.add(country);
            }
        }

        adapter.notifyDataSetChanged();
    }

    private void scrollToHeadingCountry() {
        if (headingCountryCode != null) {
            for (int i = 0; i < listview.getCount(); i++) {
                if (((Country) listview.getItemAtPosition(i)).getIsoCode().toLowerCase()
                        .equals(headingCountryCode.toLowerCase())) {
                    listview.setSelection(i);
                }
            }
        }
    }

    public Country getCountryFromIsoCode(String isoCode) {
        for (int i = 0; i < countries.size(); i++) {
            if (countries.get(i).getIsoCode().equals(isoCode.toUpperCase())) {
                return countries.get(i);
            }
        }
        return null;
    }
}
