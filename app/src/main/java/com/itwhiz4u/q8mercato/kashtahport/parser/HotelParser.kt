package com.itwhiz4u.q8mercato.kashtahport.parser

import android.text.TextUtils
import android.util.Log
import com.itwhiz4u.q8mercato.kashtahport.R
import com.itwhiz4u.q8mercato.kashtahport.database.AppDatabase
import com.itwhiz4u.q8mercato.kashtahport.helper.ConstantFunction
import com.itwhiz4u.q8mercato.kashtahport.helper.MyApplication
import com.itwhiz4u.q8mercato.kashtahport.model.*
import org.jetbrains.anko.doAsync
import org.json.JSONObject
import java.lang.Exception
import java.util.*

class HotelParser {

    val TAG  = "HotelParser"
    fun parse(jsonObject: JSONObject):HotelModel{

        val hotelPropertyModel = HotelModel()

        try {

            if(jsonObject.has("_id")){
                hotelPropertyModel.id = jsonObject.getString("_id")
            }

            if(jsonObject.has("location")){

            }

            if(jsonObject.has("owner_id")){
                hotelPropertyModel.ownerId = jsonObject.getString("owner_id")
            }

            if(jsonObject.has("type")){
                hotelPropertyModel.type = jsonObject.getString("type")
            }

            if(jsonObject.has("currency")){
                hotelPropertyModel.currency = jsonObject.getString("currency")
            }

            if(jsonObject.has("name")){
                hotelPropertyModel.name = jsonObject.getString("name")
                if(!TextUtils.isEmpty(hotelPropertyModel.name)){
                    val name = ConstantFunction.capsFirstLetter(hotelPropertyModel.name)
                    hotelPropertyModel.name = name
                }
            }

            if(jsonObject.has("city")){
                hotelPropertyModel.city = jsonObject.getString("city")
            }

            if(jsonObject.has("country")){
                hotelPropertyModel.country = jsonObject.getString("country")
            }


            if(jsonObject.has("rooms_number")){
                hotelPropertyModel.rooms = jsonObject.getString("rooms_number")
            }

            if(jsonObject.has("wc_number")){
                hotelPropertyModel.wc_number = jsonObject.getString("wc_number")
            }

            if(jsonObject.has("person_number")){
                hotelPropertyModel.person = jsonObject.getString("person_number")
            }

            if(jsonObject.has("children")){
                hotelPropertyModel.children = jsonObject.getString("children")
            }

            if(jsonObject.has("conditions")){
                hotelPropertyModel.conditions = jsonObject.getString("conditions")
                var conditions = ""
                val array = jsonObject.getJSONArray("conditions")
                for (i in 0 until array.length()){
                    val str = array.get(i).toString()
                    conditions += MyApplication.instance.getString(R.string.bullet_text) +" $str\n"
                }
                hotelPropertyModel.conditions = conditions
            }

            if(jsonObject.has("rate")){
                hotelPropertyModel.rate = jsonObject.getString("rate")
            }


            if(jsonObject.has("contact_info")){
                hotelPropertyModel.phoneNumber = jsonObject.getString("contact_info")
            }

            if(jsonObject.has("address")){
                hotelPropertyModel.fullAddress = jsonObject.getString("address")
            }

            if(jsonObject.has("rating")){
                hotelPropertyModel.rating = jsonObject.get("rating").toString()
            }

            if(jsonObject.has("status")){
                hotelPropertyModel.status = jsonObject.getBoolean("status")
            }

            if(jsonObject.has("longitude")){
                hotelPropertyModel.longitude = jsonObject.get("longitude").toString()
            }

            if(jsonObject.has("latitude")){
                hotelPropertyModel.latitude = jsonObject.get("latitude").toString()
            }

            if(jsonObject.has("created_at")){
                hotelPropertyModel.created_at = jsonObject.get("created_at").toString()
            }

            if(jsonObject.has("description")){
                hotelPropertyModel.description = jsonObject.get("description").toString()
            }

            if(jsonObject.has("is_enable")){
                hotelPropertyModel.isEnable = jsonObject.getBoolean("is_enable")
            }

            if(jsonObject.has("is_approved")){
                hotelPropertyModel.isApprove = jsonObject.getBoolean("is_approved")
            }

            if(jsonObject.has("is_delete")){
                hotelPropertyModel.isDelete = jsonObject.getBoolean("is_delete")
            }

            if(jsonObject.has("is_deleted")){
                hotelPropertyModel.isDelete = jsonObject.getBoolean("is_deleted")
            }

            if(jsonObject.has("amenities")){
                val array = jsonObject.getJSONArray("amenities")
                val list = ArrayList<AmenitiesModel>()
                for (i in 0 until  array.length()){
                    val obj = array.getJSONObject(i)
                    val amenitiesModel = AmenitiesModel()

                    if(obj.has("name")){
                        amenitiesModel.name = obj.getString("name")
                    }

                    if(obj.has("status")){
                        amenitiesModel.status = obj.get("status").toString().toBoolean()
                    }


                    list.add(amenitiesModel)
                }

                hotelPropertyModel.amenitiesList = list
            }

            if(jsonObject.has("pictures")){
                val array = jsonObject.getJSONArray("pictures")
                val list = ArrayList<HotelImageModel>()

                for (i in 0 until  array.length()){
                    val hotelImageModel = HotelImageModel()
                    hotelImageModel.url = array.get(i).toString()
                    list.add(hotelImageModel)
                }

                hotelPropertyModel.imageList = list
            }

            if(jsonObject.has("available_date")){
                val array = jsonObject.getJSONArray("available_date")
                val list = ArrayList<HotelDateModel>()
                for (i in 0 until  array.length()){
                    var obj = JSONObject()

                    if(array.get(i) is JSONObject){
                        obj = array.getJSONObject(i)
                    }else{
                        obj = JSONObject(array.get(i).toString())
                    }

                    Log.d("available_date",obj.toString())
                    val dateModel = HotelDateModel()

                    if(obj.has("start_date")){
                        dateModel.startDate = obj.getString("start_date")
                        dateModel.startDate = (dateModel.startDate.toLong()-TimeZone.getDefault().rawOffset).toString()
                    }

                    if(obj.has("end_date")){
                        dateModel.endDate = obj.getString("end_date")
                        dateModel.endDate = (dateModel.endDate.toLong()-TimeZone.getDefault().rawOffset).toString()
                    }

                    if(obj.has("start_time")){
                        dateModel.startTime = obj.getString("start_time")
                    }

                    if(obj.has("end_time")){
                        dateModel.endTime = obj.getString("end_time")
                    }

                    if(obj.has("rate")){
                        dateModel.rent = obj.getString("rate")
                    }

                    if(obj.has("day_rate")){
                        dateModel.rent = obj.getString("day_rate")
                    }

                    if(obj.has("night_rate")){
                        dateModel.nightRent = obj.getString("night_rate")

                        if(TextUtils.isEmpty(dateModel.rent)){
                            dateModel.rent = obj.getString("night_rate")
                        }

                    }else{
                        dateModel.nightRent =dateModel.rent
                    }

                    list.add(dateModel)
                }

                hotelPropertyModel.availableDates = list
            }

            doAsync {
                val favHotel = AppDatabase.getAppDatabase().favPostDao().getFavPost(hotelPropertyModel.id)
               if(favHotel != null){
                   if(favHotel.id.equals(hotelPropertyModel.id)){
                       hotelPropertyModel.isFav=true
                   }
               }
            }
        }catch (e:Exception){
            Log.e(TAG, " ss "+e.message)
            e.printStackTrace()
        }
        finally {

            return  hotelPropertyModel
        }

    }
}

/*        {
            "location": {
                "type": "Point",
                "coordinates": [
                    -73.97,
                    40.77
                ]
            },
            "owner_id": "5cc2d0b8b290fb2fbae21488",
            "type": "farm",
            "name": "Hotel Ashirwad",
            "city": "bhopal",
            "country": "india",
            "pictures": [
                "wwwww",
                "wwwww",
                "wwwwww"
            ],
            "rooms_number": "23",
            "wc_number": "1",
            "person_number": "46",
            "children": "",
            "amenities": [],
            "conditions": [
                "1",
                "2",
                "3",
                "4",
                "5"
            ],
            "available_date": [
                "{}"
            ],
            "rate": "1000",
            "is_enable": false,
            "rating": 0,
            "is_aprooved": false,
            "_id": "5cc2d352b290fb2fbae21489",
            "status": false,
            "__v": 0
        },*/