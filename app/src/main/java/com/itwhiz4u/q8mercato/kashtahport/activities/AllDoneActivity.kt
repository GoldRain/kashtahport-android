package com.itwhiz4u.q8mercato.kashtahport.activities

import android.os.Bundle
import com.itwhiz4u.q8mercato.kashtahport.R
import kotlinx.android.synthetic.main.activity_all_done.*

class AllDoneActivity : BaseActivity() {

    var fromPay = false
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_all_done)
        supportActionBar?.hide()

        fromPay = intent.getBooleanExtra("fromPay",false)

        if(fromPay){
            des.text = getString(R.string.hint_payment_complete)
        }
        frame_btn.setOnClickListener {

            finish()
        }
    }
}
