package com.itwhiz4u.q8mercato.kashtahport.activities

import android.content.Intent
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.text.TextUtils
import android.util.Log
import com.google.firebase.dynamiclinks.FirebaseDynamicLinks
import com.itwhiz4u.q8mercato.kashtahport.R
import com.itwhiz4u.q8mercato.kashtahport.helper.Constant
import java.text.SimpleDateFormat
import java.util.*

class SplashActivity : BaseActivity() {

    var check = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        supportActionBar?.hide()

        //  DaysCount().getHash(this)

        setLocale()

        getDynamicLink()
    }

    var linkUrl = ""
    private fun getDynamicLink() {
        Log.e("testing","In get link method")
        var fireBaseListener = FirebaseDynamicLinks.getInstance()
                .getDynamicLink(intent)
                .addOnSuccessListener(this@SplashActivity) { pendingDynamicLinkData ->
                    check = true
                    var deepLink: Uri? = null
                    if (pendingDynamicLinkData != null) {
                        deepLink = pendingDynamicLinkData.link
                        if (deepLink != null) {
                            linkUrl = "$deepLink"
                            Constant.LINK_URL = linkUrl
                        } else {
                            Constant.LINK_URL = ""
                        }
                    }
                    Log.e("testing","In OnSuccess method")
                    moveNext()
                    Log.e("LINK_GET", ": getDynamicLink: ${Constant.LINK_URL}")
                }
                .addOnFailureListener(this@SplashActivity) { e ->
                    Log.e("Analytic", "getDynamicLink:onFailure", e)
                    Log.e("testing","fdkljhfdgh")
                    moveNext()
                }
                .addOnCompleteListener {
                    if(!check){
                        moveNext()
                    }
                    Log.e("testing","In onComplete Listener")
                }
                .addOnCanceledListener {

                    Log.e("testing","In onCancel Listener")
                }
    }

    private fun setLocale() {

        Log.e("testing","In set locale")
        var lan = prefManager?.getString(Constant.KEY_LAN)
        if (TextUtils.isEmpty(lan)) {
            lan = "en"
        }
        Log.e("LANGUAGE", "$lan   *** ")
        val myLocale = Locale(lan)
        Locale.setDefault(myLocale)
        val res = resources
        val dm = res.displayMetrics
        val conf = res.configuration
        conf.locale = myLocale

        if (Build.VERSION.SDK_INT >= 17) {
            conf.setLayoutDirection(myLocale)
        }

        baseContext.resources.updateConfiguration(conf, dm)
        prefManager?.setString(Constant.KEY_LAN, lan)

    }

    private fun moveNext() {
        Log.e("testing","In move Next method")

        val flag = intent.getBooleanExtra(Constant.KEY_NOTIFICATION, false)

        if (flag) {
            var type = intent.getStringExtra(Constant.KEY_NOTIFICATION_TYPE)

            when (type.toLowerCase()) {

                "book" -> {
                    val intent = Intent(this@SplashActivity, MainActivity::class.java)
                    intent.putExtra(Constant.KEY_OPEN_FOR, Constant.OPEN_FOR_PROFILE)
                    startActivity(intent)
                    finishAffinity()
                    finish()
                }

                "book_get" -> {

                    val intent = Intent(this@SplashActivity, MainActivity::class.java)
                    intent.putExtra(Constant.KEY_OPEN_FOR, Constant.OPEN_FOR_BOOKING_LIST)
                    startActivity(intent)
                    finish()
                }

                else -> {

                    val intent = Intent(this@SplashActivity, MainActivity::class.java)
                    startActivity(intent)
                    finish()
                }
            }
        } else {
            Handler().postDelayed(object : Runnable {
                override fun run() {
                    if (intent.getBooleanExtra("fromLan", false)) {
                        val intent = Intent(this@SplashActivity, MainActivity::class.java)
                        intent.putExtra("fromLan", true)
                        startActivity(intent)

                        finish()
                    } else {
                        try {
                            MainActivity.getInstance()?.finishAffinity()
                        } catch (e: Exception) {
                            e.printStackTrace()
                        } finally {
                            startActivity(Intent(this@SplashActivity, MainActivity::class.java))
                            finish()
                        }

                    }
                }

            }, 1000)
        }
    }
}
