package com.itwhiz4u.q8mercato.kashtahport.Retrofit.Interface

import com.itwhiz4u.q8mercato.kashtahport.Api.ApiUrl
import com.itwhiz4u.q8mercato.kashtahport.Retrofit.Model.Request.AddToFavRequest
import com.itwhiz4u.q8mercato.kashtahport.Retrofit.Model.Request.LoginRequest
import com.itwhiz4u.q8mercato.kashtahport.Retrofit.Model.Response.AddToFavResponse
import com.itwhiz4u.q8mercato.kashtahport.Retrofit.Model.Response.UserDataResponse
import okhttp3.OkHttpClient
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.Body
import retrofit2.http.Headers
import retrofit2.http.POST
import java.util.concurrent.TimeUnit

interface ApiService {

    @POST("login")
    @Headers("content-type: application/json")
    fun loginRequest(@Body request: LoginRequest): Call<UserDataResponse>

    @POST("addToFavourite")
    @Headers("content-type: application/json")
    fun addToFavorite(@Body request: AddToFavRequest):Call<AddToFavResponse>

    companion object {


        var client = OkHttpClient.Builder()
                .connectTimeout(10, TimeUnit.SECONDS)
                .readTimeout(30, TimeUnit.SECONDS).build()

        fun create(): ApiService{
            val retrofit = Retrofit.Builder()
                    .baseUrl(ApiUrl.SERVER)
                    .client(client)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build()

            return retrofit.create(ApiService::class.java)
        }
    }
}