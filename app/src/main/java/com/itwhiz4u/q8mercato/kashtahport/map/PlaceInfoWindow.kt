package com.itwhiz4u.q8mercato.kashtahport.map

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import com.itwhiz4u.q8mercato.kashtahport.R
import com.itwhiz4u.q8mercato.kashtahport.model.PlaceModel
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.model.Marker
import kotlinx.android.synthetic.main.view_marker_place_info.view.*

class PlaceInfoWindow(val context: Context, val place: PlaceModel, val onPlaceSelect: OnPlaceSelect):GoogleMap.InfoWindowAdapter {
    override fun getInfoContents(p0: Marker?): View? {

      return  null
    }

    override fun getInfoWindow(p0: Marker?): View? {

        val layInflator =  context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val view   = layInflator.inflate(R.layout.view_marker_place_info,null)

        view.place_name.text = place.name
        view.place_address.text = place.address

        view.book_now.setOnClickListener {
            onPlaceSelect.onPlaceSelect()
        }

        return view
    }

    interface OnPlaceSelect{
        fun onPlaceSelect()
    }
}