package com.itwhiz4u.q8mercato.kashtahport.Api

object ApiUrl {

    val SERVER =  "http://18.191.10.69:8080/"
   // val SERVER =  "http://192.168.0.130:8080/"

    val UPDATE_NOTIFICATION_TOKEN = "${SERVER}updateNotificationToken"

    val ABOUT_US_WEB_VIEW = "${SERVER}public/aboutus.html"
    val ABOUT_US_WEB_VIEW_AR = "${SERVER}public/aboutusArbic.html"
    val PRIVACY_US_WEB_VIEW = "${SERVER}public/policy.html"
    val PRIVACY_US_WEB_VIEW_AR = "${SERVER}public/policyArbic.html"
    val TERM_WEB_VIEW = "${SERVER}public/term.html"
    val TERM_WEB_VIEW_AR = "${SERVER}public/termArbic.html"
    val GET_COUNTRIES = "${SERVER}getCountries"
    val LOGIN_URL = "${SERVER}login"
    val LOGOUT_URL = "${SERVER}logout"
    val SEND_OTP = "${SERVER}getOtp"
    val CEHCK_EMAIL = "${SERVER}checkEmail"
    val SEND_EMAIL = "${SERVER}sendEmailForChangePassword"
    val VERIFY_OTP = "${SERVER}verifyOtp"
    val FACEBOOK_VALIDATION = "${SERVER}facebook"
    val REGISTER = "${SERVER}registerUser"
    val ADD_PROPERTY = "${SERVER}addProperty"
    val UPDATE_PROPERTY = "${SERVER}updateProperty"
    val GET_PROPERTY_LIST = "${SERVER}listProperty"
    val GET_PROPERTY = "${SERVER}singleProperty"
    val GET_PROPERTY_STATUS = "${SERVER}hotelCheck"
    val MANAGE_PROPERTY = "${SERVER}manageProperty"
    val SEARCH_HOTEL = "${SERVER}searchHotel"
    val SEARCH_NEAR_BY_HOTEL = "${SERVER}nearByHotel"
    val ADD_TO_FAV = "${SERVER}addToFavourite"
    val REMOVE_TO_FAV = "${SERVER}removeToFavourite"
    val FAV_HOTEL_LIST = "${SERVER}favouriteHotelList"
    val BOOOK_HOTEL = "${SERVER}createBooking"
    val GET_ALL_BOOKINGS = "${SERVER}bookingRequest"
    val UPDATE_BOOKING = "${SERVER}updateBooking"
    val GET_BOOKING_DETAILS = "${SERVER}singleBookingRequest"
    val GET_MY_BOOKINGS = "${SERVER}viewProfile"
    val UPDATE_PROFILE = "${SERVER}updateProfile"
    val PROVIDE_FEEDBACK = "${SERVER}feedback"
    val GET_HOTEL_FEEDBACK = "${SERVER}getFeedbackOfSingleHotel"
    val DELETE_HOTEL = "${SERVER}deleteHotel"
    val GET_NOTIFICATION = "${SERVER}getActivity"
    val CHECK_EMAIL_PHONE = "${SERVER}checkEmailAndPhone"
    val UPDATE_IBAN = "${SERVER}updateIBAN"
    val GET_OTP_ON_CALL = "${SERVER}makeCall"


 val getPaymetLoader = "${SERVER}public/"
 val PAYMENT_REQUEST = "${SERVER}paymentRequest/"
 val GET_PAYMENT_LIST = "${SERVER}getPaymentList/"
 val DELETE_ALL_ACTIVITY = "${SERVER}deleteAllActivity/"
 val DELETE_SINGLE_ACTIVITY = "${SERVER}deleteSingleActivity/"
 val UPDATE_NOTIFICATION_STATUS = "${SERVER}updateNotificationEnable/"
    val GET_ADDRESS_FROM_MAP= "https://maps.googleapis.com/maps/api/geocode/json?"


 /******************************************************************  PAYMENTS ******************************** */
 val CREATE_CHARGE_REQUEST = "https://api.tap.company/v2/charges"
}