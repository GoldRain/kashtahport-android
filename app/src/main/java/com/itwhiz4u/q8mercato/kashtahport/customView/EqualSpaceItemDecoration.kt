package com.itwhiz4u.q8mercato.kashtahport.customView

import android.graphics.Rect
import android.support.v7.widget.RecyclerView
import android.view.View

/**
 * Created by bodacious on 8/4/19.
 */
class EqualSpaceItemDecoration(val space:Int) : RecyclerView.ItemDecoration() {

    override fun getItemOffsets(outRect: Rect, view: View, parent: RecyclerView, state: RecyclerView.State) {

        outRect!!.bottom = space;
        outRect.top = space
    }
}