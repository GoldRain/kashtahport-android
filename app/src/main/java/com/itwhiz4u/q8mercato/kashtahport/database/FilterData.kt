package com.itwhiz4u.q8mercato.kashtahport.database
import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import android.os.Parcel
import android.os.Parcelable

@Entity(tableName = "filterTable")
class FilterData() :Parcelable {
    @PrimaryKey(autoGenerate = false)
    @ColumnInfo(name = "id")
    var id:String = ""

    @ColumnInfo(name = "type")
    var type:String = ""

    @ColumnInfo(name = "rooms")
    var rooms:String = ""

    @ColumnInfo(name = "person")
    var person:String = ""

    @ColumnInfo(name = "wc")
    var wc:String = ""

    @ColumnInfo(name = "child")
    var child:String = ""

    @ColumnInfo(name = "startPrice")
    var startPrice:String = ""

    @ColumnInfo(name = "endPrice")
    var endPrice:String = ""

    @ColumnInfo(name = "startDate")
    var startDate:String = ""

    @ColumnInfo(name = "endDate")
    var endDate:String = ""

    @ColumnInfo(name = "country")
    var country:String = ""

    @ColumnInfo(name = "city")
    var city:String = ""

    @ColumnInfo(name = "filterCount")
    var filterCount:Int = 0

    constructor(parcel: Parcel) : this() {
        id = parcel.readString()
        type = parcel.readString()
        rooms = parcel.readString()
        person = parcel.readString()
        wc = parcel.readString()
        child = parcel.readString()
        startPrice = parcel.readString()
        endPrice = parcel.readString()
        startDate = parcel.readString()
        endDate = parcel.readString()
        country = parcel.readString()
        city = parcel.readString()
        filterCount = parcel.readInt()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(id)
        parcel.writeString(type)
        parcel.writeString(rooms)
        parcel.writeString(person)
        parcel.writeString(wc)
        parcel.writeString(child)
        parcel.writeString(startPrice)
        parcel.writeString(endPrice)
        parcel.writeString(startDate)
        parcel.writeString(endDate)
        parcel.writeString(country)
        parcel.writeString(city)
        parcel.writeInt(filterCount)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<FilterData> {
        override fun createFromParcel(parcel: Parcel): FilterData {
            return FilterData(parcel)
        }

        override fun newArray(size: Int): Array<FilterData?> {
            return arrayOfNulls(size)
        }
    }


}


/* var id :String?= null
    var name :String?= null
    var packagePrice :String?= null
    var packageType :String?= null
    var description :String?= null
    var status :String?= null
    var createdAt :String?= null
    var updatedAt :String?= null
    var amountPayable :String?= null
    var modelId :String?= null
    var brandId :String?= null
    var isExpand:Boolean = false*/