package com.itwhiz4u.q8mercato.kashtahport.Controller.notification

import android.content.Context
import com.itwhiz4u.q8mercato.kashtahport.adapter.NotificationListAdapter
import com.itwhiz4u.q8mercato.kashtahport.model.FeedBackModel
import com.itwhiz4u.q8mercato.kashtahport.model.NotificationModel
import kotlinx.android.synthetic.main.list_item_notification_feedback.view.*

class NotificationFeedbackController(val context: Context, val  adapter: NotificationListAdapter, val holder: NotificationListAdapter.MyFeedBackViewHolder, val  model: NotificationModel, val listener: NotificationListAdapter.OnItemClick, val position: Int) {
    fun control(){


        holder.itemView!!.setOnClickListener {

            listener.onItemClick(position)
        }
        holder.itemView.noti_heading.text = model.message
        adapter.viewBinderHelper.bind(holder.itemView.swipe_revel,""+position)
        val ind = model.message.indexOf("@")
        if(ind != -1){
            val name = model.message.substring(0,ind).split(" ")
            val content = model.message.substring(ind+1)

         //   Log.e("NotificationFe", " "+name.size+" "+content+" "+name[0])
            holder.itemView.noti_heading.text = name[0]+" "+content
        }else{
            holder.itemView.noti_heading.text = model.message
        }
       // holder.itemView.time.text = ConstantFunction.getOnlyDate(model.created_at.toLong(), Constant.DEFAULT_DATE_FORMAT2)
        holder.itemView.time.text = adapter.daysCount.printDifference(System.currentTimeMillis(),model.created_at.toLong())

        if(model.feedBack != null){
            val hotelModel = model.feedBack as FeedBackModel
            if(hotelModel != null){
                holder.itemView.hotel_des.text =hotelModel.comment
                holder.itemView.rating_bar.rating = hotelModel.avg.toFloat()
            }
        }

        holder.itemView!!.frame_delete.setOnClickListener {

            listener.onDelete(position)
        }

        holder.itemView!!.cl_front.setOnClickListener {
            listener.onItemClick(position)
        }
    }
}