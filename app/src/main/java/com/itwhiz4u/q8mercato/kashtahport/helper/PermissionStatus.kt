package com.itwhiz4u.q8mercato.kashtahport.helper

import android.Manifest
import android.support.v4.app.ActivityCompat
import android.content.pm.PackageManager
import android.support.v4.content.ContextCompat
import android.app.Activity
import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Build
import android.provider.Settings
import com.itwhiz4u.q8mercato.kashtahport.R

class PermissionStatus {

    companion object {

        val GRANTED = 0
        val DENIED = 1
        val BLOCKED_OR_NEVER_ASKED = 2

        val PERMISSION_STORAGE = Manifest.permission.WRITE_EXTERNAL_STORAGE
        val PERMISSION_CAMERA = Manifest.permission.CAMERA
        val PERMISSION_CONTACT = Manifest.permission.READ_CONTACTS
        val PERMISSION_LOCATION = Manifest.permission.ACCESS_COARSE_LOCATION
        val PERMISSION_LOCATION_FINE = Manifest.permission.ACCESS_FINE_LOCATION
        val PERMISSION_CALL = Manifest.permission.CALL_PHONE
    }


    fun getPermissionStatus(activity: Activity, androidPermissionName: String,flag:Boolean = true): Int {
         if (ContextCompat.checkSelfPermission(activity, androidPermissionName) != PackageManager.PERMISSION_GRANTED) {
            if (!ActivityCompat.shouldShowRequestPermissionRationale(activity, androidPermissionName)) {
                if(flag){
                    openPermissionAgain(activity,androidPermissionName)
                }
               return BLOCKED_OR_NEVER_ASKED
            } else{
                if(flag){
                    //askPermissin(activity,androidPermissionName)
                }
                return  DENIED
            }

        } else
             return  GRANTED
    }

    fun askPermissions(activity: Activity, androidPermissionName: String) {
        if(Build.VERSION.SDK_INT >= 23){
            ActivityCompat.requestPermissions(activity, arrayOf(androidPermissionName),12345)
        }
    }

    fun getPermissionStatus(context: Context, androidPermissionName: String): Int {
        if (ContextCompat.checkSelfPermission(context, androidPermissionName) != PackageManager.PERMISSION_GRANTED) {

            return  DENIED
        } else
            return  GRANTED
    }

    private fun openPermissionAgain(activity: Activity, androidPermissionName: String):Int {

        if(!MyApplication.instance.prefManager.getBoolean(androidPermissionName)){
            MyApplication.instance.prefManager.setBoolean(androidPermissionName,true)
            askPermissions(activity,androidPermissionName)

            return BLOCKED_OR_NEVER_ASKED
        }
        var msg = "Permission required to continue"

        when(androidPermissionName){
            PERMISSION_STORAGE ->{

                msg = activity.getString(R.string.per_war_storage)
            }

            PERMISSION_CAMERA ->{

                msg = activity.getString(R.string.per_war_camera)
            }

            PERMISSION_CONTACT ->{

                msg = activity.getString(R.string.per_war_contact)
            }
            PERMISSION_CALL ->{

                msg = activity.getString(R.string.per_war_call)
            }

            PERMISSION_LOCATION ->{

                msg = activity.getString(R.string.per_war_location)
            }

            PERMISSION_LOCATION_FINE ->{

                msg = activity.getString(R.string.per_war_location)
            }
        }


        val builder = AlertDialog.Builder(activity)
        builder.setTitle("Allow Permission")
        builder.setMessage(msg)
        builder.setNegativeButton("CANCEL") { dialog, which -> dialog.dismiss() }

        builder.setPositiveButton("SETTING") { dialog, which ->
            val intent = Intent()
            intent.action = Settings.ACTION_APPLICATION_DETAILS_SETTINGS
            val uri = Uri.fromParts("package", activity.getPackageName(), null)
            intent.data = uri
            activity.startActivity(intent)
        }
        builder.show()

        return  BLOCKED_OR_NEVER_ASKED
    }

}