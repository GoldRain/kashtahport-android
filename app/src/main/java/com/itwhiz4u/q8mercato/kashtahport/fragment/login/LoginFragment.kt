package com.itwhiz4u.q8mercato.kashtahport.fragment.login


import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.itwhiz4u.q8mercato.kashtahport.R
import com.itwhiz4u.q8mercato.kashtahport.activities.LoginActivity


/**
 * A simple [Fragment] subclass.
 *
 */
class LoginFragment : Fragment() {
    lateinit var fragView :View
    lateinit var loginActivity: LoginActivity

    fun setActivity(registerActivity: LoginActivity){
        this.loginActivity = registerActivity
    }
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        fragView = inflater.inflate(R.layout.fragment_login, container, false)

      /*  fragView.signup1.setOnClickListener {
            startActivity(Intent(loginActivity,RegisterActivity::class.java))
        }

        fragView.signup.setOnClickListener {
            startActivity(Intent(loginActivity,RegisterActivity::class.java))
        }

        fragView.forgot_pass.setOnClickListener {
            startActivity(Intent(loginActivity,ForgotPasswordActivity::class.java))
        }

        fragView.frame_register.setOnClickListener {
            startActivity(Intent(loginActivity,RegisterActivity::class.java))
            activity?.finish()
        }*/

        return fragView
    }


}
