package com.itwhiz4u.q8mercato.kashtahport.customDialog

import android.app.Dialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.view.Gravity
import android.view.Window
import android.view.WindowManager
import com.itwhiz4u.q8mercato.kashtahport.R


class CustomProgress(private val context: Context) {
    private var dialog: Dialog

    init {
        dialog = Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.window.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.setContentView(R.layout.dialog_progress)
        val window = dialog.getWindow()
        val wlp = window.getAttributes()
        wlp.width = WindowManager.LayoutParams.MATCH_PARENT
        wlp.height = WindowManager.LayoutParams.WRAP_CONTENT
        wlp.gravity = Gravity.CENTER_VERTICAL
        window.setAttributes(wlp)

        dialog.setCanceledOnTouchOutside(false)
        dialog.setCancelable(false)

    }

    fun show() {
        dialog.show()
        //Glide.with(context).asGif().load(R.drawable.loader_icon).into(dialog.findViewById(R.id.image))
    }

    fun dismiss() {
        dialog.cancel()
    }



}
