package com.itwhiz4u.q8mercato.kashtahport.activities

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.app.Dialog
import android.content.DialogInterface
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.os.Handler
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.text.TextUtils
import android.util.Log
import android.view.*
import android.widget.*
import com.applandeo.materialcalendarview.EventDay
import com.itwhiz4u.q8mercato.kashtahport.Api.ApiResponseListener
import com.itwhiz4u.q8mercato.kashtahport.Api.ServiceRequest
import com.itwhiz4u.q8mercato.kashtahport.R
import com.itwhiz4u.q8mercato.kashtahport.adapter.SpinnerListAdapter
import com.itwhiz4u.q8mercato.kashtahport.customDialog.CustomProgress
import com.itwhiz4u.q8mercato.kashtahport.helper.Constant
import com.itwhiz4u.q8mercato.kashtahport.helper.ConstantFunction
import com.itwhiz4u.q8mercato.kashtahport.model.HotelDateModel
import com.itwhiz4u.q8mercato.kashtahport.model.PlaceModel
import com.itwhiz4u.q8mercato.kashtahport.upload.UploadAWS
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.github.florent37.runtimepermission.kotlin.askPermission
import com.lite.imagepickerlib.GalleryActivity
import com.itwhiz4u.q8mercato.kashtahport.helper.DaysCount
import com.itwhiz4u.q8mercato.kashtahport.helper.PermissionStatus
import com.mazadlive.Interface.ProgressListener
import kotlinx.android.synthetic.main.activity_seller_add_property.*
import kotlinx.android.synthetic.main.dialog_add_date.*
import kotlinx.android.synthetic.main.dialog_calender_range.*
import kotlinx.android.synthetic.main.view_add_date.view.*
import kotlinx.android.synthetic.main.view_amenities.*
import kotlinx.android.synthetic.main.view_header_toolbar.*
import org.json.JSONArray
import org.json.JSONObject
import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.HashMap

class SellerAddPropertyActivity : BaseActivity() {

    val DATE_PICKER_ID = 111
    val TIME_PICKER_ID = 112
    val ALL_IMAGE_PICKER_ID = 113
    val PLACE_CODE = 114
    val LOCATION_PERMISSION = 115
    val MAX_IMAGES = 10

    val TAG = "SellerAddProperty"

    lateinit var dialogDate: Dialog
    lateinit var dialogCalendar: Dialog

    var dateModel = HotelDateModel()


    val dateList = ArrayList<HotelDateModel>()
    val selectedImagesList = ArrayList<String>()
    val selectedImagesUrlList = ArrayList<String>()

    var images = arrayOfNulls<ImageView>(5)
    var amentiesOnDrawbleList = ArrayList<Int>()
    var amentiesOffDrawbleList = ArrayList<Int>()
    var amenties = arrayOfNulls<ImageView>(10)
    var amenitiesList: List<String> = ArrayList()

    var properTyList: List<String> = ArrayList()
    var currencyList: List<String> = ArrayList()
    var childrenList: List<String> = ArrayList()
    var personList: List<String> = ArrayList()
    var roomList: List<String> = ArrayList()
    var wcList: List<String> = ArrayList()
    var countryList: List<String> = ArrayList()

    var isEnable = false
    var placeModel: PlaceModel? = null

    var maxDate = 0L
    var minDate = 0L
    var mapDisableDays = HashMap<Long, Calendar>()
    val eventList = ArrayList<EventDay>()

    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_seller_add_property)
        supportActionBar?.hide()
        progress = CustomProgress((this))

        initViewes()

        initSpinner()

        initClick()

        initCalender()
    }



    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)

        if (requestCode == LOCATION_PERMISSION) {
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED &&
                    ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                openLocationActivity()
            }
        }

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == Constant.GALLARY_IMAGE_REQUEST && resultCode == Activity.RESULT_OK) {

            val list = data?.getStringArrayListExtra("images")
            selectedImagesList.addAll(list!!)
            setImages()
        }

        if (requestCode == ALL_IMAGE_PICKER_ID && resultCode == Activity.RESULT_OK) {

            val list = data?.getStringArrayListExtra("images")
            selectedImagesList.clear()
            selectedImagesList.addAll(list!!)
            setImages()
        }

        if (requestCode == PLACE_CODE && resultCode == Activity.RESULT_OK) {
            add_location.setText(getString(R.string.ap_hint_change_loc))
            ll_address.visibility = View.VISIBLE

            placeModel = data?.getParcelableExtra<PlaceModel>(Constant.KEY_ADDRESS)

            full_address.setText("${placeModel?.address}")
            et_city.setText(placeModel?.city)
            et_country.setText(placeModel?.country)
        }
    }

    private fun checkPermissions(): Boolean {


        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED ||
                ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(this,
                    arrayOf(Manifest.permission.ACCESS_FINE_LOCATION,
                            Manifest.permission.ACCESS_COARSE_LOCATION), LOCATION_PERMISSION)
            return false
        }

        return true
    }

    private fun openLocationActivity() {

        // Places.initialize(applicationContext,getString(R.string.googleMap_apiKey))
        //  val buider = PlacePicker.IntentBuilder()
        //startActivityForResult(buider.build(this), PLACE_CODE)

        startActivityForResult(Intent(this, MapLocationActivity::class.java), PLACE_CODE)
    }

    private fun openGallary() {
        askPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA, Manifest.permission.RECORD_AUDIO) {

            val maxSelection = MAX_IMAGES - selectedImagesList.size
            val intent = Intent(this@SellerAddPropertyActivity, GalleryActivity::class.java)
            intent.putExtra(GalleryActivity.KEY_MAX_SELECTION, maxSelection)
            intent.putExtra(GalleryActivity.KEY_ALLOW_MULTIPLE, true)
            startActivityForResult(intent, Constant.GALLARY_IMAGE_REQUEST)

        }.onDeclined { result ->
            if (result.hasDenied()) {

            } else if (result.hasForeverDenied()) {

            }
        }
    }


    private fun initClick() {

        frame_add_date.setOnClickListener {
            showDateDialog()
        }
        frame_another_date.setOnClickListener {
            frame_add_date.performClick()
        }

        frame_add.setOnClickListener {
            if (selectedImagesList.size < MAX_IMAGES) {
                openGallary()
            } else {
                Toast.makeText(this, getString(R.string.ap_hint_ad_image_error), Toast.LENGTH_SHORT).show()
            }

        }

        term_condition.setOnClickListener {
            val intent = Intent(this,TermCondActivity::class.java)
            startActivity(intent)
        }
        frame_more_image.setOnClickListener {
            val intent = Intent(this, ImageListActivity::class.java)
            intent.putStringArrayListExtra("images", selectedImagesList)
            startActivityForResult(intent, ALL_IMAGE_PICKER_ID)
        }

        icon_back.visibility = View.VISIBLE
        header_name.setText(getString(R.string.ap_hint_header_name))
        icon_back.setOnClickListener {
            super.onBackPressed()
        }


        card_location.setOnClickListener {
            if (checkPermissions()) {
                openLocationActivity()
            }
        }

        frame_post.setOnClickListener {
            finish()
        }

        frame_saved.setOnClickListener {
            if (validation()) {
                isEnable = false
                progress?.show()
                uploadAllImages()
            } else {
                errorDialog?.show(getString(R.string.hint_error), getString(R.string.ap_hint_fill_data))
            }
        }

        frame_post.setOnClickListener {

            if (validation()) {
                isEnable = true
                progress?.show()
                uploadAllImages()
            } else {
                errorDialog?.show(getString(R.string.hint_error), getString(R.string.ap_hint_fill_data))
            }
        }

        et_des.setOnTouchListener { v, event ->
            v.getParent().requestDisallowInterceptTouchEvent(true);
            when (event.getAction()) {

                MotionEvent.ACTION_SCROLL -> {
                    v.getParent().requestDisallowInterceptTouchEvent(false);
                    true;
                }

            }
            false;
        }

        et_condition.setOnTouchListener { v, event ->
            v.getParent().requestDisallowInterceptTouchEvent(true);
            when (event.getAction()) {

                MotionEvent.ACTION_SCROLL -> {
                    v.getParent().requestDisallowInterceptTouchEvent(false);
                    true;
                }

            }
            false;
        }
    }

    private fun initSpinner() {

        var arrayAmenities = resources.getStringArray(R.array.amenities_array)
        amenitiesList = arrayAmenities.asList()

        var arrayCurrencies = resources.getStringArray(R.array.currency_type)
        currencyList = arrayCurrencies.asList()

        var arrayProperty = resources.getStringArray(R.array.property_type)
        properTyList = arrayProperty.asList()

        var arrayPerson = resources.getStringArray(R.array.person_array)
        personList = arrayPerson.asList()

        var arrayChild = resources.getStringArray(R.array.child_array)
        childrenList = arrayChild.asList()

        var arrayRoom = resources.getStringArray(R.array.room_array)
        roomList = arrayRoom.asList()

        var arrayWc = resources.getStringArray(R.array.wc_array)
        wcList = arrayWc.asList()

        val adapterCurrency = SpinnerListAdapter(this, currencyList)

        spinner_currency.adapter = adapterCurrency
        spinner_currency.setSelection(0)

        val adapterProperty = SpinnerListAdapter(this, properTyList)

        spinner_type.adapter = adapterProperty
        spinner_type.setSelection(0)

        val adapterPerson = SpinnerListAdapter(this, personList)

        spinner_person.adapter = adapterPerson
        spinner_person.setSelection(0)

        val adapterChild = SpinnerListAdapter(this, childrenList)

        spinner_children.adapter = adapterChild
        spinner_children.setSelection(0)

        val adapterRoom = SpinnerListAdapter(this, roomList)

        spinner_room.adapter = adapterRoom
        spinner_room.setSelection(0)

        val adapterWc = SpinnerListAdapter(this, wcList)

        spinner_wc.adapter = adapterWc
        spinner_wc.setSelection(0)
    }

    private fun initViewes() {
        amentiesOnDrawbleList.add(R.drawable.ic_wifi_white)
        amentiesOnDrawbleList.add(R.drawable.ic_desk_white)
        amentiesOnDrawbleList.add(R.drawable.ic_drink_white)
        amentiesOnDrawbleList.add(R.drawable.ic_parking_white)
        amentiesOnDrawbleList.add(R.drawable.ic_spa_white)
        amentiesOnDrawbleList.add(R.drawable.ic_dining_white)
        amentiesOnDrawbleList.add(R.drawable.ic_gym_white)
        amentiesOnDrawbleList.add(R.drawable.ic_smart_tv_white_)
        amentiesOnDrawbleList.add(R.drawable.ic_swim_white)
        amentiesOnDrawbleList.add(R.drawable.ic_flame_white)

        amentiesOffDrawbleList.add(R.drawable.ic_wifi_gray)
        amentiesOffDrawbleList.add(R.drawable.ic_desk_gray)
        amentiesOffDrawbleList.add(R.drawable.ic_drink_gray)
        amentiesOffDrawbleList.add(R.drawable.ic_parking_gray)
        amentiesOffDrawbleList.add(R.drawable.ic_spa_gray)
        amentiesOffDrawbleList.add(R.drawable.ic_dining_gray)
        amentiesOffDrawbleList.add(R.drawable.ic_gym_gray)
        amentiesOffDrawbleList.add(R.drawable.ic_smart_tv_gray)
        amentiesOffDrawbleList.add(R.drawable.ic_swim_gray)
        amentiesOffDrawbleList.add(R.drawable.ic_flame_gray)

        images = arrayOf(pic_1, pic_2, pic_3, pic_4, pic_5)
        amenties = arrayOf(im_wifi, im_desk, im_drink, im_park, im_spa, im_kitchen, im_gym, im_smartTV, im_pool, im_bornFire)


        for (i in 0 until images.size) {
            images[i]!!.setOnClickListener {
                if (selectedImagesList.size >= (i + 1)) {
                    frame_more_image.performClick()
                } else {
                    frame_add.performClick()
                }
            }
        }


        for (i in 0 until amenties.size) {
            amenties[i]!!.tag = true
            amenties[i]!!.setOnClickListener {
                setAmentiesUi(amenties[i], i)
            }

            setAmentiesUi(amenties[i], i)
        }

    }

    private fun setAmentiesUi(imageView: ImageView?, i: Int) {
        if ((amenties[i]!!.tag as Boolean)) {
            amenties[i]!!.tag = false
            amenties[i]!!.setBackgroundResource(R.drawable.round_gray)
            amenties[i]!!.setImageResource(amentiesOffDrawbleList[i])
        } else {
            amenties[i]!!.tag = true
            amenties[i]!!.setBackgroundResource(R.drawable.round_pri)
            amenties[i]!!.setImageResource(amentiesOnDrawbleList[i])
        }
    }

    private fun setImages() {

        for (i in 0 until images.size) {
            images[i]!!.setImageResource(0)
        }

        var count = 5
        if (selectedImagesList.size > 5) {
            count = 5
            frame_more_image.visibility = View.VISIBLE
            extra_image_count.text = "+${selectedImagesList.size - 4}"
        } else {
            count = selectedImagesList.size
            frame_more_image.visibility = View.GONE
        }


        for (i in 0 until count) {
            if (images[i] != null) {
               // Log.d("SetImages"," ** "+selectedImagesList[i])
                Glide.with(this).load(selectedImagesList[i]).apply(RequestOptions().override(80).error(R.drawable.hotl_dummy)).into(images[i]!!)
            }
        }
    }

    /**********************************************************************************************************************************/
    var isStartDate = true
    var isStartTime = true
    var startTime = "0"
    var endTime = "0"
    var startDate = "0"
    var endDate = "0"

    private fun initCalender() {

        Handler().postDelayed(object : Runnable {
            override fun run() {
                progress?.dismiss()

                dialogCalendar = Dialog(this@SellerAddPropertyActivity)
                dialogCalendar.requestWindowFeature(Window.FEATURE_NO_TITLE)
                dialogCalendar.window.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
                dialogCalendar.setContentView(R.layout.dialog_calender_range)
                val window = dialogCalendar.getWindow()
                val wlp = window.getAttributes()
                wlp.width = WindowManager.LayoutParams.MATCH_PARENT
                wlp.height = WindowManager.LayoutParams.WRAP_CONTENT
                wlp.gravity = Gravity.CENTER_VERTICAL
                window.setAttributes(wlp)

                val cal = Calendar.getInstance()
                cal.timeInMillis = System.currentTimeMillis()
                dialogCalendar.calender.setDate(cal)


                dialogCalendar.calender.setMinimumDate(cal)

                dialogCalendar.dialog_frame_done1.setOnClickListener {

                    selectDate()
                }

                dialogCalendar.calender.setOnDayClickListener {
                    val calSel = it.calendar
                    val calPrev = Calendar.getInstance()
                    val calNext = Calendar.getInstance()
                    calPrev.timeInMillis = calSel.timeInMillis - (1 * 24 * 60 * 60 * 1000)
                    calNext.timeInMillis = calSel.timeInMillis + (1 * 24 * 60 * 60 * 1000)

                    val flag = mapDisableDays.containsKey(calPrev.timeInMillis)
                    val flag2 = mapDisableDays.containsKey(calNext.timeInMillis)

                    if (flag && flag2) {
                        mapDisableDays.put(calSel.timeInMillis, calSel)
                        val list = ArrayList<Calendar>()
                        mapDisableDays.keys.forEach { ml ->
                            val calD = mapDisableDays.get(ml)
                            list.add(calD!!)
                        }
                        dialogCalendar.calender.setDisabledDays(list)
                        dialogCalendar.calender.setEvents(ArrayList<EventDay>())
                    } else {
                        setAvailableDates()
                    }
                }

                //setDisableDays()
            }

        }, 100)
    }

    private fun setAvailableDates() {

        Handler().postDelayed(object : Runnable {
            override fun run() {

                val calenders = dialogCalendar.calender.selectedDates

                //
                val disableList = ArrayList<Calendar>()
                if (calenders.size > 1) {
                    val startCal = calenders[0]
                    val endCal = calenders[calenders.size - 1]

                    // get all dates b/w two selected dates
                    val days = DaysCount().getDates(startCal.timeInMillis, endCal.timeInMillis)
                    var match = false

                    for (i in 0 until days.size) {
                        //break if this date already selected
                        if (mapDisableDays.containsKey(days[i])) {
                            break
                        }

                        // add this dates to a list
                        val cal = Calendar.getInstance()
                        cal.timeInMillis = days[i]
                        disableList.add(cal)
                    }

                    // set selected list to calender...
                    dialogCalendar.calender.selectedDates = disableList
                    dialogCalendar.calender.setEvents(ArrayList<EventDay>())

                }
            }
        }, 200)
    }

    private fun selectDate() {

        Handler().postDelayed(object : Runnable {
            override fun run() {
                val calendar = dialogCalendar.calender.selectedDates
                if (calendar.size > 1) {

                    val calStart = calendar[0]
                    val calEnd = calendar[calendar.size - 1]

                    // add more m.s. according to timezone // in india add 5.30 hrs ms
                    startDate = (calStart.timeInMillis + TimeZone.getDefault().rawOffset).toString()
                    endDate = (calEnd.timeInMillis + TimeZone.getDefault().rawOffset).toString()

                    dialogDate.date_start.text = ConstantFunction.getOnlyDate(startDate.toLong())
                    dialogDate.date_end.text = ConstantFunction.getOnlyDate(endDate.toLong())
                    dialogCalendar.dismiss()
                }
            }
        }, 100)
    }

    private fun showDateDialog() {
        dialogCalendar.calender.selectedDates = ArrayList<Calendar>()
        dialogCalendar.calender.setEvents(ArrayList<EventDay>())

        dateModel = HotelDateModel()

        dialogDate = Dialog(this)
        dialogDate.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialogDate.window.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialogDate.setContentView(R.layout.dialog_add_date)
        val window = dialogDate.getWindow()
        val wlp = window.getAttributes()
        wlp.width = WindowManager.LayoutParams.MATCH_PARENT
        wlp.height = WindowManager.LayoutParams.WRAP_CONTENT
        wlp.gravity = Gravity.CENTER_VERTICAL
        window.setAttributes(wlp)

        dialogDate.dialog_add_frame_cal.setOnClickListener {
            setDisableDays()
            dialogCalendar.show()
        }


        dialogDate.dialog_frame_add_date.setOnClickListener {
            if (validateDate()) {

                dateModel.startDate = startDate
                dateModel.endDate = endDate
                dateModel.startTime = startTime
                dateModel.endTime = endTime
                dateModel.rent = dialogDate.dialog_rate.text.toString().trim()
                dateModel.nightRent = dialogDate.dialog_night_rate.text.toString().trim()

                ConstantFunction.hideKeyboard(this, dialogDate.dialog_rate)
                addDateView(dateModel)


                if (endDate.toLong() > maxDate) {
                    maxDate = endDate.toLong()
                }

                if (startDate.toLong() < minDate || minDate == 0L) {
                    minDate = startDate.toLong()
                }
                dialogDate.dismiss()
            }

        }

        dialogDate.show()

        dialogDate.setOnDismissListener(object : DialogInterface.OnDismissListener {
            override fun onDismiss(dialog: DialogInterface?) {
                startTime = "0"
                endTime = "0"
            }

        })
    }

    private fun setDisableDays() {

        if (maxDate > 0L) {

            val list = ArrayList<Calendar>()

            /*val cal = Calendar.getInstance()
            cal.timeInMillis =maxDate
            dialogCalendar.calender.setMinimumDate(cal)
            list.add(cal)
            dialogCalendar.calender.setDisabledDays(list)*/

            /* create disable list from map*/
            mapDisableDays.keys.forEach {
                val cal = mapDisableDays.get(it)
                list.add(cal!!)
            }

            /* disable all selected range add to map*/
            dialogCalendar.calender.setDisabledDays(list)
            dialogCalendar.calender.setEvents(ArrayList<EventDay>())
        }
    }

    private fun validateDate(): Boolean {

        if (TextUtils.isEmpty(dialogDate.date_start.text.toString())) {
            Toast.makeText(this, getString(R.string.ap_hint_add_start_date), Toast.LENGTH_SHORT).show()
            return false
        }

        if (TextUtils.isEmpty(dialogDate.date_end.text.toString())) {
            Toast.makeText(this, getString(R.string.ap_hint_add_end_date), Toast.LENGTH_SHORT).show()
            return false
        }

        /*  if (TextUtils.isEmpty(dialogDate.time_start.text.toString())) {
              Toast.makeText(this, getString(R.string.ap_hint_add_start_time), Toast.LENGTH_SHORT).show()
              return false
          }

          if (TextUtils.isEmpty(dialogDate.time_end.text.toString())) {
              Toast.makeText(this, getString(R.string.ap_hint_add_end_time), Toast.LENGTH_SHORT).show()
              return false
          }*/

        if (TextUtils.isEmpty(dialogDate.dialog_rate.text.toString())) {
            Toast.makeText(this, getString(R.string.ap_hint_enter_rent), Toast.LENGTH_SHORT).show()
            return false
        }

        /*  if (TextUtils.isEmpty(dialogDate.dialog_night_rate.text.toString())) {
              Toast.makeText(this, getString(R.string.ap_hint_enter_night_rent), Toast.LENGTH_SHORT).show()
              return false
          }*/

        return true
    }

    private fun checkValidDate(cal: Calendar?, calSelect: Calendar?): Boolean {
        Log.e("checkValidDate", "  TIME " + startTime + " " + endTime)
        val sD = ConstantFunction.getOnlyDate(calSelect!!.timeInMillis)
        var err = false
        for (i in 0 until dateList.size) {
            val date = dateList[i]
            Log.e("checkValidDate", date.endDate + " **  " + calSelect.timeInMillis)
            if (calSelect.timeInMillis <= date.endDate.toLong()) {
                Toast.makeText(this@SellerAddPropertyActivity, getString(R.string.ap_hint_already_select), Toast.LENGTH_SHORT).show()
                return false
            }
        }

        return true
    }

    val daysCount = DaysCount()
    private fun addDateView(model: HotelDateModel = HotelDateModel()) {

        val view = layoutInflater.inflate(R.layout.view_add_date, null)

        view.start_date.text = ConstantFunction.getOnlyDate(model.startDate.toLong())
        view.end_date.text = ConstantFunction.getOnlyDate(model.endDate.toLong())
        view.start_time.text = ConstantFunction.getTime(model.startTime.toLong())
        view.end_time.text = ConstantFunction.getTime(model.endTime.toLong())
        view.rate_text.text = model.rent + getString(R.string.ap_hint_pernight)
        view.rate_night_text.text = model.nightRent


        view.dialog_frame_cal.setOnClickListener {
            if (view.ll_date_details.visibility == View.VISIBLE) {
                view.ll_date_details.visibility = View.GONE
                view.dialog_frame_delete.setBackgroundResource(R.color.colorWhite)
            } else {
                view.ll_date_details.visibility = View.VISIBLE
            }


            Handler().postDelayed(object : Runnable {
                override fun run() {
                    val layParam = RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT)
                    layParam.width = 200
                    layParam.height = view.dialog_ll.height
                    view.dialog_frame_delete.layoutParams = layParam
                    view.dialog_frame_delete.setBackgroundResource(R.color.color_status_red)

                }

            }, 100)
        }

        frame_another_date.visibility = View.VISIBLE
        frame_add_date.visibility = View.GONE

        view.dialog_frame_delete.setOnClickListener {
            dateList.remove(model)
            lay_dates.removeView(view)

            deleteFromMaps(model)
        }

        lay_dates.addView(view)

        dateList.add(model)


        val dates = daysCount.getDates(model.startDate.toLong(), model.endDate.toLong())
        dates.forEach {
            // minus Timezoe offset M.s.
            val ml = it - TimeZone.getDefault().rawOffset
            val cal = Calendar.getInstance()
            cal.timeInMillis = ml
            mapDisableDays.put(ml, cal)
        }
    }

    private fun deleteFromMaps(model: HotelDateModel) {

        val days = DaysCount().getDates(model.startDate.toLong(), model.endDate.toLong())
        days.forEach {
            val ml = it- TimeZone.getDefault().rawOffset
            mapDisableDays.remove(ml)
        }
        setMinMaxDates()
    }

    private fun setMinMaxDates() {
        minDate = 0
        maxDate = 0


        for (i in 0 until dateList.size) {
            val date = dateList[i]
            if (minDate == 0L || date.startDate.toLong() < minDate) {
                minDate = date.startDate.toLong()
            }

            if (maxDate == 0L || date.endDate.toLong() > maxDate) {
                maxDate = date.endDate.toLong()
            }
        }
    }


    private fun validation(): Boolean {
        if (TextUtils.isEmpty(et_hotelname.text.toString().trim())) {
            et_hotelname.error = getString(R.string.ap_hint_hotel_empty)
            return false
        }

        if (placeModel == null) {

            errorDialog?.show(getString(R.string.hint_error), getString(R.string.ap_hint_hotel_loc))
            return false
        }

        if (TextUtils.isEmpty(et_phone.text.toString().trim())) {

            et_phone.error = getString(R.string.ap_hint_empty_phone)
            return false
        }


        if (selectedImagesList.size <= 1) {
            errorDialog?.show(getString(R.string.hint_error), getString(R.string.ap_hint_add_image))
            return false
        }

        if (TextUtils.isEmpty(et_hotel_rent.text.toString().trim())) {
            //Toast.makeText(this,"Add available dates!!",Toast.LENGTH_SHORT).show()
            et_hotel_rent.error = getString(R.string.ap_hint_hotel_rate)
            return false
        }

        /* if (dateList.size <= 0) {

             errorDialog?.show(getString(R.string.hint_error),"Add available dates!!")
             return false
         }*/

        if (TextUtils.isEmpty(et_condition.text.toString().trim())) {
            et_condition.error = getString(R.string.ap_hint_cond_list)
            return false
        }

        if (TextUtils.isEmpty(et_des.text.toString().trim())) {
            // Toast.makeText(this,"Add available dates!!",Toast.LENGTH_SHORT).show()
            et_des.error = getString(R.string.ap_hint_emoty_des)
            return false
        }

        if (!cond_check.isChecked) {
            errorDialog?.show(getString(R.string.hint_error), getString(R.string.ap_hint_accept_term))
            return false
        }

        return true
    }

    private fun createParam(): JSONObject {

        val hotelName = et_hotelname.text.toString().trim()
        var type = properTyList[spinner_type.selectedItemPosition]

        var currencyType = currencyList[spinner_currency.selectedItemPosition]
        when(currencyType){
            getString(R.string.hint_usd) ->{
                currencyType = "usd"
            }

            getString(R.string.hint_kwd) ->{
                currencyType = "kwd"
            }
        }

        when(type){
            getString(R.string.hint_camp) -> {
                type = "Camp"
            }

            getString(R.string.hint_farm) -> {
                type = "Farm"
            }

            getString(R.string.hint_chalet) -> {
                type = "Chalet"
            }
        }

        val city = et_city.text.toString().trim()
        val country = et_country.text.toString().trim()
        val rooms = roomList[spinner_room.selectedItemPosition]
        val wcNumber = wcList[spinner_wc.selectedItemPosition]
        val person = wcList[spinner_person.selectedItemPosition]
        val children = childrenList[spinner_children.selectedItemPosition]
        val dates = getDateParam()
        val amenities = getAmenitiesParam()
        val ownerId = prefManager.getString(Constant.KEY_USER_ID)
        val rate = et_hotel_rent.text.toString().trim()
        val longitude = placeModel?.lon.toString()
        val latitude = placeModel?.lat.toString()
        val phone = et_phone.text.toString().trim()
        val des = et_des.text.toString().trim()
        val address = full_address.text.toString().trim()
        val conditions = et_condition.text.toString().trim()

        var pictures = ""

        selectedImagesUrlList.forEach {
            pictures = it + ","
        }

        if (!TextUtils.isEmpty(pictures)) {
            pictures += pictures.substring(0, pictures.length - 1)
        }

        Log.e("PARAM_PROPERTY", pictures)

        val param = JSONObject()

        param.put("api_key", Constant.API_KEY)
        param.put("name", hotelName)
        param.put("type", type)
        param.put("city", city)
        param.put("country", country)
        param.put("rooms_number", rooms)
        param.put("wc_number", wcNumber)
        param.put("person_number", person)
        param.put("children_number", children)
        param.put("available_date", dates)
        param.put("amenities", amenities)
        param.put("owner_id", ownerId)
        param.put("rate", rate)
        param.put("longitude", longitude)
        param.put("latitude", latitude)
        param.put("pictures", pictures)
        param.put("description", des)
        param.put("contact_info", phone)
        param.put("address", address)
        param.put("conditions", conditions)
        param.put("is_deleted", false)
        param.put("currency", currencyType)
        Log.e("PARAM_PROPERTY", param.toString())

        return param
    }

    private fun getDateParam(): JSONArray {
        val dateParam = JSONArray()

        dateList.forEach {
            val obj = JSONObject()
            obj.put("start_date", it.startDate)
            obj.put("end_date", it.endDate)
            obj.put("night_rate", it.rent)

            dateParam.put(obj)
        }

        return dateParam
    }

    private fun getAmenitiesParam(): JSONArray {
        val param = JSONArray()

        for (i in 0 until amenties.size) {
            val status = (amenties[i]!!.tag) as Boolean
            val name = amenitiesList[i]

            val obj = JSONObject()
            obj.put("name", name)
            obj.put("status", status)
            param.put(obj)

        }

        return param
    }

    private fun uploadAllImages() {
        val path = selectedImagesList[0]
        selectedImagesList.removeAt(0)

        val stats = PermissionStatus().getPermissionStatus(this, PermissionStatus.PERMISSION_STORAGE, false)
        var upload = UploadAWS(this, path, UploadAWS.UPLOAD_TYPE_HOTEL)
        if (stats == PermissionStatus.GRANTED) {
            var bitmap = ConstantFunction.getBitmapFromPath(path)
            bitmap = ConstantFunction.getResizedBitmap(bitmap!!, 700)
            val updatePath = ConstantFunction.saveHotelImage(bitmap, "")

            upload = UploadAWS(this, updatePath?.path!!, UploadAWS.UPLOAD_TYPE_HOTEL)
        }
        upload.uploadToAws(object : ProgressListener {
            override fun onProgressUpdate(progress: Int) {

            }

            override fun onSuccess(url: String) {
                selectedImagesUrlList.add(url)
                Log.e(TAG, " uploadAllImages ${selectedImagesList.size} ${selectedImagesUrlList.size} $url")

                if (selectedImagesList.size == 0) {
                    postProperty()
                } else {
                    uploadAllImages()
                }
            }

            override fun onFileNotFound() {
                if (selectedImagesList.size == 0) {
                    postProperty()
                } else {
                    uploadAllImages()
                }
            }

            override fun onCancel() {
                if (selectedImagesList.size == 0) {
                    postProperty()
                } else {
                    uploadAllImages()
                }
            }

        })

    }

    private fun postProperty() {
        val param = createParam()
        param.put("is_enable", isEnable)

        progress?.show()
        ServiceRequest(object : ApiResponseListener {
            override fun onCompleted(`object`: Any) {
                progress?.dismiss()
                ConstantFunction.deleteHotelImage()
                Toast.makeText(this@SellerAddPropertyActivity, getString(R.string.ap_hint_perpety_added), Toast.LENGTH_SHORT).show()
                finish()

            }

            override fun onError(errorMessage: String) {
                progress?.dismiss()
                ConstantFunction.deleteHotelImage()
                errorDialog?.show(getString(R.string.hint_error), "$errorMessage")
            }

        }).addProperty(param)
    }
}
