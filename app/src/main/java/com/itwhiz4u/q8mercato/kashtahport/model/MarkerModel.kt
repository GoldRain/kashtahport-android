package com.itwhiz4u.q8mercato.kashtahport.model

import android.os.Parcel
import android.os.Parcelable
import com.google.android.gms.maps.model.LatLng

class MarkerModel() :Parcelable {

    var startLat:LatLng?= null
    var endLat:LatLng?= null

    constructor(parcel: Parcel) : this() {
        startLat = parcel.readParcelable(LatLng::class.java.classLoader)
        endLat = parcel.readParcelable(LatLng::class.java.classLoader)
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeParcelable(startLat, flags)
        parcel.writeParcelable(endLat, flags)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<MarkerModel> {
        override fun createFromParcel(parcel: Parcel): MarkerModel {
            return MarkerModel(parcel)
        }

        override fun newArray(size: Int): Array<MarkerModel?> {
            return arrayOfNulls(size)
        }
    }
}