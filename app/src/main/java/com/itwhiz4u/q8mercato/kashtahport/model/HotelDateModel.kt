package com.itwhiz4u.q8mercato.kashtahport.model

import android.os.Parcel
import android.os.Parcelable

class HotelDateModel() :Parcelable{
    var startDateTime = 0L
    var endDateTime = 0L
    var startDate = ""
    var endDate = ""
    var startTime = ""
    var endTime = ""
    var rent = ""
    var nightRent = ""

    constructor(parcel: Parcel) : this() {
        startDateTime = parcel.readLong()
        endDateTime = parcel.readLong()
        startDate = parcel.readString()
        endDate = parcel.readString()
        startTime = parcel.readString()
        endTime = parcel.readString()
        rent = parcel.readString()
        nightRent = parcel.readString()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeLong(startDateTime)
        parcel.writeLong(endDateTime)
        parcel.writeString(startDate)
        parcel.writeString(endDate)
        parcel.writeString(startTime)
        parcel.writeString(endTime)
        parcel.writeString(rent)
        parcel.writeString(nightRent)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<HotelDateModel> {
        override fun createFromParcel(parcel: Parcel): HotelDateModel {
            return HotelDateModel(parcel)
        }

        override fun newArray(size: Int): Array<HotelDateModel?> {
            return arrayOfNulls(size)
        }
    }


}