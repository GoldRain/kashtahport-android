package com.itwhiz4u.q8mercato.kashtahport.Api

import android.util.Log
import com.itwhiz4u.q8mercato.kashtahport.helper.MyApplication

abstract class ApiRequester() {

    var isNetworkAvailable = false

    init {
        isNetworkAvailable = MyApplication.instance.isNetworkAvailable()
        Log.e("ApiRequester", "request initialisation with network : $isNetworkAvailable")
    }

}