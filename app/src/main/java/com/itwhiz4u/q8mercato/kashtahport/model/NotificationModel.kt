package com.itwhiz4u.q8mercato.kashtahport.model

import android.os.Parcel
import android.os.Parcelable

class NotificationModel() :Parcelable {

    var id = ""
    var userId = ""
    var hintId = ""
    var hint = ""
    var message = ""
    var created_at = ""
    var model: Any? = null
    var feedBack: Any? = null

    constructor(parcel: Parcel) : this() {
        id = parcel.readString()
        userId = parcel.readString()
        hintId = parcel.readString()
        hint = parcel.readString()
        message = parcel.readString()
        created_at = parcel.readString()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(id)
        parcel.writeString(userId)
        parcel.writeString(hintId)
        parcel.writeString(hint)
        parcel.writeString(message)
        parcel.writeString(created_at)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<NotificationModel> {
        override fun createFromParcel(parcel: Parcel): NotificationModel {
            return NotificationModel(parcel)
        }

        override fun newArray(size: Int): Array<NotificationModel?> {
            return arrayOfNulls(size)
        }
    }

}