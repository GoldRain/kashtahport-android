package com.itwhiz4u.q8mercato.kashtahport.Retrofit.Model.Response

import com.google.gson.annotations.SerializedName

data class AddToFavResponse(
    @SerializedName("error")
    var error :Boolean,

    @SerializedName("message")
    var message :String,

    @SerializedName("data")
    var userData: UserData? = null

)


