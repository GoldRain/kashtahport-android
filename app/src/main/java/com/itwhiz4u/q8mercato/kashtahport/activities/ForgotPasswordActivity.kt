package com.itwhiz4u.q8mercato.kashtahport.activities

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.text.TextUtils
import android.view.View
import android.widget.Toast
import com.itwhiz4u.q8mercato.kashtahport.Api.ApiResponseListener
import com.itwhiz4u.q8mercato.kashtahport.Api.ServiceRequest
import com.itwhiz4u.q8mercato.kashtahport.R
import com.itwhiz4u.q8mercato.kashtahport.customDialog.DialogListener
import com.itwhiz4u.q8mercato.kashtahport.helper.Constant
import kotlinx.android.synthetic.main.activity_forgot_password.*
import kotlinx.android.synthetic.main.view_header_toolbar.*
import org.json.JSONObject
import java.lang.Exception

class ForgotPasswordActivity : BaseActivity() {

    companion object {

        var ins :ForgotPasswordActivity?= null

        fun getInstance():ForgotPasswordActivity?{

            return ins

        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_forgot_password)
        supportActionBar?.hide()

        ins = this
        icon_back.visibility = View.VISIBLE

        icon_back.setOnClickListener {
            onBackPressed()
        }

        frame_btn.setOnClickListener {

            val email = et_email.text.toString().trim()
            if(TextUtils.isEmpty(email)){
                et_email.error = getString(R.string.fp_hint_phone_empty)
                return@setOnClickListener
            }

            checkEmail()
        }
    }

    override fun onActivityReenter(resultCode: Int, data: Intent?) {
        super.onActivityReenter(resultCode, data)
        if(resultCode ==1234 && resultCode == Activity.RESULT_OK){
            finish()
        }
    }

    private fun checkEmail(){
        val email  = et_email.text.toString().trim()
        val param =JSONObject()
        param.put("api_key", Constant.API_KEY)
        param.put("email",email)

        progress?.show()
        ServiceRequest(object : ApiResponseListener {
            override fun onCompleted(`object`: Any) {

                try {

                    val jsonObject = JSONObject(`object`.toString())

                    if(!jsonObject.getBoolean("error")){
                        sendEmail()
                    }else{
                        progress?.dismiss()
                    }

                }catch (e: Exception){
                    e.printStackTrace()
                    progress?.dismiss()
                    errorDialog?.show(getString(R.string.hint_error),getString(R.string.error_connect_to_server))
                }
            }

            override fun onError(errorMessage: String) {
                //Toast.makeText(activity!!,"Registration failed $errorMessage",Toast.LENGTH_SHORT).show()
                errorDialog?.show(getString(R.string.hint_error),"$errorMessage")
                progress?.dismiss()
            }

        }).checkEmail(param)
    }

    private fun sendEmail(){
        val email  = et_email.text.toString().trim()
        val param =JSONObject()
        param.put("api_key", Constant.API_KEY)
        param.put("email",email)

        progress?.dismiss()

        ServiceRequest(object : ApiResponseListener {
            override fun onCompleted(`object`: Any) {
                progress?.dismiss()
                try {

                    val jsonObject = JSONObject(`object`.toString())

                    if(!jsonObject.getBoolean("error")){
                        alert?.title = ""
                        alert?.content = getString(R.string.fp_hint_psw_reset_link_text)
                        alert?.listenr = object :DialogListener(){
                            override fun okClick() {
                               finish()
                            }
                        }
                        alert?.show()
                    }

                }catch (e: Exception){
                    e.printStackTrace()

                    errorDialog?.show(getString(R.string.hint_error),getString(R.string.error_connect_to_server))
                }
            }

            override fun onError(errorMessage: String) {
                //Toast.makeText(activity!!,"Registration failed $errorMessage",Toast.LENGTH_SHORT).show()
                errorDialog?.show(getString(R.string.hint_error),"$errorMessage")
                progress?.dismiss()
            }

        }).sendEmail(param)
    }

    private fun sendOtp() {

        val phone  = et_phone.text.toString().trim()
        var code  = et_code.text.toString().trim()

        code = code.replace("+","")
        val paramOtp = JSONObject()
        paramOtp.put("api_key", Constant.API_KEY)
        paramOtp.put("phone",phone)
        paramOtp.put("country_code",code)

        progress?.show()
        ServiceRequest(object : ApiResponseListener {
            override fun onCompleted(`object`: Any) {
                progress?.dismiss()
                try {

                    val jsonObject = JSONObject(`object`.toString())

                    if(!jsonObject.getBoolean("error")){

                        Toast.makeText(this@ForgotPasswordActivity,jsonObject.getString("message"),Toast.LENGTH_SHORT).show()

                        val intent = Intent(this@ForgotPasswordActivity,CodeVerificationActivity::class.java)
                        intent.putExtra("phone",phone)
                        intent.putExtra("forget",true)

                        startActivityForResult(intent,1234)

                    }

                }catch (e: Exception){
                    e.printStackTrace()

                    errorDialog?.show(getString(R.string.hint_error),getString(R.string.error_connect_to_server))
                }
            }

            override fun onError(errorMessage: String) {
                //Toast.makeText(activity!!,"Registration failed $errorMessage",Toast.LENGTH_SHORT).show()
                errorDialog?.show(getString(R.string.hint_error),"$errorMessage")
                progress?.dismiss()
            }

        }).sendOtp(paramOtp)
    }
}
