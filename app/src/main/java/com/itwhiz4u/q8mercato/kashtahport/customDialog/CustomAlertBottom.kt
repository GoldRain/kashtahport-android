package com.itwhiz4u.q8mercato.kashtahport.customDialog

import android.app.Dialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.text.TextUtils
import android.view.*
import com.itwhiz4u.q8mercato.kashtahport.R
import kotlinx.android.synthetic.main.dialog_alert.*


class CustomAlertBottom(private val context: Context) {
    private var dialog: Dialog

    var listenr :DialogListener?= null
    var title = ""
    var content =""
    init {
        dialog = Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.window.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.setContentView(R.layout.dialog_alert)

        val window = dialog.getWindow()
        window.setWindowAnimations(R.style.Animation)
        val wlp = window.getAttributes()
        wlp.width = WindowManager.LayoutParams.MATCH_PARENT
        wlp.height = WindowManager.LayoutParams.WRAP_CONTENT
        wlp.gravity = Gravity.BOTTOM
        window.setAttributes(wlp)

        dialog.alt_ok.setOnClickListener {
            dialog.cancel()
            listenr?.okClick()
        }

        dialog.alt_cancel.setOnClickListener {
            dialog.cancel()
        }
    }

    fun show() {
        dialog.alt_title.setText(title)
        dialog.alt_content.setText(content)

        if(TextUtils.isEmpty(title)){
            dialog.alt_title.visibility = View.GONE
        }

        dialog.show()

    }

    fun dismiss() {
        dialog.cancel()
    }

}
