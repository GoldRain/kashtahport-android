package com.itwhiz4u.q8mercato.kashtahport.database

import android.arch.persistence.room.*

@Dao
interface NotificationDao {
    /*@Query("SELECT * FROM Users WHERE userId LIKE :userId")
    fun getFriend(userId:String): List<Users>*/ //packagesInfo

    @Query("SELECT * FROM notification ")
    fun getAllFilter(): List<NotificationData>

    @Query("SELECT * FROM notification WHERE id LIKE :id ")
    fun getNotification(id:String): NotificationData

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(notificationData: NotificationData)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(notifications: List<NotificationData>)

    @Update(onConflict = OnConflictStrategy.REPLACE)
    fun update(usernotifications: NotificationData)

    @Query("UPDATE notification SET read = 1")
    fun updateNotificationRead()

    @Query("DELETE from notification")
    fun deleteAllNotification()

    @Query("DELETE from notification WHERE id LIKE :id ")
    fun deleteNotification(id:String)

    @Query("SELECT COUNT(*) from notification WHERE read = 0")
    fun getTotalNotificationCount(): Int

    @Query("SELECT * FROM notification WHERE read = 0")
    fun getTotalUnreadNotification(): List<NotificationData>
}