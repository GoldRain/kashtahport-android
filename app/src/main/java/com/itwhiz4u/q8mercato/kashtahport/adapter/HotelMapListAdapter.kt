package com.itwhiz4u.q8mercato.kashtahport.adapter

import android.app.Activity
import android.content.Context
import android.graphics.Bitmap
import android.support.v4.view.PagerAdapter
import android.view.View
import android.support.v4.view.ViewPager
import android.view.ViewGroup
import com.itwhiz4u.q8mercato.kashtahport.R
import com.itwhiz4u.q8mercato.kashtahport.helper.Constant
import com.itwhiz4u.q8mercato.kashtahport.helper.ConstantFunction
import com.itwhiz4u.q8mercato.kashtahport.helper.MyApplication
import com.itwhiz4u.q8mercato.kashtahport.model.HotelModel
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.RequestOptions
import com.bumptech.glide.request.target.Target
import kotlinx.android.synthetic.main.list_item_hotel_map.view.*


class HotelMapListAdapter(val context: Context, val list:ArrayList<HotelModel>,val listener:OnItemClick) :PagerAdapter(){
    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        (container as ViewPager).removeView(`object` as View)
    }

    override fun isViewFromObject(view: View, `object`: Any): Boolean {
        return view === `object` as View
    }

    override fun getItemPosition(`object`: Any): Int {
        return POSITION_NONE
    }
    override fun getCount(): Int {
       return list.size
    }

    override fun instantiateItem(container: ViewGroup, position: Int): Any {

        val viewItem = (context as Activity).layoutInflater.inflate(R.layout.list_item_hotel_map,container,false)
        (container as ViewPager).addView(viewItem)
        val model = list[position]
        viewItem.hotel_name.setText(model.name)
      //  viewItem.rent.text = "$"+model.rate
        ConstantFunction.setPrice(context,model.currency,model.rate,viewItem.rent)

        viewItem.hotel_des.setText(model.description)
        viewItem.rating_bar.rating = model.rating.toFloat()

        if(model.isFav ){
            viewItem.im_fav.setImageResource(R.drawable.ic_like_red_on)
        }else{
            viewItem.im_fav.setImageResource(R.drawable.ic_like_gray_off)
        }
        if (model.imageList.size > 0) {
           // Log.d("HotelListController", " "+model.name+ " "+model.imageList[0].url)
            Glide.with(context)
                    .asBitmap()
                    .load(model.imageList[0].url)
                    .apply(RequestOptions().override(200))
                    .listener(object : RequestListener<Bitmap> {
                        override fun onLoadFailed(e: GlideException?, model: Any?, target: Target<Bitmap>?, isFirstResource: Boolean): Boolean {

                            return false
                        }

                        override fun onResourceReady(resource: Bitmap?, model: Any?, target: Target<Bitmap>?, dataSource: DataSource?, isFirstResource: Boolean): Boolean {
                            return false
                        }

                    })
                    .into(viewItem.hotel_image)
        }

        viewItem.setOnClickListener {
            listener.onItemClick(position)
        }

        viewItem.im_fav.setOnClickListener {
            listener.onAddFav(position)
        }

        if(MyApplication.instance.prefManager.getBoolean(Constant.KEY_USER_LOGIN_STATUS)){
            viewItem.im_fav.visibility = View.GONE
        }else{
            viewItem.im_fav.visibility = View.GONE
        }
        return viewItem
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return ""
    }


    abstract class OnItemClick {
        abstract fun onItemClick(position: Int)
        open fun onAddFav(position: Int){}
    }
}