package com.itwhiz4u.q8mercato.kashtahport.Controller

import android.content.Context
import android.graphics.Bitmap
import android.util.Log
import android.view.View
import com.itwhiz4u.q8mercato.kashtahport.R
import com.itwhiz4u.q8mercato.kashtahport.adapter.HotelListAdapter
import com.itwhiz4u.q8mercato.kashtahport.database.AppDatabase
import com.itwhiz4u.q8mercato.kashtahport.helper.Constant
import com.itwhiz4u.q8mercato.kashtahport.helper.ConstantFunction
import com.itwhiz4u.q8mercato.kashtahport.helper.MyApplication
import com.itwhiz4u.q8mercato.kashtahport.model.HotelModel
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.RequestOptions
import com.bumptech.glide.request.target.Target
import kotlinx.android.synthetic.main.list_item_hotel.view.*
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread

class HotelListController(val context: Context, val hotelListAdapter: HotelListAdapter, val holder: HotelListAdapter.MyHotelViewHolder, val hotelModel: HotelModel, val listener: HotelListAdapter.OnItemClick, val position: Int) {

    fun control() {
        if (position == 0) {
            holder.itemView.header_name.visibility = View.VISIBLE

            when(hotelModel.type.toLowerCase()){
                "camp" ->{
                    holder.itemView.header_name.setText(context.getString(R.string.li_hint_popular_camp))
                }

                "farm" ->{
                    holder.itemView.header_name.setText(context.getString(R.string.li_hint_popular_farm))
                }

                "chalet" ->{
                    holder.itemView.header_name.setText(context.getString(R.string.li_hint_popular_chalet))
                }
            }
        } else {
            holder.itemView.header_name.visibility = View.GONE
        }

        holder.itemView.hotel_name.setText(hotelModel.name)

        holder.itemView.hotel_des.text = hotelModel.description

        ConstantFunction.setPrice(context,hotelModel.currency,hotelModel.rate,holder.itemView.rent)
        holder.itemView.rating_bar.rating = hotelModel.rating.toFloat()
/*

        if(hotelModel.isFav){
            holder.itemView.im_fav.setImageResource(R.drawable.ic_like_red_on)
        }else{
            holder.itemView.im_fav.setImageResource(R.drawable.ic_like_gray_off)
        }
*/



        doAsync {
            val fav = AppDatabase.getAppDatabase().favPostDao().getFavPost(hotelModel!!.id)
            Log.d("HotelListController", " 11 "+fav)
            if(fav != null && fav.id.equals(hotelModel!!.id)){
                uiThread {
                    holder.itemView.im_fav.setImageResource(R.drawable.ic_like_red_on)
                    holder.itemView.im_fav.tag = true
                    hotelModel.isFav = true
                }
            }else{
                uiThread {
                    holder.itemView.im_fav.setImageResource(R.drawable.ic_like_gray_off)
                    holder.itemView.im_fav.tag = false
                    hotelModel.isFav = false
                }
            }
        }

        Log.d("HotelListController", " "+hotelModel.name+ " ")
        if (hotelModel.imageList.size > 0) {
            Log.d("HotelListController", " "+hotelModel.name+ " "+hotelModel.imageList[0].url)
            Glide.with(context)
                    .asBitmap()
                    .load(hotelModel.imageList[0].url)
                    .apply(RequestOptions().override(500))
                    .listener(object : RequestListener<Bitmap> {
                        override fun onLoadFailed(e: GlideException?, model: Any?, target: Target<Bitmap>?, isFirstResource: Boolean): Boolean {
                            holder.itemView.frame_load_image.visibility = View.GONE
                            return false
                        }

                        override fun onResourceReady(resource: Bitmap?, model: Any?, target: Target<Bitmap>?, dataSource: DataSource?, isFirstResource: Boolean): Boolean {
                            holder.itemView.frame_load_image.visibility = View.GONE
                            return false
                        }

                    })
                    .into(holder.itemView.hotel_image)
        }


        holder.itemView!!.setOnClickListener {

            listener.onItemClick(position)
        }



        holder.itemView.im_fav.setOnClickListener {

            listener.onAddFav(position)
        }

        if(MyApplication.instance.prefManager.getBoolean(Constant.KEY_USER_LOGIN_STATUS)){
            holder.itemView.im_fav.visibility = View.VISIBLE
        }else{
            holder.itemView.im_fav.visibility = View.GONE
        }

    }
}