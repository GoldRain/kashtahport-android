package com.itwhiz4u.q8mercato.kashtahport.fragment.main


import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.itwhiz4u.q8mercato.kashtahport.Api.ApiResponseListener
import com.itwhiz4u.q8mercato.kashtahport.Api.ServiceRequest

import com.itwhiz4u.q8mercato.kashtahport.R
import com.itwhiz4u.q8mercato.kashtahport.activities.HotelDetailsActivity
import com.itwhiz4u.q8mercato.kashtahport.activities.LoginActivity
import com.itwhiz4u.q8mercato.kashtahport.activities.MainActivity
import com.itwhiz4u.q8mercato.kashtahport.adapter.HotelFavListAdapter
import com.itwhiz4u.q8mercato.kashtahport.customView.EqualSpaceItemDecoration
import com.itwhiz4u.q8mercato.kashtahport.database.AppDatabase
import com.itwhiz4u.q8mercato.kashtahport.helper.Constant
import com.itwhiz4u.q8mercato.kashtahport.model.HotelModel
import kotlinx.android.synthetic.main.fragment_fav.view.*
import org.greenrobot.eventbus.EventBus
import com.itwhiz4u.q8mercato.kashtahport.model.EventModel
import com.itwhiz4u.q8mercato.kashtahport.parser.HotelParser
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.dialog_login_warning.*
import kotlinx.android.synthetic.main.fragment_fav.*
import kotlinx.android.synthetic.main.view_header_toolbar.*
import org.greenrobot.eventbus.ThreadMode
import org.greenrobot.eventbus.Subscribe
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread
import org.json.JSONObject
import java.lang.Exception


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 *
 */
class FavFragment : Fragment() {
    val TAG = "FavFragment"
    lateinit var fragView: View
    var fragPosition = -1

    var mainActivity: MainActivity?= null
    lateinit var adapter: HotelFavListAdapter

    var hotelList = ArrayList<HotelModel>()

    fun setActivity(mainActivity: MainActivity) {
        this.mainActivity = mainActivity
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_fav, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        header_name.text = getString(R.string.frag_fav_hint_header_name)
        hotelList = ArrayList()

        adapter = HotelFavListAdapter(activity!!, hotelList, object : HotelFavListAdapter.OnItemClick() {
            override fun onItemClick(position: Int) {
                val intent = Intent(activity!!, HotelDetailsActivity::class.java)
                intent.putExtra(Constant.KEY_HOTEL, hotelList[position])
                startActivityForResult(intent, Constant.HOTEL_DETAIL_REQUEST)
            }

            override fun removeFromFav(position: Int) {
                removeFav(position)
            }
        })

        recycler_view.layoutManager = LinearLayoutManager(activity!!)
        recycler_view.addItemDecoration(EqualSpaceItemDecoration(16))
        recycler_view.adapter = adapter

        adapter.notifyDataSetChanged()

        swipe_to_refresh.setOnRefreshListener {
            if(swipe_to_refresh.isRefreshing){
                getHotelList()
            }
        }

        frame_register.setOnClickListener {
            LoginActivity.startLogin(activity!!,true,Constant.OPEN_FOR_FAV_HOTEL)
        }

        card_login.setOnTouchListener { v, event ->
            true
        }
    }

    override fun onResume() {
        EventBus.getDefault().register(this)
        super.onResume()
    }

    override fun onPause() {
        EventBus.getDefault().unregister(this)
        super.onPause()
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onMessageEvent(event: EventModel) {
        when (event.type) {
            Constant.EVENT_UPDATE_MAIN -> {
                if (mainActivity?.view_pager?.currentItem == Constant.FAV_POSITION) {
                    getHotelList()
                }
            }
        }
    }


    fun getHotelList() {

        if(!mainActivity?.prefManager!!.getBoolean(Constant.KEY_USER_LOGIN_STATUS)){
            mainActivity?.hideProgress(frame_loader,swipe_to_refresh)
            //Toast.makeText(activity!!,"You are not login!!",Toast.LENGTH_SHORT).show()
        //    mainActivity?.errorDialog?.show("","You are not login")

            if(mainActivity!!.prefManager.getBoolean(Constant.KEY_USER_LOGIN_STATUS)){
                card_login.visibility = View.GONE
            }else{
                card_login.visibility = View.VISIBLE
                hotelList.clear()
                adapter.notifyDataSetChanged()
            }
            return
        }
        val param = JSONObject()
        param.put("api_key", Constant.API_KEY)
        param.put("user_id", mainActivity?.prefManager?.getString(Constant.KEY_USER_ID))

       //  mainActivity.progress?.show()

        if(!swipe_to_refresh.isRefreshing){
            mainActivity?.showProgress(frame_loader)

        }
        hotelList.clear()
        ServiceRequest(object : ApiResponseListener {
            override fun onCompleted(`object`: Any) {

                mainActivity?.hideProgress(frame_loader,swipe_to_refresh)

                try {
                    val json = JSONObject(`object`.toString())

                    if (json.has("data")) {
                        val array = json.getJSONArray("data")
                        val parser = HotelParser()

                        for (i in 0 until array.length()) {

                            val property = parser.parse(array.getJSONObject(i))

                            if(property.isEnable){
                                hotelList.add(property)
                            }

                        }

                        adapter.notifyDataSetChanged()
                    }
                } catch (e: Exception) {

                    Log.e(TAG, " ss " + e.message)
                    e.printStackTrace()
                }finally {
                    mainActivity?.showEmptyView(frame_empty,(hotelList.size== 0))
                }
            }

            override fun onError(errorMessage: String) {
               // mainActivity.progress?.dismiss()
                mainActivity?.hideProgress(frame_loader,swipe_to_refresh)
                mainActivity?.showEmptyView(frame_empty,(hotelList.size== 0))
               // Toast.makeText(activity!!, "Failed to get List", Toast.LENGTH_SHORT).show()
            }

        }).getFavList(param)


    }

    private fun removeFav(position: Int) {

        val param = JSONObject()
        param.put("api_key", Constant.API_KEY)
        param.put("user_id", mainActivity?.prefManager!!.getString(Constant.KEY_USER_ID))
        param.put("hotel_id", hotelList[position].id)

        mainActivity?.progress?.show()

        ServiceRequest(object : ApiResponseListener {
            override fun onCompleted(`object`: Any) {
                mainActivity?.progress?.dismiss()

                try {
                    doAsync {
                        AppDatabase.getAppDatabase().favPostDao().deleteFavPost(hotelList[position].id)
                        uiThread {
                            hotelList.removeAt(position)
                            adapter.notifyDataSetChanged()
                        }
                    }
                } catch (e: Exception) {
                    mainActivity?.progress?.dismiss()
                    Log.e(TAG, " ss " + e.message)
                    e.printStackTrace()
                }finally {
                    mainActivity?.showEmptyView(frame_empty,(hotelList.size== 0))
                }
            }

            override fun onError(errorMessage: String) {
                mainActivity?.progress?.dismiss()
                mainActivity?.errorDialog?.show(getString(R.string.hint_error),"$errorMessage")
            }

        }).removeToFav(param)
    }

}
