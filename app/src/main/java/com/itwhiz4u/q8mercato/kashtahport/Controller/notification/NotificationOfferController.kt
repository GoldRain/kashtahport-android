package com.itwhiz4u.q8mercato.kashtahport.Controller.notification

import android.content.Context
import com.itwhiz4u.q8mercato.kashtahport.adapter.NotificationListAdapter
import com.itwhiz4u.q8mercato.kashtahport.model.NotificationModel

class NotificationOfferController(val context: Context, val  notificationListAdapter: NotificationListAdapter, val holder: NotificationListAdapter.MyOfferViewHolder, val  notificationModel: NotificationModel, val listener: NotificationListAdapter.OnItemClick, val position: Int) {
    fun control(){


        holder.itemView!!.setOnClickListener {

            listener.onItemClick(position)
        }
    }
}