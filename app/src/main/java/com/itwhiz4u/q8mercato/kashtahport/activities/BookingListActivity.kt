package com.itwhiz4u.q8mercato.kashtahport.activities

import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.support.v4.view.ViewPager
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import android.widget.Toast
import com.itwhiz4u.q8mercato.kashtahport.Api.ApiResponseListener
import com.itwhiz4u.q8mercato.kashtahport.Api.ServiceRequest
import com.itwhiz4u.q8mercato.kashtahport.R
import com.itwhiz4u.q8mercato.kashtahport.adapter.HotelBookingRequestAdapter
import com.itwhiz4u.q8mercato.kashtahport.adapter.ViewPagerAdapter
import com.itwhiz4u.q8mercato.kashtahport.fragment.booking.BookingHistoryFragment
import com.itwhiz4u.q8mercato.kashtahport.fragment.booking.BookingListFragment
import com.itwhiz4u.q8mercato.kashtahport.helper.Constant
import com.itwhiz4u.q8mercato.kashtahport.model.EventModel
import com.itwhiz4u.q8mercato.kashtahport.model.HotelBookingModel
import com.itwhiz4u.q8mercato.kashtahport.parser.BookingParser
import kotlinx.android.synthetic.main.activity_booking_list.*
import kotlinx.android.synthetic.main.view_header_toolbar.*
import org.greenrobot.eventbus.EventBus
import org.json.JSONObject
import java.lang.Exception

class BookingListActivity : BaseActivity() {


   lateinit var adapter: HotelBookingRequestAdapter
   lateinit var viewPagerAdater: ViewPagerAdapter

    var hotelList = ArrayList<HotelBookingModel>()

    lateinit var bookingListFragment: BookingListFragment
    lateinit var bookingHistoryFragment: BookingHistoryFragment

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_booking_list)
        supportActionBar?.hide()

        hotelList = ArrayList()

        header_name.setText(getString(R.string.header_booking))
        icon_back.visibility = View.VISIBLE
        icon_back.setOnClickListener {
            super.onBackPressed()
        }
        initViewPager()
       // initList()
    }

    private fun initViewPager() {
        viewPagerAdater = ViewPagerAdapter(supportFragmentManager)

        bookingListFragment = BookingListFragment()
        bookingListFragment.setActivity(this)

        bookingHistoryFragment = BookingHistoryFragment()
        bookingHistoryFragment.setActivity(this)

        viewPagerAdater.addFragment(bookingListFragment,getString(R.string.bl_hint_booking_list))
        viewPagerAdater.addFragment(bookingHistoryFragment,getString(R.string.bl_hint_booking_history))

        view_pager.adapter = viewPagerAdater
        tab_layout.setupWithViewPager(view_pager)

        view_pager.addOnPageChangeListener(object :ViewPager.OnPageChangeListener{
            override fun onPageScrollStateChanged(p0: Int) {

            }

            override fun onPageScrolled(p0: Int, p1: Float, p2: Int) {

            }

            override fun onPageSelected(p0: Int) {
                EventBus.getDefault().post(EventModel(Constant.EVENT_UPDATE_BOOKING))
            }

        })

    }

    private fun initList() {


        adapter = HotelBookingRequestAdapter(this,hotelList,object : HotelBookingRequestAdapter.OnItemClick() {
            override fun onItemClick(position: Int) {

            }

            override fun onUpdateBooking(position: Int, status: String) {
                updateStatus(position,status)
            }
        })

        recycler_view.layoutManager = LinearLayoutManager(this)

        val item = DividerItemDecoration(this, DividerItemDecoration.VERTICAL)
        item.setDrawable(ContextCompat.getDrawable(this,R.drawable.list_div)!!)
        recycler_view.addItemDecoration(item)
        recycler_view.adapter = adapter

        adapter.notifyDataSetChanged()


        icon_back.visibility = View.VISIBLE
        icon_back.setOnClickListener {
            super.onBackPressed()
        }

        swipe_to_refresh.setOnRefreshListener {
            if(swipe_to_refresh.isRefreshing){
                getBookings()
            }
        }
        getBookings()

    }


    fun getBookings(){
        val param = JSONObject()
        param.put("api_key", Constant.API_KEY)
        param.put("user_id", prefManager.getString(Constant.KEY_USER_ID))

        if(!swipe_to_refresh.isRefreshing){
            showProgress(frame_loader)
        }

        hotelList.clear()
        ServiceRequest(object : ApiResponseListener {
            override fun onCompleted(`object`: Any) {
               // progress?.dismiss()

                hideProgress(frame_loader,swipe_to_refresh)
                try {
                    val json = JSONObject(`object`.toString())
                    if(json.has("data")){
                        val array  = json.getJSONArray("data")
                        val parser  = BookingParser()
                        for(i in 0 until array.length()) {
                            val bookingModel = parser.parse(array.getJSONObject(i))
                            hotelList.add(bookingModel)
                        }

                        adapter.notifyDataSetChanged()
                    }
                }catch (e: Exception){
                    e.printStackTrace()
                }finally {
                    showEmptyView(frame_empty,(hotelList.size == 0))
                }
            }

            override fun onError(errorMessage: String) {
                hideProgress(frame_loader,swipe_to_refresh)
                showEmptyView(frame_empty,(hotelList.size == 0))
                Toast.makeText(this@BookingListActivity,"$errorMessage", Toast.LENGTH_SHORT).show()
            }

        }).getBookingList(param)
    }

    fun updateStatus(position: Int, status: String) {
        if(hotelList[position].hotelModel!!.isDelete){
            errorDialog?.show(getString(R.string.hint_error),getString(R.string.propety_deleted))
            return
        }

        val param = JSONObject()
        param.put("api_key", Constant.API_KEY)
        param.put("booking_id", hotelList[position].id)
        param.put("status", status+"")
        progress?.show()

        ServiceRequest(object : ApiResponseListener {
            override fun onCompleted(`object`: Any) {
                progress?.dismiss()

                try {

                }catch (e: Exception){
                    e.printStackTrace()
                }
            }

            override fun onError(errorMessage: String) {
                progress?.dismiss()

                Toast.makeText(this@BookingListActivity,"$errorMessage", Toast.LENGTH_SHORT).show()
            }

        }).updateBooking(param)
    }
}
