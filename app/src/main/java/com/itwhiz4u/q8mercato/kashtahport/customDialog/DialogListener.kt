package com.itwhiz4u.q8mercato.kashtahport.customDialog

abstract class DialogListener {

    open fun noClick(){}
    open fun yesClick(){}
    open fun okClick(){}
    open fun okClick(any: Any){}
}