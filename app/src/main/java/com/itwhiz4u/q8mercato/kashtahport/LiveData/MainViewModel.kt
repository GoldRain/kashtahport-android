package com.itwhiz4u.q8mercato.kashtahport.LiveData

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import android.support.v4.app.Fragment
import com.itwhiz4u.q8mercato.kashtahport.fragment.main.*
import com.itwhiz4u.q8mercato.kashtahport.helper.Constant

class MainViewModel:ViewModel() {
    private var fragments: MutableLiveData<ArrayList<Fragment>>? = null

    fun getMainFragments(){
        fragments = MutableLiveData<ArrayList<Fragment>>()

        val list = ArrayList<Fragment>()
        val homeFragment = HomeFragment()
        homeFragment.fragPosition = Constant.HOME_POSITION

        val favFragment = FavFragment()
        favFragment.fragPosition = Constant.FAV_POSITION

        val notificationFragment = NotificationFragment()
        notificationFragment.fragPosition = Constant.NOTIFICATION_POSITION

        val profileFragment = ProfileFragment()
        profileFragment.fragPosition = Constant.PROFILE_POSITION

        val settingFragment = SettingFragment()
        settingFragment.fragPosition = Constant.SETTING_POSITION

        list.add(homeFragment)
        list.add(favFragment)
        list.add(notificationFragment)
        list.add(profileFragment)
        list.add(settingFragment)

        fragments?.value = list
    }
}