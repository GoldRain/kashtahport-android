package com.itwhiz4u.q8mercato.kashtahport.activities

import android.content.Intent
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import android.widget.AdapterView
import com.itwhiz4u.q8mercato.kashtahport.Api.ApiResponseListener
import com.itwhiz4u.q8mercato.kashtahport.Api.PaymentRequest
import com.itwhiz4u.q8mercato.kashtahport.Api.ServiceRequest
import com.itwhiz4u.q8mercato.kashtahport.R
import com.itwhiz4u.q8mercato.kashtahport.adapter.SpinnerSortListAdapter
import com.itwhiz4u.q8mercato.kashtahport.adapter.TransactionListAdapter
import com.itwhiz4u.q8mercato.kashtahport.helper.Constant
import com.itwhiz4u.q8mercato.kashtahport.model.TransactionModel
import com.itwhiz4u.q8mercato.kashtahport.parser.BookingParser
import com.itwhiz4u.q8mercato.kashtahport.parser.TransactionParser
import kotlinx.android.synthetic.main.activity_transaction_list.*
import kotlinx.android.synthetic.main.view_header_toolbar.*
import org.json.JSONObject
import java.lang.Exception
import java.util.*
import kotlin.Comparator

class TransactionListActivity : BaseActivity() {

    lateinit var adapter: TransactionListAdapter
    val mList = ArrayList<TransactionModel>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_transaction_list)
        supportActionBar?.hide()

        header_name.setText(getString(R.string.tl_hint_header_name))
        icon_back.visibility = View.VISIBLE
        icon_back.setOnClickListener {
            super.onBackPressed()
        }

        adapter = TransactionListAdapter(this, mList, object : TransactionListAdapter.OnItemClick() {
            override fun onItemClick(position: Int) {

                getBookingDetails(position)
            }

        })

        recycler_view.layoutManager = LinearLayoutManager(this)
        val item = DividerItemDecoration(this, DividerItemDecoration.VERTICAL)
        item.setDrawable(ContextCompat.getDrawable(this, R.drawable.list_div)!!)
        recycler_view.adapter = adapter


        swipe_to_refresh.setOnRefreshListener {
            if (swipe_to_refresh.isRefreshing) {
                getTransaction()
            }
        }

        initSpinner()

        getTransaction()
    }

    private fun initSpinner() {
        var arrayProperty = resources.getStringArray(R.array.sort_tran_array)
        val sortList = arrayProperty.asList()

        val adapterSort = SpinnerSortListAdapter(this, sortList)

        spinner_sort.adapter = adapterSort
        spinner_sort.setSelection(0)

        spinner_sort.onItemSelectedListener = object :AdapterView.OnItemSelectedListener{
            override fun onNothingSelected(parent: AdapterView<*>?) {

            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                if(position == 0){
                    Collections.sort(mList,SortDate())
                    adapter.notifyDataSetChanged()
                }else{
                    Collections.sort(mList,SortPrice())
                    adapter.notifyDataSetChanged()
                }
            }
        }

    }

    fun getTransaction() {

        if (!prefManager!!.getBoolean(Constant.KEY_USER_LOGIN_STATUS)) {
            hideProgress(frame_loader, swipe_to_refresh)
        //    Toast.makeText(this, "You are not login!!", Toast.LENGTH_SHORT).show()
            errorDialog?.show(getString(R.string.hint_error),getString(R.string.hint_not_login))
            return
        }

        val param = JSONObject()
        param.put("api_key", Constant.API_KEY)
        param.put("buyer_id", prefManager!!.getString(Constant.KEY_USER_ID))
        //  param.put("hotel_id", hotelList[position].id)

        if (!swipe_to_refresh.isRefreshing) {
            showProgress(frame_loader)
        }

        mList.clear()
        PaymentRequest(object : ApiResponseListener {
            override fun onCompleted(`object`: Any) {
                hideProgress(frame_loader, swipe_to_refresh)

                try {
                    val json = JSONObject(`object`.toString())

                    if (json.has("data")) {
                        val array = json.getJSONArray("data")
                        val parser = TransactionParser()

                        for (i in 0 until array.length()) {
                            val model = parser.parse(array.getJSONObject(i))
                            mList.add(model)

                        }
                        Collections.sort(mList,SortDate())
                        adapter.notifyDataSetChanged()
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                } finally {
                    showEmptyView(frame_empty, (mList.size == 0))
                }
            }

            override fun onError(errorMessage: String) {
                hideProgress(frame_loader, swipe_to_refresh)
                showEmptyView(frame_empty, (mList.size == 0))
                errorDialog?.show(getString(R.string.hint_error), "Failed $errorMessage")
            }

        }).getPaymentList(param)
    }


    private fun getBookingDetails(position: Int) {
        progress?.show()
        val param = JSONObject()
        param.put("api_key", Constant.API_KEY)
        // param.put("user_id", mainActivity?.prefManager!!.getString(Constant.KEY_USER_ID))
        param.put("booking_id", mList[position].booking_id)

        ServiceRequest(object : ApiResponseListener {
            override fun onCompleted(`object`: Any) {
                progress?.dismiss()
                try {
                    val json = JSONObject(`object`.toString())
                    if (json.has("data")) {
                        val array = json.getJSONArray("data")
                        val parser = BookingParser()
                        for (i in 0 until array.length()) {
                            val bookingModel = parser.parse(array.getJSONObject(i))
                            val hotelModel = bookingModel.hotelModel
                            val intent = Intent(this@TransactionListActivity, TransactionActivity::class.java)
                            intent.putExtra(Constant.KEY_BOOKING, bookingModel)
                            intent.putExtra(Constant.KEY_TRANSACTION, mList[position])
                            startActivity(intent)

                        }
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

            override fun onError(errorMessage: String) {
                progress?.dismiss()
                errorDialog?.show(getString(R.string.hint_error), "$errorMessage")
            }

        }).getBookingDetails(param)
        /*try {
                    val hotelModel = model.model as HotelModel
                    if(hotelModel.ownerId.equals(mainActivity!!.prefManager!!.getString(Constant.KEY_USER_ID))){
                        if(model.message.toLowerCase().contains("got a booking")){

                        }else{

                        }
                    }else{
                        val intent = Intent(activity!!, BookingDetailsActivity::class.java)
                        // intent.putExtra(Constant.KEY_BOOKING,hotelList[position])
                        intent.putExtra(Constant.KEY_BOOK_CAN,true)
                        startActivity(intent)
                    }
                }catch (e:Exception){
                    mainActivity.errorDialog!!.show("Error!!","Booking not found")
                }*/

    }

    inner class SortPrice:Comparator<TransactionModel>{
        override fun compare(o1: TransactionModel?, o2: TransactionModel?): Int {
          return  if(o1!!.amount.toLong() >o2!!.amount.toLong())1 else -1
        }

    }

    inner class SortDate:Comparator<TransactionModel>{
        override fun compare(o1: TransactionModel?, o2: TransactionModel?): Int {
            return  if(o1!!.created_at.toLong() >o2!!.created_at.toLong())-1 else 1
        }

    }
}
