package com.itwhiz4u.q8mercato.kashtahport.Api

interface ApiResponseListener {

    fun onCompleted(`object`: Any)
    fun onError(errorMessage: String)

}