package com.itwhiz4u.q8mercato.kashtahport.activities

import android.content.Intent
import android.graphics.Bitmap
import android.os.Bundle
import android.text.TextUtils
import android.view.View
import android.widget.Toast
import com.itwhiz4u.q8mercato.kashtahport.Api.ApiResponseListener
import com.itwhiz4u.q8mercato.kashtahport.Api.ServiceRequest
import com.itwhiz4u.q8mercato.kashtahport.R
import com.itwhiz4u.q8mercato.kashtahport.helper.Constant
import com.itwhiz4u.q8mercato.kashtahport.helper.PermissionStatus
import com.itwhiz4u.q8mercato.kashtahport.helper.PickImageHelper
import com.itwhiz4u.q8mercato.kashtahport.upload.UploadAWS
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.RequestOptions
import com.bumptech.glide.request.target.Target
import com.github.florent37.runtimepermission.kotlin.askPermission
import com.itwhiz4u.q8mercato.kashtahport.countryPicker.Utils
import com.mazadlive.Interface.ProgressListener
import kotlinx.android.synthetic.main.activity_edit_profile.*
import kotlinx.android.synthetic.main.view_header_toolbar.*
import org.json.JSONObject
import java.lang.Exception
import java.util.*

class EditProfileActivity : BaseActivity() {

    var pickImageHelper:PickImageHelper?= null
    var bitmap:Bitmap?= null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_edit_profile)
        supportActionBar?.hide()

        header_name.setText(getString(R.string.hint_edit_profile))
        icon_back.visibility = View.VISIBLE
        icon_back.setOnClickListener {
            super.onBackPressed()
        }

        pickImageHelper = PickImageHelper(this,null,true,ImagePicked())
        frame_update.setOnClickListener {
            if(validation()){
                progress?.show()

                if(!TextUtils.isEmpty(imageFilePath)){
                    uploadToServer()
                }else{
                    updateProfile()
                }
            }
        }

        profile.setOnClickListener {
            add_profile.performClick()
        }

        add_profile.setOnClickListener {
            val status = PermissionStatus().getPermissionStatus(this, PermissionStatus.PERMISSION_STORAGE)
           // val statusCam = PermissionStatus().getPermissionStatus(this, PermissionStatus.PERMISSION_CAMERA)

            if(status != PermissionStatus.BLOCKED_OR_NEVER_ASKED){
                askPermission(PermissionStatus.PERMISSION_STORAGE){
                    pickImageHelper!!.pickImage(PickImageHelper.OPEN_FOR_OPTION)

                }.onDeclined {
                    if(it.hasForeverDenied()){
                        Toast.makeText(this,getString(R.string.per_war_storage),Toast.LENGTH_SHORT).show()

                        errorDialog?.show(getString(R.string.hint_error),"${getString(R.string.per_war_storage)}")
                    }
                }
            }
        }

        setData()
    }

    var upload:UploadAWS?= null
    private fun uploadToServer() {
       upload = UploadAWS(this,imageFilePath,UploadAWS.UPLOAD_TYPE_USER)

        upload?.uploadToAws(object :ProgressListener{
            override fun onProgressUpdate(progress: Int) {

            }

            override fun onSuccess(path: String) {


                updateProfile(path)
            }

            override fun onFileNotFound() {
                updateProfile()
               Toast.makeText(this@EditProfileActivity,getString(R.string.prof_ed_hint_file_not_found),Toast.LENGTH_SHORT).show()
            }

            override fun onCancel() {
                updateProfile()
                Toast.makeText(this@EditProfileActivity,getString(R.string.prof_ed_hint_can_uploading),Toast.LENGTH_SHORT).show()
            }

        })
    }

    private fun validation(): Boolean {

        if(TextUtils.isEmpty(et_username.text.toString().trim())){

            et_username.error = getString(R.string.prof_ed_hint_user_empty)
            return false
        }
        return true
    }


    private fun setData() {
        et_username.setText(prefManager.getString(Constant.KEY_USER_NAME))
        et_email.setText(prefManager.getString(Constant.KEY_USER_EMAIL))
        et_code.setText(prefManager.getString(Constant.KEY_USER_CODE))
        et_phone.setText(prefManager.getString(Constant.KEY_USER_PHONE))
        et_profession.setText(prefManager.getString(Constant.KEY_USER_PROFESSION))
        et_des.setText(prefManager.getString(Constant.KEY_USER_DESCRIPTION))

        val imageUrl = prefManager.getString(Constant.KEY_USER_IMAGE_URL)

        if(!TextUtils.isEmpty(imageUrl)){
            Glide.with(this)
                    .load(imageUrl)
                    .apply(RequestOptions().error(R.drawable.user_default_pic).override(200))
                    .into(profile)
        }

        /*if(et_code.text.toString().equals(Constant.DEFAULT_CODE)){
            et_code.setCompoundDrawablesRelativeWithIntrinsicBounds(R.drawable.flag_ar,0,0,0)
        }else{
            et_code.setCompoundDrawablesRelativeWithIntrinsicBounds(R.drawable.ic_globe_black,0,0,0)
        }*/

        val countries = Utils.parseCountries(Utils.getCountriesJSON(this)!!)

        for (i in countries.indices){
            val country = countries[i]
            if(country.dialingCode.equals(et_code.text.toString())){

                et_code.setText("+ "+country.dialingCode)
                val drawableName = country.isoCode.toLowerCase(Locale.ENGLISH) + "_flag"
                im_flag.setImageResource(Utils.getMipmapResId(this, drawableName))
                break
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        pickImageHelper!!.onActivityResult(requestCode,resultCode,data)
    }

    fun updateProfile(url:String = ""){
        val param = JSONObject()
        param.put("api_key", Constant.API_KEY)
        param.put("user_id", prefManager.getString(Constant.KEY_USER_ID))
        param.put("name", et_username.text.toString().trim())
        param.put("phone", prefManager.getString(Constant.KEY_USER_PHONE))

        if(!TextUtils.isEmpty(url)){
            param.put("profile_image_url",url)
        }

        if(!TextUtils.isEmpty(et_des.text.toString().trim())){
            param.put("description", et_des.text.toString())
        }

        if(!TextUtils.isEmpty(et_profession.text.toString().trim())){
            param.put("profession", et_profession.text.toString())
        }

        ServiceRequest(object :ApiResponseListener{
            override fun onCompleted(`object`: Any) {
                progress?.dismiss()

                Toast.makeText(this@EditProfileActivity,getString(R.string.prof_ed_hint_prof_updated),Toast.LENGTH_SHORT).show()
                try{
                    val json = JSONObject(`object`.toString())
                    if(json.has("data")){
                        var obj  = JSONObject()
                        if(json.get("data") is JSONObject){
                            obj = json.getJSONObject("data")

                           if(obj.has("name")){
                               val name = obj.getString("name")
                               prefManager.setString(Constant.KEY_USER_NAME,name)
                           }

                            if(obj.has("description")){
                                val des = obj.getString("description")
                                prefManager.setString(Constant.KEY_USER_DESCRIPTION,des)
                            }

                            if(obj.has("profession")){
                                val des = obj.getString("profession")
                                prefManager.setString(Constant.KEY_USER_PROFESSION,des)
                            }


                            if(obj.has("profile_image_url")){
                                upload?.deleteImage(prefManager?.getString(Constant.KEY_USER_IMAGE_URL),UploadAWS.UPLOAD_TYPE_USER)
                                val des = obj.getString("profile_image_url")
                                prefManager.setString(Constant.KEY_USER_IMAGE_URL,des)
                            }
                        }
                        imageFilePath = ""
                        finish()
                    }

                }catch (e:Exception){
                    e.printStackTrace()
                }


            }

            override fun onError(errorMessage: String) {
                progress?.dismiss()

                errorDialog?.show(getString(R.string.hint_error),"$errorMessage")
            }

        }).updateProfile(param)
    }


    var imageFilePath = ""
    inner class ImagePicked:PickImageHelper.OnImagePickerListener{
        override fun onImagePicked(imagePath: String, requestCode: Int) {

            if (requestCode == Constant.CAMERA_IMAGE_REQUEST) {
                imageFilePath = imagePath
                //  mProfileImage.setPadding(0, 0, 0, 0)
                // bitmap = getBitmapFromPath(imagePath)
                Glide.with(this@EditProfileActivity).asBitmap().load(imagePath).apply(RequestOptions().override(200)).listener(object : RequestListener<Bitmap> {
                    override fun onLoadFailed(e: GlideException?, model: Any?, target: Target<Bitmap>?, isFirstResource: Boolean): Boolean {
                        return false
                    }

                    override fun onResourceReady(resource: Bitmap?, model: Any?, target: Target<Bitmap>?, dataSource: DataSource?, isFirstResource: Boolean): Boolean {
                        bitmap = resource
                        //bitmapString = Constant.getStringFromBitmap(bitmap!!)
                        profile.setImageBitmap(resource)
                        return true
                    }

                }).into(profile)

            } else if (requestCode == Constant.GALLARY_IMAGE_REQUEST) {
                // bitmap = getBitmapFromPath(imagePath)
                imageFilePath = imagePath
                // mProfileImage.setPadding(0, 0, 0, 0)
                Glide.with(this@EditProfileActivity).asBitmap().load(imagePath).apply(RequestOptions().override(200)).listener(object : RequestListener<Bitmap> {
                    override fun onLoadFailed(e: GlideException?, model: Any?, target: Target<Bitmap>?, isFirstResource: Boolean): Boolean {
                        return false
                    }

                    override fun onResourceReady(resource: Bitmap?, model: Any?, target: Target<Bitmap>?, dataSource: DataSource?, isFirstResource: Boolean): Boolean {
                        bitmap = resource
                        // bitmapString = Constant.getStringFromBitmap(bitmap!!)
                        // mProfileImage.setImageBitmap(resource)
                        profile.setImageBitmap(resource)
                        //writeToFile(bitmapString)
                        return true
                    }

                }).into(profile)
            }
        }

    }


}
