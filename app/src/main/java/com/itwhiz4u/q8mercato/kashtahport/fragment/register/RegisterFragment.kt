package com.itwhiz4u.q8mercato.kashtahport.fragment.register


import android.app.Activity
import android.content.Intent
import android.graphics.Bitmap
import android.os.Build
import android.os.Bundle
import android.support.annotation.RequiresApi
import android.support.v4.app.Fragment
import android.text.Editable
import android.text.TextUtils
import android.text.TextWatcher
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.itwhiz4u.q8mercato.kashtahport.Api.ApiResponseListener
import com.itwhiz4u.q8mercato.kashtahport.Api.ServiceRequest

import com.itwhiz4u.q8mercato.kashtahport.R
import com.itwhiz4u.q8mercato.kashtahport.activities.CodeVerificationActivity
import com.itwhiz4u.q8mercato.kashtahport.activities.MainActivity
import com.itwhiz4u.q8mercato.kashtahport.activities.RegisterActivity
import com.itwhiz4u.q8mercato.kashtahport.helper.*
import com.itwhiz4u.q8mercato.kashtahport.upload.UploadAWS
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.RequestOptions
import com.bumptech.glide.request.target.Target
import com.github.florent37.runtimepermission.kotlin.askPermission
import com.itwhiz4u.q8mercato.kashtahport.activities.TermCondActivity
import com.itwhiz4u.q8mercato.kashtahport.countryPicker.Country
import com.itwhiz4u.q8mercato.kashtahport.countryPicker.CountryPickerCallbacks
import com.itwhiz4u.q8mercato.kashtahport.countryPicker.CountryPickerDialog
import com.itwhiz4u.q8mercato.kashtahport.countryPicker.Utils
import com.mazadlive.Interface.ProgressListener
import kotlinx.android.synthetic.main.fragment_register.*
import kotlinx.android.synthetic.main.fragment_register.view.*
import kotlinx.android.synthetic.main.view_user_profile.*
import kotlinx.android.synthetic.main.view_user_profile.view.*
import org.json.JSONObject
import java.lang.Exception
import java.util.*

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 *
 */
class RegisterFragment : Fragment() {


    lateinit var fragView :View
    lateinit var registerActivity: RegisterActivity

    fun setActivity(registerActivity: RegisterActivity){
        this.registerActivity = registerActivity
    }

    var pickImageHelper:PickImageHelper?= null
    var bitmap:Bitmap?= null


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
      
        return inflater.inflate(R.layout.fragment_register, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initView()
    }
    
    private  fun initView(){
        pickImageHelper = PickImageHelper(activity!!,this,false,ImagePicked())

        val countries = Utils.parseCountries(Utils.getCountriesJSON(activity)!!)

        for (i in countries.indices){
            val country = countries[i]
            if(country.dialingCode.equals(Constant.DEFAULT_CODE)){

                et_code.setText("+ "+country.dialingCode)
                val drawableName = country.isoCode.toLowerCase(Locale.ENGLISH) + "_flag"
                im_flag.setImageResource(Utils.getMipmapResId(activity!!, drawableName))
                break
            }
        }
        frame_register.setOnClickListener {
            if(validation()){
                registerActivity?.progress!!.show()
                registerUser()
            }

        }

        frame_cCode.setOnClickListener {
            val cDialog = CountryPickerDialog(activity!!,object :CountryPickerCallbacks{
                override fun onCountrySelected(country: Country?, flagResId: Int) {
                    Log.e("CountryPickerDialog"," *** "+country!!.isoCode+" "+country.dialingCode+" "+flagResId)
                    et_code.setText("+ "+country.dialingCode)
                    im_flag.setImageResource(flagResId)
                }
            })

            cDialog.show()
        }

        et_code.setOnClickListener {
            frame_cCode.performClick()
        }

        login.setOnClickListener {
            registerActivity.onBackPressed()
        }

        login1.setOnClickListener {
            registerActivity.onBackPressed()
        }


        profile.setOnClickListener {
            add_profile.performClick()
        }

        add_profile.setOnClickListener {
            val status = PermissionStatus().getPermissionStatus(registerActivity,PermissionStatus.PERMISSION_STORAGE)

            if(status != PermissionStatus.BLOCKED_OR_NEVER_ASKED){
                askPermission(PermissionStatus.PERMISSION_STORAGE){
                    pickImageHelper!!.pickImage(4)

                }.onDeclined {
                    if(it.hasForeverDenied()){
                        Toast.makeText(activity!!,getString(R.string.per_war_storage),Toast.LENGTH_SHORT).show()
                    }
                }
            }
        }

        term_condition.setOnClickListener {
            val intent = Intent(activity!!, TermCondActivity::class.java)
            startActivity(intent)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if(requestCode ==Constant.OTP_REQUEST && resultCode == Activity.RESULT_OK) {
            registerActivity.setResult(Activity.RESULT_OK)
            registerActivity?.finish()
        }

        pickImageHelper!!.onActivityResult(requestCode,resultCode,data)

    }
    private fun validation(): Boolean {

        if(TextUtils.isEmpty(et_username.text.toString().trim())){

          //  et_username.error = getString(R.string.hint_reg_error_username)
            registerActivity?.errorDialog?.show(getString(R.string.hint_error), getString(R.string.hint_reg_error_username))
            return false
        }

        if(TextUtils.isEmpty(et_code.text.toString().trim())){
           // et_code.error = getString(R.string.hint_reg_error_country_code)
            registerActivity?.errorDialog?.show(getString(R.string.hint_error), getString(R.string.hint_reg_error_country_code))
            return false
        }

        if(TextUtils.isEmpty(et_phone.text.toString().trim())){
            //et_phone.error = getString(R.string.hint_reg_error_phone)
            registerActivity?.errorDialog?.show(getString(R.string.hint_error), getString(R.string.hint_reg_error_phone))
            return false
        }

        if(TextUtils.isEmpty(et_email.text.toString().trim())){
            //et_email.error = getString(R.string.hint_reg_error_email)
            registerActivity?.errorDialog?.show(getString(R.string.hint_error), getString(R.string.hint_reg_error_email))
            return false
        }

        if(TextUtils.isEmpty(et_password.text.toString().trim())){
           // et_password.error = getString(R.string.hint_reg_error_psd)
            registerActivity?.errorDialog?.show(getString(R.string.hint_error), getString(R.string.hint_reg_error_psd))
            return false
        }else{
            val len = et_password.text.toString().trim().length
            if(len <8){
              //  et_password.error = getString(R.string.hint_reg_error_weak_psd)
                registerActivity?.errorDialog?.show(getString(R.string.hint_error), getString(R.string.hint_reg_error_weak_psd))
                return false
            }
        }

        if(TextUtils.isEmpty(et_cnf_password.text.toString().trim())){
          //  et_cnf_password.error = getString(R.string.hint_reg_error_psd_cnf)
            registerActivity?.errorDialog?.show(getString(R.string.hint_error), getString(R.string.hint_reg_error_psd_cnf))
            return false
        }else{
            val len = et_cnf_password.text.toString().trim().length
            if(len <8){
               // et_cnf_password.error = getString(R.string.hint_reg_error_weak_psd)
                registerActivity?.errorDialog?.show(getString(R.string.hint_error), getString(R.string.hint_reg_error_weak_psd))
                return false
            }else{
                val psd = et_password.text.toString()
                if(!psd.equals(et_cnf_password.text.toString().trim())){
                   // et_cnf_password.error = getString(R.string.hint_reg_error_psd_match)
                    registerActivity?.errorDialog?.show(getString(R.string.hint_error), getString(R.string.hint_reg_error_psd_match))
                    return false
                }
            }
        }


        if(!cond_check.isChecked ){
            registerActivity?.errorDialog?.show(getString(R.string.hint_error), getString(R.string.ap_hint_accept_term))
            return false
        }
        return true
    }

    /*api_key:T0BAnEzf6CaWwIMulImtA05nT6fDVjXKHoJM136S
email:chandanray@gmail.com
name:Ray
phone:9074452230
password:123456
profile_image_url:www.asdf.com*/


    private fun uploadToServer() {
        val upload = UploadAWS(activity!!,imageFilePath, UploadAWS.UPLOAD_TYPE_USER)

        upload.uploadToAws(object : ProgressListener {
            override fun onProgressUpdate(progress: Int) {

            }

            override fun onSuccess(path: String) {

                registerUser(path)
            }

            override fun onFileNotFound() {
                registerUser()
                Toast.makeText(activity!!,"File Not Found!!",Toast.LENGTH_SHORT).show()
            }

            override fun onCancel() {
                registerUser()
                Toast.makeText(activity!!,"Cancel Image Uploading!!",Toast.LENGTH_SHORT).show()
            }

        })
    }

    private fun registerUser(url:String = "") {

        val name = et_username.text.toString().trim()
        var code = et_code.text.toString().trim()
        val phone = et_phone.text.toString().trim()
        val email = et_email.text.toString().trim()
        val psd = et_password.text.toString().trim()

        code = code.replace("+","")
        //val cnf_psd = et_cnf_password.text.toString().trim()

        val param = JSONObject()
        param.put("api_key",Constant.API_KEY)
        param.put("email",email)
        param.put("name",name)
        param.put("phone",phone)
        param.put("country_code",code)
        param.put("password",psd)
        param.put("profile_image_url",url)


        val paramOtp = JSONObject()
        paramOtp.put("api_key",Constant.API_KEY)
        paramOtp.put("phone",phone)
        paramOtp.put("country_code",code)
        paramOtp.put("email",email)

        ServiceRequest(object :ApiResponseListener{
            override fun onCompleted(`object`: Any) {
                registerActivity.progress?.dismiss()
                try {

                    val jsonObject = JSONObject(`object`.toString())

                    if(!jsonObject.getBoolean("error")){
                        Toast.makeText(activity!!,jsonObject.getString("message"),Toast.LENGTH_SHORT).show()

                        /*if(jsonObject.has("data")){
                            val user = UserParser().parse(jsonObject.getJSONObject("data"))
                            ConstantFunction.saveUserDataToPref(user)
                            MyApplication.instance.prefManager.setBoolean(Constant.KEY_USER_LOGIN_STATUS,true)
                        }
                        moveNext()*/

                        val intent = Intent(activity!!, CodeVerificationActivity::class.java)
                        intent.putExtra("canRegister",true)
                        intent.putExtra("param",param.toString())
                        intent.putExtra("paramOtp", paramOtp.toString())
                        intent.putExtra("phone",param.get("phone").toString())
                        intent.putExtra("otpCall", true)
                        intent.putExtra("image",imageFilePath)
                        intent.putExtra(Constant.KEY_OPEN_FOR,"${registerActivity.openFor}")
                        intent.putExtra(Constant.KEY_OPEN_FROM_MAIN,registerActivity.fromMain)
                        startActivityForResult(intent,Constant.OTP_REQUEST)
                    }

                }catch (e:Exception){
                    e.printStackTrace()

                    registerActivity.errorDialog?.show(getString(R.string.hint_error),getString(R.string.error_connect_to_server))
                }
            }

            override fun onError(errorMessage: String) {
                //Toast.makeText(activity!!,"Registration failed $errorMessage",Toast.LENGTH_SHORT).show()
                registerActivity.errorDialog?.show(getString(R.string.hint_error),"$errorMessage")
                registerActivity.progress?.dismiss()
            }

        }).sendOtp(paramOtp)

    }

    private fun moveNext() {

        val intent = Intent(registerActivity,MainActivity::class.java)
        intent.putExtra(Constant.KEY_CAN_SELL,registerActivity.canSell)
        intent.putExtra(Constant.KEY_CAN_BOOK,registerActivity.canBook)
        intent.putExtra(Constant.KEY_OPEN_FOR,"${registerActivity.openFor}")
        startActivity(intent)

        registerActivity.finishAffinity()
        registerActivity.finish()
    }
    var imageFilePath = ""
    inner class ImagePicked:PickImageHelper.OnImagePickerListener{
        override fun onImagePicked(imagePath: String, requestCode: Int) {

            if (requestCode == Constant.CAMERA_IMAGE_REQUEST) {
                imageFilePath = imagePath
              //  mProfileImage.setPadding(0, 0, 0, 0)
                // bitmap = getBitmapFromPath(imagePath)
                Glide.with(activity!!).asBitmap().load(imagePath).apply(RequestOptions().override(200)).listener(object : RequestListener<Bitmap> {

                    override fun onLoadFailed(e: GlideException?, model: Any?, target: Target<Bitmap>?, isFirstResource: Boolean): Boolean {
                        return false
                    }

                    override fun onResourceReady(resource: Bitmap?, model: Any?, target: Target<Bitmap>?, dataSource: DataSource?, isFirstResource: Boolean): Boolean {
                        bitmap = resource
                        //bitmapString = Constant.getStringFromBitmap(bitmap!!)
                        profile.setImageBitmap(resource)
                        return true
                    }

                }).into(profile)

            } else if (requestCode == Constant.GALLARY_IMAGE_REQUEST) {
                // bitmap = getBitmapFromPath(imagePath)
                imageFilePath = imagePath
                // mProfileImage.setPadding(0, 0, 0, 0)
                Glide.with(activity!!).asBitmap().load(imagePath).apply(RequestOptions().override(200)).listener(object : RequestListener<Bitmap> {
                    override fun onLoadFailed(e: GlideException?, model: Any?, target: Target<Bitmap>?, isFirstResource: Boolean): Boolean {
                        return false
                    }

                    override fun onResourceReady(resource: Bitmap?, model: Any?, target: Target<Bitmap>?, dataSource: DataSource?, isFirstResource: Boolean): Boolean {
                        bitmap = resource
                        profile.setImageBitmap(resource)
                        return true
                    }

                }).into(profile)
            }
        }

    }
}
