package com.itwhiz4u.q8mercato.kashtahport.activities

import android.app.Activity
import android.app.Dialog
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.os.Handler
import android.text.TextUtils
import android.util.Log
import android.view.Gravity
import android.view.Window
import android.view.WindowManager
import android.widget.Toast
import com.google.gson.JsonObject
import com.itwhiz4u.q8mercato.kashtahport.Api.ApiResponseListener
import com.itwhiz4u.q8mercato.kashtahport.Api.ServiceRequest
import com.itwhiz4u.q8mercato.kashtahport.R
import com.itwhiz4u.q8mercato.kashtahport.Retrofit.Interface.ApiService
import com.itwhiz4u.q8mercato.kashtahport.helper.Constant
import com.itwhiz4u.q8mercato.kashtahport.helper.ConstantFunction
import com.itwhiz4u.q8mercato.kashtahport.helper.MyApplication
import com.itwhiz4u.q8mercato.kashtahport.parser.UserParser
import com.itwhiz4u.q8mercato.kashtahport.upload.UploadAWS
import com.mazadlive.Interface.ProgressListener
import kotlinx.android.synthetic.main.activity_code_verification.*
import kotlinx.android.synthetic.main.dialog_not_get_otp.*
import org.json.JSONObject
import java.lang.Exception

class CodeVerificationActivity : BaseActivity() {

    var openFor = ""
    var canReg = false
    var fbLogin = false
    var fgtPsd = false
    var otpCall = false
    var param = JSONObject()
    var phone = ""
    var code = ""
    var paramOtp: JSONObject ?= null

    var dialog: Dialog ?= null
    companion object {

        fun startCodeVerification(context: Activity, intent: Intent){
            intent.putExtra(Constant.KEY_OPEN_FOR,"")
            context.startActivity(intent)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_code_verification)
        supportActionBar?.hide()

        dialog = Dialog(this)
        paramOtp = JSONObject()

        initDialog()

        frame_btn.setOnClickListener {

            if (!TextUtils.isEmpty(et_code.text.toString().trim())) {
                if (et_code.text.toString().length != 6) {
                    et_code.error = getString(R.string.cv_otp_should6_digit)
                } else {
                    verifyCode()
                }

            } else {
                et_code.error = getString(R.string.cv_otp_can_empty)
            }
        }

        canReg = intent.getBooleanExtra("canRegister", false)
        fbLogin = intent.getBooleanExtra("fbLogin", false)
        fgtPsd = intent.getBooleanExtra("forget", false)
        otpCall = intent.getBooleanExtra("otpCall", false)

        Handler().postDelayed(object: Runnable{
            override fun run() {
                Log.e("otpCall","otp call boolean et: ${otpCall}")
            }

        }, 500)

        if (canReg) {
            param = JSONObject(intent.getStringExtra("param"))
            paramOtp = JSONObject(intent.getStringExtra("paramOtp"))
        }

        if(otpCall){
            param = JSONObject(intent.getStringExtra("param"))
            paramOtp = JSONObject(intent.getStringExtra("paramOtp"))
            Log.e("otpCall","param get as: ${param.toString()}")
        }

        phone = intent.getStringExtra("phone")
       // code = intent.getStringExtra("countryCode")

        et_phone.text = (getString(R.string.hint_otp_verfify_header) + "+" + paramOtp!!.getString("country_code").trim() + " " + phone)

        not_get_otp.setOnClickListener {

            dialog!!.show()
        }

    }

    private fun initDialog() {

        dialog!!.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog!!.window.setWindowAnimations(R.style.Animation)
        dialog!!.window.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog!!.setContentView(R.layout.dialog_not_get_otp)

        val window = dialog!!.getWindow()

        val wlp = window.getAttributes()
        wlp.gravity = Gravity.BOTTOM
        wlp.width = WindowManager.LayoutParams.MATCH_PARENT
        wlp.height = WindowManager.LayoutParams.WRAP_CONTENT
        window.setAttributes(wlp)

        dialog!!.alt_cancel.setOnClickListener {

            dialog!!.cancel()
        }

        dialog!!.otp_again.setOnClickListener {

           getOtp(paramOtp!!)
            dialog!!.cancel()
        }

        dialog!!.otp_on_phone.setOnClickListener {

            Log.e("otpCall"," dialog button clicked")

            ServiceRequest(object: ApiResponseListener{
               override fun onCompleted(`object`: Any) {

                   Log.e("otpCall","on respone we get: ${`object`.toString()}")
               }

               override fun onError(errorMessage: String) {

                   Log.e("otpCall","on error we get: ${errorMessage}")
               }

           }).getOTPOverCall(param.getString("phone"), param.getString("country_code").trim())

            dialog!!.cancel()
        }
    }

    private fun verifyCode() {

        val paramOtp = JSONObject()
        paramOtp.put("phone", phone)
        paramOtp.put("otp", et_code.text.toString().trim())
        paramOtp.put("api_key", Constant.API_KEY)

        progress?.show()
        ServiceRequest(object : ApiResponseListener {
            override fun onCompleted(`object`: Any) {
                progress?.dismiss()
                try {

                    val jsonObject = JSONObject(`object`.toString())

                    if (!jsonObject.getBoolean("error")) {
                        if(fgtPsd){
                            Log.e("qwert","in If condition in verifyCode")
                           setResult(Activity.RESULT_OK)
                            finish()
                        }

                        if (canReg) {
                            progress?.show()
                            uploadToServer()
                        }else{
                            if(fbLogin){
                                var url = ""
                                val param = JSONObject(intent.getStringExtra("param"))
                                if(param.has("profile_image_url")){
                                    url = param.getString("profile_image_url")
                                }
                                registerUser(url)
                            }

                        }
                    }

                } catch (e: Exception) {
                    e.printStackTrace()
                    errorDialog?.show(getString(R.string.hint_error),getString(R.string.error_connect_to_server))
                }
            }

            override fun onError(errorMessage: String) {
                errorDialog?.show(getString(R.string.hint_error),"$errorMessage")
                progress?.dismiss()
            }

        }).verifyOtp(paramOtp)

    }


    private fun uploadToServer() {
        val imageFilePath = intent.getStringExtra("image")

        if (TextUtils.isEmpty(imageFilePath)) {
            registerUser("")
        } else {
            val upload = UploadAWS(this, imageFilePath, UploadAWS.UPLOAD_TYPE_USER)

            upload.uploadToAws(object : ProgressListener {
                override fun onProgressUpdate(progress: Int) {

                }

                override fun onSuccess(path: String) {

                    registerUser(path)
                }

                override fun onFileNotFound() {
                    registerUser()
                    Toast.makeText(this@CodeVerificationActivity, getString(R.string.cv_file_not_found), Toast.LENGTH_SHORT).show()
                }

                override fun onCancel() {
                    registerUser()
                    Toast.makeText(this@CodeVerificationActivity, getString(R.string.cv_cancel_uploading), Toast.LENGTH_SHORT).show()
                }

            })
        }

    }

    private fun registerUser(url: String = "") {

        val param = JSONObject(intent.getStringExtra("param"))

        param.put("profile_image_url",url)
        ServiceRequest(object : ApiResponseListener {
            override fun onCompleted(`object`: Any) {
                progress?.dismiss()
                try {

                    val jsonObject = JSONObject(`object`.toString())

                    if (!jsonObject.getBoolean("error")) {
                        Log.e("qwert","register User successully")
                        Toast.makeText(this@CodeVerificationActivity, jsonObject.getString("message"), Toast.LENGTH_SHORT).show()
                        if (jsonObject.has("data")) {
                            val user = UserParser().parse(jsonObject.getJSONObject("data"))
                            ConstantFunction.saveUserDataToPref(user)
                            MyApplication.instance.prefManager.setBoolean(Constant.KEY_USER_LOGIN_STATUS, true)
                            MyApplication.instance.prefManager.setBoolean(Constant.KEY_NOTIFICATION_SOUND, true)
                        }
                        moveNext()
                    }

                } catch (e: Exception) {


                    Log.e("TAG_USER", " ********** "+e.message)
                    errorDialog?.show(getString(R.string.hint_error),getString(R.string.error_connect_to_server))
                    e.printStackTrace()
                }
            }

            override fun onError(errorMessage: String) {
                //Toast.makeText(this@CodeVerificationActivity, "Registration failed $errorMessage", Toast.LENGTH_SHORT).show()
                errorDialog?.show(getString(R.string.hint_error),"$errorMessage")
                progress?.dismiss()
            }

        }).register(param)
    }

    private fun moveNext() {

        if(!TextUtils.isEmpty(intent.getStringExtra(Constant.KEY_OPEN_FOR))){
            openFor = intent.getStringExtra(Constant.KEY_OPEN_FOR)
        }

        val flag = intent.getBooleanExtra(Constant.KEY_OPEN_FROM_MAIN, true)
        if (flag) {
            Log.e("qwert","Launching Main Activity")
            val intent = Intent(this, MainActivity::class.java)
            intent.putExtra(Constant.KEY_OPEN_FOR, "$openFor")
            startActivity(intent)
           // finishAffinity()
            finish()
        } else {
            setResult(Activity.RESULT_OK)
            finish()
        }
    }

    private fun getOtp(param: JSONObject){

        Log.e("otpCall","paramOtp is: ${paramOtp}")
        ServiceRequest(object: ApiResponseListener{
            override fun onCompleted(`object`: Any) {
                try {

                    val jsonObject = JSONObject(`object`.toString())

                    if (!jsonObject.getBoolean("error")) {
                        Toast.makeText(this@CodeVerificationActivity, jsonObject.getString("message"), Toast.LENGTH_SHORT).show()
                    }
                }catch (e:Exception){

                }
            }

            override fun onError(errorMessage: String) {

            }

        }).sendOtp(param)
    }

}
