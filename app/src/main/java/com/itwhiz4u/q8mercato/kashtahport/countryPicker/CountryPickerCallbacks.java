package com.itwhiz4u.q8mercato.kashtahport.countryPicker;

/**
 * Created by GODARD Tuatini on 07/05/15.
 */
public interface CountryPickerCallbacks {
    void onCountrySelected(Country country, int flagResId);
}
