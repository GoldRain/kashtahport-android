package com.itwhiz4u.q8mercato.kashtahport.activities

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.text.TextUtils
import android.util.Log
import android.view.View
import android.widget.Toast
import com.itwhiz4u.q8mercato.kashtahport.Api.ApiResponseListener
import com.itwhiz4u.q8mercato.kashtahport.Api.ServiceRequest
import com.itwhiz4u.q8mercato.kashtahport.R
import com.itwhiz4u.q8mercato.kashtahport.Retrofit.Interface.ApiService
import com.itwhiz4u.q8mercato.kashtahport.Retrofit.Model.Request.LoginRequest
import com.itwhiz4u.q8mercato.kashtahport.Retrofit.Model.Response.UserData
import com.itwhiz4u.q8mercato.kashtahport.Retrofit.Model.Response.UserDataResponse
import com.itwhiz4u.q8mercato.kashtahport.adapter.ViewPagerAdapter
import com.itwhiz4u.q8mercato.kashtahport.database.AppDatabase
import com.itwhiz4u.q8mercato.kashtahport.database.FavPost
import com.itwhiz4u.q8mercato.kashtahport.fragment.login.LoginFragment
import com.itwhiz4u.q8mercato.kashtahport.helper.Constant
import com.itwhiz4u.q8mercato.kashtahport.helper.ConstantFunction
import com.itwhiz4u.q8mercato.kashtahport.helper.MyApplication
import com.itwhiz4u.q8mercato.kashtahport.parser.UserParser
import com.facebook.*
import com.facebook.login.LoginResult

import kotlinx.android.synthetic.main.activity_login.*
import org.jetbrains.anko.doAsync
import org.json.JSONObject
import java.lang.Exception
import java.util.*
import com.facebook.FacebookException
import com.facebook.FacebookCallback
import com.facebook.accountkit.AccountKitLoginResult
import com.facebook.accountkit.ui.AccountKitActivity
import com.facebook.accountkit.ui.LoginType
import com.facebook.accountkit.ui.AccountKitConfiguration
import com.facebook.appevents.AppEventsLogger
import kotlinx.android.synthetic.main.view_header_toolbar.*
import org.jetbrains.anko.uiThread
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class LoginActivity : BaseActivity() {
    val TAG = "LoginActivity"
    lateinit var viewPagerAdater: ViewPagerAdapter
    lateinit var loginFragment1: LoginFragment
    lateinit var loginFragment2: LoginFragment

    var canSell = false
    var canBook = false
    var OPEN_FOR = ""

    val FB_PHONE_REQUEST_CODE = 123
    val APP_REQUEST_CODE = 124
    private val EMAIL = "email"
    lateinit var callbackManager: CallbackManager

    companion object {
        fun startLogin(activity: Activity, fromMain: Boolean, openFor: String = "") {
            val intent = Intent(activity!!, LoginActivity::class.java)
            intent.putExtra(Constant.KEY_OPEN_FOR, openFor)
            intent.putExtra(Constant.KEY_OPEN_FROM_MAIN, fromMain)
            activity.startActivity(intent)
        }

        fun startLoginActivity(activity: Activity, fromMain: Boolean = false) {
            val intent = Intent(activity!!, LoginActivity::class.java)
            intent.putExtra(Constant.KEY_OPEN_FROM_MAIN, fromMain)
            activity.startActivity(intent)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        supportActionBar?.hide()


        //icon_back.visibility = View.VISIBLE
        header_name.setText(getString(R.string.log_hint_signin))

        FacebookSdk.sdkInitialize(MyApplication.instance);

        canSell = intent.getBooleanExtra(Constant.KEY_CAN_SELL, false)
        canBook = intent.getBooleanExtra(Constant.KEY_CAN_BOOK, false)
        OPEN_FOR = intent.getStringExtra(Constant.KEY_OPEN_FOR)

        loginFragment1 = LoginFragment()
        loginFragment2 = LoginFragment()
        loginFragment1.setActivity(this)
        loginFragment2.setActivity(this)

        viewPagerAdater = ViewPagerAdapter(supportFragmentManager)
        viewPagerAdater.addFragment(loginFragment1, "Buyer")
        viewPagerAdater.addFragment(loginFragment2, "Seller")

        view_pager.adapter = viewPagerAdater
        tab_layout.setupWithViewPager(view_pager)


        callbackManager = CallbackManager.Factory.create()

          login_button.setReadPermissions(Arrays.asList(EMAIL));
        //login_button.setPermissions(Arrays.asList(EMAIL));

        icon_back.visibility = View.VISIBLE

        icon_back.setOnClickListener {

            super.onBackPressed()
        }
        signup1.setOnClickListener {
            openRegisterScreen()
        }

        signup.setOnClickListener {
            openRegisterScreen()
        }

        forgot_pass.setOnClickListener {
            startActivity(Intent(this, ForgotPasswordActivity::class.java))
        }

        frame_register.setOnClickListener {
            if (validation()) {
                loginUser()
            }

        }

        facebook_login.setOnClickListener {
            login_button.performClick()

        }

        login_button.registerCallback(callbackManager, object : FacebookCallback<LoginResult> {

            override fun onSuccess(loginResult: LoginResult) {

                Log.e(TAG, "In On Success user id is: ")

                val request = GraphRequest.newMeRequest(
                        loginResult.accessToken, object : GraphRequest.GraphJSONObjectCallback {

                    override fun onCompleted(jsonResponse: JSONObject?, response: GraphResponse?) {

                        Log.e(TAG, "response we get is: ${jsonResponse}")
                        facebookValidation(jsonResponse)
                        //phoneLogin(facebook_login)
                    }
                })

                var bundle1 = Bundle()
                bundle1.putString("fields", "email,name,id,picture.type(large)")

                request.parameters = bundle1

                request.executeAsync()
            }

            override fun onCancel() {

                Toast.makeText(this@LoginActivity, getString(R.string.log_hint_server_error), Toast.LENGTH_SHORT).show()

            }

            override fun onError(exception: FacebookException) {
                exception.printStackTrace()
                Log.e(TAG, "In On onError : ${exception.message}")
                Toast.makeText(this@LoginActivity, getString(R.string.log_hint_smthing_wrong), Toast.LENGTH_SHORT).show()
            }
        })

    }

    fun facebookValidation(jsonResponse: JSONObject?) {
        val param = JSONObject()
        param.put("api_key", Constant.API_KEY)
        if (jsonResponse!!.has("email")) {
            param.put("email", jsonResponse.getString("email"))
        }
        // param.put("email","darshansingh@gmail.com")
        if (jsonResponse!!.has("id")) {
            param.put("facebook_id", jsonResponse.getString("id"))
        }

        progress?.show()
        ServiceRequest(object : ApiResponseListener {
            override fun onCompleted(`object`: Any) {
                progress?.dismiss()
                try {
                    val json = JSONObject(`object`.toString())

                    if (!json.getBoolean("error")) {
                        if (json.has("data")) {
                            var obj = JSONObject()
                            if (json.get("data") is JSONObject) {
                                obj = json.getJSONObject("data")
                            } else {
                                val array = json.getJSONArray("data")
                                for (i in 0 until array.length()) {
                                    obj = array.getJSONObject(i)

                                }
                            }

                            val user = UserParser().parse(obj)
                            ConstantFunction.saveUserDataToPref(user)
                            MyApplication.instance.prefManager.setBoolean(Constant.KEY_USER_LOGIN_STATUS, true)
                            MyApplication.instance.prefManager.setBoolean(Constant.KEY_NOTIFICATION_SOUND, true)

                            doAsync {
                                if (obj.has("favourites")) {
                                    val array = obj.getJSONArray("favourites")
                                    for (i in 0 until array.length()) {
                                        val id = array.getString(i)
                                        val fav = FavPost()
                                        fav.id = id
                                        fav.post_id = id
                                        AppDatabase.getAppDatabase().favPostDao().insert(fav)
                                    }
                                }
                            }

                            openNextActivity()
                        }
                    } else {
                        if (jsonResponse.has("picture")) {
                            val pic = jsonResponse.getJSONObject("picture")
                            if (pic.has("data")) {
                                val data = pic.getJSONObject("data")
                                if (data.has("url")) {
                                    val url = data.getString("url")
                                    jsonResponse.put("profile_image_url", url)
                                }

                            }

                            jsonResponse.remove("picture")
                        }

                        if (jsonResponse.has("id")) {
                            jsonResponse.put("facebook_id", jsonResponse.getString("id"))
                        }
                        Log.e(TAG, " JSON ${jsonResponse.toString()}")
                        val intent = Intent(this@LoginActivity, PhoneVerificationActivity::class.java)
                        intent.putExtra("param", jsonResponse.toString())
                        startActivity(intent)
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

            override fun onError(errorMessage: String) {
                progress?.dismiss()
                errorDialog?.show("Error!!", "Failed $errorMessage")
            }

        }).facebookValidate(param)
    }

    fun phoneLogin(view: View? = null) {
        val intent = Intent(this, AccountKitActivity::class.java)
        val configurationBuilder = AccountKitConfiguration.AccountKitConfigurationBuilder(
                LoginType.PHONE,
                AccountKitActivity.ResponseType.CODE) // or .ResponseType.TOKEN
        // ... perform additional configuration ...
        intent.putExtra(AccountKitActivity.ACCOUNT_KIT_ACTIVITY_CONFIGURATION, configurationBuilder.build())
        startActivityForResult(intent, APP_REQUEST_CODE)
    }

    private fun validation(): Boolean {


        if (TextUtils.isEmpty(et_email.text.toString().trim())) {
            et_email.error = getString(R.string.log_hint_emil_empty)
            return false
        }

        if (TextUtils.isEmpty(et_password.text.toString().trim())) {
            et_password.error = getString(R.string.log_hint_psd_empty)
            return false
        }

        return true
    }


    private fun openRegisterScreen() {
        val flag = intent.getBooleanExtra(Constant.KEY_OPEN_FROM_MAIN, true)
        RegisterActivity.startRegister(this, OPEN_FOR, flag)

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        callbackManager.onActivityResult(requestCode, resultCode, data)
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == Constant.REGISTER_REQUEST && resultCode == Activity.RESULT_OK) {
            finish()
        }

        if (requestCode == APP_REQUEST_CODE) { // confirm that this response matches your request
            val loginResult = data!!.getParcelableExtra<AccountKitLoginResult>(AccountKitLoginResult.RESULT_KEY);
            var toastMessage = "";
            if (loginResult.getError() != null) {
                toastMessage = loginResult!!.getError()!!.getErrorType().getMessage();
                // showErrorActivity(loginResult.getError());
            } else if (loginResult.wasCancelled()) {
                toastMessage = "Login Cancelled";
            } else {
                if (loginResult.getAccessToken() != null) {
                    toastMessage = "Success:" + loginResult!!.getAccessToken()!!.getAccountId();
                } else {
                    // toastMessage = String.format("Success:%s...", loginResult.getAuthorizationCode()!!.substring(0,10));
                }

                // If you have an authorization code, retrieve it from
                // loginResult.getAuthorizationCode()
                // and pass it to your server and exchange it for an access token.

                // Success! Start your next activity...
                // goToMyLoggedInActivity();
            }

            // Surface the result to your user in an appropriate way.
            Toast.makeText(this@LoginActivity, toastMessage, Toast.LENGTH_LONG).show();
        }
    }

    private fun loginUser() {
        progress?.show()

        val email = et_email.text.toString().trim()
        val psd = et_password.text.toString().trim()
        //val cnf_psd = fragView.et_cnf_password.text.toString().trim()

        val param = JSONObject()
        param.put("api_key", Constant.API_KEY)
        param.put("email", email)
        param.put("password", psd)

        val logRequest = LoginRequest(Constant.API_KEY, email, psd)
        val call = ApiService.create().loginRequest(logRequest)

        call.enqueue(getLoginParser())

    }


    private fun getLoginParser(): Callback<UserDataResponse> {
        return object : Callback<UserDataResponse> {
            override fun onFailure(call: Call<UserDataResponse>, t: Throwable) {
                Toast.makeText(this@LoginActivity, "${t.message}", Toast.LENGTH_SHORT).show()
                progress?.dismiss()
            }

            override fun onResponse(call: Call<UserDataResponse>, response: Response<UserDataResponse>) {
                progress?.dismiss()
                Log.e("onResponse", " ** " + response.isSuccessful)
                if (response.isSuccessful) {
                    val result = response.body()
                    if (!result!!.error) {
                        val user = result!!.userData
                        moveNext(user!!)


                    } else {
                        Toast.makeText(this@LoginActivity, result.message, Toast.LENGTH_SHORT).show()
                    }
                } else {
                    progress?.dismiss()
                    Toast.makeText(this@LoginActivity, getString(R.string.error_connect_to_server), Toast.LENGTH_SHORT).show()
                }

            }

        }
    }

    private fun moveNext(user:UserData) {

        progress?.show()
        ConstantFunction.saveUserDataToPref(user)
        MyApplication.instance.prefManager.setBoolean(Constant.KEY_USER_LOGIN_STATUS, true)
        MyApplication.instance.prefManager.setBoolean(Constant.KEY_NOTIFICATION_SOUND, true)
        doAsync {
            for (i in 0 until user.favList.size) {
                val id = user.favList[i]
                val fav = FavPost()
                fav.id = id
                fav.post_id = id
                AppDatabase.getAppDatabase().favPostDao().insert(fav)
            }

            uiThread {
                progress?.dismiss()
                openNextActivity()
            }
        }

        ServiceRequest.updateNotificationToken()

    }

    private fun openNextActivity() {

        val flag = intent.getBooleanExtra(Constant.KEY_OPEN_FROM_MAIN, true)
        if (flag) {
            val intent = Intent(this@LoginActivity, MainActivity::class.java)
            intent.putExtra(Constant.KEY_OPEN_FOR, OPEN_FOR)
            startActivity(intent)
           // finishAffinity()
            finish()
        } else {
            finish()
        }
    }

}



/*   ServiceRequest(object : ApiResponseListener {
       override fun onCompleted(`object`: Any) {
           progress?.dismiss()
           try {

               val jsonObject = JSONObject(`object`.toString())

               if (!jsonObject.getBoolean("error")) {


                   if (jsonObject.has("userData")) {
                       val obj = jsonObject.getJSONObject("userData")
                       val user = UserParser().parse(obj)
                       ConstantFunction.saveUserDataToPref(user)
                       MyApplication.instance.prefManager.setBoolean(Constant.KEY_USER_LOGIN_STATUS, true)
                       MyApplication.instance.prefManager.setBoolean(Constant.KEY_NOTIFICATION_SOUND, true)

                       doAsync {
                           if (obj.has("favourites")){
                               val array = obj.getJSONArray("favourites")
                               for (i in 0 until  array.length()){
                                   val id  = array.getString(i)
                                   val fav = FavPost()
                                   fav.id = id
                                   fav.post_id = id
                                   AppDatabase.getAppDatabase().favPostDao().insert(fav)
                               }
                           }
                       }
                   }
                   moveNext()
               }

           } catch (e: Exception) {
               e.printStackTrace()
               Toast.makeText(this@LoginActivity, getString(R.string.log_hint_failed_to_connect), Toast.LENGTH_SHORT).show()
           }
       }

       override fun onError(errorMessage: String) {
           Toast.makeText(this@LoginActivity, "$errorMessage", Toast.LENGTH_SHORT).show()
           progress?.dismiss()
       }

   }).loginRequest(param)*/