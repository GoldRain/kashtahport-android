package com.itwhiz4u.q8mercato.kashtahport.fragment.home


import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.text.TextUtils
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.itwhiz4u.q8mercato.kashtahport.Api.ApiResponseListener
import com.itwhiz4u.q8mercato.kashtahport.Api.ServiceRequest

import com.itwhiz4u.q8mercato.kashtahport.R
import com.itwhiz4u.q8mercato.kashtahport.activities.HotelDetailsActivity
import com.itwhiz4u.q8mercato.kashtahport.adapter.HotelListAdapter
import com.itwhiz4u.q8mercato.kashtahport.customView.EqualSpaceItemDecoration
import com.itwhiz4u.q8mercato.kashtahport.database.AppDatabase
import com.itwhiz4u.q8mercato.kashtahport.database.FavPost
import com.itwhiz4u.q8mercato.kashtahport.fragment.main.HomeFragment
import com.itwhiz4u.q8mercato.kashtahport.helper.Constant
import com.itwhiz4u.q8mercato.kashtahport.helper.ConstantFunction
import com.itwhiz4u.q8mercato.kashtahport.helper.MyApplication
import com.itwhiz4u.q8mercato.kashtahport.model.EventModel
import com.itwhiz4u.q8mercato.kashtahport.model.HotelModel
import com.itwhiz4u.q8mercato.kashtahport.parser.HotelParser
import kotlinx.android.synthetic.main.fragment_home.*
import kotlinx.android.synthetic.main.fragment_home.view.*
import kotlinx.android.synthetic.main.fragment_hotel_cam.*
import kotlinx.android.synthetic.main.fragment_hotel_cam.view.*
import kotlinx.android.synthetic.main.view_home_filter.*
import kotlinx.android.synthetic.main.view_home_filter.view.*
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread
import org.json.JSONObject
import java.lang.Exception
import java.util.*

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 *
 */
class HotelCamFragment : Fragment() {

    val TAG = "HotelCamFragment"

    lateinit var homeFragment: HomeFragment
    lateinit var adapter: HotelListAdapter

    var hotelList = ArrayList<HotelModel>()
    fun setActivity(homeFragment: HomeFragment) {
        this.homeFragment = homeFragment
    }

    var clickPosition = 0

    var param = JSONObject()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_hotel_cam, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        hotelList = ArrayList()

        swipe_to_refresh.setOnRefreshListener {
            if (swipe_to_refresh.isRefreshing) {
                getHotelList()

            }
        }


        initView()


    }

    private fun initView() {


        adapter = HotelListAdapter(activity!!, hotelList, object : HotelListAdapter.OnItemClick() {
            override fun onItemClick(position: Int) {

                clickPosition = position
                val intent = Intent(activity!!, HotelDetailsActivity::class.java)
                intent.putExtra(Constant.KEY_HOTEL, hotelList[position])
                startActivityForResult(intent, Constant.HOTEL_DETAIL_REQUEST)
            }

            override fun onAddFav(position: Int) {

                val param = JSONObject()
                param.put("api_key", Constant.API_KEY)
                param.put("hotel_id", hotelList[position]?.id)

                homeFragment.mainActivity?.progress?.show()

                ServiceRequest(object : ApiResponseListener {
                    override fun onCompleted(`object`: Any) {
                        activity!!.runOnUiThread {
                            if (hotelList[position].isFav) {
                                removeToFav(position)
                            } else {
                                addToFav(position)
                            }
                        }
                    }

                    override fun onError(errorMessage: String) {
                        homeFragment.mainActivity?.progress?.dismiss()
                        homeFragment.mainActivity?.errorDialog?.show(getString(R.string.hint_error), errorMessage)
                    }

                }).getPropertyStatus(param)


            }

        })

        recycler_view.layoutManager = LinearLayoutManager(activity!!)
        recycler_view.addItemDecoration(EqualSpaceItemDecoration(16))
        recycler_view.adapter = adapter

        adapter.notifyDataSetChanged()


    }

    override fun onResume() {
        EventBus.getDefault().register(this)
        if (adapter != null) {
            adapter.notifyDataSetChanged()
        }
        super.onResume()
    }

    override fun onPause() {
        EventBus.getDefault().unregister(this)
        super.onPause()
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onMessageEvent(event: EventModel) {
        when (event.type) {
            Constant.EVENT_UPDATE_HOME -> {
                if (homeFragment.view_pager.currentItem == 0) {
                    getHotelList()
                }
            }

            Constant.EVENT_SEARCH_CITY -> {
                if (homeFragment.view_pager.currentItem == 0) {
                    getHotelList()
                }
            }

            Constant.EVENT_FILTER -> {
                if (homeFragment.view_pager.currentItem == 0) {
                    getHotelList()
                }
            }

            Constant.EVENT_FILTER_RESET -> {
                if (homeFragment.view_pager.currentItem == 0) {
                    getHotelList()
                }
            }
        }
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        Log.e(TAG, " $requestCode")

        if (requestCode == Constant.HOTEL_DETAIL_REQUEST && resultCode == Activity.RESULT_OK) {
            if (data != null) {
                try {
                    //  val model = data.getParcelableExtra<HotelModel>(Constant.KEY_HOTEL)
                    // hotelList[clickPosition] = model
                    //adapter.notifyItemChanged(clickPosition)
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        }
    }


    private fun getHotelList() {

        if (homeFragment.mainActivity!!.prefManager.getBoolean(Constant.KEY_FILTER_ENABLE)) {
            doAsync {
                val filterData = AppDatabase.getAppDatabase().filterDao().getFilter()
                uiThread {
                    if (filterData != null) {
                        param = ConstantFunction.getFilterParam(filterData)
                        param.put("type", "Camp")
                        if (!TextUtils.isEmpty(homeFragment.et_search.text.toString())) {
                            param.put("city", homeFragment.et_search.text.toString())
                        }
                        searchList(param)
                    }
                }
            }

        } else {
            param = JSONObject()
            param.put("api_key", Constant.API_KEY)
            param.put("type", "Camp")
            param.put("city", homeFragment.et_search.text.toString())
            searchList(param)
        }
    }

    fun searchList(param: JSONObject) {

        if (!swipe_to_refresh.isRefreshing) {
            homeFragment.mainActivity?.showProgress(frame_loader)
        }
        hotelList.clear()


        ServiceRequest(object : ApiResponseListener {
            override fun onCompleted(`object`: Any) {

                homeFragment.mainActivity?.hideProgress(frame_loader, swipe_to_refresh)

                try {
                    val json = JSONObject(`object`.toString())

                    if (json.has("data")) {
                        val array = json.getJSONArray("data")
                        val parser = HotelParser()
                        hotelList.clear()
                        for (i in 0 until array.length()) {
                            val property = parser.parse(array.getJSONObject(i))
                            if (property.isEnable) {
                                hotelList.add(property)
                            }
                        }
                        Collections.reverse(hotelList)
                        adapter.notifyDataSetChanged()
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                } finally {
                    homeFragment.mainActivity?.showEmptyView(frame_empty, (hotelList.size == 0))
                }
            }

            override fun onError(errorMessage: String) {

                homeFragment.mainActivity?.hideProgress(frame_loader, swipe_to_refresh)
                homeFragment.mainActivity?.showEmptyView(frame_empty, (hotelList.size == 0))
            }

        }).searchHotel(param)
    }

    private fun addToFav(position: Int) {

        val hotelModel = hotelList[position]
        val param = JSONObject()
        param.put("api_key", Constant.API_KEY)
        param.put("hotel_id", hotelList[position]?.id)
        param.put("user_id", MyApplication.instance.prefManager.getString(Constant.KEY_USER_ID))

        homeFragment.mainActivity?.progress?.show()

        ServiceRequest(object : ApiResponseListener {
            override fun onCompleted(`object`: Any) {
                homeFragment.mainActivity?.progress?.dismiss()

                doAsync {
                    val fav = FavPost()
                    fav.id = hotelModel?.id!!
                    fav.post_id = hotelModel?.id!!
                    AppDatabase.getAppDatabase().favPostDao().insert(fav)

                    hotelModel.isFav = true
                    uiThread {
                        adapter.notifyItemChanged(position)
                    }
                }
            }

            override fun onError(errorMessage: String) {
                homeFragment.mainActivity?.progress?.dismiss()
                homeFragment?.mainActivity?.errorDialog?.show(getString(R.string.hint_error), "$errorMessage")
            }

        }).addToFav(param)
    }

    private fun removeToFav(position: Int) {
        val hotelModel = hotelList[position]
        if (hotelModel == null) {
            return
        }
        val param = JSONObject()
        param.put("api_key", Constant.API_KEY)
        param.put("hotel_id", hotelModel?.id)
        param.put("user_id", homeFragment.mainActivity?.prefManager!!.getString(Constant.KEY_USER_ID))

        homeFragment.mainActivity?.progress?.show()

        ServiceRequest(object : ApiResponseListener {
            override fun onCompleted(`object`: Any) {
                homeFragment.mainActivity?.progress?.dismiss()

                doAsync {
                    val fav = FavPost()
                    fav.id = hotelModel?.id!!
                    fav.post_id = hotelModel?.id!!
                    AppDatabase.getAppDatabase().favPostDao().deleteFavPost(fav.id)
                    hotelModel.isFav = false

                    uiThread {
                        adapter.notifyItemChanged(position)
                    }
                }
            }

            override fun onError(errorMessage: String) {
                homeFragment.mainActivity?.progress?.dismiss()
                homeFragment?.mainActivity?.errorDialog?.show(getString(R.string.hint_error), "$errorMessage")
            }

        }).removeToFav(param)
    }


}
