package com.itwhiz4u.q8mercato.kashtahport.activities

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.View
import com.itwhiz4u.q8mercato.kashtahport.R
import com.itwhiz4u.q8mercato.kashtahport.adapter.ViewPagerAdapter
import com.itwhiz4u.q8mercato.kashtahport.fragment.register.RegisterFragment
import com.itwhiz4u.q8mercato.kashtahport.helper.Constant
import kotlinx.android.synthetic.main.activity_register.*
import kotlinx.android.synthetic.main.view_header_toolbar.*

class RegisterActivity : BaseActivity() {
    lateinit var viewPagerAdater: ViewPagerAdapter
    lateinit var registerFragment1: RegisterFragment
    lateinit var registerFragment2: RegisterFragment

    var canSell = false
    var canBook = false
    var openFor = ""
    var fromMain = true

    companion object {
        fun startRegister(activity: Activity, openFor:String= "",fromMain:Boolean= true){
            val intent = Intent(activity!!,RegisterActivity::class.java)

            intent.putExtra(Constant.KEY_OPEN_FOR,openFor)
            intent.putExtra(Constant.KEY_OPEN_FROM_MAIN,fromMain)
            activity.startActivityForResult(intent,Constant.REGISTER_REQUEST)
        }
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)
        supportActionBar?.hide()

        canSell = intent.getBooleanExtra(Constant.KEY_CAN_SELL,false)
        canBook = intent.getBooleanExtra(Constant.KEY_CAN_BOOK,false)
        openFor = intent.getStringExtra(Constant.KEY_OPEN_FOR)

        header_name.setText(getString(R.string.log_hint_register))

        icon_back.visibility = View.VISIBLE

        icon_back.setOnClickListener {

            super.onBackPressed()
        }

        registerFragment1 =  RegisterFragment()
      //  registerFragment2 =  RegisterFragment()
        registerFragment1.setActivity(this)
      //  registerFragment2.setActivity(this)

        viewPagerAdater = ViewPagerAdapter(supportFragmentManager)
        viewPagerAdater.addFragment(registerFragment1,"")
       // viewPagerAdater.addFragment(registerFragment2,"Seller")

        view_pager.adapter = viewPagerAdater
       // tab_layout.setupWithViewPager(view_pager)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

    }
}
