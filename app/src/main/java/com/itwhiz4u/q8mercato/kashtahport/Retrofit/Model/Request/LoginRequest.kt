package com.itwhiz4u.q8mercato.kashtahport.Retrofit.Model.Request

import com.google.gson.annotations.SerializedName

data  class LoginRequest(

        @SerializedName("api_key")
        var api_key: String,

        @SerializedName("email")
        var email: String,

        @SerializedName("password")
        var password: String

        )