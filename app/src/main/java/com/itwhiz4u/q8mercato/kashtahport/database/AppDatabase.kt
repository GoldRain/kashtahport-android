package com.itwhiz4u.q8mercato.kashtahport.database
import android.arch.persistence.room.Database
import android.arch.persistence.room.Room
import android.arch.persistence.room.RoomDatabase
import com.itwhiz4u.q8mercato.kashtahport.helper.MyApplication

/**
 * {Created by bodacious on 28/9/18.}
 */
@Database(entities = [(FavPost::class),(FilterData::class),(NotificationData::class)],
        version = 1, exportSchema = false)
abstract class AppDatabase : RoomDatabase() {

    abstract fun favPostDao(): FavPostDao
    abstract fun filterDao(): FilterDao
    abstract fun notificationDao(): NotificationDao

    companion object {
        @Volatile private var INSTANCE: AppDatabase? = null

        fun getAppDatabase(): AppDatabase {
            if (INSTANCE == null) {
                INSTANCE = Room.databaseBuilder(MyApplication.instance, AppDatabase::class.java, "kasta_port_database")
                        .fallbackToDestructiveMigration()
                        .build()
            }
            return INSTANCE as AppDatabase
        }

        fun destroyInstance() {
            INSTANCE = null
        }
    }

}