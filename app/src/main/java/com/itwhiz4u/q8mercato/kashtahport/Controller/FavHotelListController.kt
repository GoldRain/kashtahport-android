package com.itwhiz4u.q8mercato.kashtahport.Controller

import android.content.Context
import android.graphics.Bitmap
import android.util.Log
import android.view.View
import com.itwhiz4u.q8mercato.kashtahport.R
import com.itwhiz4u.q8mercato.kashtahport.adapter.HotelFavListAdapter
import com.itwhiz4u.q8mercato.kashtahport.helper.ConstantFunction
import com.itwhiz4u.q8mercato.kashtahport.model.HotelModel
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.RequestOptions
import com.bumptech.glide.request.target.Target
import kotlinx.android.synthetic.main.list_item_fav_hotel.view.*

class FavHotelListController(val context: Context, val hotelListAdapter: HotelFavListAdapter, val holder: HotelFavListAdapter.MyViewHolder, val hotelModel: HotelModel, val listener: HotelFavListAdapter.OnItemClick, val position: Int) {

    fun control() {

        if(position%2 == 0){
            holder.itemView.back1.visibility = View.GONE
        }else{
            holder.itemView.back1.visibility = View.VISIBLE
        }

        holder.itemView.hotel_name.setText(hotelModel.name)
        holder.itemView.hotel_des.text = hotelModel.description
        holder.itemView.rating_bar.rating = hotelModel.rating.toFloat()
       // holder.itemView.rent.text = "$"+hotelModel.rate
        ConstantFunction.setPrice(context,hotelModel.currency,hotelModel.rate,holder.itemView.rent)

        holder.itemView.room_count.text = hotelModel?.rooms+" "+context.getString(R.string.hint_room)
        holder.itemView.adult_count.text = hotelModel?.person+" "+context.getString(R.string.hint_adult)
        holder.itemView.children_count.text = hotelModel?.wc_number+" "+context.getString(R.string.hint_wc)

        Log.d("HotelListController", " "+hotelModel.name+ " ")
        if (hotelModel.imageList.size > 0) {
            Log.d("HotelListController", " "+hotelModel.name+ " "+hotelModel.imageList[0].url)
            Glide.with(context)
                    .asBitmap()
                    .load(hotelModel.imageList[0].url)
                    .apply(RequestOptions().override(200))
                    .listener(object : RequestListener<Bitmap> {
                        override fun onLoadFailed(e: GlideException?, model: Any?, target: Target<Bitmap>?, isFirstResource: Boolean): Boolean {
                            holder.itemView.frame_load_image.visibility = View.GONE
                            return false
                        }

                        override fun onResourceReady(resource: Bitmap?, model: Any?, target: Target<Bitmap>?, dataSource: DataSource?, isFirstResource: Boolean): Boolean {
                            holder.itemView.frame_load_image.visibility = View.GONE
                            return false
                        }

                    })
                    .into(holder.itemView.hotel_image)
        }

        holder.itemView!!.setOnClickListener {

            listener.onItemClick(position)
        }

        holder.itemView!!.im_cancel.setOnClickListener {

            listener.removeFromFav(position)
        }

    }
}