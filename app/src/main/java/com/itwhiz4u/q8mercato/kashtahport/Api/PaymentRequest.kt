package com.itwhiz4u.q8mercato.kashtahport.Api

import android.util.Log
import com.androidnetworking.AndroidNetworking
import com.androidnetworking.common.Priority
import com.androidnetworking.error.ANError
import com.androidnetworking.interfaces.JSONObjectRequestListener
import com.itwhiz4u.q8mercato.kashtahport.R
import com.itwhiz4u.q8mercato.kashtahport.helper.Constant
import com.itwhiz4u.q8mercato.kashtahport.helper.MyApplication
import com.itwhiz4u.q8mercato.kashtahport.model.TapPaymentModel
import company.tap.gosellapi.api.facade.APIRequestCallback
import company.tap.gosellapi.api.facade.GoSellAPI
import company.tap.gosellapi.api.facade.GoSellError
import company.tap.gosellapi.api.model.Charge
import company.tap.gosellapi.api.model.Redirect
import company.tap.gosellapi.api.requests.CreateChargeRequest
import org.json.JSONObject
import java.util.HashMap

class PaymentRequest(private val apiResponseListener: ApiResponseListener?) : ApiRequester() {
    private var TAG = PaymentRequest::class.java.name

    fun getChargeRequest(model: TapPaymentModel) {

        if (!isNetworkAvailable) {
            apiResponseListener?.onError(MyApplication.instance.resources.getString(R.string.network_not_available_message))
            return
        }
        val chargeMetadata = HashMap<String, String>()
        chargeMetadata["Order Number"] = model.orderNumber

        Log.e("Order_Amount", model.amount + "Heree come")

        //val secKey= MyApplication.instance.prefManager.pref.getString(Constant.KEY_ENCP_SEC,"sk_live_JcmiLUzWl20noA9kyqsfYhNK")

        GoSellAPI.getInstance(Constant.TEST_TAP_SEC_KEY).createCharge(
                CreateChargeRequest.Builder(model.amount.toDouble(), model.currency, Redirect(model.redirect, ""))
                        .statement_descriptor(model.statementDescriptor)
                        .capture(model.capture)
                        .receipt_email(model.receiptEmail)
                        .receipt_sms(model.receiptSms)
                        .first_name(model.firstName)
                        .description(model.description)
                        .source(null)
                        .metadata(chargeMetadata)
                        .build(),
                object : APIRequestCallback<Charge> {
                    override fun onSuccess(responseCode: Int, serializedResponse: Charge) {
                        Log.d("API", "onSuccess createCharge: serializedResponse:$serializedResponse")
                        apiResponseListener?.onCompleted(serializedResponse.redirect.url)
                    }

                    override fun onFailure(errorDetails: GoSellError) {
                        Log.d("API_Error", "onFailure createCharge, errorCode: " + errorDetails.errorCode + ", errorBody: " + errorDetails.errorBody + ", throwable: " + errorDetails.throwable)

                        if (errorDetails.errorBody != null) {
                            val jsonObject = JSONObject(errorDetails.errorBody)
                            val eArray = jsonObject.getJSONArray("errors")
                            if (eArray.length() > 0) {
                                val obj = eArray.getJSONObject(0)
                                apiResponseListener?.onError(obj.getString("message"))
                            }

                        } else {
                            apiResponseListener?.onError("Failed ")
                        }

                    }
                }
        )
    }


   /* fun getChargeRequestt(param: JSONObject) {

        if (!isNetworkAvailable) {
            apiResponseListener?.onError(MyApplication.instance.resources.getString(R.string.network_not_available_message))
            return
        }

        Log.e(TAG, "REG ::  " + param.toString() + "  " + ApiUrl.CREATE_CHARGE_REQUEST)
        var request = AndroidNetworking.post(ApiUrl.CREATE_CHARGE_REQUEST)
        request.addHeaders("Authorization", Constant.TEST_TAP_SEC_KEY)
        request.addHeaders("API_KEY", Constant.API_KEY)
        request.addHeaders("Content-Type", "application/json")
        request.addJSONObjectBody(param)
        request.setTag("PAYMENT_REQUEST").setPriority(Priority.HIGH).build()

                .getAsJSONObject(object : JSONObjectRequestListener {
                    override fun onResponse(response: JSONObject) {
                        Log.e("API_RESPONSE", response.toString())
                        if (!response.getBoolean("error")) {

                            apiResponseListener?.onCompleted(response.toString())
                        } else {
                            apiResponseListener?.onError(response.getString("message"))
                        }
                    }

                    override fun onError(anError: ANError) {
                        apiResponseListener?.onError(anError.errorDetail)
                    }
                })

    }
*/

    fun verifyPayments(param: JSONObject) {
        if (!isNetworkAvailable) {
            apiResponseListener?.onError(MyApplication.instance.resources.getString(R.string.network_not_available_message))
            return
        }
        Log.e(TAG, "REG ::  " + param.toString() + "  " + ApiUrl.PAYMENT_REQUEST)
        var request = AndroidNetworking.post(ApiUrl.PAYMENT_REQUEST)
        request.addHeaders("API_KEY", Constant.API_KEY)
        request.addHeaders("Content-Type", "application/json")
        request.addJSONObjectBody(param)
        request.setTag("PAYMENT_REQUEST").setPriority(Priority.HIGH).build()

                .getAsJSONObject(object : JSONObjectRequestListener {
                    override fun onResponse(response: JSONObject) {
                        Log.e("API_RESPONSE", response.toString())
                        if (!response.getBoolean("error")) {

                            apiResponseListener?.onCompleted(response.toString())
                        } else {
                            apiResponseListener?.onError(response.getString("message"))
                        }
                    }

                    override fun onError(anError: ANError) {
                        apiResponseListener?.onError(anError.errorDetail)
                    }
                })
    }

    fun getPaymentList(param: JSONObject) {
        if (!isNetworkAvailable) {
            apiResponseListener?.onError(MyApplication.instance.resources.getString(R.string.network_not_available_message))
            return
        }
        Log.e(TAG, "REG ::  " + param.toString() + "  " + ApiUrl.GET_PAYMENT_LIST)
        var request = AndroidNetworking.post(ApiUrl.GET_PAYMENT_LIST)
        request.addHeaders("API_KEY", Constant.API_KEY)
        request.addHeaders("Content-Type", "application/json")
        request.addJSONObjectBody(param)
        request.setTag("GET_PAYMENT_LIST").setPriority(Priority.HIGH).build()

                .getAsJSONObject(object : JSONObjectRequestListener {
                    override fun onResponse(response: JSONObject) {
                        Log.e("API_RESPONSE", response.toString())
                        if (!response.getBoolean("error")) {

                            apiResponseListener?.onCompleted(response.toString())
                        } else {
                            apiResponseListener?.onError(response.getString("message"))
                        }
                    }

                    override fun onError(anError: ANError) {
                        apiResponseListener?.onError(anError.errorDetail)
                    }
                })
    }
}