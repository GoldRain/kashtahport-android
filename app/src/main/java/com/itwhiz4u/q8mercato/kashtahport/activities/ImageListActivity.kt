package com.itwhiz4u.q8mercato.kashtahport.activities

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.support.v7.widget.GridLayoutManager
import android.util.Log
import android.view.View
import android.view.Window
import com.itwhiz4u.q8mercato.kashtahport.R
import com.itwhiz4u.q8mercato.kashtahport.adapter.ListImageAdapter
import com.itwhiz4u.q8mercato.kashtahport.customDialog.CustomDialog
import com.itwhiz4u.q8mercato.kashtahport.customView.ImageOverLayView
import com.facebook.common.util.UriUtil
import com.facebook.drawee.backends.pipeline.Fresco
import com.itwhiz4u.q8mercato.kashtahport.customDialog.DialogListener
import com.itwhiz4u.q8mercato.kashtahport.customView.GridSpacingItemDecoration
import com.stfalcon.frescoimageviewer.ImageViewer
import kotlinx.android.synthetic.main.activity_image_list.*
import kotlinx.android.synthetic.main.view_header_toolbar.*
import java.io.File
import java.util.*

class ImageListActivity : BaseActivity() {

    companion object {
        fun start(context: Context, list: ArrayList<String>?,isVideo:Boolean) {
            val intent = Intent(context, ImageListActivity::class.java)
            list?.let {
                intent.putExtra("images", list)
                intent.putExtra("isVideo", isVideo)
            }
            (context as Activity).startActivityForResult(intent,111)
        }
    }

    private var isUpdate = false
    private var imageList =  ArrayList<String>()
    private var imageList2 =  ArrayList<String>()

    private var isVideo = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        setContentView(R.layout.activity_image_list)
        supportActionBar!!.hide()

        imageList = intent.getStringArrayListExtra("images")
        isVideo = intent.getBooleanExtra("isVideo",false)

        imageList.forEach {
            val im = UriUtil.getUriForFile(File(it)).toString()
            if(it.startsWith("http")){
                imageList2.add(it)
            }else{
                imageList2.add(im)
            }

        }
        recycleView.layoutManager = GridLayoutManager(this,3)
        recycleView.addItemDecoration(GridSpacingItemDecoration(3,16,true))
        recycleView.adapter = ListImageAdapter(this, imageList,OnItemClick())
        recycleView.adapter!!.notifyDataSetChanged()

        icon_back.visibility = View.VISIBLE
        header_name.setText(getString(R.string.im_list_hint_header))
        icon_back.setOnClickListener {
            onBackPressed()
        }
    }

    override fun onBackPressed() {
        if(isUpdate){
            val intent = Intent()
            intent.putExtra("images",imageList)
            setResult(Activity.RESULT_OK,intent)
        }
        Log.e("IMAGES_SIZE","** ${imageList.size} list")
        super.onBackPressed()
    }

    inner class OnItemClick :ListImageAdapter.OnItemClick{
        override fun onItemClick(position: Int) {

            CustomDialog(this@ImageListActivity).showViewerDialog(object : DialogListener() {
                override fun okClick(any: Any) {
                    if(any.toString().equals("view",true)){
                        showAllImage(position)
                    }else if(any.toString().equals("delete",true)){
                        isUpdate = true;
                        imageList2.removeAt(position)
                        imageList.removeAt(position)
                        recycleView.adapter!!.notifyDataSetChanged()
                    }
                }
            })
        }
    }

    private fun showAllImage(position: Int) {

        var currentImagePositon = position
        Fresco.initialize(this@ImageListActivity);
        val overView = ImageOverLayView(this@ImageListActivity)

        val mImageViewerBuilder = ImageViewer.Builder<String>(this@ImageListActivity, imageList2)
        val mImageViewer = mImageViewerBuilder.setStartPosition(position)
                .setOverlayView(overView)
                .setBackgroundColor(ContextCompat.getColor(this@ImageListActivity, R.color.colorBlack))
                .show()





    }
}
