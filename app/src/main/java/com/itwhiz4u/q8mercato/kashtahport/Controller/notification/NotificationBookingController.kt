package com.itwhiz4u.q8mercato.kashtahport.Controller.notification

import android.content.Context
import android.graphics.Bitmap
import android.util.Log
import com.itwhiz4u.q8mercato.kashtahport.adapter.NotificationListAdapter
import com.itwhiz4u.q8mercato.kashtahport.model.HotelModel
import com.itwhiz4u.q8mercato.kashtahport.model.NotificationModel
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.RequestOptions
import com.bumptech.glide.request.target.Target
import kotlinx.android.synthetic.main.list_item_notification.view.*
import java.lang.Exception

class NotificationBookingController(val context: Context, val  adapter: NotificationListAdapter, val holder: NotificationListAdapter.MyBookViewHolder, val  model: NotificationModel, val listener: NotificationListAdapter.OnItemClick, val position: Int) {
    fun control(){

        holder.itemView.noti_heading.text = model.message
     //   holder.itemView.time.text = ConstantFunction.getDateAndTime(model.created_at.toLong())
        holder.itemView.time.text = adapter.daysCount.printDifference(System.currentTimeMillis(),model.created_at.toLong())

       /* if(position%2 == 0){
            holder.itemView.back1.visibility = View.GONE
            holder.itemView.back2.visibility = View.VISIBLE
        }else{
            holder.itemView.back1.visibility = View.VISIBLE
            holder.itemView.back2.visibility = View.GONE
        }*/

        adapter.viewBinderHelper.bind(holder.itemView.swipe_revel,""+position)
        try {
            val hotelModel = model.model as HotelModel
            holder.itemView.hotel_des.text = hotelModel.description

            if (hotelModel.imageList.size > 0) {
                Log.d("HotelListController", " "+hotelModel.name+ " "+hotelModel.imageList[0].url)
                Glide.with(context)
                        .asBitmap()
                        .load(hotelModel.imageList[0].url)
                        .apply(RequestOptions().override(80))
                        .listener(object : RequestListener<Bitmap> {
                            override fun onLoadFailed(e: GlideException?, model: Any?, target: Target<Bitmap>?, isFirstResource: Boolean): Boolean {
                                // holder.itemView.frame_load_image.visibility = View.GONE
                                return false
                            }

                            override fun onResourceReady(resource: Bitmap?, model: Any?, target: Target<Bitmap>?, dataSource: DataSource?, isFirstResource: Boolean): Boolean {
                                //  holder.itemView.frame_load_image.visibility = View.GONE
                                return false
                            }

                        })
                        .into(holder.itemView.hotel_image)
            }
        }catch (e:Exception){
            e.printStackTrace()
        }

        holder.itemView!!.setOnClickListener {

            listener.onItemClick(position)
        }

        holder.itemView!!.frame_delete.setOnClickListener {

            listener.onDelete(position)
        }
    }
}