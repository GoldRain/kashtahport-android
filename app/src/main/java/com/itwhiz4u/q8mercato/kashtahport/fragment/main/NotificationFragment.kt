package com.itwhiz4u.q8mercato.kashtahport.fragment.main


import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.text.TextUtils
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.itwhiz4u.q8mercato.kashtahport.Api.ApiResponseListener
import com.itwhiz4u.q8mercato.kashtahport.Api.ServiceRequest
import com.itwhiz4u.q8mercato.kashtahport.R
import com.itwhiz4u.q8mercato.kashtahport.activities.*
import com.itwhiz4u.q8mercato.kashtahport.adapter.NotificationListAdapter
import com.itwhiz4u.q8mercato.kashtahport.adapter.SpinnerSortListAdapter
import com.itwhiz4u.q8mercato.kashtahport.customDialog.DialogListener
import com.itwhiz4u.q8mercato.kashtahport.customView.EqualSpaceItemDecoration
import com.itwhiz4u.q8mercato.kashtahport.database.AppDatabase
import com.itwhiz4u.q8mercato.kashtahport.helper.Constant
import com.itwhiz4u.q8mercato.kashtahport.model.*
import com.itwhiz4u.q8mercato.kashtahport.parser.BookingParser
import com.itwhiz4u.q8mercato.kashtahport.parser.NotificationParser
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.dialog_login_warning.*
import kotlinx.android.synthetic.main.fragment_notification.*
import kotlinx.android.synthetic.main.view_header_toolbar.*
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode
import org.jetbrains.anko.doAsync
import org.json.JSONObject
import java.util.*

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 *
 */
class NotificationFragment : Fragment() {
    
    var fragPosition = -1

    lateinit var mainActivity: MainActivity

    lateinit var adapter: NotificationListAdapter

    var notificaionList = ArrayList<NotificationModel>()
    var sortList: List<String> = ArrayList<String>()
    var activityList = ArrayList<ActivityModel>()

    fun setActivity(mainActivity: MainActivity) {
        this.mainActivity = mainActivity
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_notification, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        notificaionList = ArrayList()

        adapter = NotificationListAdapter(activity!!, notificaionList, object : NotificationListAdapter.OnItemClick() {
            override fun onItemClick(position: Int) {
                getDetails(position)
            }

            override fun onDelete(position: Int) {
                deleteActivity(position)
            }

        })

        recycler_view.layoutManager = LinearLayoutManager(activity!!)
        recycler_view.addItemDecoration(EqualSpaceItemDecoration(16))
        recycler_view.adapter = adapter


        swipe_to_refresh.setOnRefreshListener {
            if (swipe_to_refresh.isRefreshing) {
                getNotifications()
            }
        }

        frame_register.setOnClickListener {
            LoginActivity.startLogin(activity!!, true, Constant.OPEN_FOR_NOTIFICATION)
        }

        header_name.setText(getString(R.string.fag_noti_notifications))

        header_clear_all.setOnClickListener {
            if(mainActivity?.prefManager.getBoolean(Constant.KEY_USER_LOGIN_STATUS)){
                mainActivity?.alert?.title = ""
                mainActivity?.alert?.content = getString(R.string.noti_hint_clearAll_warn)
                mainActivity?.alert?.listenr = object : DialogListener() {
                    override fun okClick() {
                        clearAllActivities()
                    }
                }
                mainActivity?.alert?.show()
            }

        }

        card_login.setOnTouchListener { v, event ->
            true
        }
        initSpinner()
    }

    override fun onResume() {
        EventBus.getDefault().register(this)
        if(mainActivity?.prefManager.getBoolean(Constant.KEY_USER_LOGIN_STATUS)){
            header_clear_all.visibility = View.VISIBLE
        }else{
            header_clear_all.visibility = View.GONE
        }

        super.onResume()
    }

    override fun onPause() {
        EventBus.getDefault().unregister(this)
        super.onPause()
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onMessageEvent(event: EventModel) {
        when (event.type) {
            Constant.EVENT_UPDATE_MAIN -> {
                if (mainActivity.view_pager.currentItem == Constant.NOTIFICATION_POSITION) {


                    getNotifications()
                }
            }
        }
    }


    private fun initSpinner() {
        var arrayProperty = resources.getStringArray(R.array.sort_array)
        sortList = arrayProperty.asList()

        val adapterSort = SpinnerSortListAdapter(activity!!, sortList)

        spinner_sort.adapter = adapterSort
        spinner_sort.setSelection(0)

    }

    private fun displayAnim() {

        if (notificaionList.size > 0) {

            if (!mainActivity.prefManager.getBoolean("SlideNotification")) {
                mainActivity.prefManager.setBoolean("SlideNotification", true)

                Handler().postDelayed(object : Runnable {
                    override fun run() {

                        adapter.viewBinderHelper.openLayout("0")
                        Handler().postDelayed(object : Runnable {
                            override fun run() {

                                adapter.viewBinderHelper.closeLayout("0")
                            }

                        }, 200)
                    }

                }, 100)
            }

        }
    }

    private fun getNotifications() {

        if (!mainActivity?.prefManager!!.getBoolean(Constant.KEY_USER_LOGIN_STATUS)) {
            mainActivity?.hideProgress(frame_loader, swipe_to_refresh)

            if (mainActivity!!.prefManager.getBoolean(Constant.KEY_USER_LOGIN_STATUS)) {
                card_login.visibility = View.GONE
            } else {
                card_login.visibility = View.VISIBLE
                notificaionList.clear()
                adapter.notifyDataSetChanged()
            }
            return
        }

        val param = JSONObject()
        param.put("api_key", Constant.API_KEY)
        param.put("user_id", mainActivity?.prefManager!!.getString(Constant.KEY_USER_ID))
        //  param.put("hotel_id", hotelList[position].id)

        if (!swipe_to_refresh.isRefreshing) {
            mainActivity?.showProgress(frame_loader)
        }

        notificaionList.clear()
        ServiceRequest(object : ApiResponseListener {
            override fun onCompleted(`object`: Any) {
                mainActivity?.hideProgress(frame_loader, swipe_to_refresh)

                try {
                    val json = JSONObject(`object`.toString())

                    if (json.has("data")) {
                        val array = json.getJSONArray("data")
                        val parser = NotificationParser()

                        for (i in 0 until array.length()) {

                            var obj = JSONObject()

                            // Log.e("NotificationParser", array.getJSONObject(i).toString())
                            if (array.get(i) is JSONObject) {
                                obj = array.getJSONObject(i)
                            } else {
                                val objArray = array.getJSONArray(i)
                                obj = objArray.getJSONObject(0)
                            }

                            val notification = parser.parse(obj)
                            notificaionList.add(notification)

                        }

                        Collections.sort(notificaionList, SortDate())
                        adapter.notifyDataSetChanged()
                    }
                } catch (e: Exception) {

                    e.printStackTrace()
                } finally {
                    mainActivity?.showEmptyView(frame_empty, (notificaionList.size == 0))

                    header_clear_all.isEnabled = !(notificaionList.size == 0)
                    displayAnim()
                }

            }

            override fun onError(errorMessage: String) {
                mainActivity?.hideProgress(frame_loader, swipe_to_refresh)
                mainActivity?.showEmptyView(frame_empty, (notificaionList.size == 0))

            }

        }).getNotifications(param)
    }


    private fun clearAllActivities() {
        val param = JSONObject()
        param.put("api_key", Constant.API_KEY)
        // param.put("user_id", mainActivity?.prefManager!!.getString(Constant.KEY_USER_ID))
        param.put("user_id", mainActivity?.prefManager.getString(Constant.KEY_USER_ID))

        mainActivity?.progress?.show()
        ServiceRequest(object : ApiResponseListener {
            override fun onCompleted(`object`: Any) {
                notificaionList.clear()
                mainActivity?.progress?.dismiss()
                mainActivity?.showEmptyView(frame_empty, (notificaionList.size == 0))
                header_clear_all.isEnabled = !(notificaionList.size == 0)
                adapter.notifyDataSetChanged()

                doAsync {
                    AppDatabase.getAppDatabase().notificationDao().deleteAllNotification()
                }
            }

            override fun onError(errorMessage: String) {
                mainActivity?.errorDialog?.show(getString(R.string.hint_error), errorMessage)
                mainActivity?.progress?.dismiss()
            }

        }).deleteAllActivities(param)

    }

    private fun getDetails(position: Int) {
        val model = notificaionList[position]

        Log.e("GetDetails", " " + model.hint)
        when (model.hint) {

            Constant.TYPE_BOOK_SELLER -> {
                getBookingDetails(position)
            }

            Constant.TYPE_BOOK_BUYER -> {
                getBookingDetails(position)
            }

            Constant.TYPE_LIKE_BUYER -> {

                mainActivity!!.view_pager.setCurrentItem(Constant.FAV_POSITION)

            }

            Constant.TYPE_LIKE_SELLER -> {

                val intent = Intent(activity!!, PropertyListActivity::class.java)
                intent.putExtra("listing", true)
                startActivity(intent)
            }

            Constant.TYPE_FEEDBACK_BUYER -> {
                val model1 = notificaionList[position].model

                val feedback = notificaionList[position].feedBack

                if (feedback != null && !TextUtils.isEmpty((feedback as FeedBackModel).bookingId)) {
                    getBookingDetails(position, feedback.bookingId)
                } else {
                    if (model1 != null && model1 is HotelModel) {
                        val intent = Intent(activity!!, HotelDetailsActivity::class.java)
                        intent.putExtra(Constant.KEY_HOTEL, model1)
                        startActivityForResult(intent, Constant.HOTEL_DETAIL_REQUEST)
                    }
                }
            }

            Constant.TYPE_FEEDBACK_SELLER -> {
                val feedback = notificaionList[position].feedBack
                val model1 = notificaionList[position].model

                //Log.e("GetDetails"," "+model.feedBack+" ** "+(feedback as FeedBackModel).bookingId)
                if (feedback != null && !TextUtils.isEmpty((feedback as FeedBackModel).bookingId)) {
                    getBookingDetails(position, feedback.bookingId)
                } else {
                    if (model1 != null && model1 is HotelModel) {
                        val intent = Intent(activity!!, HotelDetailsActivity::class.java)
                        intent.putExtra(Constant.KEY_HOTEL, model1)
                        startActivityForResult(intent, Constant.HOTEL_DETAIL_REQUEST)
                    }
                }
            }

            Constant.TYPE_APPROVED -> {
                val intent = Intent(activity!!, PropertyListActivity::class.java)
                startActivity(intent)
            }

            else -> {
                //  mainActivity.errorDialog!!.show("Error!!","data not found")
            }
        }
    }


    private fun deleteActivity(position: Int) {
        val param = JSONObject()
        param.put("api_key", Constant.API_KEY)
        param.put("activity_id", notificaionList[position].id)
        param.put("user_id", mainActivity?.prefManager.getString(Constant.KEY_USER_ID))

        mainActivity?.progress?.show()
        ServiceRequest(object : ApiResponseListener {
            override fun onCompleted(`object`: Any) {
                deleteItemFromList(position)
            }

            override fun onError(errorMessage: String) {
                mainActivity?.errorDialog?.show(getString(R.string.hint_error), errorMessage)
                mainActivity?.progress?.dismiss()
            }

        }).deleteActivity(param)

    }

    private fun deleteItemFromList(position: Int) {

        var id = notificaionList[position].id

        mainActivity?.showEmptyView(frame_empty, (notificaionList.size == 0))
        header_clear_all.isEnabled = !(notificaionList.size == 0)

        adapter.viewBinderHelper.closeLayout("" + position)
        Handler().postDelayed(object : Runnable {
            override fun run() {
                mainActivity?.progress!!.dismiss()

                recycler_view.adapter = null

                notificaionList.removeAt(position)
                recycler_view.adapter = adapter
                adapter.notifyDataSetChanged()
            }

        }, 500)

        //getNotifications()
        doAsync {
            AppDatabase.getAppDatabase().notificationDao().deleteNotification(id)
        }
    }

    private fun getBookingDetails(position: Int, bookingId: String = "") {
        mainActivity?.progress?.dismiss()
        val param = JSONObject()
        param.put("api_key", Constant.API_KEY)
        // param.put("user_id", mainActivity?.prefManager!!.getString(Constant.KEY_USER_ID))
        param.put("booking_id", notificaionList[position].hintId)

        if (!TextUtils.isEmpty(bookingId)) {
            param.put("booking_id", bookingId)
        }
        ServiceRequest(object : ApiResponseListener {
            override fun onCompleted(`object`: Any) {
                mainActivity?.progress?.dismiss()
                try {
                    val json = JSONObject(`object`.toString())
                    if (json.has("data")) {
                        val array = json.getJSONArray("data")
                        val parser = BookingParser()
                        for (i in 0 until array.length()) {
                            val bookingModel = parser.parse(array.getJSONObject(i))
                            performClick(bookingModel, position)
                        }
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

            override fun onError(errorMessage: String) {
                mainActivity?.progress?.dismiss()
                mainActivity?.errorDialog?.show(getString(R.string.hint_error), "$errorMessage")
            }

        }).getBookingDetails(param)

    }

    private fun performClick(bookingModel: HotelBookingModel, position: Int) {
        val models = notificaionList[position]
        // Log.e("PerformClick"," "+models.hint+" ** ")
        when (notificaionList[position].hint) {

            Constant.TYPE_BOOK_SELLER -> {
                val hotelModel = bookingModel.hotelModel
                if (!hotelModel!!.isDelete) {
                    val intent = Intent(activity!!, BookingDetailsActivity::class.java)
                    intent.putExtra(Constant.KEY_BOOKING, bookingModel)
                    startActivity(intent)

                } else {
                    mainActivity?.errorDialog!!.show(getString(R.string.hint_error), getString(R.string.propety_deleted))
                }

            }

            Constant.TYPE_BOOK_BUYER -> {
                val hotelModel = bookingModel.hotelModel
                if (!hotelModel!!.isDelete) {
                    val intent = Intent(activity!!, BookingDetailsActivity::class.java)
                    intent.putExtra(Constant.KEY_BOOKING, bookingModel)
                    intent.putExtra(Constant.KEY_BOOK_CAN, true)
                    startActivity(intent)
                } else {
                    mainActivity?.errorDialog!!.show(getString(R.string.hint_error), getString(R.string.propety_deleted))
                }
            }

            Constant.TYPE_FEEDBACK_SELLER -> {
                val hotelModel = bookingModel.hotelModel
                if (!hotelModel!!.isDelete) {
                    val intent = Intent(activity!!, BookingDetailsActivity::class.java)
                    intent.putExtra(Constant.KEY_BOOKING, bookingModel)
                    startActivity(intent)
                } else {
                    mainActivity?.errorDialog!!.show(getString(R.string.hint_error), getString(R.string.propety_deleted))
                }

            }

            Constant.TYPE_FEEDBACK_BUYER -> {
                val hotelModel = bookingModel.hotelModel
                if (!hotelModel!!.isDelete) {
                    val intent = Intent(activity!!, ProvideFeedbackActivity::class.java)
                    intent.putExtra(Constant.KEY_BOOKING, bookingModel)
                    startActivity(intent)

                } else {
                    mainActivity?.errorDialog!!.show(getString(R.string.hint_error), getString(R.string.propety_deleted))
                }
            }
        }
    }


    inner class SortDate : Comparator<NotificationModel> {
        override fun compare(o1: NotificationModel?, o2: NotificationModel?): Int {
            return if (o1!!.created_at.toLong() > o2!!.created_at.toLong()) -1 else 1
        }

    }
}
