package com.itwhiz4u.q8mercato.kashtahport.activities

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.text.TextUtils
import android.view.View
import com.itwhiz4u.q8mercato.kashtahport.R
import com.itwhiz4u.q8mercato.kashtahport.helper.Constant
import com.itwhiz4u.q8mercato.kashtahport.model.HotelModel
import kotlinx.android.synthetic.main.activity_hotel_details.*
import kotlinx.android.synthetic.main.view_header_toolbar.*

class HotelDecriptionActivity : AppCompatActivity() {

    var hotel:HotelModel ?= null
    var show:String? = "des"
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_hotel_decription)
        supportActionBar?.hide()

        hotel = intent.getParcelableExtra(Constant.KEY_HOTEL)
        show = intent.getStringExtra("show")

        header_name.setText(hotel?.name)


        if(!TextUtils.isEmpty(show)){
            when (show){
                "des" ->{
                    hotel_des.setText(hotel?.description)
                    hint_des.setText(getString(R.string.h_des_hint_des))
                }

                "term" ->{
                    hotel_des.setText(hotel?.conditions)
                    hint_des.setText(getString(R.string.h_des_hint_standard_term))
                }
            }
        }
        icon_back.visibility = View.VISIBLE

        icon_back.setOnClickListener {
            super.onBackPressed()
        }

    }
}
