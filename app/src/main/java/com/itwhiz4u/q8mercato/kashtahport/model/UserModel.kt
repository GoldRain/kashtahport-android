package com.itwhiz4u.q8mercato.kashtahport.model

import android.os.Parcel
import android.os.Parcelable

class UserModel() :Parcelable {

    var id = ""
    var name  = ""
    var description = ""
    var profession = ""
    var imageUrl = ""
    var email = ""
    var password = ""
    var countryCode = ""
    var faceBookId = ""
    var phone = ""
    var type = ""
    var device_id = ""
    var device_type = ""
    var notification_token = ""
    var access_token = ""
    var socket_id = ""
    var online_status = false
    var is_login = false
    var is_blocked = ""
    var last_login = ""
    var can_sale = false
    var created_at = ""

    var ibanCountry = ""
    var ibanNumber = ""
    var ibankName = ""
    var ibanPhone = ""


    constructor(parcel: Parcel) : this() {
        id = parcel.readString()
        name = parcel.readString()
        description = parcel.readString()
        profession = parcel.readString()
        imageUrl = parcel.readString()
        email = parcel.readString()
        password = parcel.readString()
        countryCode = parcel.readString()
        faceBookId = parcel.readString()
        phone = parcel.readString()
        type = parcel.readString()
        device_id = parcel.readString()
        device_type = parcel.readString()
        notification_token = parcel.readString()
        access_token = parcel.readString()
        socket_id = parcel.readString()
        online_status = parcel.readByte() != 0.toByte()
        is_login = parcel.readByte() != 0.toByte()
        is_blocked = parcel.readString()
        last_login = parcel.readString()
        can_sale = parcel.readByte() != 0.toByte()
        created_at = parcel.readString()
        ibanCountry = parcel.readString()
        ibanNumber = parcel.readString()
        ibankName = parcel.readString()
        ibanPhone = parcel.readString()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(id)
        parcel.writeString(name)
        parcel.writeString(description)
        parcel.writeString(profession)
        parcel.writeString(imageUrl)
        parcel.writeString(email)
        parcel.writeString(password)
        parcel.writeString(countryCode)
        parcel.writeString(faceBookId)
        parcel.writeString(phone)
        parcel.writeString(type)
        parcel.writeString(device_id)
        parcel.writeString(device_type)
        parcel.writeString(notification_token)
        parcel.writeString(access_token)
        parcel.writeString(socket_id)
        parcel.writeByte(if (online_status) 1 else 0)
        parcel.writeByte(if (is_login) 1 else 0)
        parcel.writeString(is_blocked)
        parcel.writeString(last_login)
        parcel.writeByte(if (can_sale) 1 else 0)
        parcel.writeString(created_at)
        parcel.writeString(ibanCountry)
        parcel.writeString(ibanNumber)
        parcel.writeString(ibankName)
        parcel.writeString(ibanPhone)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<UserModel> {
        override fun createFromParcel(parcel: Parcel): UserModel {
            return UserModel(parcel)
        }

        override fun newArray(size: Int): Array<UserModel?> {
            return arrayOfNulls(size)
        }
    }


}