package com.itwhiz4u.q8mercato.kashtahport.activities

import android.content.Intent
import android.graphics.Bitmap
import android.os.Build
import android.os.Bundle
import android.support.annotation.RequiresApi
import android.support.v4.app.Fragment
import android.text.Editable
import android.text.TextUtils
import android.text.TextWatcher
import android.util.Log
import android.view.View
import android.widget.Toast
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.RequestOptions
import com.itwhiz4u.q8mercato.kashtahport.Api.ApiResponseListener
import com.itwhiz4u.q8mercato.kashtahport.Api.ServiceRequest
import com.itwhiz4u.q8mercato.kashtahport.R
import com.itwhiz4u.q8mercato.kashtahport.countryPicker.Country
import com.itwhiz4u.q8mercato.kashtahport.countryPicker.CountryPickerCallbacks
import com.itwhiz4u.q8mercato.kashtahport.countryPicker.CountryPickerDialog
import com.itwhiz4u.q8mercato.kashtahport.countryPicker.Utils
import com.itwhiz4u.q8mercato.kashtahport.helper.Constant
import com.itwhiz4u.q8mercato.kashtahport.helper.PickImageHelper
import kotlinx.android.synthetic.main.activity_phone_verification.*
import kotlinx.android.synthetic.main.additional_counrty_picker_layout.*
import kotlinx.android.synthetic.main.view_header_toolbar.*
import org.json.JSONObject
import java.lang.Exception
import java.util.*

class PhoneVerificationActivity : BaseActivity() {

    var pickImageHelper: PickImageHelper?= null
    var imageFilePath = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_phone_verification)
        supportActionBar?.hide()

        icon_back.visibility = View.VISIBLE

        icon_back.setOnClickListener {
            onBackPressed()
        }

        frame_btn.setOnClickListener {

            val email = et_phone.text.toString().trim()
            if(TextUtils.isEmpty(email)){
                et_phone.error = getString(R.string.pv_hint_phone_empty)
                return@setOnClickListener
            }

            sendOtp()
        }

       /* et_code.addTextChangedListener(object : TextWatcher {
            @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
            override fun afterTextChanged(s: Editable?) {

                var str = s.toString()
                if(!TextUtils.isEmpty(str)){
                    str = str.replace("+","")
                    if(str.equals("965")){
                        flag_country.setCompoundDrawablesRelativeWithIntrinsicBounds(R.drawable.flag_ar,0,0,0)
                    }else{
                        flag_country.setCompoundDrawablesRelativeWithIntrinsicBounds(R.drawable.ic_globe_black,0,0,0)
                    }
                }
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

            }

        })*/

        frame_cCode.setOnClickListener {

            val countryPickerDialog = CountryPickerDialog(this, object: CountryPickerCallbacks{
                override fun onCountrySelected(country: Country?, flagResId: Int) {
                    et_code.setText("+ "+country!!.dialingCode)
                    im_flag.setImageResource(flagResId)
                }
            })

            countryPickerDialog.show()
        }

        et_code.setOnClickListener {
            frame_cCode.performClick()
        }
    }

    private fun sendOtp() {

        val phone  = et_phone.text.toString().trim()
        var code  = et_code.text.toString().trim()

        code = code.replace("+","").trim()
        val paramOtp = JSONObject()
        paramOtp.put("api_key", Constant.API_KEY)
        paramOtp.put("phone",phone)
        paramOtp.put("country_code",code)

        progress?.show()
        ServiceRequest(object : ApiResponseListener {
            override fun onCompleted(`object`: Any) {
               progress?.dismiss()
                try {

                    val jsonObject = JSONObject(`object`.toString())

                    if(!jsonObject.getBoolean("error")){

                        val param = JSONObject(intent.getStringExtra("param"))


                        param.put("phone",phone)
                        param.put("country_code",code)
                        param.put("api_key",Constant.API_KEY)

                        Toast.makeText(this@PhoneVerificationActivity,jsonObject.getString("message"),Toast.LENGTH_SHORT).show()

                        val intent = Intent(this@PhoneVerificationActivity,CodeVerificationActivity::class.java)
                        intent.putExtra("phone",phone)
                        intent.putExtra("countryCode", code)
                        intent.putExtra("fbLogin",true)
                        intent.putExtra("otpCall", true)
                        intent.putExtra("param",param.toString())
                        intent.putExtra("paramOtp",param.toString())
                        startActivity(intent)

                    }

                }catch (e: Exception){
                    e.printStackTrace()

                    errorDialog?.show(getString(R.string.hint_error),getString(R.string.error_connect_to_server))
                }
            }

            override fun onError(errorMessage: String) {
                //Toast.makeText(activity!!,"Registration failed $errorMessage",Toast.LENGTH_SHORT).show()
                errorDialog?.show(getString(R.string.hint_error),"$errorMessage")
                progress?.dismiss()
            }

        }).sendOtp(paramOtp)
    }

}
