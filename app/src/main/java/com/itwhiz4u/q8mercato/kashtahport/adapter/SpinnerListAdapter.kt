package com.itwhiz4u.q8mercato.kashtahport.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import com.itwhiz4u.q8mercato.kashtahport.R
import kotlinx.android.synthetic.main.list_item_spinner.view.*

class SpinnerListAdapter(val context:Context, val items:List<String>):BaseAdapter(){
    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        var view =convertView
        if(view == null){
            val layoutInflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
            view = layoutInflater.inflate(R.layout.list_item_spinner,null)
        }

        view!!.name.setText(items[position])

        return  view!!
    }

    override fun getItem(position: Int): Any {

        return items[position]
    }

    override fun getItemId(position: Int): Long {

        return  position.toLong()
    }

    override fun getCount(): Int {

        return  items.size
    }
}