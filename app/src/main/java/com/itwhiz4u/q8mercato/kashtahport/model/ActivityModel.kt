package com.itwhiz4u.q8mercato.kashtahport.model

import android.os.Parcel
import android.os.Parcelable

class ActivityModel() :Parcelable {
    /*{"user_id":"5cd6cdc8ef1720392a21cdc6","hint_id":"5cd95c197a7cf333636a2ee9","hint":"Book","message":"you book a hotel","created_at":"1557747699742","_id":"5cd95c327a7cf333636a2eeb","__v":0}*/

    var id = ""
    var userId = ""
    var hintId = ""
    var hint = ""
    var message = ""
    var created_at = ""
    var model:Any? = null

    constructor(parcel: Parcel) : this() {
        id = parcel.readString()
        userId = parcel.readString()
        hintId = parcel.readString()
        hint = parcel.readString()
        message = parcel.readString()
        created_at = parcel.readString()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(id)
        parcel.writeString(userId)
        parcel.writeString(hintId)
        parcel.writeString(hint)
        parcel.writeString(message)
        parcel.writeString(created_at)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<ActivityModel> {
        override fun createFromParcel(parcel: Parcel): ActivityModel {
            return ActivityModel(parcel)
        }

        override fun newArray(size: Int): Array<ActivityModel?> {
            return arrayOfNulls(size)
        }
    }
}