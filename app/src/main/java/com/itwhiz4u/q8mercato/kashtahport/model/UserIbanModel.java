package com.itwhiz4u.q8mercato.kashtahport.model;

import android.databinding.BaseObservable;
import android.databinding.Bindable;
import android.os.Parcel;
import android.os.Parcelable;

import com.android.databinding.library.baseAdapters.BR;
import com.google.gson.annotations.SerializedName;

public class UserIbanModel extends BaseObservable implements Parcelable {

    @SerializedName("IBAN")
    private String IBAN = "";

    @SerializedName("country_code")
    private String countryCode ="";

    @SerializedName("bank_name")
    private String bankName ="";

    @SerializedName("phone")
    private String phone="";


    public UserIbanModel(){

    }
    protected UserIbanModel(Parcel in) {
        IBAN = in.readString();
        countryCode = in.readString();
        bankName = in.readString();
        phone = in.readString();
    }

    public static final Creator<UserIbanModel> CREATOR = new Creator<UserIbanModel>() {
        @Override
        public UserIbanModel createFromParcel(Parcel in) {
            return new UserIbanModel(in);
        }

        @Override
        public UserIbanModel[] newArray(int size) {
            return new UserIbanModel[size];
        }
    };

    @Bindable
    public String getIBAN() {
        return IBAN;
    }

    public void setIBAN(String IBAN) {
        this.IBAN = IBAN;
        notifyPropertyChanged(BR.iBAN);
    }

    @Bindable
    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
        notifyPropertyChanged(BR.bankName);
    }

    @Bindable
    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
        notifyPropertyChanged(BR.phone);
    }

    @Bindable
    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
        notifyPropertyChanged(BR.countryCode);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(IBAN);
        dest.writeString(countryCode);
        dest.writeString(bankName);
        dest.writeString(phone);
    }
}
