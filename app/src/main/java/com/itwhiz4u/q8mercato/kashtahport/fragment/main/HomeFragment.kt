package com.itwhiz4u.q8mercato.kashtahport.fragment.main


import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.support.v4.app.Fragment
import android.support.v4.view.ViewPager
import android.text.Editable
import android.text.TextUtils
import android.text.TextWatcher
import android.util.Log
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.widget.TextView

import com.itwhiz4u.q8mercato.kashtahport.R
import com.itwhiz4u.q8mercato.kashtahport.activities.*
import com.itwhiz4u.q8mercato.kashtahport.adapter.ViewPagerAdapter
import com.itwhiz4u.q8mercato.kashtahport.database.AppDatabase
import com.itwhiz4u.q8mercato.kashtahport.database.FilterData
import com.itwhiz4u.q8mercato.kashtahport.fragment.home.HotelCamFragment
import com.itwhiz4u.q8mercato.kashtahport.fragment.home.HotelChaletFragment
import com.itwhiz4u.q8mercato.kashtahport.fragment.home.HotelFarmFragment
import com.itwhiz4u.q8mercato.kashtahport.helper.Constant
import com.itwhiz4u.q8mercato.kashtahport.helper.ConstantFunction
import com.itwhiz4u.q8mercato.kashtahport.helper.MyApplication
import com.itwhiz4u.q8mercato.kashtahport.model.EventModel
import kotlinx.android.synthetic.main.activity_main.tab_layout
import kotlinx.android.synthetic.main.fragment_home.*
import kotlinx.android.synthetic.main.view_home_filter.*
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 *
 */
class HomeFragment : Fragment() {

    companion object {
        val CAM_POSITION = 0
        val FARM_POSITION = 1
        val CHALET_POSITION = 2
    }
    
    var fragPosition= -1
    var  mainActivity: MainActivity?= null
    lateinit var viewPagerAdater: ViewPagerAdapter

    lateinit var hotelCamFragment: HotelCamFragment
    lateinit var hotelChaletFragment: HotelChaletFragment
    lateinit var hotelFarmFragment: HotelFarmFragment

    lateinit var hotelMapListFragment: HotelMapListFragment

    val FILTER_CODE = 1234
    lateinit var filterData:FilterData

    fun setActivity(mainActivity: MainActivity){
        this.mainActivity = mainActivity
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment

        return inflater.inflate(R.layout.fragment_home, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        filterData =FilterData()

        if(mainActivity == null){
            mainActivity = MainActivity.getInstance()
        }


        view_in_map.visibility = View.GONE


        im_filter.setOnClickListener {
            val intent =  Intent(activity!!,FilterActivity::class.java)
            startActivityForResult(intent,FILTER_CODE)
        }
        title_sell.setOnClickListener {

            if(MyApplication.instance.prefManager.getBoolean(Constant.KEY_USER_LOGIN_STATUS)){
                val intent = Intent(activity!!,SellerAddPropertyActivity::class.java)
                startActivity(intent)
            }else{
                LoginActivity.startLogin(activity!!,true,Constant.OPEN_FOR_ADD_PROPERTY)
            }
        }

        // hotelMapListFragment = HotelMapListFragment()
        //  hotelMapListFragment.setActivity(mainActivity)

        view_in_map.setOnClickListener {
            openMapFragment()
        }

        view_pager.addOnPageChangeListener(object :ViewPager.OnPageChangeListener{
            override fun onPageScrollStateChanged(p0: Int) {

            }

            override fun onPageScrolled(p0: Int, p1: Float, p2: Int) {

            }

            override fun onPageSelected(p0: Int) {
                val eventModel = EventModel(Constant.EVENT_UPDATE_HOME)
                EventBus.getDefault().post(eventModel)
            }

        })

        et_search.setOnEditorActionListener(object :TextView.OnEditorActionListener{
            override fun onEditorAction(v: TextView?, actionId: Int, event: KeyEvent?): Boolean {
                Log.e("SEARCH", " "+actionId+" "+EditorInfo.IME_ACTION_SEARCH)
                if(actionId == EditorInfo.IME_ACTION_SEARCH){
                    Log.e("SEARCH", " "+actionId+" "+EditorInfo.IME_ACTION_SEARCH)
                    ConstantFunction.hideKeyboard(activity!!,et_search)
                    EventBus.getDefault().post(EventModel(Constant.EVENT_SEARCH_CITY))
                    return false
                }
                return false
            }

        })

        et_search.addTextChangedListener(object :TextWatcher{
            override fun afterTextChanged(s: Editable?) {

            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                if(TextUtils.isEmpty(et_search.text.toString())){
                    EventBus.getDefault().post(EventModel(Constant.EVENT_SEARCH_CITY))
                    ConstantFunction.hideKeyboard(activity!!,et_search)
                }
            }

        })


        initUI()

    }
     fun openMapFragment() {

        view_in_map.visibility = View.GONE
        list_lay.visibility = View.GONE
        frame_map.visibility = View.VISIBLE
        childFragmentManager.beginTransaction().replace(R.id.frame_map,hotelMapListFragment).commit()
    }


    override fun onResume() {
        EventBus.getDefault().register(this)

        if(MyApplication.instance.prefManager.getBoolean(Constant.KEY_FILTER_ENABLE)){

            doAsync {
                val filterData = AppDatabase.getAppDatabase().filterDao().getFilter()
                if(filterData!= null){
                    uiThread {
                        if(filterData.filterCount >0){
                            filter_count.visibility = View.VISIBLE
                        }

                        filter_count.text = ""+filterData?.filterCount
                    }
                }
            }
        }else{
            filter_count.visibility = View.GONE
        }
        super.onResume()
    }

    override fun onPause() {
        EventBus.getDefault().unregister(this)
        super.onPause()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if(requestCode == FILTER_CODE && resultCode == Activity.RESULT_OK){
           if(data != null){
               doAsync {
                   filterData =AppDatabase.getAppDatabase().filterDao().getFilter()
                   uiThread {
                       when(filterData.type.toLowerCase()){
                           "camp" ->{
                               view_pager.setCurrentItem(0)
                           }

                           "farm" ->{
                               view_pager.setCurrentItem(1)
                           }

                           "chalet" ->{
                               view_pager.setCurrentItem(2)
                           }
                       }
                       EventBus.getDefault().post(EventModel(Constant.EVENT_FILTER))

                   }
               }
           }else{
               EventBus.getDefault().post(EventModel(Constant.EVENT_FILTER_RESET))
           }

        }

    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onMessageEvent(event: EventModel) {
        Log.e("onMessageEvent",event.type)
        when(event.type){
            Constant.EVENT_UPDATE_MAIN ->{

                if(mainActivity!!.view_pager.currentItem == Constant.HOME_POSITION){

                   /* Handler().postDelayed(object :Runnable{
                        override fun run() {
                           // EventBus.getDefault().post(EventModel(Constant.EVENT_UPDATE_HOME))
                        }

                    },100)*/
                }
            }
        }
    }

    fun initUI(){


        if(mainActivity!!.prefManager.getBoolean(Constant.KEY_FILTER_ENABLE)){
            doAsync {
                filterData = AppDatabase.getAppDatabase().filterDao().getFilter()
            }
        }
        hotelCamFragment = HotelCamFragment()
        hotelCamFragment.setActivity(this)

        hotelChaletFragment = HotelChaletFragment()
        hotelChaletFragment.setActivity(this)

        hotelFarmFragment = HotelFarmFragment()
        hotelFarmFragment.setActivity(this)

        viewPagerAdater = ViewPagerAdapter(childFragmentManager)

        viewPagerAdater.addFragment(hotelCamFragment,getString(R.string.hint_camp))
        viewPagerAdater.addFragment(hotelFarmFragment,getString(R.string.hint_farm))
        viewPagerAdater.addFragment(hotelChaletFragment,getString(R.string.hint_chalet))

        view_pager.adapter = viewPagerAdater
        tab_layout.setupWithViewPager(view_pager)

        view_pager.setCurrentItem( 0)

        Handler().postDelayed(object :Runnable{
            override fun run() {
                EventBus.getDefault().post(EventModel(Constant.EVENT_UPDATE_HOME))
            }

        },100)
    }


}
