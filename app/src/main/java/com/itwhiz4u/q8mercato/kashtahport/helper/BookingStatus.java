package com.itwhiz4u.q8mercato.kashtahport.helper;

import com.itwhiz4u.q8mercato.kashtahport.R;

public class BookingStatus {

    public static final String CANCEL = MyApplication.Companion.getInstance().getString(R.string.bk_status_cancel);
    public static final String CANCELLED = MyApplication.Companion.getInstance().getString(R.string.bk_status_canceled);

    public static final String CONFIRM = MyApplication.Companion.getInstance().getString(R.string.bk_status_confirm);
    public static final String CONFIRMED = MyApplication.Companion.getInstance().getString(R.string.bk_status_confirmed);//"Confirmed";

    public static final String ACCEPT = MyApplication.Companion.getInstance().getString(R.string.bk_status_accept);//"Accept";
    public static final String ACCEPTED = MyApplication.Companion.getInstance().getString(R.string.bk_status_accepted);//"Accepted";

    public static final String REJECT = MyApplication.Companion.getInstance().getString(R.string.bk_status_reject);//"Reject";
    public static final String REJECTED = MyApplication.Companion.getInstance().getString(R.string.bk_status_rejected);//"Rejected";

    public static final String PAID = MyApplication.Companion.getInstance().getString(R.string.bk_status_paid);//"Paid";

    public static final String CHECKOUT = MyApplication.Companion.getInstance().getString(R.string.bk_status_checkout);//"Checkout";

    public static final String COMPLETE = MyApplication.Companion.getInstance().getString(R.string.bk_status_complete);//"Complete";
    public static final String PENDING = MyApplication.Companion.getInstance().getString(R.string.bk_status_pending);//"Pending";
}
