package com.itwhiz4u.q8mercato.kashtahport.model

import android.os.Parcel
import android.os.Parcelable

class PlaceModel() :Parcelable {

    var id = ""
    var name = ""
    var address = ""
    var city = ""
    var country = ""
    var lat =0.0
    var lon =0.0

    constructor(parcel: Parcel) : this() {
        id = parcel.readString()
        name = parcel.readString()
        address = parcel.readString()
        city = parcel.readString()
        country = parcel.readString()
        lat = parcel.readDouble()
        lon = parcel.readDouble()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(id)
        parcel.writeString(name)
        parcel.writeString(address)
        parcel.writeString(city)
        parcel.writeString(country)
        parcel.writeDouble(lat)
        parcel.writeDouble(lon)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<PlaceModel> {
        override fun createFromParcel(parcel: Parcel): PlaceModel {
            return PlaceModel(parcel)
        }

        override fun newArray(size: Int): Array<PlaceModel?> {
            return arrayOfNulls(size)
        }
    }


}