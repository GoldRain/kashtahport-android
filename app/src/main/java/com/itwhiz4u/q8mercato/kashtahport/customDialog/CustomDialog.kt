package com.itwhiz4u.q8mercato.kashtahport.customDialog

import android.app.Dialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.view.*
import com.itwhiz4u.q8mercato.kashtahport.R
import com.itwhiz4u.q8mercato.kashtahport.helper.ConstantFunction
import com.itwhiz4u.q8mercato.kashtahport.model.TapPaymentModel
import kotlinx.android.synthetic.main.dialog_image_picker.*
import kotlinx.android.synthetic.main.dialog_image_viewer.*
import kotlinx.android.synthetic.main.dialog_payment_failed.*

class CustomDialog(val context: Context) {

    fun showPickerDialog(listener: DialogListener) {
        val dialog= Dialog(context)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)
        dialog.setContentView(R.layout.dialog_image_picker)

        val window = dialog.getWindow()
        val wlp = window.getAttributes()
        wlp.width = WindowManager.LayoutParams.MATCH_PARENT
        wlp.height = WindowManager.LayoutParams.WRAP_CONTENT
        wlp.gravity = Gravity.CENTER_VERTICAL
        window.setAttributes(wlp)

        dialog.setCanceledOnTouchOutside(false)
        dialog.setCancelable(true)

        dialog.dialog_frame_gal.setOnClickListener {
            dialog.cancel()
            listener.okClick("Gallary")
        }

        dialog.dialog_frame_cam.setOnClickListener {
            dialog.cancel()
            listener.okClick("Camera")
        }

        dialog.show()
    }

    fun showViewerDialog(listener: DialogListener) {
        val dialog= Dialog(context)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
      //  dialog.window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)
        dialog.setContentView(R.layout.dialog_image_viewer)

        val window = dialog.getWindow()
        val wlp = window.getAttributes()
        wlp.width = WindowManager.LayoutParams.MATCH_PARENT
        wlp.height = WindowManager.LayoutParams.WRAP_CONTENT
        wlp.gravity = Gravity.CENTER_VERTICAL
        window.setAttributes(wlp)

        dialog.setCanceledOnTouchOutside(false)
        dialog.setCancelable(true)

        dialog.dialog_frame_view.setOnClickListener {

            listener.okClick("view")
            dialog.dismiss()
        }

        dialog.dialog_frame_delete.setOnClickListener {
            listener.okClick("delete")
            dialog.dismiss()
        }

        dialog.show()
    }

    fun showPaymentFailedDialog(tapPaymentModel: TapPaymentModel,listener: DialogListener) {
        val dialog= Dialog(context)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        //  dialog.window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)
        dialog.setContentView(R.layout.dialog_payment_failed)

        val window = dialog.getWindow()
        val wlp = window.getAttributes()
        wlp.width = WindowManager.LayoutParams.MATCH_PARENT
        wlp.height = WindowManager.LayoutParams.WRAP_CONTENT
        wlp.gravity = Gravity.CENTER_VERTICAL
        window.setAttributes(wlp)

        dialog.setCanceledOnTouchOutside(false)
        dialog.setCancelable(true)

        dialog.payment_id.text = tapPaymentModel.ref.toString()
        dialog.time.text = ConstantFunction.getDateAndTime(System.currentTimeMillis())
        dialog.back.setOnClickListener {

            dialog.dismiss()
        }

        dialog.continue_btn.setOnClickListener {
            listener.okClick()
            dialog.dismiss()
        }

        dialog.show()
    }

    fun showCountryDialog(){

    }
}