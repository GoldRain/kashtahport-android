package com.itwhiz4u.q8mercato.kashtahport.map

import android.content.Context
import android.support.v4.content.ContextCompat
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.widget.Toast
import com.itwhiz4u.q8mercato.kashtahport.R
import com.itwhiz4u.q8mercato.kashtahport.helper.OnInfoWindowElemTouchListener
import com.itwhiz4u.q8mercato.kashtahport.model.HotelModel
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.model.Marker
import kotlinx.android.synthetic.main.view_marker_info.view.*

class MapInfoWindow(val context: Context,val hotelModel:HotelModel, val map : GoogleMap ?= null):GoogleMap.InfoWindowAdapter {

    override fun getInfoContents(p0: Marker?): View? {

        return null
    }

    override fun getInfoWindow(p0: Marker?): View? {

        val layInflator = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val view = layInflator.inflate(R.layout.view_marker_info, null)

        view.hotel_name.setText(hotelModel.name)
        view.hotel_des.setText(hotelModel.description)
        view.rent.setText("$"+hotelModel.rate)
        view.rating_bar.rating = hotelModel.rating.toFloat()


/*view.book_now.setOnTouchListener { v, event ->
Log.e("getInfoWindow"," book_now *** ")
true
}*/


        val infoButtonListener = object : OnInfoWindowElemTouchListener(view.book_now, ContextCompat.getDrawable(context, R.drawable.back_corner_back_redd), ContextCompat.getDrawable(context, R.drawable.back_corner_pri)) {
            override fun onClickConfirmed(v: View?, marker: Marker?) {

                Log.e("sdfgsd", "Button is pressed")

                Toast.makeText(context, "Your Button Is Pressed", Toast.LENGTH_SHORT).show()
            }

        }

        view.book_now.setOnTouchListener(infoButtonListener)

        return view
    }

    fun getPixelsFromDp(context: Context, dp: Float): Int {
        val scale = context.resources.displayMetrics.density
        return (dp * scale + 0.5f).toInt()
    }
}