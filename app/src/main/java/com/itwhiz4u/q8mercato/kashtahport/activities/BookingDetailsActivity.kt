package com.itwhiz4u.q8mercato.kashtahport.activities

import android.Manifest
import android.app.Activity
import android.app.Dialog
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.net.Uri
import android.os.Bundle
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.text.TextUtils
import android.util.Log
import android.view.View
import android.widget.ImageView
import android.widget.Toast
import com.itwhiz4u.q8mercato.kashtahport.Api.ApiResponseListener
import com.itwhiz4u.q8mercato.kashtahport.Api.ServiceRequest
import com.itwhiz4u.q8mercato.kashtahport.R
import com.itwhiz4u.q8mercato.kashtahport.adapter.RoomTypeAdapter
import com.itwhiz4u.q8mercato.kashtahport.customView.ImageOverLayView
import com.itwhiz4u.q8mercato.kashtahport.database.AppDatabase
import com.itwhiz4u.q8mercato.kashtahport.database.FavPost
import com.itwhiz4u.q8mercato.kashtahport.helper.*
import com.itwhiz4u.q8mercato.kashtahport.model.*
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.RequestOptions
import com.bumptech.glide.request.target.Target
import com.facebook.drawee.backends.pipeline.Fresco
import com.itwhiz4u.q8mercato.kashtahport.customDialog.DialogListener
import com.facebook.drawee.generic.GenericDraweeHierarchyBuilder
import com.google.firebase.FirebaseApp
import com.google.firebase.dynamiclinks.DynamicLink
import com.google.firebase.dynamiclinks.FirebaseDynamicLinks
import com.stfalcon.frescoimageviewer.ImageViewer
import kotlinx.android.synthetic.main.activity_booking_details.*
import kotlinx.android.synthetic.main.view_all_rating.*
import kotlinx.android.synthetic.main.view_amenities.*
import kotlinx.android.synthetic.main.view_room_type.*
import kotlinx.android.synthetic.main.view_user_details.*
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread
import org.json.JSONObject
import java.lang.Exception
import java.util.*

class BookingDetailsActivity : BaseActivity() {

    lateinit var adapter: RoomTypeAdapter
    lateinit var dialogDate: Dialog
    lateinit var dialogCalendar: Dialog


    var amentiesOnDrawbleList = java.util.ArrayList<Int>()
    var amentiesOffDrawbleList = java.util.ArrayList<Int>()
    var amenties = arrayOfNulls<ImageView>(10)
    var amenitiesList: List<String> = java.util.ArrayList()
    var amenitiesModelList = java.util.ArrayList<AmenitiesModel>()
    var dateModel = HotelDateModel()
    var isStartDate = false
    var startDate = ""
    var endDate = ""

    var hotelModel: HotelModel? = null
    var hotelBookingModel: HotelBookingModel? = null

    val roomTypeList = ArrayList<RoomTypeModel>()

    var update = false
    var can = false


    var linkText = ""
    val TAG = "generateLink"


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_booking_details)
        supportActionBar?.hide()

        hotelBookingModel = intent.getParcelableExtra(Constant.KEY_BOOKING)
        hotelModel = hotelBookingModel?.hotelModel

        can = intent.getBooleanExtra(Constant.KEY_BOOK_CAN, false)

        if (can) {
            frame_user_contact.visibility = View.GONE
            card_hotel_contact.visibility = View.GONE
        } else {
            frame_user_contact.visibility = View.VISIBLE
            card_hotel_contact.visibility = View.GONE
        }


        initClicks()
    }

    private fun sharePost() {
        progress?.show()
        generateLink()
    }
    private fun initClicks() {

        frame_location.setOnClickListener {
            if (checkPermissions()) {
                update = true
                val intent = Intent(this, MapNewActivity::class.java)
                intent.putExtra(Constant.KEY_HOTEL, hotelModel)
                startActivity(intent)
            }
        }


        im_fav.tag = false
        im_fav.setOnClickListener {
            update = true
            if ((im_fav.tag as Boolean)) {
                removeToFav()
            } else {
                addToFav()
            }

        }

        back.setOnClickListener {

            super.onBackPressed()
        }

        read_more.setOnClickListener {
            val intent = Intent(this, HotelDecriptionActivity::class.java)
            intent.putExtra(Constant.KEY_HOTEL, hotelModel)
            intent.putExtra("show", "des")
            startActivity(intent)
        }

        standard_term.setOnClickListener {
            val intent = Intent(this, HotelDecriptionActivity::class.java)
            intent.putExtra(Constant.KEY_HOTEL, hotelModel)
            intent.putExtra("show", "term")
            startActivity(intent)
        }

        hotel_phone.setOnClickListener {
            val status = PermissionStatus().getPermissionStatus(this, PermissionStatus.PERMISSION_CALL)
            if (status == PermissionStatus.GRANTED) {
                val intent = Intent(Intent.ACTION_DIAL, Uri.parse("tel:${hotelModel?.phoneNumber}"))
                startActivity(intent)
            }

        }

        frame_cancel.setOnClickListener {
            alert?.title = ""
            alert?.content = getString(R.string.bd_hint_warn_cancel_book)
            alert?.listenr = object : DialogListener() {
                override fun okClick() {
                    updateStatus(BookingStatus.CANCEL)
                }
            }
            alert?.show()
        }

        frame_accept.setOnClickListener {
            val satus = ConstantFunction.capsFirstLetter(hotelBookingModel?.status!!)
            when(satus){

                BookingStatus.CONFIRM ->{
                    if (can) {
                        //  Toast.makeText(this,"This feature is not implement!!",Toast.LENGTH_SHORT).show()
                        val intent = Intent(this, PaymentActivity::class.java)
                        intent.putExtra(Constant.KEY_BOOKING, hotelBookingModel)
                        startActivityForResult(intent, Constant.PAYMENT_REQUEST)
                    }
                }

                BookingStatus.ACCEPT ->{
                    if (can) {
                        //  Toast.makeText(this,"This feature is not implement!!",Toast.LENGTH_SHORT).show()
                        val intent = Intent(this, PaymentActivity::class.java)
                        intent.putExtra(Constant.KEY_BOOKING, hotelBookingModel)
                        startActivityForResult(intent, Constant.PAYMENT_REQUEST)
                    }
                }

                else ->{
                    updateStatus(BookingStatus.CONFIRM)
                }
            }

        }

        frame_reject.setOnClickListener {


            alert?.title = ""
            when (ConstantFunction.capsFirstLetter(status_reject.text.toString())) {
                getString(R.string.bd_hint_cancel) -> {
                    alert?.content = getString(R.string.bd_hint_warn_cancel_book)
                    alert?.listenr = object : DialogListener() {
                        override fun okClick() {
                            updateStatus(BookingStatus.CANCEL)
                        }
                    }

                }

                getString(R.string.bd_hint_reject) -> {
                    alert?.content = getString(R.string.bd_hint_warn_rej_book)
                    alert?.listenr = object : DialogListener() {
                        override fun okClick() {
                            updateStatus(BookingStatus.REJECT)
                        }
                    }

                }
            }

            alert?.show()
        }

        im_share.setOnClickListener {
            sharePost()
        }
    }

    override fun onResume() {
        setData()

        getFeedback()
        super.onResume()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == Constant.PAYMENT_REQUEST && resultCode == Activity.RESULT_OK) {
            setResult(Activity.RESULT_OK)
            finish()
        }
    }

    private fun setData() {

        try {
            hotel_name.setText(hotelModel?.name)
            hotel_des.setText(hotelModel?.description)
            hint_price_pm.setText("USD $${hotelModel!!.rate} " + getString(R.string.ap_hint_pernight))
            hotel_phone.setText("${hotelModel!!.phoneNumber}")
            hotel_address.setText("${hotelModel!!.fullAddress}")

            try {
                rating_bar.rating = hotelModel?.rating!!.toFloat()
                date_start.setText(ConstantFunction.getOnlyDate(hotelBookingModel?.dateFrom!!.toLong()))
                date_end.setText(ConstantFunction.getOnlyDate(hotelBookingModel!!.dateTo.toLong()))
            } catch (e: Exception) {
                e.printStackTrace()
                date_start.setText(hotelBookingModel!!.dateFrom)
                date_end.setText(hotelBookingModel!!.dateTo)
            }
            setUserData()
            setStatus()

            if (!TextUtils.isEmpty(hotelModel?.description)) {
                if (hotelModel!!.description.length > 200) {
                    read_more.visibility = View.VISIBLE
                } else {
                    read_more.visibility = View.GONE
                }
            }
//        rating_bar.rating(hotelModel?.rate)
            if (hotelModel?.imageList!!.size > 0) {

                if (prefManager.getBoolean(Constant.KEY_USER_LOGIN_STATUS)) {
                    im_fav.visibility = View.VISIBLE
                } else {
                    im_fav.visibility = View.GONE
                }
                doAsync {
                    val fav = AppDatabase.getAppDatabase().favPostDao().getFavPost(hotelModel!!.id)

                    if (fav != null && fav.id.equals(hotelModel!!.id)) {
                        uiThread {
                            im_fav.setImageResource(R.drawable.ic_like_red_on)
                            im_fav.tag = true
                        }
                    } else {
                        uiThread {
                            im_fav.setImageResource(R.drawable.ic_like_gray_off)
                            im_fav.tag = false
                        }
                    }
                }

                hotel_image.setOnClickListener {
                    showAllImage(hotelModel!!.imageList, 0)
                }


                downloadImage(hotelModel!!.imageList[0]!!.url, hotel_image)
                if (hotelModel?.imageList!!.size > 1) {
                    frame_more.visibility = View.VISIBLE
                    downloadImage(hotelModel!!.imageList[1]!!.url, hotel_image2)

                    val count = (hotelModel?.imageList!!.size) - 2
                    if (count > 0) {
                        more_image.visibility = View.VISIBLE
                        more_image.text = "+$count " + getString(R.string.bd_hint_more)
                    } else {
                        more_image.visibility = View.GONE
                    }

                    frame_more.setOnClickListener {
                        showAllImage(hotelModel!!.imageList, 1)
                    }
                } else {
                    frame_more.visibility = View.GONE
                }

            }

          /*  val roomModel = RoomTypeModel()
            roomModel.hotelId = hotelModel?.id!!
            roomModel.person = hotelModel?.person!!
            roomModel.rooms = hotelModel?.rooms!!
            roomModel.children = hotelModel?.children!!
            roomModel.wc = hotelModel?.wc_number!!

            roomTypeList.clear()
            roomTypeList.add(roomModel)

            adapter = RoomTypeAdapter(this, roomTypeList)
            view_pager_room.adapter = adapter
            tab_layout_room.setupWithViewPager(view_pager_room)
*/

            adult_count.setText(hotelModel?.person+" "+getString(R.string.hint_adult))
            room_count.setText(hotelModel?.rooms+" "+getString(R.string.hint_room))
            children_count.setText(hotelModel?.children+" "+getString(R.string.hint_child))
            wc_count.setText(hotelModel?.wc_number+" "+getString(R.string.hint_wc))

            initViewes()

        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    private fun setStatus() {

        val satus = ConstantFunction.capsFirstLetter(hotelBookingModel?.status!!)
        when (satus) {
            BookingStatus.PENDING -> {
                if (can) {
                    status_reject.setText(getString(R.string.bd_hint_warn_cancel))
                    frame_accept.visibility = View.GONE
                } else {
                    status_confirm.setText(getString(R.string.bd_hint_warn_accept))
                    status_reject.setText(getString(R.string.bd_hint_warn_reject))
                    frame_accept.visibility = View.VISIBLE
                    frame_reject.visibility = View.VISIBLE
                }

            }

            BookingStatus.CONFIRM -> {
                if (can) {
                    status_confirm.setText(getString(R.string.hint_pay_now))

                    status_reject.setText(getString(R.string.bk_status_cancel))
                } else {
                    status_confirm.setText(getString(R.string.bk_status_accepted))
                    frame_reject.visibility = View.GONE
                }
            }

            BookingStatus.ACCEPT -> {
                if (can) {
                    status_confirm.setText(getString(R.string.bd_hint_warn_pay_now))

                    status_reject.setText(getString(R.string.bk_status_cancel))
                } else {
                    status_confirm.setText(getString(R.string.bk_status_accepted))
                    frame_reject.visibility = View.GONE
                }
            }

            BookingStatus.REJECT -> {
                status_reject.setText(getString(R.string.bk_status_rejected))
                frame_reject.isEnabled = false
                frame_reject.setBackgroundResource(R.drawable.back_corner_gray)

                frame_accept.visibility = View.GONE
            }

            BookingStatus.CANCEL -> {

                status_reject.setText(getString(R.string.bk_status_canceled))
                frame_reject.isEnabled = false
                frame_reject.setBackgroundResource(R.drawable.back_corner_gray)

                frame_accept.visibility = View.GONE
            }

            BookingStatus.PAID -> {
                status_confirm.setText(getString(R.string.bk_status_paid))
                //   frame_accept.isEnabled = false
                frame_accept.setBackgroundResource(R.drawable.back_corner_green)

                frame_reject.isEnabled = false
                frame_reject.visibility = View.GONE

                card_hotel_contact.visibility = if(can)View.VISIBLE else View.GONE
            }

            BookingStatus.CHECKOUT -> {

            }
        }
    }

    private fun setUserData() {
        val userModel = hotelBookingModel?.userModel

        if (userModel != null) {
            username.setText(userModel.name)
            hint_about_user.setText(getString(R.string.bd_hint_con_to) + " " + userModel.name)
            //user_role.setText(userModel.name)
            user_des.setText(userModel.description)
            user_email.setText(userModel.email)
            user_phone.setText(userModel.phone)

            user_phone.setOnClickListener {
                val status = PermissionStatus().getPermissionStatus(this, PermissionStatus.PERMISSION_CALL)
                if (status == PermissionStatus.GRANTED) {
                    val intent = Intent(Intent.ACTION_DIAL, Uri.parse("tel:${userModel.phone}"))
                    startActivity(intent)
                }
            }

            if (!TextUtils.isEmpty(userModel.imageUrl)) {
                Glide.with(this)
                        .asBitmap()
                        .load(userModel.imageUrl)
                        .apply(RequestOptions().override(200).error(R.drawable.user_default_pic))
                        .into(user_profile)
            }
        }
    }


    private fun initViewes() {
        amenitiesModelList.addAll(hotelModel?.amenitiesList!!)

        amentiesOnDrawbleList.add(R.drawable.ic_wifi_white)
        amentiesOnDrawbleList.add(R.drawable.ic_desk_white)
        amentiesOnDrawbleList.add(R.drawable.ic_drink_white)
        amentiesOnDrawbleList.add(R.drawable.ic_parking_white)
        amentiesOnDrawbleList.add(R.drawable.ic_spa_white)
        amentiesOnDrawbleList.add(R.drawable.ic_dining_white)
        amentiesOnDrawbleList.add(R.drawable.ic_gym_white)
        amentiesOnDrawbleList.add(R.drawable.ic_smart_tv_white_)
        amentiesOnDrawbleList.add(R.drawable.ic_swim_white)
        amentiesOnDrawbleList.add(R.drawable.ic_flame_white)

        amentiesOffDrawbleList.add(R.drawable.ic_wifi_gray)
        amentiesOffDrawbleList.add(R.drawable.ic_desk_gray)
        amentiesOffDrawbleList.add(R.drawable.ic_drink_gray)
        amentiesOffDrawbleList.add(R.drawable.ic_parking_gray)
        amentiesOffDrawbleList.add(R.drawable.ic_spa_gray)
        amentiesOffDrawbleList.add(R.drawable.ic_dining_gray)
        amentiesOffDrawbleList.add(R.drawable.ic_gym_gray)
        amentiesOffDrawbleList.add(R.drawable.ic_smart_tv_gray)
        amentiesOffDrawbleList.add(R.drawable.ic_swim_gray)
        amentiesOffDrawbleList.add(R.drawable.ic_flame_gray)


        amenties =  arrayOf(im_wifi, im_desk, im_drink, im_park, im_spa, im_kitchen, im_gym, im_smartTV, im_pool, im_bornFire)

        if (amenitiesModelList.size == amenties.size) {
            for (i in 0 until amenties.size) {
                val amenitiesModel = amenitiesModelList[i]
                amenties[i]!!.tag = !amenitiesModel.status
                setAmentiesUi(amenties[i], i)
//                amenties[i]!!.setOnClickListener {
//                    setAmentiesUi(amenties[i], i)
//                }
            }
        }
    }

    private fun setAmentiesUi(imageView: ImageView?, i: Int) {
        if ((amenties[i]!!.tag as Boolean)) {
            amenties[i]!!.tag = false
            amenties[i]!!.setBackgroundResource(R.drawable.round_gray)
            amenties[i]!!.setImageResource(amentiesOffDrawbleList[i])
        } else {
            amenties[i]!!.tag = true
            amenties[i]!!.setBackgroundResource(R.drawable.round_pri)
            amenties[i]!!.setImageResource(amentiesOnDrawbleList[i])
        }
    }

    private fun downloadImage(url: String, hotel_image: ImageView) {

        Glide.with(this)
                .asBitmap()
                .load(url)
                .apply(RequestOptions().override(500))
                .listener(object : RequestListener<Bitmap> {
                    override fun onLoadFailed(e: GlideException?, model: Any?, target: Target<Bitmap>?, isFirstResource: Boolean): Boolean {
                        return false
                    }

                    override fun onResourceReady(resource: Bitmap?, model: Any?, target: com.bumptech.glide.request.target.Target<Bitmap>?, dataSource: com.bumptech.glide.load.DataSource?, isFirstResource: Boolean): Boolean {

                        return false
                    }

                })
                .into(hotel_image)
    }

    private fun showAllImage(imageList2: ArrayList<HotelImageModel>, position: Int) {

        var currentImagePositon = position
        Fresco.initialize(this@BookingDetailsActivity);
        val overView = ImageOverLayView(this@BookingDetailsActivity)

        val mImageViewerBuilder = ImageViewer.Builder<HotelImageModel>(this@BookingDetailsActivity, imageList2)
                .setFormatter(object : ImageViewer.Formatter<HotelImageModel> {
                    override fun format(t: HotelImageModel?): String {
                        return t!!.url
                    }

                })
        val hierarchyBuilder = GenericDraweeHierarchyBuilder.newInstance(getResources())
                .setFailureImage(R.drawable.ic_warning_blue)
                .setProgressBarImage(R.drawable.style_circular)
        // .setPlaceholderImage(R.drawable.placeholderDrawable);
        mImageViewerBuilder.setStartPosition(position)
                .setOverlayView(overView)
                .setBackgroundColor(ContextCompat.getColor(this@BookingDetailsActivity, R.color.colorBlack))
                .show()

    }

    private fun checkPermissions(): Boolean {

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED ||
                ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(this,
                    arrayOf(Manifest.permission.ACCESS_FINE_LOCATION,
                            Manifest.permission.ACCESS_COARSE_LOCATION), 112)
            return false
        }

        return true
    }


    private fun getFeedback() {

        val param = JSONObject()
        param.put("api_key", Constant.API_KEY)
        param.put("hotel_id", hotelModel?.id)

        frame_load_rat.visibility = View.VISIBLE
        ServiceRequest(object : ApiResponseListener {
            override fun onCompleted(`object`: Any) {
                try {
                    frame_load_rat.visibility = View.GONE
                    val json = JSONObject(`object`.toString())
                    if (json.has("data")) {
                        val array = json.getJSONArray("data")
                        for (i in 0 until array.length()) {
                            var obj = JSONObject()
                            if (array.get(i) is JSONObject) {
                                obj = array.getJSONObject(i)

                                if (obj.has("avgCommunication")) {
                                    rat_communication.rating = obj.get("avgCommunication").toString().toFloat()
                                }

                                if (obj.has("avgProficiency")) {
                                    rating_proficiency.rating = obj.get("avgProficiency").toString().toFloat()
                                }

                                if (obj.has("avgPrice")) {
                                    rating_price.rating = obj.get("avgPrice").toString().toFloat()
                                }

                                if (obj.has("avgDelivery")) {
                                    rating_delivery.rating = obj.get("avgDelivery").toString().toFloat()
                                }
                            }
                        }

                        if (array.length() <= 0) {
                            rating_price.rating = 1F
                            rat_communication.rating = 1F
                            rating_delivery.rating = 1F
                            rating_proficiency.rating = 1F
                        }
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

            override fun onError(errorMessage: String) {
                frame_load_rat.visibility = View.GONE
                errorDialog?.show(getString(R.string.hint_error), getString(R.string.bd_hint_failed) + " $errorMessage")
            }

        }).getHotelFeedbackFeedback(param)
    }

    private fun addToFav() {
        if (hotelModel == null) {
            return
        }
        val param = JSONObject()
        param.put("api_key", Constant.API_KEY)
        param.put("hotel_id", hotelModel?.id)
        param.put("user_id", prefManager.getString(Constant.KEY_USER_ID))

        progress?.show()

        ServiceRequest(object : ApiResponseListener {
            override fun onCompleted(`object`: Any) {
                progress?.dismiss()
                Toast.makeText(this@BookingDetailsActivity, getString(R.string.bd_hint_added_to_fav), Toast.LENGTH_SHORT).show()
                update = true
                im_fav.setImageResource(R.drawable.ic_like_red_on)
                im_fav.tag = true
                doAsync {
                    val fav = FavPost()
                    fav.id = hotelModel?.id!!
                    fav.post_id = hotelModel?.id!!
                    AppDatabase.getAppDatabase().favPostDao().insert(fav)
                }
            }

            override fun onError(errorMessage: String) {
                progress?.dismiss()
                errorDialog?.show(getString(R.string.hint_error), "Failed $errorMessage")
            }

        }).addToFav(param)
    }

    private fun removeToFav() {
        if (hotelModel == null) {
            return
        }
        val param = JSONObject()
        param.put("api_key", Constant.API_KEY)
        param.put("hotel_id", hotelModel?.id)
        param.put("user_id", prefManager.getString(Constant.KEY_USER_ID))

        progress?.show()

        ServiceRequest(object : ApiResponseListener {
            override fun onCompleted(`object`: Any) {
                progress?.dismiss()

                update = true
                Toast.makeText(this@BookingDetailsActivity, getString(R.string.bd_hint_remove_to_fav), Toast.LENGTH_SHORT).show()
                im_fav.setImageResource(R.drawable.ic_like_gray_off)
                im_fav.tag = false
                doAsync {
                    val fav = FavPost()
                    fav.id = hotelModel?.id!!
                    fav.post_id = hotelModel?.id!!
                    AppDatabase.getAppDatabase().favPostDao().deleteFavPost(fav.id)
                }
            }

            override fun onError(errorMessage: String) {
                progress?.dismiss()
                errorDialog?.show(getString(R.string.hint_error), "Failed $errorMessage")
            }

        }).removeToFav(param)
    }

    private fun updateStatus(status: String) {
        if (hotelBookingModel?.hotelModel!!.isDelete) {
            errorDialog?.show(getString(R.string.hint_error), getString(R.string.propety_deleted))
            return
        }

        val param = JSONObject()
        param.put("api_key", Constant.API_KEY)
        param.put("booking_id", hotelBookingModel?.id)
        param.put("status", status)
        progress?.show()

        ServiceRequest(object : ApiResponseListener {
            override fun onCompleted(`object`: Any) {
                progress?.dismiss()

                if (status == BookingStatus.CONFIRM || status == BookingStatus.ACCEPT) {
                    Toast.makeText(this@BookingDetailsActivity, getString(R.string.bd_hint_req_accept), Toast.LENGTH_SHORT).show()
                } else {
                    if (status == BookingStatus.REJECT) {
                        Toast.makeText(this@BookingDetailsActivity, getString(R.string.bd_hint_req_reject), Toast.LENGTH_SHORT).show()
                    } else {
                        Toast.makeText(this@BookingDetailsActivity, getString(R.string.bd_hint_req_cancel), Toast.LENGTH_SHORT).show()
                    }

                }
                hotelBookingModel!!.status = status
                setStatus()
            }

            override fun onError(errorMessage: String) {
                progress?.dismiss()

                errorDialog?.show(getString(R.string.hint_error), "$errorMessage")
            }

        }).updateBooking(param)
    }


    private fun generateLink() {
        //linkText = ConstantFunction.getDynamicLink(hotelModel!!.id)
        if (TextUtils.isEmpty(linkText)) {
            FirebaseApp.initializeApp(this)
            val url = "https://kashtahPORT.com?post_id=${hotelModel?.id}&owner_id=${hotelModel!!.ownerId}&type=${hotelModel!!.type}"
            val shortLinkTask = FirebaseDynamicLinks.getInstance().createDynamicLink()
                    .setLink(Uri.parse(url))
                    .setDomainUriPrefix(Constant.PREFIX)
                    .setAndroidParameters(
                            DynamicLink.AndroidParameters.Builder("com.itwhiz4u.q8mercato.kashtahport")
                                    .build())

                    .setIosParameters(
                            DynamicLink.IosParameters.Builder("com.itwhiz4u.q8mercato.kashtahport")
                                    .setAppStoreId("1467932974")
                                    .build())

                    .setSocialMetaTagParameters(DynamicLink.SocialMetaTagParameters.Builder()
                            .setTitle(hotelModel!!.name)
                            .setDescription(hotelModel!!.description)
                            .setImageUrl(Uri.parse(hotelModel!!.imageList[0].url))
                            .build())

                    .setGoogleAnalyticsParameters(
                            DynamicLink.GoogleAnalyticsParameters.Builder()
                                    .setSource("orkut")
                                    .setMedium("social")
                                    .setCampaign("example-promo")
                                    .build())

                    .buildShortDynamicLink()

                    .addOnSuccessListener { result ->

                        val shortLink = result.shortLink
                        val flowchartLink = result.previewLink
                        linkText = shortLink.toString()

                        ConstantFunction.saveDynamicLink(hotelModel!!.id, linkText)
                        Log.e(TAG, "LINK :: $shortLink  *** $flowchartLink")
                        shareLink()

                    }.addOnFailureListener {
                        Log.e(TAG, "LINK :: addOnFailureListener  *** ")
                        progress?.dismiss()
                    }
        } else {
            shareLink()
        }
    }

    private fun shareLink() {

        runOnUiThread {

            progress?.dismiss()

            val shareIntent = Intent()
            shareIntent.setAction(Intent.ACTION_SEND)
            shareIntent.type = "text/plain"
            shareIntent.putExtra(Intent.EXTRA_TEXT, linkText)
            shareIntent.putExtra(Intent.EXTRA_SUBJECT, linkText)

            startActivity(Intent.createChooser(shareIntent,"Select"))

        }


    }

}
