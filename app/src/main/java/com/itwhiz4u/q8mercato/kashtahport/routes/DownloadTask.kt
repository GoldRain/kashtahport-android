package com.itwhiz4u.q8mercato.kashtahport.routes

import android.os.AsyncTask
import android.util.Log

import com.google.android.gms.maps.model.LatLng

class DownloadTask : AsyncTask<String, Void, String>() {
    // Downloading data in non-ui thread
    override fun doInBackground(vararg url: String): String {

        // For storing data from web service
        var data = ""

        try {
            // Fetching the data from web service
          //  data = downloadUrl(url[0])
        } catch (e: Exception) {
            Log.d("Background Task", e.toString())
        }

        return data
    }

    // Executes in UI thread, after the execution of
    // doInBackground()
    override fun onPostExecute(result: String) {
        super.onPostExecute(result)

        //val parserTask = ParserTask()

        // Invokes the thread for parsing the JSON data
       // parserTask.execute(result)
    }


    private fun getDirectionsUrl(origin: LatLng, dest: LatLng): String {

        // Origin of route
        val str_origin = "origin=" + origin.latitude + "," + origin.longitude

        // Destination of route
        val str_dest = "destination=" + dest.latitude + "," + dest.longitude

        // Sensor enabled
        val sensor = "sensor=false"

        // Building the parameters to the web service
        val parameters = "$str_origin&$str_dest&$sensor"

        // Output format
        val output = "json"

        // Building the url to the web service

        return "https://maps.googleapis.com/maps/api/directions/$output?$parameters"
    }

}
