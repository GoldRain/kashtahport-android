package com.itwhiz4u.q8mercato.kashtahport.Retrofit.Model.Request

import com.google.gson.annotations.SerializedName

data class AddToFavRequest(
        @SerializedName("api_key")
        var api_key: String,

        @SerializedName("hotel_id")
        var hotel_id: String,

        @SerializedName("user_id")
        var user_id: String
)