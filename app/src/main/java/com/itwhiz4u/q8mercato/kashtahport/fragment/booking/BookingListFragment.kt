package com.itwhiz4u.q8mercato.kashtahport.fragment.booking


import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.itwhiz4u.q8mercato.kashtahport.Api.ApiResponseListener
import com.itwhiz4u.q8mercato.kashtahport.Api.ServiceRequest

import com.itwhiz4u.q8mercato.kashtahport.R
import com.itwhiz4u.q8mercato.kashtahport.activities.BookingDetailsActivity
import com.itwhiz4u.q8mercato.kashtahport.activities.BookingListActivity
import com.itwhiz4u.q8mercato.kashtahport.adapter.HotelBookingRequestAdapter
import com.itwhiz4u.q8mercato.kashtahport.customDialog.DialogListener
import com.itwhiz4u.q8mercato.kashtahport.helper.BookingStatus
import com.itwhiz4u.q8mercato.kashtahport.helper.Constant
import com.itwhiz4u.q8mercato.kashtahport.model.EventModel
import com.itwhiz4u.q8mercato.kashtahport.model.HotelBookingModel
import com.itwhiz4u.q8mercato.kashtahport.parser.BookingParser
import kotlinx.android.synthetic.main.activity_booking_list.*
import kotlinx.android.synthetic.main.fragment_booking_list.view.*
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode
import org.json.JSONObject
import java.lang.Exception
import java.util.*

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"


class BookingListFragment : Fragment() {

    val TAG = "BookingHistory"
    lateinit var fragView: View
    var fragPosition = 0

    var bookingListActivity: BookingListActivity?= null
    lateinit var adapter: HotelBookingRequestAdapter

    var hotelList = ArrayList<HotelBookingModel>()

    fun setActivity(bookingListActivity: BookingListActivity) {
        this.bookingListActivity = bookingListActivity
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        fragView = inflater.inflate(R.layout.fragment_booking_list, container, false)

        hotelList = ArrayList()

      //  header_name.setText(getString(R.string.header_booking))

        initList()
        if(bookingListActivity!!.view_pager.currentItem == 0){
            getBookings()
        }

        fragView.swipe_to_refresh.setOnRefreshListener {
            if(fragView.swipe_to_refresh.isRefreshing){
                getBookings()
            }
        }
        return fragView
    }

    private fun initList() {

        adapter = HotelBookingRequestAdapter(bookingListActivity!!,hotelList,object : HotelBookingRequestAdapter.OnItemClick() {
            override fun onItemClick(position: Int) {

                getBookingDetails(position,"")

            }

            override fun onUpdateBooking(position: Int, status: String) {
                if(status == BookingStatus.REJECT){
                    bookingListActivity?.alert?.title = ""
                    bookingListActivity?.alert?.content = getString(R.string.bd_hint_warn_rej_book)
                    bookingListActivity?.alert?.listenr = object : DialogListener(){
                        override fun okClick() {
                            //updateStatus(position,status)
                            getBookingDetails(position,status,1)
                        }
                    }

                    bookingListActivity?.alert?.show()
                }else{
                    getBookingDetails(position,status,1)
                }

            }
        })

        fragView.recycler_view.layoutManager = LinearLayoutManager(activity!!)

        val item = DividerItemDecoration(activity!!, DividerItemDecoration.VERTICAL)
        item.setDrawable(ContextCompat.getDrawable(activity!!,R.drawable.list_div)!!)
        fragView.recycler_view.addItemDecoration(item)
        fragView.recycler_view.adapter = adapter

        adapter.notifyDataSetChanged()

    }

    override fun onResume() {
        EventBus.getDefault().register(this)
        super.onResume()
    }

    override fun onPause() {
        EventBus.getDefault().unregister(this)
        super.onPause()
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onMessageEvent(event: EventModel) {
        when(event.type){
            Constant.EVENT_UPDATE_BOOKING ->{
                if(bookingListActivity!!.view_pager.currentItem == 0){
                    getBookings()
                }
            }
        }
    }

    fun getBookings(){
        val param = JSONObject()
        param.put("api_key", Constant.API_KEY)
        param.put("user_id", bookingListActivity!!.prefManager.getString(Constant.KEY_USER_ID))

        if(!fragView.swipe_to_refresh.isRefreshing){
            bookingListActivity!!.showProgress(fragView.frame_loader)
        }

        hotelList.clear()
        ServiceRequest(object : ApiResponseListener {
            override fun onCompleted(`object`: Any) {
                // progress?.dismiss()

                bookingListActivity!!.hideProgress(fragView.frame_loader,fragView.swipe_to_refresh)
                try {
                    val json = JSONObject(`object`.toString())
                    if(json.has("data")){
                        val array  = json.getJSONArray("data")
                        val parser  = BookingParser()
                        for(i in 0 until array.length()){
                            val bookingModel = parser.parse(array.getJSONObject(i))

                            if(bookingModel.dateTo.toLong() > System.currentTimeMillis()){
                                hotelList.add(bookingModel)
                            }

                        }
                        Collections.reverse(hotelList)
                        adapter.notifyDataSetChanged()
                    }
                }catch (e: Exception){
                    e.printStackTrace()
                }finally {
                    bookingListActivity!!.showEmptyView(fragView.frame_empty,(hotelList.size == 0))
                }
            }

            override fun onError(errorMessage: String) {
                bookingListActivity!!.hideProgress(fragView.frame_loader,fragView.swipe_to_refresh)
                bookingListActivity!!.showEmptyView(fragView.frame_empty,(hotelList.size == 0))

            }

        }).getBookingList(param)
    }

    fun updateStatus(position: Int, status: String) {

        if(hotelList[position].hotelModel!!.isDelete){
            bookingListActivity!!.errorDialog?.show(getString(R.string.hint_error),getString(R.string.propety_deleted))
            return
        }

        val param = JSONObject()
        param.put("api_key", Constant.API_KEY)
        param.put("booking_id", hotelList[position].id)
        param.put("status", status)
        bookingListActivity!!.progress?.show()

        ServiceRequest(object : ApiResponseListener {
            override fun onCompleted(`object`: Any) {
                bookingListActivity!!.progress?.dismiss()

                try {
                    if(status == BookingStatus.CONFIRM || status == BookingStatus.ACCEPT){

                        Toast.makeText(activity!!,getString(R.string.frag_bl_req_accept),Toast.LENGTH_SHORT).show()
                    }else{

                        Toast.makeText(activity!!,getString(R.string.frag_bl_req_rej),Toast.LENGTH_SHORT).show()
                    }
                    hotelList[position].status = status
                    adapter.notifyItemChanged(position)
                }catch (e: Exception){
                    e.printStackTrace()
                }
            }

            override fun onError(errorMessage: String) {
                bookingListActivity!!.progress?.dismiss()

                bookingListActivity?.errorDialog?.show(getString(R.string.hint_error),"$errorMessage")
            }

        }).updateBooking(param)
    }

    private fun getBookingDetails(position: Int,status: String, action: Int=0) {
        activity!!.runOnUiThread {
            bookingListActivity?.progress?.show()
        }

        val param = JSONObject()
        param.put("api_key", Constant.API_KEY)

        param.put("booking_id", hotelList[position].id)

        ServiceRequest(object :ApiResponseListener{
            override fun onCompleted(`object`: Any) {
                bookingListActivity?.progress?.dismiss()
                try {
                    val json = JSONObject(`object`.toString())
                    if(json.has("data")){
                        val array  = json.getJSONArray("data")
                        val parser  = BookingParser()
                        for(i in 0 until array.length()){
                            val bookingModel = parser.parse(array.getJSONObject(i))

                            Log.e("bookingModel"," *** "+bookingModel!!.hotelModel!!.isDelete)
                            if(!bookingModel!!.hotelModel!!.isDelete ){
                                when(action){
                                    0 ->{
                                        val intent = Intent(activity!!,BookingDetailsActivity::class.java)
                                        intent.putExtra(Constant.KEY_BOOKING,hotelList[position])
                                        startActivityForResult(intent,Constant.HOTEL_DETAIL_REQUEST)
                                    }

                                    1 ->{
                                        updateStatus(position,status)
                                    }

                                }

                            }else{
                                bookingListActivity?.errorDialog!!.show(getString(R.string.hint_error),getString(R.string.propety_deleted))
                            }
                            break
                        }
                    }
                }catch (e: Exception){
                    e.printStackTrace()
                }
            }

            override fun onError(errorMessage: String) {
                activity!!.runOnUiThread {
                    bookingListActivity!!.progress?.dismiss()
                    bookingListActivity!!.errorDialog?.show(getString(R.string.hint_error),"$errorMessage")
                }
            }

        }).getBookingDetails(param)


    }
}
