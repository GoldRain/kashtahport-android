package com.itwhiz4u.q8mercato.kashtahport.parser

import android.util.Log
import com.itwhiz4u.q8mercato.kashtahport.model.TransactionModel
import org.json.JSONObject

class TransactionParser {

    fun parse(jsonObject: JSONObject):TransactionModel{

        val transactionModel = TransactionModel()

        Log.e("TransactionParser"," "+jsonObject.toString())
        if(jsonObject.has("hotel_id")){
            transactionModel.hotel_id = jsonObject.getString("hotel_id")
        }

        if(jsonObject.has("owner_id")){
            transactionModel.owner_id = jsonObject.getString("owner_id")
        }

        if(jsonObject.has("buyer_id")){
            transactionModel.buyer_id = jsonObject.getString("buyer_id")
        }

        if(jsonObject.has("booking_id")){
            transactionModel.booking_id = jsonObject.getString("booking_id")
        }

        if(jsonObject.has("amount")){
            transactionModel.amount = jsonObject.getString("amount")
        }

        if(jsonObject.has("transaction_id")){
            transactionModel.txn_id = jsonObject.getString("transaction_id")
        }
        if(jsonObject.has("display_id")){
            transactionModel.display_id = jsonObject.getString("display_id")
        }

        if(jsonObject.has("title")){
            transactionModel.title = jsonObject.getString("title")
        }

        if(jsonObject.has("_id")){
            transactionModel.id = jsonObject.getString("_id")
        }

        if(jsonObject.has("card_type")){
            transactionModel.card_type = jsonObject.getString("card_type")
        }

        if(jsonObject.has("ref_no")){
            transactionModel.ref_no = jsonObject.getString("ref_no")
        }

        if(jsonObject.has("_id")){
            transactionModel.id = jsonObject.getString("_id")
        }

        if(jsonObject.has("created_at")){
            transactionModel.created_at = jsonObject.get("created_at").toString()
        }

        if(jsonObject.has("currency")){
            transactionModel.currency = jsonObject.get("currency").toString()
        }

        return  transactionModel

    }
}