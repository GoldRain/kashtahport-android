package com.itwhiz4u.q8mercato.kashtahport.Notification

import android.util.Log
import com.onesignal.OSNotification
import com.onesignal.OneSignal

/**
 * Created by bodacious on 28/12/18.
 */
class ExampleNotificationReceivedHandler : OneSignal.NotificationReceivedHandler {
    override fun notificationReceived(notification: OSNotification) {
        Log.e("OneSignalExample", "ExampleNotificationReceivedHandler")
        val data = notification.payload.additionalData
        val notificationID = notification.payload.notificationID
        val customKey: String?
        Log.e("OneSignalExample", " Open NotificationID received: " + notificationID)
        if (data != null) {
            customKey = data.optString("customkey", null)
            if (customKey != null)
                Log.e("OneSignalExample", "customkey set with value: " + customKey)
        }
    }
}