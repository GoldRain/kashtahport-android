package com.itwhiz4u.q8mercato.kashtahport.activities

import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import android.view.View
import android.widget.Toast
import com.itwhiz4u.q8mercato.kashtahport.Api.ApiResponseListener
import com.itwhiz4u.q8mercato.kashtahport.Api.ServiceRequest
import com.itwhiz4u.q8mercato.kashtahport.R
import com.itwhiz4u.q8mercato.kashtahport.adapter.HotelPropertyAdapter
import com.itwhiz4u.q8mercato.kashtahport.helper.Constant
import com.itwhiz4u.q8mercato.kashtahport.model.HotelPropertyModel
import com.itwhiz4u.q8mercato.kashtahport.parser.PropertyParser
import kotlinx.android.synthetic.main.activity_property_list.*
import kotlinx.android.synthetic.main.view_header_toolbar.*
import org.json.JSONObject
import java.lang.Exception
import java.util.*

class PropertyListActivity : BaseActivity() {


    val TAG = "PropertyList"
    lateinit var adapter: HotelPropertyAdapter

    var hotelList = ArrayList<HotelPropertyModel>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_property_list)
        supportActionBar?.hide()

        hotelList = ArrayList()

        header_name.setText(getString(R.string.header_property))

        adapter = HotelPropertyAdapter(this,hotelList,object : HotelPropertyAdapter.OnItemClick() {
            override fun onItemClick(position: Int,flag:Boolean) {
                updateProperty(position,flag)
            }

        })

        recycler_view.layoutManager = LinearLayoutManager(this)

        val item = DividerItemDecoration(this, DividerItemDecoration.VERTICAL)
        item.setDrawable(ContextCompat.getDrawable(this,R.drawable.list_div)!!)
        recycler_view.addItemDecoration(item)
        recycler_view.adapter = adapter

        adapter.notifyDataSetChanged()

        icon_back.visibility = View.VISIBLE
        icon_back.setOnClickListener {
            super.onBackPressed()
        }

        swipe_to_refresh.setOnRefreshListener {
            if(swipe_to_refresh.isRefreshing){
              getProperty()
            }
        }
    }

    override fun onResume() {
        getProperty()
        super.onResume()
    }

    fun getProperty(){
        val param = JSONObject()
        param.put("api_key", Constant.API_KEY)
        param.put("user_id", prefManager.getString(Constant.KEY_USER_ID))

        if(!swipe_to_refresh.isRefreshing){
            showProgress(frame_loader)
        }

        hotelList.clear()

        ServiceRequest(object : ApiResponseListener {
            override fun onCompleted(`object`: Any) {

                hideProgress(frame_loader,swipe_to_refresh)
                try {
                    val json = JSONObject(`object`.toString())
                    Log.e(TAG, " ss "+hotelList.size)
                    if(json.has("data")){
                        val array  = json.getJSONArray("data")
                        val parser  = PropertyParser()

                        for(i in 0 until array.length()){
                            Log.e(TAG," OBJ ${array.getJSONObject(i)}")
                            val property = parser.parse(array.getJSONObject(i))
                            hotelList.add(property)
                        }

                        Collections.reverse(hotelList)
                        Log.e(TAG, " ss "+hotelList.size)
                        adapter.notifyDataSetChanged()
                    }
                }catch (e: Exception){
                    Log.e(TAG, " ss "+e.message)
                    e.printStackTrace()
                }finally {
                    showEmptyView(frame_empty,(hotelList.size ==0))
                }
            }

            override fun onError(errorMessage: String) {
                hideProgress(frame_loader,swipe_to_refresh)
                showEmptyView(frame_empty,(hotelList.size ==0))
                Toast.makeText(this@PropertyListActivity,"Failed $errorMessage", Toast.LENGTH_SHORT).show()
            }

        }).getPropertyList(param)
    }

    private fun updateProperty(position: Int, flag: Boolean) {
        val param = JSONObject()
        param.put("api_key", Constant.API_KEY)
        param.put("hotel_id", hotelList[position].id)
        param.put("is_enable", ""+flag)
        progress?.show()

        ServiceRequest(object : ApiResponseListener {
            override fun onCompleted(`object`: Any) {
                progress?.dismiss()

                try {
                    hotelList[position].isEnable = flag

                    if(flag){
                        Toast.makeText(this@PropertyListActivity,getString(R.string.pl_hint_property_enabled),Toast.LENGTH_SHORT).show()
                    }else{
                        Toast.makeText(this@PropertyListActivity,getString(R.string.pl_hint_property_disabled),Toast.LENGTH_SHORT).show()
                    }
                    adapter.notifyItemChanged(position)
                }catch (e: Exception){
                    Log.e(TAG, " ss "+e.message)
                    e.printStackTrace()
                }
            }

            override fun onError(errorMessage: String) {
                progress?.dismiss()
                Toast.makeText(this@PropertyListActivity,"$errorMessage", Toast.LENGTH_SHORT).show()
            }

        }).manageProperty(param)
    }


}
