package com.itwhiz4u.q8mercato.kashtahport.model

import android.os.Parcel
import android.os.Parcelable

class RoomTypeModel() :Parcelable {

    var id = ""
    var hotelId = ""
    var person= ""
    var rooms= ""
    var wc= ""
    var children= ""
    var created_at= "0"

    constructor(parcel: Parcel) : this() {
        id = parcel.readString()
        hotelId = parcel.readString()
        person = parcel.readString()
        rooms = parcel.readString()
        wc = parcel.readString()
        children = parcel.readString()
        created_at = parcel.readString()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(id)
        parcel.writeString(hotelId)
        parcel.writeString(person)
        parcel.writeString(rooms)
        parcel.writeString(wc)
        parcel.writeString(children)
        parcel.writeString(created_at)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<RoomTypeModel> {
        override fun createFromParcel(parcel: Parcel): RoomTypeModel {
            return RoomTypeModel(parcel)
        }

        override fun newArray(size: Int): Array<RoomTypeModel?> {
            return arrayOfNulls(size)
        }
    }


}