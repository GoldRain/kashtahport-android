package com.itwhiz4u.q8mercato.kashtahport.activities

import android.databinding.DataBindingUtil
import android.os.Bundle
import android.text.TextUtils
import android.view.View
import android.widget.Toast
import com.itwhiz4u.q8mercato.kashtahport.Api.ApiResponseListener
import com.itwhiz4u.q8mercato.kashtahport.Api.ServiceRequest
import com.itwhiz4u.q8mercato.kashtahport.R
import com.itwhiz4u.q8mercato.kashtahport.helper.Constant
import com.itwhiz4u.q8mercato.kashtahport.model.UserIbanModel
import kotlinx.android.synthetic.main.activity_iban.*
import kotlinx.android.synthetic.main.view_header_toolbar.*
import org.json.JSONObject

class IBANActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_iban)
        supportActionBar?.hide()

        setData()
        icon_back.visibility = View.VISIBLE
        header_name.setText(getString(R.string.ib_hint_header))
        icon_back.setOnClickListener {
            onBackPressed()
        }


    }


    private fun setData() {

        val bankName =  prefManager?.getString(Constant.KEY_IBAN_BANK_NAME)
        val banNumber = prefManager?.getString(Constant.KEY_IBAN_BAN_NUMBER)
        val phone = prefManager?.getString(Constant.KEY_IBAN_PHONE)
        val country = prefManager?.getString(Constant.KEY_IBAN_COUNTRY)

        val ibanModel = UserIbanModel()


        if(!TextUtils.isEmpty(country) && (""+country)!= "null"){

            ibanModel.countryCode = country
        }

        if(!TextUtils.isEmpty(banNumber) && (""+banNumber)!= "null"){
            ibanModel.iban = banNumber
        }

        if(!TextUtils.isEmpty(phone) && (""+phone)!= "null"){
            ibanModel.phone = phone
        }

        if(!TextUtils.isEmpty(bankName) && (""+bankName)!= "null"){

            ibanModel.bankName = bankName
        }

        et_ban.setText(ibanModel.iban)
        et_bank_name.setText(ibanModel.bankName)
        et_country.setText(ibanModel.countryCode)
        et_phone.setText(ibanModel.phone)

    }


    private fun validation(): Boolean {

        if(TextUtils.isEmpty(et_country.text.toString().trim())){
            errorDialog?.show(getString(R.string.hint_error),getString(R.string.ib_hint_country_error))
            return false
        }

        if(TextUtils.isEmpty(et_ban.text.toString().trim())){
            errorDialog?.show(getString(R.string.hint_error),getString(R.string.ib_hint_ban_error))
            return false
        }

        if(TextUtils.isEmpty(et_bank_name.text.toString().trim())){
            errorDialog?.show(getString(R.string.hint_error),getString(R.string.ib_hint_bankName_error))
            return false
        }

        if(TextUtils.isEmpty(et_phone.text.toString().trim())){
            errorDialog?.show(getString(R.string.hint_error),getString(R.string.ib_hint_phone_error))
            return false
        }

        return true
    }


     fun saveIbanData(view: View) {

        if(!validation()){
            return
        }

        val country = et_country.text.toString()
        val phone = et_phone.text.toString()
        val ban = et_ban.text.toString()
        val bankName = et_bank_name.text.toString()


        val param = JSONObject()
        param.put("api_key",Constant.API_KEY)
        param.put("user_id",prefManager?.getString(Constant.KEY_USER_ID))
        param.put("country_code",country)
        param.put("iban",ban)
        param.put("bank_name",bankName)
        param.put("phone",phone)

        progress?.show()
        ServiceRequest(object :ApiResponseListener{
            override fun onCompleted(`object`: Any) {
                progress?.dismiss()


                prefManager?.setString(Constant.KEY_IBAN_BANK_NAME,bankName)
                prefManager?.setString(Constant.KEY_IBAN_BAN_NUMBER,ban)
                prefManager?.setString(Constant.KEY_IBAN_PHONE,phone)
                prefManager?.setString(Constant.KEY_IBAN_COUNTRY,country)

                Toast.makeText(this@IBANActivity,getString(R.string.ib_hint_data_saved),Toast.LENGTH_SHORT).show()
                finish()
            }

            override fun onError(errorMessage: String) {
                progress?.dismiss()
                errorDialog?.show(getString(R.string.hint_error),errorMessage)
            }

        }).updateIban(param)


    }
}
