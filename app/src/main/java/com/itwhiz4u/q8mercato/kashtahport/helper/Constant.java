package com.itwhiz4u.q8mercato.kashtahport.helper;

import android.os.Environment;

/**
 * Created by bodacious on 1/8/17.
 */

public class Constant {

    public static final String PREFIX = "https://kashtahport.page.link";
    public static  String LINK_URL = "";
    public static final String KEY_AWS_POOL = "aws_pool";
    public static final String KEY_DYNEMIC_LINK = "KEY_DYNEMIC_LINK";
    public static final String KEY_USER_TYPE = "user_type";
    public static final String KEY_USER_ID = "user_id";
    public static final String KEY_USER_NAME = "KEY_USER_NAME";
    public static final String KEY_USER_EMAIL = "KEY_USER_EMAIL";
    public static final String KEY_USER_PHONE = "KEY_USER_PHONE";
    public static final String KEY_USER_PASSWORD = "KEY_USER_PASSWORD";
    public static final String KEY_USER_CODE = "KEY_USER_CODE";
    public static final String KEY_USER_IMAGE_URL = "KEY_USER_IMAGE_URL";
    public static final String KEY_USER_DEVICE_ID = "KEY_USER_DEVICE_ID";
    public static final String KEY_USER_DEVICE_TYPE = "KEY_USER_DEVICE_TYPE";
    public static final String KEY_USER_ACCESS_TOKEN = "KEY_USER_ACCESS_TOKEN";
    public static final String KEY_USER_LOGIN_STATUS = "KEY_USER_LOGIN_STATUS";
    public static final String KEY_USER_DESCRIPTION = "KEY_USER_DESCRIPTION";
    public static final String KEY_USER_PROFESSION = "KEY_USER_PROFESSION";
    public static final String KEY_USER_CREATED = "KEY_USER_CREATED";
    public static final String KEY_USER_CAN_SELL = "KEY_USER_CAN_SELL";
    public static final String KEY_USER_LAST_LOGIN = "KEY_USER_LAST_LOGIN";
    public static final String KEY_USER_NOTIFICATION_TOKEN = "KEY_USER_NOTIFICATION_TOKEN";
    public static final String KEY_NOTIFICATION_SOUND = "KEY_NOTIFICATION_SOUND";
    public static final String KEY_USER_FACE_BOOK_ID = "KEY_USER_FACE_BOOK_ID";
    public static final String KEY_LAN = "KEY_LAN";
    public static final String KEY_IBAN_BANK_NAME = "KEY_IBAN_BANK_NAME";
    public static final String KEY_IBAN_BAN_NUMBER = "KEY_IBAN_NUMBER";
    public static final String KEY_IBAN_COUNTRY = "KEY_IBAN_COUNTRY";
    public static final String KEY_IBAN_COUNTRY_CODE = "KEY_IBAN_COUNTRY_CODE";
    public static final String KEY_IBAN_PHONE = "KEY_IBAN_PHONE";
    public static final String KEY_NOTIFICATION_COUNT = "KEY_NOTIFICATION_COUNT";
    public static final String KEY_ENCP_SEC = "KEY_ENCP_SEC";

    /*                */
    public static final String KEY_FILTER_ENABLE = "KEY_FILTER_ENABLE";
    public static final String KEY_CAN_SELL = "canSell";
    public static final String KEY_CAN_BOOK = "canBook";
    public static final String KEY_OPEN_FOR = "openFor";
    public static final String KEY_OPEN_FROM_MAIN = "KEY_OPEN_FROM_MAIN";
    public static final String KEY_ADDRESS = "KEY_ADDRESS";
    public static final String KEY_HOTEL_PROPERTY = "KEY_HOTEL_PROPERTY";
    public static final String KEY_HOTEL = "KEY_HOTEL";
    public static final String KEY_BOOKING = "KEY_BOOKING";
    public static final String KEY_BOOK_ID = "KEY_BOOK_ID";
    public static final String KEY_BOOK_CAN = "KEY_BOOK_CAN";
    public static final String KEY_FILTER_DATA = "FILTER_DATA";
    public static final String KEY_TRANSACTION = "KEY_TRANSACTION";
    public static final String KEY_NOTIFICATION = "KEY_FROM_NOTIFICATION";
    public static final String KEY_NOTIFICATION_PARAM = "KEY_NOTIFICATION_PARAM";
    public static final String KEY_NOTIFICATION_TYPE = "KEY_NOTIFICATION_TYPE";
    public static final String KEY_PLACE = "KEY_PLACE";



    public static final String OPEN_FOR_BOOK_HOTEL = "OPEN_FOR_BOOK_HOTEL";
    public static final String OPEN_FOR_ADD_PROPERTY = "OPEN_FOR_ADD_PROPERTY";
    public static final String OPEN_FOR_PROFILE = "OPEN_FOR_PROFILE";
    public static final String OPEN_FOR_BOOKING_LIST = "OPEN_FOR_BOOKING_LIST";
    public static final String OPEN_FOR_FAV_HOTEL = "OPEN_FOR_FAV_HOTEL";
    public static final String OPEN_FOR_NOTIFICATION = "OPEN_FOR_NOTIFICATION";
    public static final String OPEN_FOR_ADD_FAV = "OPEN_FOR_ADD_FAV";

    public static final String API_KEY = "T0BAnEzf6CaWwIMulImtA05nT6fDVjXKHoJM136S";
    public static final String DEFAULT_USER = "https://s3.ap-south-1.amazonaws.com/tecmaths/DefaultImages/chat_user_default_pic.png";

   // public static final String TEST_TAP_SEC_KEY = "sk_test_XKokBfNWv6FIYuTMg5sLPjhJ";
    public static final String TEST_TAP_SEC_KEY = "sk_live_JcmiLUzWl20noA9kyqsfYhNK";
    public static final String TEST_CARD_NUMBER = "4508750015741019";//05 / 21 //100
    public static final String TEST_TAP_ENCRYPT_KEY = "MIIBIDANBgkqhkiG9w0BAQEFAAOCAQ0AMIIBCAKCAQEA21z7Vcrrraiksj31If5K f7XGv6QNoHP7SRPjxxbxAnPrrI597NI683pHIaIgb0UNaOUggU6FYN+w+tBc1Mwk 1aOBsM8Ok6W0SsFxpa+Jt3VdOfF4iBw7k4sdd+EP5PfaiFdbrndRcCmV32mb87+I cuzDRxyqgl1Bx0dCPqmw0YCCWTuM+LXN60MHr56M5WO7J64AXn5YVzspZkon4Leg d9QbycUC77e/MUmhZL5QcGvXaBYWS5Lw5ROhjMYrLK15f4gWoYLtDcUTtMEEEtef EF4tus0Vx7XTrHa9vGbH9qUmH5F9HUkYOUX+UaFj7qVdfaR/VecB5xCwrt5ixV6y 3QIBEQ==";


    public static final String DEFAULT_DATE_FORMAT = "EEE, MMM dd";
    public static final String DEFAULT_DATE_YEAR_FORMAT = "EEE, MMM dd yyyy";
    public static final String DEFAULT_DATE_FORMAT2 = "dd MMM yyyy";
    public static final String DEFAULT_TIME_FORMAT = "hh:mm a";
    public static final String DEFAULT_DATE_TIME_FORMAT = "EEE, MMM dd, hh:mm a";
    public static final String DEFAULT_DATE_TIME_YEAR_FORMAT = "EEE, MMM dd yyyy, hh:mm a";


    public static String  LOCAL_BASE_PATH = Environment.getExternalStorageDirectory().toString() + "/KastaPort/";

    public static final String EVENT_UPDATE_MAIN = "EVENT_UPDATE_MAIN";
    public static final String EVENT_UPDATE_HOME = "EVENT_UPDATE_HOME";
    public static final String EVENT_UPDATE_BOOKING = "EVENT_UPDATE_BOOKING";
    public static final String EVENT_SEARCH_CITY = "EVENT_SEARCH_CITY";
    public static final String EVENT_FILTER = "EVENT_FILTER";
    public static final String EVENT_FILTER_RESET = "EVENT_FILTER_RESET";
    public static final String EVENT_NOTIFICATION = "EVENT_NOTIFICATION";


    public static final String BASE_LOCAL_PATH = Environment.getExternalStorageDirectory().toString();
    public static final String HOTEL_IMAGE_PATH = Environment.getExternalStorageDirectory().toString()+"/Kastaport/Media/.Hotels/images/";
    public static final String USER_IMAGE_PATH = Environment.getExternalStorageDirectory().toString()+"/Kastaport/Media/.User/images/";


    public static final String DEFAULT_CODE = "965";
    public static final int CAMERA_IMAGE_REQUEST = 111;
    public static final int GALLARY_IMAGE_REQUEST = 112;
    public static final int GALLARY_VIDEO_REQUEST = 113;
    public static final int HOTEL_DETAIL_REQUEST = 114;
    public static final int PAYMENT_REQUEST = 115;
    public static final int REGISTER_REQUEST = 1112;
    public static final int OTP_REQUEST = 1113;

    public static final int HOME_POSITION = 0;
    public static final int FAV_POSITION = 1;
    public static final int NOTIFICATION_POSITION = 2;
    public static final int PROFILE_POSITION = 3;
    public static final int SETTING_POSITION = 4;


    public static String TYPE_BOOK_SELLER = "Book_Seller";
    public static String TYPE_BOOK_BUYER = "Book_Buyer";
    public static String TYPE_BOOK_REJECT_SELLER = "TYPE_BOOK_REJECT_SELLER";
    public static String TYPE_BOOK_REJECT_BUYER = "TYPE_BOOK_REJECT_BUYER";
    public static String TYPE_BOOK_CONFIRM_SELLER = "TYPE_BOOK_CONFIRM_SELLER";
    public static String TYPE_BOOK_CONFIRM_BUYER = "TYPE_BOOK_CONFIRM_BUYER";
    public static String TYPE_BOOK_PAID_SELLER = "TYPE_BOOK_PAID_SELLER";
    public static String TYPE_BOOK_PAID_BUYER = "TYPE_BOOK_PAID_BUYER";
    public static String TYPE_BOOK_CANCEL_SELLER = "TYPE_BOOK_CANCEL_SELLER";
    public static String TYPE_BOOK_CANCEL_BUYER = "TYPE_BOOK_CANCEL_BUYER";

    public static String TYPE_FEEDBACK_SELLER = "Feedback_Seller";
    public static String TYPE_FEEDBACK_BUYER = "Feedback_Buyer";

    public static String TYPE_LIKE_SELLER = "Like_Seller";
    public static String TYPE_LIKE_BUYER = "Like_Buyer";


    public static String TYPE_OFFER = "Offer";
    public static String TYPE_APPROVED = "Approved";

}
