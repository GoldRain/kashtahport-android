package com.itwhiz4u.q8mercato.kashtahport.model

import android.os.Parcel
import android.os.Parcelable


/*"_id":"5ce69192e678895a47a3aafe",
"user_id":"5cdfd9fed1ea2a45c037cbe1",
"hotel_id":"5ce6398e54d8b8359ac45671",
"communication":2,
"price":5,
"delivery":4,
"proficiency":3,
"created_at":"1558614231352",
"comment":"Hahahhahah ",
"__v":0*/
class FeedBackModel() :Parcelable {

    var id = ""
    var user_id = ""
    var hotel_id = ""
    var price = ""
    var communication = ""
    var delivery = ""
    var proficiency = ""
    var comment = ""
    var created_at = ""
    var avg = ""
    var bookingId = ""

    constructor(parcel: Parcel) : this() {
        id = parcel.readString()
        user_id = parcel.readString()
        hotel_id = parcel.readString()
        price = parcel.readString()
        communication = parcel.readString()
        delivery = parcel.readString()
        proficiency = parcel.readString()
        comment = parcel.readString()
        created_at = parcel.readString()
        avg = parcel.readString()
        bookingId = parcel.readString()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(id)
        parcel.writeString(user_id)
        parcel.writeString(hotel_id)
        parcel.writeString(price)
        parcel.writeString(communication)
        parcel.writeString(delivery)
        parcel.writeString(proficiency)
        parcel.writeString(comment)
        parcel.writeString(created_at)
        parcel.writeString(avg)
        parcel.writeString(bookingId)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<FeedBackModel> {
        override fun createFromParcel(parcel: Parcel): FeedBackModel {
            return FeedBackModel(parcel)
        }

        override fun newArray(size: Int): Array<FeedBackModel?> {
            return arrayOfNulls(size)
        }
    }
}