package com.itwhiz4u.q8mercato.kashtahport.parser

import com.itwhiz4u.q8mercato.kashtahport.model.HotelBookingModel
import org.json.JSONObject

class BookingParser {

    fun parse(jsonObject: JSONObject):HotelBookingModel{

        val hotelBookingModel = HotelBookingModel()

        if(jsonObject.has("_id")){
            hotelBookingModel.id = jsonObject.getString("_id")
        }

        if(jsonObject.has("owner_id")){
            hotelBookingModel.ownerId = jsonObject.getString("owner_id")
        }

        if(jsonObject.has("buyer_id")){
            hotelBookingModel.buyerId = jsonObject.getString("buyer_id")
        }

        if(jsonObject.has("hotel_id")){
            hotelBookingModel.hotelId = jsonObject.getString("hotel_id")
        }

        if(jsonObject.has("childrens")){
            hotelBookingModel.childrens = jsonObject.getString("childrens")
        }

        if(jsonObject.has("adults")){
            hotelBookingModel.person = jsonObject.getString("adults")
        }

        if(jsonObject.has("rooms")){
            hotelBookingModel.rooms = jsonObject.getString("rooms")
        }
        if(jsonObject.has("status")){
            hotelBookingModel.status = jsonObject.getString("status")
        }

        if(jsonObject.has("price")){
            hotelBookingModel.price = jsonObject.getString("price")
        }

        if(jsonObject.has("date_from")){
            hotelBookingModel.dateFrom = jsonObject.getString("date_from")
        }

        if(jsonObject.has("date_to")){
            hotelBookingModel.dateTo = jsonObject.getString("date_to")
        }

        if(jsonObject.has("created_at")){
            hotelBookingModel.created_at = jsonObject.get("created_at").toString()
        }

        if(jsonObject.has("buyerData")){
            if(jsonObject.get("buyerData") is JSONObject){
                val obj  = jsonObject.getJSONObject("buyerData")
                val user = UserParser().parse(obj)
                hotelBookingModel.userModel = user
            }else{
                val objArray  = jsonObject.getJSONArray("buyerData")
                if(objArray.length() >0){
                    val user = UserParser().parse(objArray.getJSONObject(0))
                    hotelBookingModel.userModel = user
                }

            }
        }

        if(jsonObject.has("hotelData")){
            if(jsonObject.get("hotelData") is JSONObject){
                val obj  = jsonObject.getJSONObject("hotelData")
                val hotel = HotelParser().parse(obj)
                hotelBookingModel.hotelModel = hotel
            }else{
                val objArray  = jsonObject.getJSONArray("hotelData")
                if(objArray.length() >0){
                    val hotelModel = HotelParser().parse(objArray.getJSONObject(0))
                    hotelBookingModel.hotelModel = hotelModel
                }
            }
        }

        return  hotelBookingModel

    }
}