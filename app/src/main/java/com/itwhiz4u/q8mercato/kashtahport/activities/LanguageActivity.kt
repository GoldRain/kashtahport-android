package com.itwhiz4u.q8mercato.kashtahport.activities

import android.app.Dialog
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.util.Log
import android.view.Gravity
import android.view.View
import android.view.Window
import android.view.WindowManager
import com.itwhiz4u.q8mercato.kashtahport.R
import com.itwhiz4u.q8mercato.kashtahport.helper.Constant
import kotlinx.android.synthetic.main.activity_language.*
import kotlinx.android.synthetic.main.dialog_alert.*
import kotlinx.android.synthetic.main.view_header_toolbar.*
import java.lang.StringBuilder
import java.util.*

class LanguageActivity : BaseActivity() {

    var dialog2: Dialog ?= null

    var clickLang = "english"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_language)
        supportActionBar?.hide()

        dialog2 = Dialog(this)

        initLanguageDialog()

        icon_back.visibility = View.VISIBLE
        header_name.setText(getString(R.string.lan_hint_lan))
        icon_back.setOnClickListener {
            onBackPressed()
        }

        card_ar.setOnClickListener {
           // setLocale("ar")
            clickLang = "arabic"
            dialog2!!.show()
        }
        card_en.setOnClickListener {
            //setLocale("en")
            clickLang = "english"
            dialog2!!.show()
        }

        val lan = prefManager?.getString(Constant.KEY_LAN)
        when(lan){


            "ar" ->{
                im_ar.setImageResource(R.drawable.ic_check_white)
                im_ar.setBackgroundResource(R.drawable.round_pri)
                im_en.setBackgroundResource(0)
                im_en.setBackgroundResource(R.drawable.round__border_pri)
            }

            else->{
                im_en.setImageResource(R.drawable.ic_check_white)
                im_en.setBackgroundResource(R.drawable.round_pri)
                im_ar.setBackgroundResource(0)
                im_ar.setBackgroundResource(R.drawable.round__border_pri)
            }
        }
    }

    private fun setLocale(lang: String) {

        Log.e("LANGUAGE","$lang   *** ")
        val myLocale = Locale(lang)
        Locale.setDefault(myLocale)
        val res = resources
        val dm = res.displayMetrics
        val conf = res.configuration
        conf.locale = myLocale
        baseContext.resources.updateConfiguration(conf, dm)

        prefManager?.setString(Constant.KEY_LAN,lang)

        val intent = Intent(this, SplashActivity::class.java)
        intent.putExtra("fromLan",true)
        startActivity(intent)
        finishAffinity()
        finish()
    }

    private fun initLanguageDialog() {

        dialog2!!.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog2!!.setContentView(R.layout.dialog_alert)
        dialog2!!.window.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

        val window = dialog2!!.window
        window.setWindowAnimations(R.style.Animation)

        val wlp = window.getAttributes()
        wlp.width = WindowManager.LayoutParams.MATCH_PARENT
        wlp.height = WindowManager.LayoutParams.WRAP_CONTENT
        wlp.gravity = Gravity.BOTTOM
        window.setAttributes(wlp)

        dialog2!!.alt_content.setText("Are you sure want to change the language?")

        dialog2!!.alt_content.visibility = View.VISIBLE

        dialog2!!.alt_ok.setOnClickListener {

            if(clickLang.equals("english"))
                setLocale("en")

            if(clickLang.equals("arabic"))
                setLocale("ar")

            dialog2!!.dismiss()
        }

        dialog2!!.alt_cancel.setOnClickListener {

            dialog2!!.dismiss()
        }
    }


}
