package com.itwhiz4u.q8mercato.kashtahport.upload

import android.content.Context
import android.text.TextUtils
import android.util.Log
import com.amazonaws.auth.CognitoCachingCredentialsProvider
import com.amazonaws.mobileconnectors.s3.transferutility.*
import com.amazonaws.regions.Regions
import com.amazonaws.services.s3.AmazonS3Client
import com.amazonaws.services.s3.model.DeleteObjectRequest
import com.itwhiz4u.q8mercato.kashtahport.helper.Constant
import com.itwhiz4u.q8mercato.kashtahport.helper.MyApplication
import com.mazadlive.Interface.ProgressListener
import org.jetbrains.anko.doAsync
import java.io.File

/**
 * Created by bodacious on 24/1/19.
 */
class UploadAWS(val context: Context, val filePath: String, val type: String) {

    companion object {
        val UPLOAD_TYPE_HOTEL = "UPLOAD_TYPE_HOTEL"
        val UPLOAD_TYPE_USER = "UPLOAD_TYPE_USER"
    }

    lateinit var s3: AmazonS3Client
    var transferUtility: TransferUtility? = null
    var file: File? = null

    val BUCKET_NAME = "kashport"
    val HOTEL_IMAGE_KEY = "HotelImages"
    val USER_IMAGE_KEY = "Userimages"
    val CHAT_IMAGE_KEY = "ChatImages"

    init {
        Log.e("UploadAWS", "** ${MyApplication.instance.prefManager.getString(Constant.KEY_AWS_POOL)}")
        setAmazonS3Client()
    }

    //sdd
    fun setAmazonS3Client() {
        // Create an S3 client
        //Log.e(TAG,"** ${  MyApplication.instance.prefManager.getStringValue(KEY_POOL)}")
        val credentialsProvider = CognitoCachingCredentialsProvider(
                context,
                "us-east-2:8a22bf4d-0226-4729-afe0-ca396b0d8f45", //indentity pool id
                Regions.US_EAST_2)

        s3 = AmazonS3Client(credentialsProvider)
        setTransferUtility()
    }

    private fun setTransferUtility() {
        transferUtility = TransferUtility(s3, context)

        if (!File(filePath).exists()) {
            return
        } else {
            file = File(filePath)
        }
    }

    var uploadId = -1

    fun uploadToAws(listener: ProgressListener) {
        doAsync {
            try {
                when (type) {
                    UPLOAD_TYPE_HOTEL -> {
                        val transferObserver = transferUtility!!.upload(BUCKET_NAME, "${HOTEL_IMAGE_KEY}/${file!!.name}", file)
                        transferObserverListener(transferObserver, file!!, listener)
                    }

                    UPLOAD_TYPE_USER -> {
                        val transferObserver = transferUtility!!.upload(BUCKET_NAME, "${USER_IMAGE_KEY}/${file!!.name}", file)
                        transferObserverListener(transferObserver, file!!, listener)
                    }

                }

            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }

    val TAG = "transferObserverr"
    private fun transferObserverListener(transferObserver: TransferObserver, file: File, listener: ProgressListener) {
        uploadId = transferObserver.id

        transferObserver.setTransferListener(object : TransferListener {
            override fun onStateChanged(id: Int, state: TransferState?) {

                Log.e(TAG, "onStateChanged ${state!!.name}")
                if (state!!.name.equals("COMPLETED", true)) {
                    uploadId = -1
                    when (type) {
                        UPLOAD_TYPE_HOTEL -> {
                            val url = s3.getResourceUrl(BUCKET_NAME, "${HOTEL_IMAGE_KEY}/${file.name}")
                            listener.onSuccess(url)
                        }

                        UPLOAD_TYPE_USER -> {
                            val url = s3.getResourceUrl(BUCKET_NAME, "${USER_IMAGE_KEY}/${file.name}")
                            listener.onSuccess(url)
                        }


                    }

                } else {
                    uploadId = -1
                    if (state.name.equals("FAILED", true)) {
                        listener.onCancel()
                    }
                }

            }

            override fun onError(id: Int, ex: Exception?) {
                Log.e(TAG, "onError")
                ex!!.printStackTrace()
                listener.onCancel()
                uploadId = -1
            }

            override fun onProgressChanged(id: Int, bytesCurrent: Long, bytesTotal: Long) {
                Log.e(TAG, "onProgressChanged ")
                val per = ((bytesCurrent * 100) / bytesTotal)
                listener.onProgressUpdate(per.toInt())
                //  Log.e("UploadAws","${per.toInt()}  $per $bytesCurrent $bytesTotal")
            }
        });
    }

    fun deleteImage(profile: String,type: String) {
        try {
            if (!TextUtils.isEmpty(profile)) {
                doAsync {
                   when(type){
                       UPLOAD_TYPE_USER ->{
                           val fName = profile.substring(profile.lastIndexOf("/"))
                           s3!!.deleteObject(DeleteObjectRequest(BUCKET_NAME, "${USER_IMAGE_KEY}/$fName"))
                       }

                       UPLOAD_TYPE_HOTEL ->{
                           val fName = profile.substring(profile.lastIndexOf("/"))
                           s3!!.deleteObject(DeleteObjectRequest(BUCKET_NAME, "${HOTEL_IMAGE_KEY}/$fName"))
                       }
                   }

                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    fun pauseUploading() {
        val flag = transferUtility?.pauseAllWithType(TransferType.UPLOAD)
        Log.e(TAG, "pauseUploading $flag")

        // val flag = transferUtility?.pause(uploadId)
        // Log.e(TAG,"pauseUploading $flag")

    }
}