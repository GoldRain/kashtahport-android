package com.itwhiz4u.q8mercato.kashtahport.parser

import com.itwhiz4u.q8mercato.kashtahport.helper.ConstantFunction
import com.itwhiz4u.q8mercato.kashtahport.model.FeedBackModel
import com.itwhiz4u.q8mercato.kashtahport.model.NotificationModel
import org.json.JSONObject

class NotificationParser {


    fun parse(jsonObject: JSONObject): NotificationModel {

        val activityModel = NotificationModel()

        if (jsonObject.has("_id")) {
            activityModel.id = jsonObject.getString("_id")
        }

        if (jsonObject.has("user_id")) {
            activityModel.userId = jsonObject.getString("user_id")
        }

        if (jsonObject.has("hint_id")) {
            activityModel.hintId = jsonObject.getString("hint_id")
        }

        if (jsonObject.has("hint")) {
            activityModel.hint = jsonObject.getString("hint")
        }

        if (jsonObject.has("message")) {
            activityModel.message = ConstantFunction.capsFirstLetter(jsonObject.getString("message"))
        }

        if (jsonObject.has("created_at")) {
            activityModel.created_at = jsonObject.getString("created_at")
        }

        if (jsonObject.has("hotelData")) {
            if (jsonObject.get("hotelData") is JSONObject) {
                val obj = jsonObject.getJSONObject("hotelData")
                val hotel = HotelParser().parse(obj)
                activityModel.model = hotel
            } else {
                val objArray = jsonObject.getJSONArray("hotelData")
                if (objArray.length() > 0) {
                    val hotelModel = HotelParser().parse(objArray.getJSONObject(0))
                    activityModel.model = hotelModel
                }
            }
        }

        if (jsonObject.has("feedbackData")) {
            if (jsonObject.get("feedbackData") is JSONObject) {
                val obj = jsonObject.getJSONObject("feedbackData")
                val feedBack =FeedbackParser().parse(obj)
                activityModel.feedBack = feedBack
                calculateAvg(feedBack)
            } else {
                val objArray = jsonObject.getJSONArray("feedbackData")
                if (objArray.length() > 0) {
                    val feedBack = FeedbackParser().parse(objArray.getJSONObject(0))
                    activityModel.feedBack = feedBack
                    calculateAvg(feedBack)
                }
            }


        }
        return activityModel
    }

    private fun calculateAvg(feedBack: FeedBackModel) {
        var total = 0
        total+=feedBack.communication.toInt()
        total+=feedBack.proficiency.toInt()
        total+=feedBack.delivery.toInt()
        total+=feedBack.price.toInt()

        total = total/4
        feedBack.avg = ""+total
    }
}