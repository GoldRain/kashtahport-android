package com.itwhiz4u.q8mercato.kashtahport.customView

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.widget.RelativeLayout
import com.itwhiz4u.q8mercato.kashtahport.R

class ImageOverLayView : RelativeLayout {

    private var overLayView : View = LayoutInflater.from(context).inflate(R.layout.view_image_overlay,this)

    constructor(context: Context) : super(context)

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs)

    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int) : super(context, attrs, defStyleAttr)

    fun getView() : View {
        return overLayView
    }

}
