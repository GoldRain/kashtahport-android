package com.itwhiz4u.q8mercato.kashtahport.database

import android.arch.persistence.room.*

@Dao
interface FilterDao {
    /*@Query("SELECT * FROM Users WHERE userId LIKE :userId")
    fun getFriend(userId:String): List<Users>*/ //packagesInfo

    @Query("SELECT * FROM filterTable ")
    fun getAllFilter(): List<FilterData>

    @Query("SELECT * FROM filterTable limit 1")
    fun getFilter(): FilterData

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(favPost: FilterData)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(users: List<FilterData>)

    @Update(onConflict = OnConflictStrategy.REPLACE)
    fun update(users: FilterData)

    @Query("DELETE from filterTable")
    fun deleteAllFilter()

    @Query("SELECT COUNT(*) from filterTable")
    fun getTotalFilterCount(): Int
}