package com.itwhiz4u.q8mercato.kashtahport.Api

import android.util.Log
import com.androidnetworking.AndroidNetworking
import com.androidnetworking.common.Priority
import com.androidnetworking.error.ANError
import com.androidnetworking.interfaces.JSONObjectRequestListener
import com.itwhiz4u.q8mercato.kashtahport.R
import com.itwhiz4u.q8mercato.kashtahport.Retrofit.Interface.ApiService
import com.itwhiz4u.q8mercato.kashtahport.Retrofit.Model.Request.AddToFavRequest
import com.itwhiz4u.q8mercato.kashtahport.Retrofit.Model.Response.AddToFavResponse
import com.itwhiz4u.q8mercato.kashtahport.database.AppDatabase
import com.itwhiz4u.q8mercato.kashtahport.database.NotificationData
import com.itwhiz4u.q8mercato.kashtahport.helper.Constant
import com.itwhiz4u.q8mercato.kashtahport.helper.MyApplication
import com.itwhiz4u.q8mercato.kashtahport.parser.NotificationParser
import com.google.gson.Gson
import org.jetbrains.anko.doAsync
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ServiceRequest(private val apiResponseListener: ApiResponseListener?) : ApiRequester() {

    companion object {
        private var TAG = ServiceRequest::class.java.name

        fun updateNotificationToken() {

            val param = JSONObject()
            param.put("api_key", Constant.API_KEY)
            param.put("user_id", MyApplication.instance.prefManager.getString(Constant.KEY_USER_ID))
            param.put("notification_token", MyApplication.instance.prefManager.getString(Constant.KEY_USER_NOTIFICATION_TOKEN))
            param.put("device_type", "Android")

            Log.e(TAG, "REG ::  " + param.toString() + "  " + ApiUrl.UPDATE_NOTIFICATION_TOKEN)
            var request = AndroidNetworking.post(ApiUrl.UPDATE_NOTIFICATION_TOKEN)
            request.addHeaders("API_KEY", Constant.API_KEY)
            request.addHeaders("Content-Type", "application/json")
            request.addJSONObjectBody(param)
            request.setTag("UPDATE_NOTIFICATION_TOKEN").setPriority(Priority.HIGH).build()

                    .getAsJSONObject(object : JSONObjectRequestListener {
                        override fun onResponse(response: JSONObject) {
                            Log.e("API_RESPONSE", response.toString())

                        }

                        override fun onError(anError: ANError) {

                        }
                    })
        }
    }


    fun facebookValidate(param: JSONObject) {
        if (!isNetworkAvailable) {
            apiResponseListener?.onError(MyApplication.instance.resources.getString(R.string.network_not_available_message))
            return
        }
        Log.e(TAG, "REG ::  " + param.toString() + "  " + ApiUrl.FACEBOOK_VALIDATION)
        var request = AndroidNetworking.post(ApiUrl.FACEBOOK_VALIDATION)
        request.addHeaders("API_KEY", Constant.API_KEY)
        request.addHeaders("Content-Type", "application/json")
        request.addJSONObjectBody(param)
        request.setTag("FACEBOOK_VALIDATION").setPriority(Priority.HIGH).build()

                .getAsJSONObject(object : JSONObjectRequestListener {
                    override fun onResponse(response: JSONObject) {
                        Log.e("API_RESPONSE", response.toString())
                        if (!response.getBoolean("error")) {

                            apiResponseListener?.onCompleted(response.toString())
                        } else {
                            apiResponseListener?.onCompleted(response.toString())
                        }
                    }

                    override fun onError(anError: ANError) {
                        apiResponseListener?.onError(anError.errorDetail)
                    }
                })
    }


    fun checkEmailOrPhone(param: JSONObject) {
        if (!isNetworkAvailable) {
            apiResponseListener?.onError(MyApplication.instance.resources.getString(R.string.network_not_available_message))
            return
        }
        Log.e(TAG, "REG ::  " + param.toString() + "  " + ApiUrl.CHECK_EMAIL_PHONE)
        var request = AndroidNetworking.post(ApiUrl.CHECK_EMAIL_PHONE)
        request.addHeaders("API_KEY", Constant.API_KEY)
        request.addHeaders("Content-Type", "application/json")
        request.addJSONObjectBody(param)
        request.setTag("FACEBOOK_VALIDATION").setPriority(Priority.HIGH).build()

                .getAsJSONObject(object : JSONObjectRequestListener {
                    override fun onResponse(response: JSONObject) {
                        Log.e("API_RESPONSE", response.toString())
                        if (!response.getBoolean("error")) {

                            apiResponseListener?.onCompleted(response.toString())
                        } else {
                            apiResponseListener?.onCompleted(response.toString())
                        }
                    }

                    override fun onError(anError: ANError) {
                        apiResponseListener?.onError(anError.errorDetail)
                    }
                })
    }

    fun checkEmail(param: JSONObject) {
        if (!isNetworkAvailable) {
            apiResponseListener?.onError(MyApplication.instance.resources.getString(R.string.network_not_available_message))
            return
        }
        Log.e(TAG, "REG ::  " + param.toString() + "  " + ApiUrl.CEHCK_EMAIL)
        var request = AndroidNetworking.post(ApiUrl.CEHCK_EMAIL)
        request.addHeaders("API_KEY", Constant.API_KEY)
        request.addHeaders("Content-Type", "application/json")
        request.addJSONObjectBody(param)
        request.setTag("register").setPriority(Priority.HIGH).build()

                .getAsJSONObject(object : JSONObjectRequestListener {
                    override fun onResponse(response: JSONObject) {
                        Log.e("API_RESPONSE", response.toString())
                        if (!response.getBoolean("error")) {

                            apiResponseListener?.onCompleted(response.toString())
                        } else {
                            apiResponseListener?.onError(response.getString("message"))
                        }
                    }

                    override fun onError(anError: ANError) {
                        apiResponseListener?.onError(anError.errorDetail)
                    }
                })
    }

    fun sendEmail(param: JSONObject) {
        if (!isNetworkAvailable) {
            apiResponseListener?.onError(MyApplication.instance.resources.getString(R.string.network_not_available_message))
            return
        }
        Log.e(TAG, "REG ::  " + param.toString() + "  " + ApiUrl.SEND_EMAIL)
        var request = AndroidNetworking.post(ApiUrl.SEND_EMAIL)
        request.addHeaders("API_KEY", Constant.API_KEY)
        request.addHeaders("Content-Type", "application/json")
        request.addJSONObjectBody(param)
        request.setTag("register").setPriority(Priority.HIGH).build()

                .getAsJSONObject(object : JSONObjectRequestListener {
                    override fun onResponse(response: JSONObject) {
                        Log.e("API_RESPONSE", response.toString())
                        if (!response.getBoolean("error")) {

                            apiResponseListener?.onCompleted(response.toString())
                        } else {
                            apiResponseListener?.onError(response.getString("message"))
                        }
                    }

                    override fun onError(anError: ANError) {
                        apiResponseListener?.onError(anError.errorDetail)
                    }
                })
    }

    fun sendOtp(param: JSONObject) {
        if (!isNetworkAvailable) {
            apiResponseListener?.onError(MyApplication.instance.resources.getString(R.string.network_not_available_message))
            return
        }
        Log.e(TAG, "REG ::  " + param.toString() + "  " + ApiUrl.SEND_OTP)
        var request = AndroidNetworking.post(ApiUrl.SEND_OTP)
        request.addHeaders("API_KEY", Constant.API_KEY)
        request.addHeaders("Content-Type", "application/json")
        request.addJSONObjectBody(param)
        request.setTag("register").setPriority(Priority.HIGH).build()

                .getAsJSONObject(object : JSONObjectRequestListener {
                    override fun onResponse(response: JSONObject) {
                        Log.e("API_RESPONSE", response.toString())
                        if (!response.getBoolean("error")) {

                            apiResponseListener?.onCompleted(response.toString())
                        } else {
                            apiResponseListener?.onError(response.getString("message"))
                        }
                    }

                    override fun onError(anError: ANError) {
                        apiResponseListener?.onError(anError.errorDetail)
                    }
                })
    }

    fun verifyOtp(param: JSONObject) {
        if (!isNetworkAvailable) {
            apiResponseListener?.onError(MyApplication.instance.resources.getString(R.string.network_not_available_message))
            return
        }
        Log.e(TAG, "REG ::  " + param.toString() + "  " + ApiUrl.VERIFY_OTP)
        var request = AndroidNetworking.post(ApiUrl.VERIFY_OTP)
        request.addHeaders("API_KEY", Constant.API_KEY)
        request.addHeaders("Content-Type", "application/json")
        request.addJSONObjectBody(param)
        request.setTag("register").setPriority(Priority.HIGH).build()

                .getAsJSONObject(object : JSONObjectRequestListener {
                    override fun onResponse(response: JSONObject) {
                        Log.e("API_RESPONSE", response.toString())
                        if (!response.getBoolean("error")) {

                            apiResponseListener?.onCompleted(response.toString())
                        } else {
                            apiResponseListener?.onError(response.getString("message"))
                        }
                    }

                    override fun onError(anError: ANError) {
                        apiResponseListener?.onError(anError.errorDetail)
                    }
                })
    }


    fun register(param: JSONObject) {
        if (!isNetworkAvailable) {
            apiResponseListener?.onError(MyApplication.instance.resources.getString(R.string.network_not_available_message))
            return
        }
        Log.e(TAG, "REG ::  " + param.toString() + "  " + ApiUrl.LOGIN_URL)
        var request = AndroidNetworking.post(ApiUrl.REGISTER)
        request.addHeaders("API_KEY", Constant.API_KEY)
        request.addHeaders("Content-Type", "application/json")
        request.addJSONObjectBody(param)
        request.setTag("register").setPriority(Priority.HIGH).build()

                .getAsJSONObject(object : JSONObjectRequestListener {
                    override fun onResponse(response: JSONObject) {
                        Log.e("API_RESPONSE", response.toString())
                        if (!response.getBoolean("error")) {

                            apiResponseListener?.onCompleted(response.toString())
                        } else {
                            apiResponseListener?.onError(response.getString("message"))
                        }
                    }

                    override fun onError(anError: ANError) {
                        apiResponseListener?.onError(anError.errorDetail)
                    }
                })
    }


    fun loginRequest(param: JSONObject) {
        if (!isNetworkAvailable) {
            apiResponseListener?.onError(MyApplication.instance.resources.getString(R.string.network_not_available_message))
            return
        }
        Log.e(TAG, " " + param.toString() + "  " + ApiUrl.LOGIN_URL)
        var request = AndroidNetworking.post(ApiUrl.LOGIN_URL)
        request.addHeaders("API_KEY", Constant.API_KEY)
        request.addHeaders("Content-Type", "application/json")
        request.addJSONObjectBody(param)
        request.setTag("login").setPriority(Priority.HIGH).build()

                .getAsJSONObject(object : JSONObjectRequestListener {
                    override fun onResponse(response: JSONObject) {
                        Log.e("API_RESPONSE", response.toString())
                        if (!response.getBoolean("error")) {

                            apiResponseListener?.onCompleted(response.toString())
                        } else {
                            apiResponseListener?.onError(response.getString("message"))
                        }
                    }

                    override fun onError(anError: ANError) {
                        apiResponseListener?.onError(anError.errorDetail)
                    }
                })
    }

    fun logoutRequest(param: JSONObject) {
        if (!isNetworkAvailable) {
            apiResponseListener?.onError(MyApplication.instance.resources.getString(R.string.network_not_available_message))
            return
        }
        Log.e(TAG, " " + param.toString() + "  " + ApiUrl.LOGOUT_URL)
        var request = AndroidNetworking.post(ApiUrl.LOGOUT_URL)
        request.addHeaders("API_KEY", Constant.API_KEY)
        request.addHeaders("Content-Type", "application/json")
        request.addJSONObjectBody(param)
        request.setTag("login").setPriority(Priority.HIGH).build()

                .getAsJSONObject(object : JSONObjectRequestListener {
                    override fun onResponse(response: JSONObject) {
                        Log.e("API_RESPONSE", response.toString())
                        if (!response.getBoolean("error")) {

                            apiResponseListener?.onCompleted(response.toString())
                        } else {
                            apiResponseListener?.onError(response.getString("message"))
                        }
                    }

                    override fun onError(anError: ANError) {
                        apiResponseListener?.onError(anError.errorDetail)
                    }
                })
    }


    fun getCountries() {

        val param = JSONObject()
        param.put("api_key", Constant.API_KEY)
        if (!isNetworkAvailable) {
            apiResponseListener?.onError(MyApplication.instance.resources.getString(R.string.network_not_available_message))
            return
        }
        Log.e(TAG, " " + param.toString() + "  " + ApiUrl.GET_COUNTRIES)
        var request = AndroidNetworking.post(ApiUrl.GET_COUNTRIES)
        request.addHeaders("API_KEY", Constant.API_KEY)
        request.addHeaders("Content-Type", "application/json")
        request.addJSONObjectBody(param)
        request.setTag("GET_COUNTRIES").setPriority(Priority.HIGH).build()

                .getAsJSONObject(object : JSONObjectRequestListener {
                    override fun onResponse(response: JSONObject) {
                        Log.e("API_RESPONSE", response.toString())
                        if (!response.getBoolean("error")) {
                            apiResponseListener?.onCompleted(response.toString())
                        } else {
                            apiResponseListener?.onError(response.getString("message"))
                        }
                    }

                    override fun onError(anError: ANError) {
                        apiResponseListener?.onError(anError.errorDetail)
                    }
                })
    }

    fun updateIban(param: JSONObject) {


        if (!isNetworkAvailable) {
            apiResponseListener?.onError(MyApplication.instance.resources.getString(R.string.network_not_available_message))
            return
        }
        Log.e(TAG, " " + param.toString() + "  " + ApiUrl.UPDATE_IBAN)
        var request = AndroidNetworking.post(ApiUrl.UPDATE_IBAN)
        request.addHeaders("API_KEY", Constant.API_KEY)
        request.addHeaders("Content-Type", "application/json")
        request.addJSONObjectBody(param)
        request.setTag("GET_COUNTRIES").setPriority(Priority.HIGH).build()

                .getAsJSONObject(object : JSONObjectRequestListener {
                    override fun onResponse(response: JSONObject) {
                        Log.e("API_RESPONSE", response.toString())
                        if (!response.getBoolean("error")) {
                            apiResponseListener?.onCompleted(response.toString())
                        } else {
                            apiResponseListener?.onError(response.getString("message"))
                        }
                    }

                    override fun onError(anError: ANError) {
                        apiResponseListener?.onError(anError.errorDetail)
                    }
                })
    }

    /***************************************************** Hotel **************************************/

    fun searchHotel(param: JSONObject) {
        if (!isNetworkAvailable) {
            apiResponseListener?.onError(MyApplication.instance.resources.getString(R.string.network_not_available_message))
            return
        }
        Log.e(TAG, " " + param.toString() + "  " + ApiUrl.SEARCH_HOTEL)
        var request = AndroidNetworking.post(ApiUrl.SEARCH_HOTEL)
        request.addHeaders("API_KEY", Constant.API_KEY)
        request.addHeaders("Content-Type", "application/json")
        request.addJSONObjectBody(param)
        request.setTag("getProperty").setPriority(Priority.HIGH).build()

                .getAsJSONObject(object : JSONObjectRequestListener {
                    override fun onResponse(response: JSONObject) {
                        Log.e("API_RESPONSE", response.toString())
                        if (!response.getBoolean("error")) {
                            apiResponseListener?.onCompleted(response.toString())
                        } else {
                            apiResponseListener?.onError(response.getString("message"))
                        }
                    }

                    override fun onError(anError: ANError) {
                        apiResponseListener?.onError(anError.errorDetail)
                    }
                })
    }

    fun searchNearByHotel(param: JSONObject) {
        if (!isNetworkAvailable) {
            apiResponseListener?.onError(MyApplication.instance.resources.getString(R.string.network_not_available_message))
            return
        }
        Log.e(TAG, " " + param.toString() + "  " + ApiUrl.SEARCH_NEAR_BY_HOTEL)
        var request = AndroidNetworking.post(ApiUrl.SEARCH_NEAR_BY_HOTEL)
        request.addHeaders("API_KEY", Constant.API_KEY)
        request.addHeaders("Content-Type", "application/json")
        request.addJSONObjectBody(param)
        request.setTag("searchNearByHotel").setPriority(Priority.HIGH).build()

                .getAsJSONObject(object : JSONObjectRequestListener {
                    override fun onResponse(response: JSONObject) {
                        Log.e("API_RESPONSE", response.toString())
                        if (!response.getBoolean("error")) {
                            apiResponseListener?.onCompleted(response.toString())
                        } else {
                            apiResponseListener?.onError(response.getString("message"))
                        }
                    }

                    override fun onError(anError: ANError) {
                        apiResponseListener?.onError(anError.errorDetail)
                    }
                })
    }

    fun deleteHotel(param: JSONObject) {
        if (!isNetworkAvailable) {
            apiResponseListener?.onError(MyApplication.instance.resources.getString(R.string.network_not_available_message))
            return
        }
        Log.e(TAG, " " + param.toString() + "  " + ApiUrl.DELETE_HOTEL)
        var request = AndroidNetworking.post(ApiUrl.DELETE_HOTEL)
        request.addHeaders("API_KEY", Constant.API_KEY)
        request.addHeaders("Content-Type", "application/json")
        request.addJSONObjectBody(param)
        request.setTag("DELETE_HOTEL").setPriority(Priority.HIGH).build()

                .getAsJSONObject(object : JSONObjectRequestListener {
                    override fun onResponse(response: JSONObject) {
                        Log.e("API_RESPONSE", response.toString())
                        if (!response.getBoolean("error")) {
                            apiResponseListener?.onCompleted(response.toString())
                        } else {
                            apiResponseListener?.onError(response.getString("message"))
                        }
                    }

                    override fun onError(anError: ANError) {
                        apiResponseListener?.onError(anError.errorDetail)
                    }
                })
    }

    fun addProperty(param: JSONObject) {
        if (!isNetworkAvailable) {
            apiResponseListener?.onError(MyApplication.instance.resources.getString(R.string.network_not_available_message))
            return
        }
        Log.e(TAG, " " + param.toString() + "  " + ApiUrl.LOGIN_URL)
        var request = AndroidNetworking.post(ApiUrl.ADD_PROPERTY)
        request.addHeaders("API_KEY", Constant.API_KEY)
        request.addHeaders("Content-Type", "application/json")
        request.addJSONObjectBody(param)
        request.setTag("addProperty").setPriority(Priority.HIGH).build()

                .getAsJSONObject(object : JSONObjectRequestListener {
                    override fun onResponse(response: JSONObject) {
                        Log.e("API_RESPONSE", response.toString())
                        if (!response.getBoolean("error")) {
                            apiResponseListener?.onCompleted(response.toString())
                        } else {
                            apiResponseListener?.onError(response.getString("message"))
                        }
                    }

                    override fun onError(anError: ANError) {
                        apiResponseListener?.onError(anError.errorDetail)
                    }
                })
    }

    fun updateProperty(param: JSONObject) {
        if (!isNetworkAvailable) {
            apiResponseListener?.onError(MyApplication.instance.resources.getString(R.string.network_not_available_message))
            return
        }
        Log.e(TAG, " " + param.toString() + "  " + ApiUrl.LOGIN_URL)
        var request = AndroidNetworking.post(ApiUrl.UPDATE_PROPERTY)
        request.addHeaders("API_KEY", Constant.API_KEY)
        request.addHeaders("Content-Type", "application/json")
        request.addJSONObjectBody(param)
        request.setTag("updateProperty").setPriority(Priority.HIGH).build()

                .getAsJSONObject(object : JSONObjectRequestListener {
                    override fun onResponse(response: JSONObject) {
                        Log.e("API_RESPONSE", response.toString())
                        if (!response.getBoolean("error")) {
                            apiResponseListener?.onCompleted(response.toString())
                        } else {
                            apiResponseListener?.onError(response.getString("message"))
                        }
                    }

                    override fun onError(anError: ANError) {
                        apiResponseListener?.onError(anError.errorDetail)
                    }
                })
    }

    fun manageProperty(param: JSONObject) {
        if (!isNetworkAvailable) {
            apiResponseListener?.onError(MyApplication.instance.resources.getString(R.string.network_not_available_message))
            return
        }
        Log.e(TAG, " " + param.toString() + "  " + ApiUrl.MANAGE_PROPERTY)
        var request = AndroidNetworking.post(ApiUrl.MANAGE_PROPERTY)
        request.addHeaders("API_KEY", Constant.API_KEY)
        request.addHeaders("Content-Type", "application/json")
        request.addJSONObjectBody(param)
        request.setTag("manageProperty").setPriority(Priority.HIGH).build()

                .getAsJSONObject(object : JSONObjectRequestListener {
                    override fun onResponse(response: JSONObject) {
                        Log.e("API_RESPONSE", response.toString())
                        if (!response.getBoolean("error")) {
                            apiResponseListener?.onCompleted(response.toString())
                        } else {
                            apiResponseListener?.onError(response.getString("message"))
                        }
                    }

                    override fun onError(anError: ANError) {
                        apiResponseListener?.onError(anError.errorDetail)
                    }
                })
    }

    fun getPropertyList(param: JSONObject) {
        if (!isNetworkAvailable) {
            apiResponseListener?.onError(MyApplication.instance.resources.getString(R.string.network_not_available_message))
            return
        }
        Log.e(TAG, " " + param.toString() + "  " + ApiUrl.LOGIN_URL)
        var request = AndroidNetworking.post(ApiUrl.GET_PROPERTY_LIST)
        request.addHeaders("API_KEY", Constant.API_KEY)
        request.addHeaders("Content-Type", "application/json")
        request.addJSONObjectBody(param)
        request.setTag("getPropertyList").setPriority(Priority.HIGH).build()

                .getAsJSONObject(object : JSONObjectRequestListener {
                    override fun onResponse(response: JSONObject) {
                        Log.e("API_RESPONSE", response.toString())
                        if (!response.getBoolean("error")) {
                            apiResponseListener?.onCompleted(response.toString())
                        } else {
                            apiResponseListener?.onError(response.getString("message"))
                        }
                    }

                    override fun onError(anError: ANError) {
                        apiResponseListener?.onError(anError.errorDetail)
                    }
                })
    }

    fun getHotelFeedbackFeedback(param: JSONObject) {
        if (!isNetworkAvailable) {
            apiResponseListener?.onError(MyApplication.instance.resources.getString(R.string.network_not_available_message))
            return
        }
        Log.e(TAG, " " + param.toString() + "  " + ApiUrl.GET_HOTEL_FEEDBACK)
        var request = AndroidNetworking.post(ApiUrl.GET_HOTEL_FEEDBACK)
        request.addHeaders("API_KEY", Constant.API_KEY)
        request.addHeaders("Content-Type", "application/json")
        request.addJSONObjectBody(param)
        request.setTag("GET_HOTEL_FEEDBACK").setPriority(Priority.HIGH).build()

                .getAsJSONObject(object : JSONObjectRequestListener {
                    override fun onResponse(response: JSONObject) {
                        Log.e("API_RESPONSE", response.toString())
                        if (!response.getBoolean("error")) {
                            apiResponseListener?.onCompleted(response.toString())
                        } else {
                            apiResponseListener?.onError(response.getString("message"))
                        }
                    }

                    override fun onError(anError: ANError) {
                        apiResponseListener?.onError(anError.errorDetail)
                    }
                })
    }

    fun getProperty(param: JSONObject) {
        if (!isNetworkAvailable) {
            apiResponseListener?.onError(MyApplication.instance.resources.getString(R.string.network_not_available_message))
            return
        }
        Log.e(TAG, " " + param.toString() + "  " + ApiUrl.GET_PROPERTY)
        var request = AndroidNetworking.post(ApiUrl.GET_PROPERTY)
        request.addHeaders("API_KEY", Constant.API_KEY)
        request.addHeaders("Content-Type", "application/json")
        request.addJSONObjectBody(param)
        request.setTag("getProperty").setPriority(Priority.HIGH).build()

                .getAsJSONObject(object : JSONObjectRequestListener {
                    override fun onResponse(response: JSONObject) {
                        Log.e("API_RESPONSE", response.toString())
                        if (!response.getBoolean("error")) {
                            apiResponseListener?.onCompleted(response.toString())
                        } else {
                            apiResponseListener?.onError(response.getString("message"))
                        }
                    }

                    override fun onError(anError: ANError) {
                        apiResponseListener?.onError(anError.errorDetail)
                    }
                })
    }

    fun getPropertyStatus(param: JSONObject) {
        if (!isNetworkAvailable) {
            apiResponseListener?.onError(MyApplication.instance.resources.getString(R.string.network_not_available_message))
            return
        }
        Log.e(TAG, " " + param.toString() + "  " + ApiUrl.GET_PROPERTY_STATUS)
        var request = AndroidNetworking.post(ApiUrl.GET_PROPERTY_STATUS)
        request.addHeaders("API_KEY", Constant.API_KEY)
        request.addHeaders("Content-Type", "application/json")
        request.addJSONObjectBody(param)
        request.setTag("getProperty").setPriority(Priority.HIGH).build()

                .getAsJSONObject(object : JSONObjectRequestListener {
                    override fun onResponse(response: JSONObject) {
                        Log.e("API_RESPONSE", response.toString())
                        if (!response.getBoolean("error")) {
                            apiResponseListener?.onCompleted(response.toString())
                        } else {
                            apiResponseListener?.onError(response.getString("message"))
                        }
                    }

                    override fun onError(anError: ANError) {
                        apiResponseListener?.onError(anError.errorDetail)
                    }
                })
    }

    fun addToFav(param: JSONObject) {
        if (!isNetworkAvailable) {
            apiResponseListener?.onError(MyApplication.instance.resources.getString(R.string.network_not_available_message))
            return
        }

        if (!MyApplication.instance.prefManager.getBoolean(Constant.KEY_USER_LOGIN_STATUS)) {
            apiResponseListener?.onError("You are not login !!")
        }

        // Retrofit
        val apiService = ApiService.create()

        val addToFavRequest = AddToFavRequest(param.getString("api_key"),param.getString("hotel_id"),param.getString("user_id"))
        val call = apiService.addToFavorite(addToFavRequest)

        call.enqueue(object :Callback<AddToFavResponse>{
            override fun onFailure(call: Call<AddToFavResponse>, t: Throwable) {
                apiResponseListener?.onError(MyApplication.instance.resources.getString(R.string.error_connect_to_server))
            }

            override fun onResponse(call: Call<AddToFavResponse>, response: Response<AddToFavResponse>) {
                if(response.isSuccessful){
                    val result = response.body()
                    if(!result!!.error){
                        val str = Gson().toJson(result)
                        Log.e("API_RESPONSE", str)
                        apiResponseListener?.onCompleted(str)
                    }else{
                        apiResponseListener?.onError(result.message)
                    }
                }else{
                    apiResponseListener?.onError(response.message())
                }
            }

        })


       /* Log.e(TAG, " " + param.toString() + "  " + ApiUrl.LOGIN_URL)
        var request = AndroidNetworking.post(ApiUrl.ADD_TO_FAV)
        request.addHeaders("API_KEY", Constant.API_KEY)
        request.addHeaders("Content-Type", "application/json")
        request.addJSONObjectBody(param)
        request.setTag("addToFav").setPriority(Priority.HIGH).build()

                .getAsJSONObject(object : JSONObjectRequestListener {
                    override fun onResponse(response: JSONObject) {
                        Log.e("API_RESPONSE", response.toString())
                        if (!response.getBoolean("error")) {
                            apiResponseListener?.onCompleted(response.toString())
                        } else {
                            apiResponseListener?.onError(response.getString("message"))
                        }
                    }

                    override fun onError(anError: ANError) {
                        apiResponseListener?.onError(anError.errorDetail)
                    }
                })*/
    }

    fun removeToFav(param: JSONObject) {
        if (!isNetworkAvailable) {
            apiResponseListener?.onError(MyApplication.instance.resources.getString(R.string.network_not_available_message))
            return
        }

        if (!MyApplication.instance.prefManager.getBoolean(Constant.KEY_USER_LOGIN_STATUS)) {
            apiResponseListener?.onError("You are login !!")
        }

        Log.e(TAG, " " + param.toString() + "  " + ApiUrl.LOGIN_URL)
        var request = AndroidNetworking.post(ApiUrl.REMOVE_TO_FAV)
        request.addHeaders("API_KEY", Constant.API_KEY)
        request.addHeaders("Content-Type", "application/json")
        request.addJSONObjectBody(param)
        request.setTag("removeToFav").setPriority(Priority.HIGH).build()

                .getAsJSONObject(object : JSONObjectRequestListener {
                    override fun onResponse(response: JSONObject) {
                        Log.e("API_RESPONSE", response.toString())
                        if (!response.getBoolean("error")) {
                            apiResponseListener?.onCompleted(response.toString())
                        } else {
                            apiResponseListener?.onError(response.getString("message"))
                        }
                    }

                    override fun onError(anError: ANError) {
                        apiResponseListener?.onError(anError.errorDetail)
                    }
                })
    }

    fun getFavList(param: JSONObject) {
        if (!isNetworkAvailable) {
            apiResponseListener?.onError(MyApplication.instance.resources.getString(R.string.network_not_available_message))
            return
        }
        Log.e(TAG, " " + param.toString() + "  " + ApiUrl.FAV_HOTEL_LIST)
        var request = AndroidNetworking.post(ApiUrl.FAV_HOTEL_LIST)
        request.addHeaders("API_KEY", Constant.API_KEY)
        request.addHeaders("Content-Type", "application/json")
        request.addJSONObjectBody(param)
        request.setTag("getFavList").setPriority(Priority.HIGH).build()

                .getAsJSONObject(object : JSONObjectRequestListener {
                    override fun onResponse(response: JSONObject) {
                        Log.e("API_RESPONSE", response.toString())
                        if (!response.getBoolean("error")) {
                            apiResponseListener?.onCompleted(response.toString())
                        } else {
                            apiResponseListener?.onError(response.getString("message"))
                        }
                    }

                    override fun onError(anError: ANError) {
                        apiResponseListener?.onError(anError.errorDetail)
                    }
                })
    }


    fun bookHotel(param: JSONObject) {
        if (!isNetworkAvailable) {
            apiResponseListener?.onError(MyApplication.instance.resources.getString(R.string.network_not_available_message))
            return
        }
        Log.e(TAG, " " + param.toString() + "  " + ApiUrl.BOOOK_HOTEL)
        var request = AndroidNetworking.post(ApiUrl.BOOOK_HOTEL)
        request.addHeaders("API_KEY", Constant.API_KEY)
        request.addHeaders("Content-Type", "application/json")
        request.addJSONObjectBody(param)
        request.setTag("bookHotel").setPriority(Priority.HIGH).build()

                .getAsJSONObject(object : JSONObjectRequestListener {
                    override fun onResponse(response: JSONObject) {
                        Log.e("API_RESPONSE", response.toString())
                        if (!response.getBoolean("error")) {
                            apiResponseListener?.onCompleted(response.toString())
                        } else {
                            apiResponseListener?.onError(response.getString("message"))
                        }
                    }

                    override fun onError(anError: ANError) {
                        apiResponseListener?.onError(anError.errorDetail)
                    }
                })
    }


    fun getBookingList(param: JSONObject) {
        if (!isNetworkAvailable) {
            apiResponseListener?.onError(MyApplication.instance.resources.getString(R.string.network_not_available_message))
            return
        }
        Log.e(TAG, " " + param.toString() + "  " + ApiUrl.GET_ALL_BOOKINGS)
        var request = AndroidNetworking.post(ApiUrl.GET_ALL_BOOKINGS)
        request.addHeaders("getBookingList", Constant.API_KEY)
        request.addHeaders("Content-Type", "application/json")
        request.addJSONObjectBody(param)
        request.setTag("bookHotel").setPriority(Priority.HIGH).build()

                .getAsJSONObject(object : JSONObjectRequestListener {
                    override fun onResponse(response: JSONObject) {
                        Log.e("API_RESPONSE", response.toString())
                        if (!response.getBoolean("error")) {
                            apiResponseListener?.onCompleted(response.toString())
                        } else {
                            apiResponseListener?.onError(response.getString("message"))
                        }
                    }

                    override fun onError(anError: ANError) {
                        apiResponseListener?.onError(anError.errorDetail)
                    }
                })
    }

    fun getBookingDetails(param: JSONObject) {
        if (!isNetworkAvailable) {
            apiResponseListener?.onError(MyApplication.instance.resources.getString(R.string.network_not_available_message))
            return
        }
        Log.e(TAG, " " + param.toString() + "  " + ApiUrl.GET_BOOKING_DETAILS)
        var request = AndroidNetworking.post(ApiUrl.GET_BOOKING_DETAILS)
        request.addHeaders("getBookingList", Constant.API_KEY)
        request.addHeaders("Content-Type", "application/json")
        request.addJSONObjectBody(param)
        request.setTag("bookHotel").setPriority(Priority.HIGH).build()

                .getAsJSONObject(object : JSONObjectRequestListener {
                    override fun onResponse(response: JSONObject) {
                        Log.e("API_RESPONSE", response.toString())
                        if (!response.getBoolean("error")) {
                            apiResponseListener?.onCompleted(response.toString())
                        } else {
                            apiResponseListener?.onError(response.getString("message"))
                        }
                    }

                    override fun onError(anError: ANError) {
                        apiResponseListener?.onError(anError.errorDetail)
                    }
                })
    }

    fun getMyBookingList(param: JSONObject) {
        if (!isNetworkAvailable) {
            apiResponseListener?.onError(MyApplication.instance.resources.getString(R.string.network_not_available_message))
            return
        }
        Log.e(TAG, " " + param.toString() + "  " + ApiUrl.GET_MY_BOOKINGS)
        var request = AndroidNetworking.post(ApiUrl.GET_MY_BOOKINGS)
        request.addHeaders("getBookingList", Constant.API_KEY)
        request.addHeaders("Content-Type", "application/json")
        request.addJSONObjectBody(param)
        request.setTag("getMyBookingList").setPriority(Priority.HIGH).build()

                .getAsJSONObject(object : JSONObjectRequestListener {
                    override fun onResponse(response: JSONObject) {
                        Log.e("API_RESPONSE", response.toString())
                        if (!response.getBoolean("error")) {
                            apiResponseListener?.onCompleted(response.toString())
                        } else {
                            apiResponseListener?.onError(response.getString("message"))
                        }
                    }

                    override fun onError(anError: ANError) {
                        apiResponseListener?.onError(anError.errorDetail)
                    }
                })
    }

    fun updateBooking(param: JSONObject) {
        if (!isNetworkAvailable) {
            apiResponseListener?.onError(MyApplication.instance.resources.getString(R.string.network_not_available_message))
            return
        }
        Log.e(TAG, " " + param.toString() + "  " + ApiUrl.UPDATE_BOOKING)
        var request = AndroidNetworking.post(ApiUrl.UPDATE_BOOKING)
        request.addHeaders("API_KEY", Constant.API_KEY)
        request.addHeaders("Content-Type", "application/json")
        request.addJSONObjectBody(param)
        request.setTag("updateBooking").setPriority(Priority.HIGH).build()

                .getAsJSONObject(object : JSONObjectRequestListener {
                    override fun onResponse(response: JSONObject) {
                        Log.e("API_RESPONSE", response.toString())
                        if (!response.getBoolean("error")) {
                            apiResponseListener?.onCompleted(response.toString())
                        } else {
                            apiResponseListener?.onError(response.getString("message"))
                        }
                    }

                    override fun onError(anError: ANError) {
                        apiResponseListener?.onError(anError.errorDetail)
                    }
                })
    }


    fun updateProfile(param: JSONObject) {
        if (!isNetworkAvailable) {
            apiResponseListener?.onError(MyApplication.instance.resources.getString(R.string.network_not_available_message))
            return
        }
        Log.e(TAG, " " + param.toString() + "  " + ApiUrl.UPDATE_PROFILE)
        var request = AndroidNetworking.post(ApiUrl.UPDATE_PROFILE)
        request.addHeaders("getBookingList", Constant.API_KEY)
        request.addHeaders("Content-Type", "application/json")
        request.addJSONObjectBody(param)
        request.setTag("UPDATE_PROFILE").setPriority(Priority.HIGH).build()

                .getAsJSONObject(object : JSONObjectRequestListener {
                    override fun onResponse(response: JSONObject) {
                        Log.e("API_RESPONSE", response.toString())
                        if (!response.getBoolean("error")) {
                            apiResponseListener?.onCompleted(response.toString())
                        } else {
                            apiResponseListener?.onError(response.getString("message"))
                        }
                    }

                    override fun onError(anError: ANError) {
                        apiResponseListener?.onError(anError.errorDetail)
                    }
                })
    }

    fun provideFeedback(param: JSONObject) {
        if (!isNetworkAvailable) {
            apiResponseListener?.onError(MyApplication.instance.resources.getString(R.string.network_not_available_message))
            return
        }
        Log.e(TAG, " " + param.toString() + "  " + ApiUrl.PROVIDE_FEEDBACK)
        var request = AndroidNetworking.post(ApiUrl.PROVIDE_FEEDBACK)
        request.addHeaders("API_KEY", Constant.API_KEY)
        request.addHeaders("Content-Type", "application/json")
        request.addJSONObjectBody(param)
        request.setTag("PROVIDE_FEEDBACK").setPriority(Priority.HIGH).build()

                .getAsJSONObject(object : JSONObjectRequestListener {
                    override fun onResponse(response: JSONObject) {
                        Log.e("API_RESPONSE", response.toString())
                        if (!response.getBoolean("error")) {
                            apiResponseListener?.onCompleted(response.toString())
                        } else {
                            apiResponseListener?.onError(response.getString("message"))
                        }
                    }

                    override fun onError(anError: ANError) {
                        apiResponseListener?.onError(anError.errorDetail)
                    }
                })
    }


    fun getNotifications(param: JSONObject) {
        if (!isNetworkAvailable) {
            apiResponseListener?.onError(MyApplication.instance.resources.getString(R.string.network_not_available_message))
            return
        }
        Log.e(TAG, " " + param.toString() + "  " + ApiUrl.GET_NOTIFICATION)
        var request = AndroidNetworking.post(ApiUrl.GET_NOTIFICATION)
        request.addHeaders("API_KEY", Constant.API_KEY)
        request.addHeaders("Content-Type", "application/json")
        request.addJSONObjectBody(param)
        request.setTag("GET_NOTIFICATION").setPriority(Priority.HIGH).build()

                .getAsJSONObject(object : JSONObjectRequestListener {
                    override fun onResponse(response: JSONObject) {
                        Log.e("API_RESPONSE", response.toString())
                        if (!response.getBoolean("error")) {
                            apiResponseListener?.onCompleted(response.toString())

                            try {
                                doAsync {
                                    val json = response

                                    if (json.has("data")) {
                                        val array = json.getJSONArray("data")
                                        val parser = NotificationParser()
                                        Log.d("Notifications"," "+array.length())
                                        for (i in 0 until array.length()) {

                                            var obj = JSONObject()

                                            // Log.e("NotificationParser", array.getJSONObject(i).toString())
                                            if (array.get(i) is JSONObject) {
                                                obj = array.getJSONObject(i)

                                            } else {
                                                val objArray = array.getJSONArray(i)
                                                obj = objArray.getJSONObject(0)
                                            }

                                            val notification = parser.parse(obj)

                                            var notificationData = AppDatabase.getAppDatabase().notificationDao().getNotification(notification.id)

                                            if(notificationData == null){
                                                notificationData = NotificationData()
                                                notificationData!!.id = notification.id
                                                notificationData!!.type = notification.hint
                                                notificationData!!.read = 0

                                                AppDatabase.getAppDatabase().notificationDao().insert(notificationData)
                                            }
                                        }

                                        Log.d("Notifications"," insert "+AppDatabase.getAppDatabase().notificationDao().getTotalNotificationCount())

                                    }
                                }
                            } catch (e: Exception) {
                                e.printStackTrace()
                            }

                        } else {
                            apiResponseListener?.onError(response.getString("message"))
                        }
                    }

                    override fun onError(anError: ANError) {
                        apiResponseListener?.onError(anError.errorDetail)
                    }
                })


    }

    fun getTransaction(param: JSONObject) {
        if (!isNetworkAvailable) {
            apiResponseListener?.onError(MyApplication.instance.resources.getString(R.string.network_not_available_message))
            return
        }
        Log.e(TAG, " " + param.toString() + "  " + ApiUrl.GET_NOTIFICATION)
        var request = AndroidNetworking.post(ApiUrl.GET_NOTIFICATION)
        request.addHeaders("API_KEY", Constant.API_KEY)
        request.addHeaders("Content-Type", "application/json")
        request.addJSONObjectBody(param)
        request.setTag("GET_NOTIFICATION").setPriority(Priority.HIGH).build()

                .getAsJSONObject(object : JSONObjectRequestListener {
                    override fun onResponse(response: JSONObject) {
                        Log.e("API_RESPONSE", response.toString())
                        if (!response.getBoolean("error")) {
                            apiResponseListener?.onCompleted(response.toString())
                        } else {
                            apiResponseListener?.onError(response.getString("message"))
                        }
                    }

                    override fun onError(anError: ANError) {
                        apiResponseListener?.onError(anError.errorDetail)
                    }
                })


    }

    fun deleteAllActivities(param: JSONObject) {
        if (!isNetworkAvailable) {
            apiResponseListener?.onError(MyApplication.instance.resources.getString(R.string.network_not_available_message))
            return
        }
        Log.e(TAG, " " + param.toString() + "  " + ApiUrl.DELETE_ALL_ACTIVITY)
        var request = AndroidNetworking.post(ApiUrl.DELETE_ALL_ACTIVITY)
        request.addHeaders("API_KEY", Constant.API_KEY)
        request.addHeaders("Content-Type", "application/json")
        request.addJSONObjectBody(param)
        request.setTag("DELETE_ALL_ACTIVITY").setPriority(Priority.HIGH).build()

                .getAsJSONObject(object : JSONObjectRequestListener {
                    override fun onResponse(response: JSONObject) {
                        Log.e("API_RESPONSE", response.toString())
                        if (!response.getBoolean("error")) {
                            apiResponseListener?.onCompleted(response.toString())
                        } else {
                            apiResponseListener?.onError(response.getString("message"))
                        }
                    }

                    override fun onError(anError: ANError) {
                        apiResponseListener?.onError(anError.errorDetail)
                    }
                })
    }

    fun deleteActivity(param: JSONObject) {
        if (!isNetworkAvailable) {
            apiResponseListener?.onError(MyApplication.instance.resources.getString(R.string.network_not_available_message))
            return
        }
        Log.e(TAG, " " + param.toString() + "  " + ApiUrl.DELETE_SINGLE_ACTIVITY)
        var request = AndroidNetworking.post(ApiUrl.DELETE_SINGLE_ACTIVITY)
        request.addHeaders("API_KEY", Constant.API_KEY)
        request.addHeaders("Content-Type", "application/json")
        request.addJSONObjectBody(param)
        request.setTag("DELETE_SINGLE_ACTIVITY").setPriority(Priority.HIGH).build()

                .getAsJSONObject(object : JSONObjectRequestListener {
                    override fun onResponse(response: JSONObject) {
                        Log.e("API_RESPONSE", response.toString())
                        if (!response.getBoolean("error")) {
                            apiResponseListener?.onCompleted(response.toString())
                        } else {
                            apiResponseListener?.onError(response.getString("message"))
                        }
                    }

                    override fun onError(anError: ANError) {
                        apiResponseListener?.onError(anError.errorDetail)
                    }
                })
    }


    fun updateNotificationStatus(param: JSONObject) {
        if (!isNetworkAvailable) {
            apiResponseListener?.onError(MyApplication.instance.resources.getString(R.string.network_not_available_message))
            return
        }
        Log.e(TAG, " " + param.toString() + "  " + ApiUrl.UPDATE_NOTIFICATION_STATUS)
        var request = AndroidNetworking.post(ApiUrl.UPDATE_NOTIFICATION_STATUS)
        request.addHeaders("API_KEY", Constant.API_KEY)
        request.addHeaders("Content-Type", "application/json")
        request.addJSONObjectBody(param)
        request.setTag("UPDATE_NOTIFICATION_STATUS").setPriority(Priority.HIGH).build()

                .getAsJSONObject(object : JSONObjectRequestListener {
                    override fun onResponse(response: JSONObject) {
                        Log.e("API_RESPONSE", response.toString())
                        if (!response.getBoolean("error")) {
                            apiResponseListener?.onCompleted(response.toString())
                        } else {
                            apiResponseListener?.onError(response.getString("message"))
                        }
                    }

                    override fun onError(anError: ANError) {
                        apiResponseListener?.onError(anError.errorDetail)
                    }
                })
    }


    fun fetchRoutes(url: String) {
        if (!isNetworkAvailable) {
            apiResponseListener?.onError(MyApplication.instance.resources.getString(R.string.network_not_available_message))
            return
        }
        Log.e(TAG, " " + "  " + url)
        var request = AndroidNetworking.get(url)

        request.setTag("fetchRoutes").setPriority(Priority.HIGH).build()

                .getAsJSONObject(object : JSONObjectRequestListener {
                    override fun onResponse(response: JSONObject) {
                        Log.e("fetchRoutes", response.toString())
                       /* if (!response.getBoolean("error")) {
                            apiResponseListener?.onCompleted(response.toString())
                        } else {
                            apiResponseListener?.onError(response.getString("message"))
                        }*/

                        apiResponseListener?.onCompleted(response.toString())
                    }

                    override fun onError(anError: ANError) {
                        apiResponseListener?.onError(anError.errorDetail)
                    }
                })
    }

    fun getOTPOverCall(phoneNumber: String, countryCode: String){
        if (!isNetworkAvailable) {
            apiResponseListener?.onError(MyApplication.instance.resources.getString(R.string.network_not_available_message))
            return
        }

        Log.e(TAG, " " + phoneNumber + "  " + countryCode + "  " + ApiUrl.GET_OTP_ON_CALL)

        val param = JSONObject()
        param.put("phone",phoneNumber)
        param.put("country_code", countryCode)
        Log.e(TAG, " Params are send as "  + "$param" + "  " + ApiUrl.GET_OTP_ON_CALL)
        var request = AndroidNetworking.post(ApiUrl.GET_OTP_ON_CALL)
        request.addHeaders("API_KEY", Constant.API_KEY)
        request.addHeaders("Content-Type", "application/json")
        request.addJSONObjectBody(param)
        request.setTag("GET_OTP_ON_CALL").setPriority(Priority.HIGH).build()

                .getAsJSONObject(object : JSONObjectRequestListener {
                    override fun onResponse(response: JSONObject) {
                        Log.e("API_RESPONSE", response.toString())
                        if (!response.getBoolean("error")) {
                            apiResponseListener?.onCompleted(response.toString())
                        } else {
                            apiResponseListener?.onError(response.getString("message"))
                        }
                    }

                    override fun onError(anError: ANError) {
                        apiResponseListener?.onError(anError.errorDetail)
                    }
                })
    }
}