package com.itwhiz4u.q8mercato.kashtahport.fragment.main


import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.itwhiz4u.q8mercato.kashtahport.R
import com.itwhiz4u.q8mercato.kashtahport.activities.HotelDetailsActivity
import com.itwhiz4u.q8mercato.kashtahport.activities.MainActivity
import com.itwhiz4u.q8mercato.kashtahport.adapter.HotelListAdapter
import com.itwhiz4u.q8mercato.kashtahport.customView.EqualSpaceItemDecoration
import com.itwhiz4u.q8mercato.kashtahport.map.MapInfoWindow
import com.itwhiz4u.q8mercato.kashtahport.model.HotelModel
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.MapView
import com.google.android.gms.maps.MapsInitializer
import com.google.android.gms.maps.OnMapReadyCallback
import kotlinx.android.synthetic.main.fragment_hotel_map_list.view.*
import java.lang.Exception
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.model.CameraPosition
import com.google.android.gms.maps.model.MarkerOptions
import com.google.android.gms.maps.model.LatLng


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 *
 */
class HotelMapListFragment : Fragment(),OnMapReadyCallback {


    var mapView:MapView?=null
    lateinit var fragView :View
    lateinit var mainActivity: MainActivity
    lateinit var adapter: HotelListAdapter
    var hotelList = ArrayList<HotelModel>()

    var map :GoogleMap?= null
    fun setActivity(mainActivity: MainActivity){
        this.mainActivity = mainActivity
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        fragView = inflater.inflate(R.layout.fragment_hotel_map_list, container, false)

        adapter = HotelListAdapter(activity!!,hotelList,object : HotelListAdapter.OnItemClick() {
            override fun onItemClick(position: Int) {
                startActivity(Intent(activity!!, HotelDetailsActivity::class.java))
            }

        })

        adapter.hotelListType = HotelListAdapter.TYPE_MAP_LIST

        fragView.recycler_view.layoutManager = LinearLayoutManager(activity!!,LinearLayoutManager.HORIZONTAL,false)
        fragView.recycler_view.addItemDecoration(EqualSpaceItemDecoration(16))
        fragView.recycler_view.adapter = adapter

        adapter.notifyDataSetChanged()
        fragView.map_view.onCreate(savedInstanceState)
        fragView.map_view.onResume()

        try {
            MapsInitializer.initialize(activity?.applicationContext)
        }catch (e:Exception){
            e.printStackTrace()
        }

        fragView.map_view.getMapAsync(this)

        return fragView
    }

    override fun onMapReady(googleMap: GoogleMap?) {

        map = googleMap
        // For showing a move to my location button
        if(ContextCompat.checkSelfPermission(activity!!,android.Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED){
            map!!.setMyLocationEnabled(true)
        }

        // For dropping a marker at a point on the Map

        val sydney = LatLng(-34.0, 151.0)
        map!!.addMarker(MarkerOptions().position(sydney).title("Hotel Name").snippet(getString(R.string.text_hotel_des)))

        val cusInfo = MapInfoWindow(activity!!,HotelModel())
        map!!.setInfoWindowAdapter(cusInfo)

        // For zooming automatically to the location of the marker
        val cameraPosition = CameraPosition.Builder().target(sydney).zoom(12f).build()
        map!!.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition))
    }

    override fun onResume() {
        super.onResume()
        fragView.map_view.onResume()
    }

    override fun onPause() {
        super.onPause()
        fragView.map_view.onPause()
    }

    override fun onDestroy() {
        super.onDestroy()
        fragView.map_view.onDestroy()
    }

    override fun onLowMemory() {
        super.onLowMemory()
        fragView.map_view.onLowMemory()
    }
}
