package com.itwhiz4u.q8mercato.kashtahport.model

import android.os.Parcel
import android.os.Parcelable

class HotelImageModel() :Parcelable {
    var id = ""
    var url = ""

    constructor(parcel: Parcel) : this() {
        id = parcel.readString()
        url = parcel.readString()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(id)
        parcel.writeString(url)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<HotelImageModel> {
        override fun createFromParcel(parcel: Parcel): HotelImageModel {
            return HotelImageModel(parcel)
        }

        override fun newArray(size: Int): Array<HotelImageModel?> {
            return arrayOfNulls(size)
        }
    }
}