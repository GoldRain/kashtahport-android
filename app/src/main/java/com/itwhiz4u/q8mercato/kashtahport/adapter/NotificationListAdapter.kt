package com.itwhiz4u.q8mercato.kashtahport.adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.itwhiz4u.q8mercato.kashtahport.Controller.notification.*
import com.itwhiz4u.q8mercato.kashtahport.R
import com.itwhiz4u.q8mercato.kashtahport.helper.Constant
import com.itwhiz4u.q8mercato.kashtahport.helper.DaysCount
import com.itwhiz4u.q8mercato.kashtahport.model.NotificationModel
import java.lang.Exception
import com.chauthai.swipereveallayout.ViewBinderHelper




/**
 * Created by bodacious on 25/3/19.
 */

class NotificationListAdapter(internal var context: Context, internal var mList: List<NotificationModel>, internal var listener: OnItemClick) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    inner class MyBookViewHolder(itemView: View): RecyclerView.ViewHolder(itemView)  {}
    inner class MyMessageViewHolder(itemView: View): RecyclerView.ViewHolder(itemView)  {}
    inner class MyFeedBackViewHolder(itemView: View): RecyclerView.ViewHolder(itemView)  {}
    inner class MyOfferViewHolder(itemView: View): RecyclerView.ViewHolder(itemView)  {}
    inner class MyListingViewHolder(itemView: View): RecyclerView.ViewHolder(itemView)  {}

    lateinit var daysCount:DaysCount
     val viewBinderHelper = ViewBinderHelper()
    init {
        daysCount = DaysCount()

        viewBinderHelper.setOpenOnlyOne(true)
    }
    override fun getItemViewType(position: Int): Int {
        return position
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {

        val type = mList[viewType].hint
        Log.e("NotificationList","** "+type)
        when(type){

            Constant.TYPE_BOOK_BUYER ->{
                val view = LayoutInflater.from(context).inflate(R.layout.list_item_notification_message, parent, false)
                return MyMessageViewHolder(view)            }

            Constant.TYPE_BOOK_SELLER ->{
                val view = LayoutInflater.from(context).inflate(R.layout.list_item_notification_message, parent, false)
                return MyMessageViewHolder(view)            }

            Constant.TYPE_LIKE_BUYER ->{
                val view = LayoutInflater.from(context).inflate(R.layout.list_item_notification_listing, parent, false)
                return MyListingViewHolder(view)            }

            Constant.TYPE_LIKE_SELLER ->{
                val view = LayoutInflater.from(context).inflate(R.layout.list_item_notification_listing, parent, false)
                return MyListingViewHolder(view)            }

            Constant.TYPE_FEEDBACK_BUYER ->{
                val view = LayoutInflater.from(context).inflate(R.layout.list_item_notification_feedback, parent, false)
                return MyFeedBackViewHolder(view)            }

            Constant.TYPE_FEEDBACK_SELLER ->{
                val view = LayoutInflater.from(context).inflate(R.layout.list_item_notification_feedback, parent, false)
                return MyFeedBackViewHolder(view)            }

            Constant.TYPE_APPROVED ->{
                val view = LayoutInflater.from(context).inflate(R.layout.list_item_notification_message, parent, false)
                return MyMessageViewHolder(view)            }
            else ->{
                val view = LayoutInflater.from(context).inflate(R.layout.list_item_notification, parent, false)
                return MyOfferViewHolder(view)            }

        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val type = mList[position].hint

        try {
            when(type){
                Constant.TYPE_BOOK_BUYER ->{
                    NotificationMessageController(context,this,holder as MyMessageViewHolder,mList[position],listener,position).control()
                }

                Constant.TYPE_BOOK_SELLER ->{
                    NotificationMessageController(context,this,holder as MyMessageViewHolder,mList[position],listener,position).control()
                }

                Constant.TYPE_LIKE_BUYER ->{
                    NotificationLikeController(context,this,holder as MyListingViewHolder,mList[position],listener,position).control()
                }

                Constant.TYPE_LIKE_SELLER ->{
                    NotificationLikeController(context,this,holder as MyListingViewHolder,mList[position],listener,position).control()
                }

                Constant.TYPE_FEEDBACK_BUYER ->{
                    NotificationFeedbackController(context,this,holder as MyFeedBackViewHolder,mList[position],listener,position).control()
                }

                Constant.TYPE_FEEDBACK_SELLER ->{
                    NotificationFeedbackController(context,this,holder as MyFeedBackViewHolder,mList[position],listener,position).control()
                }

                Constant.TYPE_APPROVED ->{
                    NotificationMessageController(context,this,holder as MyMessageViewHolder,mList[position],listener,position).control()
                }
                else ->{
                    NotificationOfferController(context,this,holder as MyOfferViewHolder,mList[position],listener,position).control()
                }
            }
        }catch (e:Exception){
            Log.d("Exception", " "+position+" "+type)
            e.printStackTrace()
        }
    }

    override fun getItemCount(): Int {
        return mList.size
    }

    abstract class OnItemClick {
        abstract fun onItemClick(position: Int)
        abstract fun onDelete(position: Int)
    }

}
