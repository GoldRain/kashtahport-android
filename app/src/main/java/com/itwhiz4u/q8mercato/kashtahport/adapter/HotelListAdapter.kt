package com.itwhiz4u.q8mercato.kashtahport.adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.itwhiz4u.q8mercato.kashtahport.Controller.HotelListController
import com.itwhiz4u.q8mercato.kashtahport.Controller.HotelListMapController
import com.itwhiz4u.q8mercato.kashtahport.R
import com.itwhiz4u.q8mercato.kashtahport.model.HotelModel


/**
 * Created by bodacious on 25/3/19.
 */

class HotelListAdapter(internal var context: Context, internal var mList: List<HotelModel>, internal var listener: OnItemClick) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    var hotelListType = TYPE_LIST
    companion object {
        val TYPE_MAP_LIST = 111
        val TYPE_LIST = 112
    }
    inner class MyHotelViewHolder(itemView: View): RecyclerView.ViewHolder(itemView)  {}
    inner class MyHotelMapViewHolder(itemView: View): RecyclerView.ViewHolder(itemView)  {}

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        when(hotelListType){
            TYPE_LIST ->{
                val view = LayoutInflater.from(context).inflate(R.layout.list_item_hotel, parent, false)
                return MyHotelViewHolder(view)
            }
            TYPE_MAP_LIST ->{
                val view = LayoutInflater.from(context).inflate(R.layout.list_item_hotel_map, parent, false)
                return MyHotelMapViewHolder(view)
            }
            else ->{
                val view = LayoutInflater.from(context).inflate(R.layout.list_item_hotel, parent, false)
                return MyHotelViewHolder(view)
            }
        }

    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {

        val hotelModel = mList[position]
        when(hotelListType){
            TYPE_LIST ->{
                HotelListController(context,this,holder as MyHotelViewHolder, hotelModel,listener,position).control()
            }

            TYPE_MAP_LIST ->{
                HotelListMapController(context,this,holder as MyHotelMapViewHolder, hotelModel,listener,position).controll()
            }
            else ->{
                HotelListController(context,this,holder as MyHotelViewHolder, hotelModel,listener,position).control()
            }
        }


    }

    override fun getItemCount(): Int {
        return mList.size
    }

    abstract class OnItemClick {
        abstract fun onItemClick(position: Int)
        open fun onAddFav(position: Int){}
    }

}
