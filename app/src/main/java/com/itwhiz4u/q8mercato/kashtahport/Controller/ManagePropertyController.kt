package com.itwhiz4u.q8mercato.kashtahport.Controller

import android.content.Context
import android.content.Intent
import android.support.v4.content.ContextCompat
import com.itwhiz4u.q8mercato.kashtahport.R
import com.itwhiz4u.q8mercato.kashtahport.activities.SellerEditPropertyActivity
import com.itwhiz4u.q8mercato.kashtahport.adapter.HotelPropertyAdapter
import com.itwhiz4u.q8mercato.kashtahport.helper.Constant
import com.itwhiz4u.q8mercato.kashtahport.helper.ConstantFunction
import com.itwhiz4u.q8mercato.kashtahport.model.HotelPropertyModel
import kotlinx.android.synthetic.main.list_item_manage_property.view.*
import org.jetbrains.anko.textColor

class ManagePropertyController(val context: Context, val hotelListAdapter: HotelPropertyAdapter, val holder:HotelPropertyAdapter.MyHotelViewHolder, val hotelModel: HotelPropertyModel, val listener: HotelPropertyAdapter.OnItemClick, val position: Int) {

    fun control(){

        holder.itemView.hotel_name.text = hotelModel.description
        holder.itemView.title.text = hotelModel.name
        if(hotelModel.isApprove){

            holder.itemView.status.text = context.getString(R.string.hint_approved)
            holder.itemView.status.textColor = ContextCompat.getColor(context,R.color.color_status_green)
        }else{
            holder.itemView.status.text = context.getString(R.string.hint_waiting_approval)
            holder.itemView.status.textColor = ContextCompat.getColor(context,R.color.color_status_red)
        }

        holder.itemView.ll_edit.setOnClickListener {
            val intent = Intent(context, SellerEditPropertyActivity::class.java)
            intent.putExtra(Constant.KEY_HOTEL_PROPERTY, hotelModel)
            context.startActivity(intent)
        }

        try {
            holder.itemView.date.text = ConstantFunction.getOnlyDate(hotelModel.created_at.toLong())
        }catch (e:Exception){
            e.printStackTrace()
        }

        holder.itemView.frame_enable.setOnClickListener {
            listener.onItemClick(position,true)
        }

        holder.itemView.frame_disable.setOnClickListener {
            listener.onItemClick(position,false)
        }

        if(hotelModel.isEnable){
            holder.itemView.frame_enable.setBackgroundResource(R.drawable.back_corner_gray)
            holder.itemView.frame_disable.setBackgroundResource(R.drawable.back_corner_back_redd)
            holder.itemView.frame_enable.isEnabled = false
            holder.itemView.frame_disable.isEnabled = true
        }else{
            holder.itemView.frame_enable.setBackgroundResource(R.drawable.back_corner_pri)
            holder.itemView.frame_disable.setBackgroundResource(R.drawable.back_corner_gray)
            holder.itemView.frame_disable.isEnabled = false
            holder.itemView.frame_enable.isEnabled = true

        }
    }

}