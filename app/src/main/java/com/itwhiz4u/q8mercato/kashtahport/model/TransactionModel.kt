package com.itwhiz4u.q8mercato.kashtahport.model

import android.os.Parcel
import android.os.Parcelable

class TransactionModel() : Parcelable {

    var id = ""
    var userId = ""
    var type = ""
    var created_at = ""

    var hotel_id = ""
    var owner_id = ""
    var buyer_id = ""
    var booking_id = ""
    var amount = ""
    var payment_type = ""
    var title = ""
    var display_id = ""
    var txn_id = ""
    var ref_no = ""
    var card_type = ""
    var currency = ""

    constructor(parcel: Parcel) : this() {
        id = parcel.readString()
        userId = parcel.readString()
        type = parcel.readString()
        created_at = parcel.readString()
        hotel_id = parcel.readString()
        owner_id = parcel.readString()
        buyer_id = parcel.readString()
        booking_id = parcel.readString()
        amount = parcel.readString()
        payment_type = parcel.readString()
        title = parcel.readString()
        display_id = parcel.readString()
        txn_id = parcel.readString()
        ref_no = parcel.readString()
        card_type = parcel.readString()
        currency = parcel.readString()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(id)
        parcel.writeString(userId)
        parcel.writeString(type)
        parcel.writeString(created_at)
        parcel.writeString(hotel_id)
        parcel.writeString(owner_id)
        parcel.writeString(buyer_id)
        parcel.writeString(booking_id)
        parcel.writeString(amount)
        parcel.writeString(payment_type)
        parcel.writeString(title)
        parcel.writeString(display_id)
        parcel.writeString(txn_id)
        parcel.writeString(ref_no)
        parcel.writeString(card_type)
        parcel.writeString(currency)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<TransactionModel> {
        override fun createFromParcel(parcel: Parcel): TransactionModel {
            return TransactionModel(parcel)
        }

        override fun newArray(size: Int): Array<TransactionModel?> {
            return arrayOfNulls(size)
        }
    }


}