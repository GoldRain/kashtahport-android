package com.itwhiz4u.q8mercato.kashtahport.Controller

import android.content.Context
import android.text.TextUtils
import android.util.Log
import android.view.View
import com.itwhiz4u.q8mercato.kashtahport.R
import com.itwhiz4u.q8mercato.kashtahport.adapter.HotelBookingRequestAdapter
import com.itwhiz4u.q8mercato.kashtahport.helper.BookingStatus
import com.itwhiz4u.q8mercato.kashtahport.helper.ConstantFunction
import com.itwhiz4u.q8mercato.kashtahport.model.HotelBookingModel
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import kotlinx.android.synthetic.main.list_item_booking_req.view.*

class BookingListController(val context: Context, val hotelListAdapter: HotelBookingRequestAdapter, val holder:HotelBookingRequestAdapter.MyHotelViewHolder, val bookingModel: HotelBookingModel, val listener: HotelBookingRequestAdapter.OnItemClick, val position: Int) {

    val TAG = "BookingList"
    fun control(){


        Log.d(TAG," "+bookingModel.hotelModel)
        holder.itemView.hotel_name.text = bookingModel.hotelModel?.name

        holder.itemView.hotel_des.text = bookingModel.hotelModel?.description

        ConstantFunction.setPrice(context,bookingModel.hotelModel!!.currency,bookingModel.price,holder.itemView.rent)
  //      holder.itemView.rent.text = "$"+bookingModel?.price
        holder.itemView.room_count.text = bookingModel.hotelModel?.rooms+" "+context.getString(R.string.hint_room)
        holder.itemView.adult_count.text = bookingModel.hotelModel?.person+" "+context.getString(R.string.hint_adult)
        holder.itemView.children_count.text = bookingModel.hotelModel?.children+" "+context.getString(R.string.hint_child)

        val status = ""+bookingModel.status
        when(status.toLowerCase()){
            "pending" ->{
                holder.itemView.frame_accept.isEnabled = true
                holder.itemView.frame_reject.isEnabled = true

                holder.itemView.status_accept.setText(context.getString(R.string.bk_status_accept))
                holder.itemView.status_reject.setText(context.getString(R.string.bk_status_reject))

                holder.itemView.frame_accept.visibility = View.VISIBLE
                holder.itemView.frame_reject.visibility = View.VISIBLE
                holder.itemView.frame_accept.setBackgroundResource(R.drawable.back_corner_pri)
                holder.itemView.frame_reject.setBackgroundResource(R.drawable.back_corner_redd)
            }

            "confirm" ->{
                holder.itemView.status_accept.setText(context.getString(R.string.bk_status_accepted))
                holder.itemView.status_reject.setText(BookingStatus.REJECT)

                holder.itemView.frame_accept.isEnabled = false
                holder.itemView.frame_reject.isEnabled = false
                holder.itemView.frame_accept.visibility = View.VISIBLE
                holder.itemView.frame_reject.visibility = View.GONE

                holder.itemView.frame_accept.setBackgroundResource(R.drawable.back_corner_pri)
                holder.itemView.frame_reject.setBackgroundResource(R.drawable.back_corner_gray)
            }

            "accept" ->{
                holder.itemView.status_accept.setText(context.getString(R.string.bk_status_accepted))
                holder.itemView.status_reject.setText(BookingStatus.REJECT)

                holder.itemView.frame_accept.isEnabled = false
                holder.itemView.frame_reject.isEnabled = false

                holder.itemView.frame_accept.visibility = View.VISIBLE
                holder.itemView.frame_reject.visibility = View.GONE

                holder.itemView.frame_accept.setBackgroundResource(R.drawable.back_corner_pri)
                holder.itemView.frame_reject.setBackgroundResource(R.drawable.back_corner_gray)
            }

            "reject" ->{
                holder.itemView.status_reject.setText(context.getString(R.string.bk_status_rejected))
                holder.itemView.status_accept.setText(BookingStatus.ACCEPT)

                holder.itemView.frame_accept.isEnabled = false
                holder.itemView.frame_reject.isEnabled = false

                holder.itemView.frame_accept.visibility = View.GONE
                holder.itemView.frame_reject.visibility = View.VISIBLE

                holder.itemView.frame_accept.setBackgroundResource(R.drawable.back_corner_gray)
                holder.itemView.frame_reject.setBackgroundResource(R.drawable.back_corner_redd)
            }

            "cancel" ->{
                holder.itemView.status_reject.setText(context.getString(R.string.bk_status_canceled))
                holder.itemView.status_accept.setText(BookingStatus.ACCEPT)

                holder.itemView.frame_accept.isEnabled = false
                holder.itemView.frame_reject.isEnabled = false

                holder.itemView.frame_accept.visibility = View.GONE
                holder.itemView.frame_reject.visibility = View.VISIBLE

                holder.itemView.frame_accept.setBackgroundResource(R.drawable.back_corner_gray)
                holder.itemView.frame_reject.setBackgroundResource(R.drawable.back_corner_redd)
            }

            "paid" ->{
                holder.itemView.status_accept.setText(context.getString(R.string.bk_status_paid))
                holder.itemView.status_reject.setText(BookingStatus.REJECT)

                holder.itemView.frame_accept.isEnabled = false
                holder.itemView.frame_reject.isEnabled = false

                holder.itemView.frame_accept.visibility = View.VISIBLE
                holder.itemView.frame_reject.visibility = View.GONE

                holder.itemView.frame_accept.setBackgroundResource(R.drawable.back_corner_green)
                holder.itemView.frame_reject.setBackgroundResource(R.drawable.back_corner_gray)
            }
            else ->{
                holder.itemView.status_accept.setText(bookingModel.status)
                holder.itemView.status_reject.setText(BookingStatus.REJECT)

                holder.itemView.frame_accept.isEnabled = false
                holder.itemView.frame_reject.isEnabled = false

                holder.itemView.frame_accept.visibility = View.VISIBLE
                holder.itemView.frame_reject.visibility = View.VISIBLE

                holder.itemView.frame_accept.setBackgroundResource(R.drawable.back_corner_gray)
                holder.itemView.frame_reject.setBackgroundResource(R.drawable.back_corner_gray)
            }
        }



        try {
            if(!TextUtils.isEmpty(bookingModel.hotelModel?.rating)){
                holder.itemView.rating_bar.rating = bookingModel.hotelModel?.rating!!.toFloat()
            }

            if(bookingModel?.hotelModel?.imageList!!.size>0){
                Glide.with(context)
                        .asBitmap()
                        .load(bookingModel.hotelModel?.imageList!![0].url)
                        .apply(RequestOptions().override(200))
                        .into(holder.itemView.hotel_image)
            }

        }catch (e:Exception){
            e.printStackTrace()
        }

        holder.itemView.setOnClickListener {
            listener.onItemClick(position)
        }

        holder.itemView.frame_accept.setOnClickListener {
            if(bookingModel.status.equals(BookingStatus.PENDING,true)){
                listener.onUpdateBooking(position,BookingStatus.CONFIRM)
            }

        }

        holder.itemView.frame_reject.setOnClickListener {
            if(bookingModel.status.equals(BookingStatus.PENDING,true)){
                listener.onUpdateBooking(position,BookingStatus.REJECT)
            }

        }
    }

}