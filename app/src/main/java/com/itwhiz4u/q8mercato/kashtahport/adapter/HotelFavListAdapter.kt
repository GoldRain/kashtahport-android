package com.itwhiz4u.q8mercato.kashtahport.adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.itwhiz4u.q8mercato.kashtahport.Controller.FavHotelListController
import com.itwhiz4u.q8mercato.kashtahport.R
import com.itwhiz4u.q8mercato.kashtahport.model.HotelModel
import kotlinx.android.synthetic.main.list_item_fav_hotel.view.*


/**
 * Created by bodacious on 25/3/19.
 */

class HotelFavListAdapter(internal var context: Context, internal var mList: List<HotelModel>, internal var listener: OnItemClick) : RecyclerView.Adapter<HotelFavListAdapter.MyViewHolder>() {

    inner class MyViewHolder(itemView: View): RecyclerView.ViewHolder(itemView)  {

        internal var description: TextView? = null

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {

        val view = LayoutInflater.from(context).inflate(R.layout.list_item_fav_hotel, parent, false)
        return MyViewHolder(view)
    }

    override fun onBindViewHolder(holder: HotelFavListAdapter.MyViewHolder, position: Int) {

        val model = mList[position]
        if(position%2 ==0){
            holder.itemView.back1.visibility = View.VISIBLE
            holder.itemView.back2.visibility = View.GONE
        }else{
            holder.itemView.back1.visibility = View.GONE
            holder.itemView.back2.visibility = View.VISIBLE
        }

        FavHotelListController(context,this,holder, model,listener,position).control()


    }

    override fun getItemCount(): Int {
        return mList.size
    }

    abstract class OnItemClick {
        abstract fun onItemClick(position: Int)
        abstract fun removeFromFav(position: Int)
    }

}
