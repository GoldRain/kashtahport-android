package com.itwhiz4u.q8mercato.kashtahport.activities

import android.Manifest
import android.app.Activity
import android.app.Dialog
import android.content.ContentValues
import android.content.DialogInterface
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Color
import android.graphics.Typeface
import android.graphics.drawable.ColorDrawable
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.support.v7.app.AlertDialog
import android.text.TextUtils
import android.util.Log
import android.view.*
import android.widget.*
import com.applandeo.materialcalendarview.CalendarUtils
import com.applandeo.materialcalendarview.EventDay
import com.applandeo.materialcalendarview.listeners.OnDayClickListener
import com.itwhiz4u.q8mercato.kashtahport.Api.ApiResponseListener
import com.itwhiz4u.q8mercato.kashtahport.Api.ServiceRequest
import com.itwhiz4u.q8mercato.kashtahport.R
import com.itwhiz4u.q8mercato.kashtahport.adapter.RoomTypeAdapter
import com.itwhiz4u.q8mercato.kashtahport.customView.ImageOverLayView
import com.itwhiz4u.q8mercato.kashtahport.database.AppDatabase
import com.itwhiz4u.q8mercato.kashtahport.database.FavPost
import com.itwhiz4u.q8mercato.kashtahport.model.*
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.RequestOptions
import com.bumptech.glide.request.target.Target
import com.facebook.drawee.backends.pipeline.Fresco
import com.itwhiz4u.q8mercato.kashtahport.customDialog.DialogListener
import com.itwhiz4u.q8mercato.kashtahport.helper.*
import com.facebook.drawee.generic.GenericDraweeHierarchyBuilder
import com.google.firebase.FirebaseApp
import com.google.firebase.dynamiclinks.DynamicLink
import com.google.firebase.dynamiclinks.FirebaseDynamicLinks
import com.stfalcon.frescoimageviewer.ImageViewer
import kotlinx.android.synthetic.main.activity_hotel_details.*
import kotlinx.android.synthetic.main.dialog_calender_range.*
import kotlinx.android.synthetic.main.dialog_register_warning.*
import kotlinx.android.synthetic.main.view_all_rating.*
import kotlinx.android.synthetic.main.view_amenities.*
import kotlinx.android.synthetic.main.view_room_type.*
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread
import org.json.JSONObject
import java.io.IOException
import java.lang.Exception
import java.net.URL
import java.text.DecimalFormat
import java.util.*
import kotlin.Comparator
import kotlin.collections.ArrayList
import kotlin.collections.HashMap

class HotelDetailsActivity : BaseActivity() {

    lateinit var adapter: RoomTypeAdapter
    lateinit var dialogDate: Dialog
    lateinit var dialogCalendar: Dialog

    var amentiesOnDrawbleList = java.util.ArrayList<Int>()
    var amentiesOffDrawbleList = java.util.ArrayList<Int>()
    var amenties = arrayOfNulls<ImageView>(10)
    var amenitiesModelList = java.util.ArrayList<AmenitiesModel>()

    var dateModel = HotelDateModel()
    var startDate = ""
    var endDate = ""

    var hotelModel: HotelModel? = null

    val roomTypeList = ArrayList<RoomTypeModel>()

    val availableList = ArrayList<Calendar>()
    val disableList = ArrayList<Calendar>()
    val mainDisableList = ArrayList<Calendar>()
    val mapDisable = HashMap<Long, Calendar>()
    val mapAvailable = HashMap<Long, HotelDateModel>()

    val eventList = ArrayList<EventDay>()
    val eventMap = HashMap<Long, EventDay>()
    val eventMapMain = HashMap<Long, EventDay>()

    var update = false

    var can = false

    val calenderStart = Calendar.getInstance()
    val calenderEnd = Calendar.getInstance()
    val selectedList = ArrayList<Calendar>()
    val mapSelected = HashMap<Long, Calendar>()

    var maxDate = 0L
    var minDate = 0L
    var rentCount = 0

    var rent = 0
    var startMs = "0"
    var endMs = "0"

    var cSign = "USD"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_hotel_details)
        supportActionBar?.hide()

        date_start.setText(getString(R.string.from))
        date_end.setText(getString(R.string.to))

        hotelModel = intent.getParcelableExtra(Constant.KEY_HOTEL)
        can = intent.getBooleanExtra(Constant.KEY_BOOK_CAN, false)

        cSign = ConstantFunction.getCurrencySign(hotelModel!!.currency)
        if (can) {
            // card_dates.visibility = View.GONE
            frame_cancel.visibility = View.VISIBLE
            register.visibility = View.GONE
        } else {
            register.visibility = View.VISIBLE
            // card_dates.visibility = View.VISIBLE
            frame_cancel.visibility = View.GONE
        }

        for (i in 0 until tab_layout_room.tabCount) {
            val tab = (tab_layout_room.getChildAt(0) as ViewGroup).getChildAt(i)

            val p = (tab.layoutParams as ViewGroup.MarginLayoutParams)
            p.setMargins(8, 0, 8, 0)
        }

        register.setOnClickListener {
            if (validation()) {
                if (!prefManager!!.getBoolean(Constant.KEY_USER_LOGIN_STATUS)) {
                    showRegisterDialog()
                } else {
                    alert?.title = ""
                    alert?.content = getString(R.string.hd_hint_book_for) + " $cSign $rentCount?"
                    alert?.listenr = object : DialogListener() {
                        override fun okClick() {
                            getHotel()
                        }
                    }
                    alert?.show()

                }
            }
        }

        frame_location.setOnClickListener {
            if (checkPermissions()) {
                update = true
                val intent = Intent(this, MapNewActivity::class.java)
                intent.putExtra(Constant.KEY_HOTEL, hotelModel)
                startActivity(intent)
            }
        }

        cal_book.setOnClickListener {
            if (!can) {
                dialogCalendar.show()
            }
        }

        im_fav.tag = false
        im_fav.setOnClickListener {
            update = true
            if ((im_fav.tag as Boolean)) {
                removeToFav()
            } else {
                addToFav()
            }

        }

        back.setOnClickListener {
            if (update) {
                hotelModel!!.isFav = (im_fav.tag as Boolean)
                val intent = Intent()
                intent.putExtra(Constant.KEY_HOTEL, hotelModel)
                setResult(Activity.RESULT_OK, intent)
            }
            super.onBackPressed()
        }

        read_more.setOnClickListener {
            val intent = Intent(this, HotelDecriptionActivity::class.java)
            intent.putExtra(Constant.KEY_HOTEL, hotelModel)
            intent.putExtra("show", "des")
            startActivity(intent)
        }

        standard_term.setOnClickListener {
            val intent = Intent(this, HotelDecriptionActivity::class.java)
            intent.putExtra(Constant.KEY_HOTEL, hotelModel)
            intent.putExtra("show", "term")
            startActivity(intent)
        }

        hotel_phone.setOnClickListener {
            val status = PermissionStatus().getPermissionStatus(this, PermissionStatus.PERMISSION_CALL)
            if (status == PermissionStatus.GRANTED) {
                val intent = Intent(Intent.ACTION_DIAL, Uri.parse("tel:${hotelModel?.phoneNumber}"))
                startActivity(intent)
            }

        }

        im_share.setOnClickListener {
            sharePost()
        }
        setAvailableDates()

        //  hint_price_pm.setText("USD $${hotelModel!!.rate} " + getString(R.string.ap_hint_pernight))
        setHotelPrice(hotelModel!!.rate)
    }

    private fun sharePost() {
        progress?.show()
        generateLink()
    }

    var linkText = ""
    val TAG = "generateLink"

    private fun setHotelPrice(price: String) {

        hint_price_pm.text = cSign + " ${price} " + getString(R.string.ap_hint_pernight)
    }

    override fun onResume() {
        setData()
        Log.e("onResume", " *** " + selectedList.size)
        getFeedback()
        super.onResume()
    }

    private fun setData() {

        try {
            hotel_name.setText(hotelModel?.name)
            hotel_des.setText(hotelModel?.description)

            if (selectedList.size > 0) {
                dialogCalendar!!.calender.selectedDates = selectedList
                dialogCalendar!!.calender.setEvents(eventList)
                setBookingDates()
            } else {
                calculateRent(true)
            }

            rating_bar.rating = hotelModel?.rating!!.toFloat()

            hotel_phone.setText("${hotelModel!!.phoneNumber}")
            hotel_address.setText("${hotelModel!!.fullAddress}")

            if (!TextUtils.isEmpty(hotelModel?.description)) {
                if (hotelModel!!.description.length > 200) {
                    read_more.visibility = View.VISIBLE
                } else {
                    read_more.visibility = View.GONE
                }
            }

            adult_count.text = (hotelModel?.person + " " + getString(R.string.hint_adult))
            room_count.text = (hotelModel?.rooms + " " + getString(R.string.hint_room))
            children_count.text = (hotelModel?.children + " " + getString(R.string.hint_child))
            wc_count.text = (hotelModel?.wc_number + " " + getString(R.string.hint_wc))

            if (hotelModel?.imageList!!.size > 0) {

                if (prefManager.getBoolean(Constant.KEY_USER_LOGIN_STATUS)) {
                    im_fav.visibility = View.VISIBLE
                } else {
                    im_fav.visibility = View.GONE
                }
                doAsync {
                    val fav = AppDatabase.getAppDatabase().favPostDao().getFavPost(hotelModel!!.id)

                    if (fav != null && fav.id.equals(hotelModel!!.id)) {
                        uiThread {
                            im_fav.setImageResource(R.drawable.ic_like_red_on)
                            im_fav.tag = true
                        }
                    } else {
                        uiThread {
                            im_fav.setImageResource(R.drawable.ic_like_gray_off)
                            im_fav.tag = false
                        }
                    }
                }

                hotel_image.setOnClickListener {
                    showAllImage(hotelModel!!.imageList, 0)
                }


                downloadImage(hotelModel!!.imageList[0]!!.url, hotel_image)
                if (hotelModel?.imageList!!.size > 1) {
                    frame_more.visibility = View.VISIBLE
                    downloadImage(hotelModel!!.imageList[1]!!.url, hotel_image2)

                    val count = (hotelModel?.imageList!!.size) - 2
                    if (count > 0) {
                        more_image.visibility = View.VISIBLE
                        more_image.setText("+ $count ")
                    } else {
                        more_image.visibility = View.GONE
                    }

                    frame_more.setOnClickListener {
                        showAllImage(hotelModel!!.imageList, 1)
                    }
                } else {
                    frame_more.visibility = View.GONE
                }

            }

            /*  val roomModel = RoomTypeModel()
              roomModel.hotelId = hotelModel?.id!!
              roomModel.person = hotelModel?.person!!
              roomModel.rooms = hotelModel?.rooms!!
              roomModel.children = hotelModel?.children!!
              roomModel.wc = hotelModel?.wc_number!!

              roomTypeList.clear()
              roomTypeList.add(roomModel)

              adapter = RoomTypeAdapter(this, roomTypeList)
              view_pager_room.adapter = adapter
              tab_layout_room.setupWithViewPager(view_pager_room)
  */
            initViews()

        } catch (e: Exception) {

            Log.e("Exception", " **  " + e.message)
            e.printStackTrace()
        }
    }

    private fun initViews() {
        amenitiesModelList.addAll(hotelModel?.amenitiesList!!)
        amentiesOnDrawbleList.add(R.drawable.ic_wifi_white)
        amentiesOnDrawbleList.add(R.drawable.ic_desk_white)
        amentiesOnDrawbleList.add(R.drawable.ic_drink_white)
        amentiesOnDrawbleList.add(R.drawable.ic_parking_white)
        amentiesOnDrawbleList.add(R.drawable.ic_spa_white)
        amentiesOnDrawbleList.add(R.drawable.ic_dining_white)
        amentiesOnDrawbleList.add(R.drawable.ic_gym_white)
        amentiesOnDrawbleList.add(R.drawable.ic_smart_tv_white_)
        amentiesOnDrawbleList.add(R.drawable.ic_swim_white)
        amentiesOnDrawbleList.add(R.drawable.ic_flame_white)

        amentiesOffDrawbleList.add(R.drawable.ic_wifi_gray)
        amentiesOffDrawbleList.add(R.drawable.ic_desk_gray)
        amentiesOffDrawbleList.add(R.drawable.ic_drink_gray)
        amentiesOffDrawbleList.add(R.drawable.ic_parking_gray)
        amentiesOffDrawbleList.add(R.drawable.ic_spa_gray)
        amentiesOffDrawbleList.add(R.drawable.ic_dining_gray)
        amentiesOffDrawbleList.add(R.drawable.ic_gym_gray)
        amentiesOffDrawbleList.add(R.drawable.ic_smart_tv_gray)
        amentiesOffDrawbleList.add(R.drawable.ic_swim_gray)
        amentiesOffDrawbleList.add(R.drawable.ic_flame_gray)


        amenties = arrayOf(im_wifi, im_desk, im_drink, im_park, im_spa, im_kitchen, im_gym, im_smartTV, im_pool, im_bornFire)

        if (amenitiesModelList.size == amenties.size) {
            for (i in 0 until amenties.size) {
                val amenitiesModel = amenitiesModelList[i]
                amenties[i]!!.tag = !amenitiesModel.status
                setAmentiesUi(amenties[i], i)
//                amenties[i]!!.setOnClickListener {
//                    setAmentiesUi(amenties[i], i)
//                }
            }
        }
    }

    private fun setAmentiesUi(imageView: ImageView?, i: Int) {
        if ((amenties[i]!!.tag as Boolean)) {
            amenties[i]!!.tag = false
            amenties[i]!!.setBackgroundResource(R.drawable.round_gray)
            amenties[i]!!.setImageResource(amentiesOffDrawbleList[i])
        } else {
            amenties[i]!!.tag = true
            amenties[i]!!.setBackgroundResource(R.drawable.round_pri)
            amenties[i]!!.setImageResource(amentiesOnDrawbleList[i])
        }
    }

    private fun downloadImage(url: String, hotel_image: ImageView) {

        Glide.with(this)
                .asBitmap()
                .load(url)
                .apply(RequestOptions().override(500))
                .listener(object : RequestListener<Bitmap> {
                    override fun onLoadFailed(e: GlideException?, model: Any?, target: Target<Bitmap>?, isFirstResource: Boolean): Boolean {
                        return false
                    }

                    override fun onResourceReady(resource: Bitmap?, model: Any?, target: com.bumptech.glide.request.target.Target<Bitmap>?, dataSource: com.bumptech.glide.load.DataSource?, isFirstResource: Boolean): Boolean {

                        return false
                    }

                })
                .into(hotel_image)
    }

    private fun showAllImage(imageList2: ArrayList<HotelImageModel>, position: Int) {

        var currentImagePositon = position
        Fresco.initialize(this@HotelDetailsActivity);
        val overView = ImageOverLayView(this@HotelDetailsActivity)

        val mImageViewerBuilder = ImageViewer.Builder<HotelImageModel>(this@HotelDetailsActivity, imageList2)
                .setFormatter(object : ImageViewer.Formatter<HotelImageModel> {
                    override fun format(t: HotelImageModel?): String {
                        return t!!.url
                    }

                })

        val hierarchyBuilder = GenericDraweeHierarchyBuilder.newInstance(getResources())
                .setFailureImage(R.drawable.ic_warning_blue)
                .setProgressBarImage(R.drawable.style_circular)
        mImageViewerBuilder.setStartPosition(position)
                .setOverlayView(overView)
                .setBackgroundColor(ContextCompat.getColor(this@HotelDetailsActivity, R.color.colorBlack))
                .show()
    }

    private fun checkPermissions(): Boolean {

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED ||
                ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(this,
                    arrayOf(Manifest.permission.ACCESS_FINE_LOCATION,
                            Manifest.permission.ACCESS_COARSE_LOCATION), 112)
            return false
        }

        return true
    }

    private fun showRegisterDialog() {

        val dialog = Dialog(this)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.window.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.setContentView(R.layout.dialog_register_warning)
        val window = dialog.getWindow()
        val wlp = window.getAttributes()
        wlp.width = WindowManager.LayoutParams.MATCH_PARENT
        wlp.height = WindowManager.LayoutParams.MATCH_PARENT
        wlp.gravity = Gravity.CENTER
        window.setAttributes(wlp)

        dialog.frame_register.setOnClickListener {
            dialog.dismiss()
            LoginActivity.startLogin(this, false, Constant.OPEN_FOR_BOOK_HOTEL)
        }
        dialog.show()
    }

    private fun setAvailableDates() {
        doAsync {

            try {

                for (i in 0 until hotelModel!!.availableDates.size) {

                    val hotelDateModel = hotelModel!!.availableDates[i]

                    //  Log.e("AvailableDates", "start date is: ${hotelDateModel.startDate} and end dcate is: ${hotelDateModel.endDate}")
                    val dates = DaysCount().getDates(hotelDateModel.startDate.toLong(), hotelDateModel.endDate.toLong())

                    if (hotelDateModel.endDate.toLong() > maxDate) {
                        maxDate = hotelDateModel.endDate.toLong()
                    }

                    if (hotelDateModel.startDate.toLong() < minDate || minDate == 0L) {
                        minDate = hotelDateModel.startDate.toLong()
                    }

                    dates.forEach {
                        val cal = Calendar.getInstance()
                        cal.timeInMillis = it

                        val ml = (System.currentTimeMillis() - 1 * 24 * 60 * 60 * 1000)

                        if (cal.timeInMillis > ml) {
                            var sym = "$"

                            if (hotelModel!!.currency.toUpperCase() == getString(R.string.hint_usd)) {
                                sym = "$"
                            } else {
                                sym = "KD"
                            }
                            val font = Typeface.createFromAsset(getAssets(), "fonts/Roboto-Regular.ttf");
                            val draw = CalendarUtils.getDrawableText(this@HotelDetailsActivity, sym + hotelDateModel.rent, font, R.color.colorBlack, 4);
                            val eventDay = EventDay(cal, draw)
                            eventMap.put(cal.timeInMillis, eventDay)
                            eventMapMain.put(cal.timeInMillis, eventDay)
                            eventList.add(eventDay)
                        }
                        mapAvailable.put(it, hotelDateModel)
                        availableList.add(cal)
                    }
                }
                maxDate += (1 * 24 * 60 * 60 * 1000)


                if (availableList.size > 0) {
                    val start = minDate
                    val end = maxDate
                    val dates = DaysCount().getDates(start, end)

                    for (i in 0 until dates.size) {

                        if (!mapAvailable.contains(dates[i])) {
                            addInDisableList(dates[i])
                        }
                    }
                }

                addInDisableList(maxDate)
                //     Log.e("AvailableDates", " ** " + mapAvailable.size + " <-->" + disableList.size)

            } catch (e: Exception) {

                Log.e("AvailableDates", "exception is: >>>> ${e.message}")
            } finally {

                uiThread {
                    initDialogDialogForCal()
                }
            }

        }
    }

    private fun addInDisableList(l: Long?) {

        val cal = Calendar.getInstance()
        cal.timeInMillis = l!!

        val font = Typeface.createFromAsset(getAssets(), "fonts/Roboto-Regular.ttf");
        val draw = CalendarUtils.getDrawableText(this@HotelDetailsActivity, "Closed", font, R.color.color_status_red, 6);
        val eventDay = EventDay(cal, draw)

        eventMap.put(cal.timeInMillis, eventDay)
        eventMapMain.put(cal.timeInMillis, eventDay)
        eventList.add(eventDay)

        disableList.add(cal)
        mainDisableList.add(cal)
        mapDisable.put(cal.timeInMillis, cal)
    }

    private fun initDialogDialogForCal() {
        val cal = Calendar.getInstance()
        cal.timeInMillis = System.currentTimeMillis()


        if (availableList.size > 0) {

            calenderStart.timeInMillis = minDate
            calenderEnd.timeInMillis = maxDate

        }

        progress?.show()
        Handler().postDelayed(object : Runnable {
            override fun run() {
                progress?.dismiss()
                dialogCalendar = Dialog(this@HotelDetailsActivity)
                dialogCalendar.requestWindowFeature(Window.FEATURE_NO_TITLE)
                dialogCalendar.window.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
                dialogCalendar.setContentView(R.layout.dialog_calender_range)
                val window = dialogCalendar.getWindow()
                val wlp = window.getAttributes()
                wlp.width = WindowManager.LayoutParams.MATCH_PARENT
                wlp.height = WindowManager.LayoutParams.WRAP_CONTENT
                wlp.gravity = Gravity.CENTER_VERTICAL
                window.setAttributes(wlp)

                val cal = Calendar.getInstance()
                cal.timeInMillis = System.currentTimeMillis()
                dialogCalendar.calender.setDate(cal)

                dialogCalendar.calender.setMinimumDate(cal)

                val ml = (System.currentTimeMillis() - 1 * 24 * 60 * 60 * 1000)
                if (calenderStart.timeInMillis >= ml) {
                    dialogCalendar.calender.setMinimumDate(calenderStart)
                }

                if (availableList.size > 0) {

                    dialogCalendar.calender.setMaximumDate(calenderEnd)
                }

                dialogCalendar.calender.setEvents(eventList)
                //   dialogCalendar.calender.setDisabledDays(disableList)

                dialogCalendar.dialog_frame_done1.setOnClickListener {

                    dialogCalendar.dismiss()

                }
                dialogCalendar.calender.setOnDayClickListener(object : OnDayClickListener {
                    override fun onDayClick(eventDay: EventDay?) {
                        //setBookAvailableDates()

                        setRangeDates()
                    }

                })

                dialogCalendar.dialog_frame_done1.setOnClickListener {
                    val sL = dialogCalendar.calender.selectedDates
                    if (sL.size > 1) {
                        setBookingDates()
                    }
                }
            }

        }, 200)

    }

    private fun setBookingDates() {
        Handler().postDelayed(object : Runnable {
            override fun run() {

                val calendar = dialogCalendar.calender.selectedDates

                if (calendar.size > 0) {
                    rentCount = 0
                    for (i in 0 until calendar.size) {
                        val cal = calendar[i]
                        checkInAvailableDates(cal)
                    }

                    val bookStart = calendar!![0]
                    var bookEnd = calendar!![calendar.size - 1]

                    setTimeInCal(bookEnd)
                    setTimeInCal(bookStart)

                    val dates = DaysCount().getDates(bookStart.timeInMillis, bookEnd.timeInMillis)

                    val rentPerDay = rentCount / (dates.size).toDouble()

                    Log.e("setBookingDates", " " + rentCount + " perDay " + rentPerDay + " " + dates.size)
                    startMs = bookStart.timeInMillis.toString()
                    endMs = bookEnd.timeInMillis.toString()

                    startDate = ConstantFunction.getOnlyDate(bookStart.timeInMillis)
                    endDate = ConstantFunction.getOnlyDate(bookEnd.timeInMillis)

                    dateModel.startDate = startDate
                    dateModel.endDate = endDate

                    setDates()

                    val df = DecimalFormat("###.00")
                    val str = df.format(rentPerDay)

                    setHotelPrice(str)
                    // hint_price_pm.text = "USD $$str " + getString(R.string.ap_hint_pernight)

                    dialogCalendar.dismiss()
                }
            }
        }, 10)
    }

    private fun setTimeInCal(calender: Calendar) {

        calender.set(Calendar.HOUR, 10)
        calender.set(Calendar.MINUTE, 0)
        calender.set(Calendar.SECOND, 0)
        calender.set(Calendar.AM_PM, 0)

    }

    private fun checkInAvailableDates(cal: Calendar) {
        var rent = 0
        for (i in 0 until hotelModel?.availableDates!!.size) {
            val date = hotelModel?.availableDates!![i]
            if (cal.timeInMillis >= date.startDate.toLong() && cal.timeInMillis <= date.endDate.toLong()) {
                rent = date.rent.toInt()

                // Log.e("checkInAvailableDates", " Date "+ConstantFunction.getOnlyDate(cal.timeInMillis) + rent +" ")
                break
            }
        }

        rentCount += rent


    }

    private fun setBookAvailableDates() {
        Handler().postDelayed(object : Runnable {
            override fun run() {

                val calendar = dialogCalendar.calender.selectedDates

                val tempDisableList = ArrayList<Calendar>()
                if (calendar!!.size > 1) {
                    val calS = calendar[0]
                    val calE = calendar[calendar.size - 1]
                    var match = false
                    match = getDisableListFromSelectedDates(tempDisableList, calS.timeInMillis, calE.timeInMillis)

                    dialogCalendar.calender.setDisabledDays(ArrayList<Calendar>())
                    dialogCalendar.calender.setEvents(eventList)

                    doAsync {
                        disableList.clear()
                        mainDisableList.forEach {
                            mapDisable.put(it.timeInMillis, it)
                            mapAvailable.remove(it.timeInMillis)
                            disableList.add(it)
                        }

                    }

                } else {
                    if (calendar.size > 0 && disableList.size > 0) {

                        val calS = calendar[0]
                        val calE = Calendar.getInstance()
                        calE.timeInMillis = maxDate
                        Log.e("calendar", ConstantFunction.getOnlyDate(calS.timeInMillis, Constant.DEFAULT_DATE_FORMAT2))
                        if (!mapAvailable.containsKey(calS.timeInMillis)) {

                            val dList = ArrayList<Calendar>()
                            dList.add(calS)
                            dialogCalendar.calender.setDisabledDays(dList)
                            dialogCalendar.calender.setEvents(eventList)
                            return
                        }

                        val list = ArrayList<Calendar>()

                        var match = getDisableListFromSelectedDates2(list, calS.timeInMillis, calE.timeInMillis)

                        Handler().postDelayed(object : Runnable {
                            override fun run() {

                                val match2 = disableBeforeSelectDay(list, minDate, calS.timeInMillis)

                                if (match) {
                                    disableList.clear()
                                    mapDisable.keys.forEach {
                                        disableList.add(mapDisable.get(it)!!)
                                    }
                                    dialogCalendar.calender.setDisabledDays(list)
                                } else {
                                    dialogCalendar.calender.setDisabledDays(list)
                                    dialogCalendar.calender.setDisabledDays(ArrayList<Calendar>())
                                }
                                dialogCalendar.calender.setEvents(eventList)

                            }
                        }, 100)
                    }
                }
            }

        }, 100)
    }

    private fun setRangeDates() {
        val ml = (System.currentTimeMillis() - 1 * 24 * 60 * 60 * 1000)
        Handler().postDelayed(object : Runnable {
            override fun run() {

                val calendar = dialogCalendar.calender.selectedDates

                if (calendar.size > 1) {
                    val cal1 = calendar[0]
                    val cal2 = calendar[calendar.size - 1]

                    val dates = DaysCount().getDates(cal1.timeInMillis, cal2.timeInMillis)

                    var match = false
                    val list = ArrayList<Calendar>()
                    for (i in dates.indices) {
                        if (mapDisable.containsKey(dates[i])) {

                            match = true
                            break
                        }
                    }

                    if (match) {
                        dialogCalendar.calender.selectedDates = ArrayList<Calendar>()
                        dialogCalendar.calender.setEvents(eventList)
                    }
                }

                if (calendar.size == 1) {
                    val cal = calendar[0]
                    if (!mapAvailable.containsKey(cal.timeInMillis)) {
                        val dList = ArrayList<Calendar>()
                        dList.add(cal)
                        dialogCalendar.calender.setDisabledDays(dList)
                        dialogCalendar.calender.setEvents(eventList)
                        return
                    } else {
                        for (i in hotelModel!!.availableDates.indices) {
                            val aDate = hotelModel!!.availableDates[i]
                            if (cal.timeInMillis >= aDate.startDate.toLong() && cal.timeInMillis <= aDate.endDate.toLong()) {
                                if (mapSelected.containsKey(cal.timeInMillis)) {

                                    mapSelected.clear()
                                } else {
                                    if (selectedList.size > 0) {
                                        val calPrev = Calendar.getInstance()
                                        val calNext = Calendar.getInstance()
                                        calPrev.timeInMillis = aDate.startDate.toLong() - (1 * 24 * 60 * 60 * 1000)
                                        calNext.timeInMillis = aDate.endDate.toLong() + (1 * 24 * 60 * 60 * 1000)

                                        val calStart = selectedList[0]
                                        val calEnd = selectedList[selectedList.size - 1]

                                        if (aDate.startDate.toLong() > calEnd.timeInMillis) {
                                            if (mapDisable.containsKey(calPrev.timeInMillis)) {
                                                mapSelected.clear()
                                            }
                                        }

                                        if (aDate.endDate.toLong() < calStart.timeInMillis) {
                                            if (mapDisable.containsKey(calNext.timeInMillis)) {
                                                mapSelected.clear()
                                            }
                                        }
                                    } else {
                                        mapSelected.clear()
                                    }

                                    val dates = DaysCount().getDates(aDate.startDate.toLong(), aDate.endDate.toLong())
                                    for (j in dates.indices) {
                                        val calSel = Calendar.getInstance()
                                        calSel.timeInMillis = dates[j]

                                        if (calSel.timeInMillis > ml) {
                                            mapSelected.put(calSel.timeInMillis, calSel)
                                        }
                                    }
                                }

                                selectedList.clear()
                                mapSelected.keys.forEach {
                                    val cal = Calendar.getInstance()
                                    cal.timeInMillis = it
                                    selectedList.add(cal)
                                }
                                Collections.sort(selectedList, SortSelectedDate())

                                dialogCalendar.calender.selectedDates = selectedList
                                dialogCalendar.calender.setEvents(eventList)

                                if (selectedList.size > 1) {
                                    val fDays = DaysCount().getDates(selectedList[0].timeInMillis, selectedList[selectedList.size - 1].timeInMillis)
                                    dialogCalendar.dialog_hint_date.text = "" + (fDays.size) + " " + getString(R.string.hd_hint_night_selected)
                                }
                                break
                            }
                        }
                    }

                }

            }

        }, 100)
    }

    private fun getDisableListFromSelectedDates(tempDisableList: ArrayList<Calendar>, calS: Long, calE: Long): Boolean {

        var match = false
        val dates = DaysCount().getDates(calS, calE)

        runOnUiThread {
            dialogCalendar.dialog_hint_date.text = "" + (dates.size) + " " + getString(R.string.hd_hint_night_selected)
        }
        for (i in 0 until dates.size) {

            val cal = Calendar.getInstance()
            cal.timeInMillis = dates[i]

            if (mapAvailable.size > 0 && !mapAvailable.contains(cal.timeInMillis)) {
                match = true
                continue
            }
            if (match) {
                tempDisableList.add(cal)
            }
        }

        return match
    }

    private fun getDisableListFromSelectedDates2(tempDisableList: ArrayList<Calendar>, calS: Long, calE: Long): Boolean {

        var match = false
        val dates = DaysCount().getDates(calS, calE)
        for (i in 0 until dates.size) {

            val cal = Calendar.getInstance()
            cal.timeInMillis = dates[i]
            Log.e("FromSelectedDates2 ", " Match ${cal.timeInMillis} " + (mapAvailable.contains(cal.timeInMillis)))

            if (!match && mapAvailable.size > 0 && !mapAvailable.contains(cal.timeInMillis)) {
                match = true
                break
            }
            if (!match) {
                tempDisableList.add(cal)
            }
        }

        return match
    }

    private fun disableBeforeSelectDay(list: ArrayList<Calendar>, calS: Long, calE: Long): Boolean {

        val dates2 = DaysCount().getDates(calS, calE)
        Collections.reverse(dates2)
        var match = false

        for (i in 0 until dates2.size) {
            val cal = Calendar.getInstance()
            cal.timeInMillis = dates2[i]
            Log.e("BeforeSelectDay ", " Match ${cal.timeInMillis} " + (mapAvailable.contains(cal.timeInMillis)))

            if (!match && (mapAvailable.size > 0 && !mapAvailable.contains(cal.timeInMillis))) {
                match = true
                break
            }
            if (!match) {
                list.add(cal)
            }
        }

        return match
    }

    private fun setDates() {
            date_start.setText(dateModel.startDate)
            date_end.setText(dateModel.endDate)

    }

    private fun calculateRent(sD: Boolean = false) {
        try {
            var dates = ArrayList<Long>()
            if (sD) {
                dates.add(System.currentTimeMillis())
            } else {
                dates = DaysCount().getDates(dateModel.startDate, dateModel.endDate)
            }

            //    Log.e("DaysCount"," days ${dates.size}")
            rent = 0

            var mRates = (hotelModel!!.rate.toInt() + hotelModel!!.rate.toInt()) / 2
            rent = mRates * dates.size
            /*for (i in 0 until dates.size){
                rent+= hotelModel!!.rate.toInt()
            }*/

            setHotelPrice(hotelModel?.rate!!)
            //hint_price_pm.text = "USD $${hotelModel?.rate} " + getString(R.string.ap_hint_pernight)
            for (i in 0 until hotelModel!!.availableDates.size) {
                val dateModel = hotelModel!!.availableDates[i]

                if (startMs.toLong() >= dateModel.startDate.toLong() && endMs.toLong() <= dateModel.endDate.toLong()) {
                    rent = 0
                    //  var mRate =  (dateModel.rent.toInt()+dateModel.nightRent.toInt())/2
                    var mRate = (dateModel.rent.toInt())
                    // mRate = mRate * dates.size
                    rent = mRate
                    /*  for (i in 0 until dates.size){
                          rent+= dateModel!!.rent.toInt()
                      }*/
                    // hint_price_pm.text = "USD $$mRate" + getString(R.string.ap_hint_pernight)

                    setHotelPrice("" + mRate)
                    break
                }
            }

        } catch (e: Exception) {
            e.printStackTrace()
        }

    }

    private fun validation(): Boolean {

        if (TextUtils.isEmpty(date_start.text.toString().trim())) {
            Toast.makeText(this@HotelDetailsActivity, getString(R.string.hd_hint_select_start_date), Toast.LENGTH_SHORT).show()
            return false
        } else {
            if (date_start.text.toString().equals(getString(R.string.from), true)) {
                Toast.makeText(this@HotelDetailsActivity, getString(R.string.hd_hint_select_start_date), Toast.LENGTH_SHORT).show()
                return false
            }
        }

        if (TextUtils.isEmpty(date_end.text.toString().trim())) {
            Toast.makeText(this@HotelDetailsActivity, getString(R.string.hd_hint_select_end_date), Toast.LENGTH_SHORT).show()
            return false
        } else {
            if (date_end.text.toString().equals(getString(R.string.hd_hint_to), true)) {
                Toast.makeText(this@HotelDetailsActivity, getString(R.string.hd_hint_select_end_date), Toast.LENGTH_SHORT).show()
                return false
            }
        }

        return true
    }

    private fun getFeedback() {

        val param = JSONObject()
        param.put("api_key", Constant.API_KEY)
        param.put("hotel_id", hotelModel?.id)

        frame_load_rat.visibility = View.VISIBLE
        ServiceRequest(object : ApiResponseListener {
            override fun onCompleted(`object`: Any) {
                try {
                    frame_load_rat.visibility = View.GONE
                    val json = JSONObject(`object`.toString())
                    if (json.has("data")) {
                        val array = json.getJSONArray("data")
                        for (i in 0 until array.length()) {
                            var obj = JSONObject()
                            if (array.get(i) is JSONObject) {
                                obj = array.getJSONObject(i)

                                if (obj.has("avgCommunication")) {
                                    rat_communication.rating = obj.get("avgCommunication").toString().toFloat()
                                }

                                if (obj.has("avgProficiency")) {
                                    rating_proficiency.rating = obj.get("avgProficiency").toString().toFloat()
                                }

                                if (obj.has("avgPrice")) {
                                    rating_price.rating = obj.get("avgPrice").toString().toFloat()
                                }

                                if (obj.has("avgDelivery")) {
                                    rating_delivery.rating = obj.get("avgDelivery").toString().toFloat()
                                }
                            }
                        }

                        if (array.length() <= 0) {
                            rating_price.rating = 1F
                            rat_communication.rating = 1F
                            rating_delivery.rating = 1F
                            rating_proficiency.rating = 1F
                        }
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

            override fun onError(errorMessage: String) {
                Toast.makeText(this@HotelDetailsActivity, "", Toast.LENGTH_SHORT).show()
            }

        }).getHotelFeedbackFeedback(param)
    }

    private fun createParam(): JSONObject {
        val param = JSONObject()
        param.put("api_key", Constant.API_KEY)
        param.put("hotel_id", hotelModel?.id)
        param.put("buyer_id", prefManager.getString(Constant.KEY_USER_ID))
        param.put("owner_id", hotelModel?.ownerId)
        param.put("children", hotelModel?.children)
        param.put("adult", hotelModel?.person)
        param.put("room", hotelModel?.rooms)
        param.put("date_from", startMs)
        param.put("date_to", endMs)
        param.put("price", rentCount)

        return param
    }

    private fun addToFav() {
        if (hotelModel == null) {
            return
        }
        val param = JSONObject()
        param.put("api_key", Constant.API_KEY)
        param.put("hotel_id", hotelModel?.id)
        param.put("user_id", prefManager.getString(Constant.KEY_USER_ID))

        progress?.show()

        ServiceRequest(object : ApiResponseListener {
            override fun onCompleted(`object`: Any) {
                progress?.dismiss()
                update = true
                im_fav.setImageResource(R.drawable.ic_like_red_on)
                im_fav.tag = true
                doAsync {
                    val fav = FavPost()
                    fav.id = hotelModel?.id!!
                    fav.post_id = hotelModel?.id!!
                    AppDatabase.getAppDatabase().favPostDao().insert(fav)
                }
            }

            override fun onError(errorMessage: String) {
                progress?.dismiss()
                errorDialog?.show(getString(R.string.hint_error), "$errorMessage")
            }

        }).addToFav(param)
    }

    private fun removeToFav() {
        if (hotelModel == null) {
            return
        }
        val param = JSONObject()
        param.put("api_key", Constant.API_KEY)
        param.put("hotel_id", hotelModel?.id)
        param.put("user_id", prefManager.getString(Constant.KEY_USER_ID))

        progress?.show()

        ServiceRequest(object : ApiResponseListener {
            override fun onCompleted(`object`: Any) {
                progress?.dismiss()

                update = true

                im_fav.setImageResource(R.drawable.ic_like_gray_off)
                im_fav.tag = false
                doAsync {
                    val fav = FavPost()
                    fav.id = hotelModel?.id!!
                    fav.post_id = hotelModel?.id!!
                    AppDatabase.getAppDatabase().favPostDao().deleteFavPost(fav.id)
                }
            }

            override fun onError(errorMessage: String) {
                progress?.dismiss()
                errorDialog?.show(getString(R.string.hint_error), "$errorMessage")
            }

        }).removeToFav(param)
    }

    private fun getHotel() {

        val param = JSONObject()
        param.put("api_key", Constant.API_KEY)
        param.put("hotel_id", hotelModel?.id)

        progress?.show()

        ServiceRequest(object : ApiResponseListener {
            override fun onCompleted(`object`: Any) {
                bookHotel()
            }

            override fun onError(errorMessage: String) {
                progress?.dismiss()
                errorDialog?.show(getString(R.string.hint_error), errorMessage)
            }

        }).getPropertyStatus(param)
    }

    private fun bookHotel() {
        val param = createParam()
        progress?.show()

        ServiceRequest(object : ApiResponseListener {
            override fun onCompleted(`object`: Any) {
                progress?.dismiss()

                errorDialog?.title = getString(R.string.book_req_sent)
                errorDialog?.content = getString(R.string.hd_hint_book_req_sent)
                errorDialog?.show()
                errorDialog?.listenr = object : DialogListener() {
                    override fun okClick() {

                        finish()
                    }
                }

            }

            override fun onError(errorMessage: String) {
                progress?.dismiss()
                //Toast.makeText(this@HotelDetailsActivity,"Booking Failed",Toast.LENGTH_SHORT).show()

                errorDialog?.show(getString(R.string.hint_error), "$errorMessage")
            }

        }).bookHotel(param)
    }


    private fun generateLink() {
        //linkText = ConstantFunction.getDynamicLink(hotelModel!!.id)
        if (TextUtils.isEmpty(linkText)) {
            FirebaseApp.initializeApp(this)
            val url = "https://kashtahPORT.com?post_id=${hotelModel?.id}&owner_id=${hotelModel!!.ownerId}&type=${hotelModel!!.type}"
            val shortLinkTask = FirebaseDynamicLinks.getInstance().createDynamicLink()
                    .setLink(Uri.parse(url))
                    .setDomainUriPrefix(Constant.PREFIX)
                    .setAndroidParameters(
                            DynamicLink.AndroidParameters.Builder("com.itwhiz4u.q8mercato.kashtahport")
                                    .build())

                    .setIosParameters(
                            DynamicLink.IosParameters.Builder("com.itwhiz4u.q8mercato.kashtahport")
                                    .setAppStoreId("1467932974")
                                    .build())

                    .setSocialMetaTagParameters(DynamicLink.SocialMetaTagParameters.Builder()
                            .setTitle(hotelModel!!.name)
                            .setDescription(hotelModel!!.description)
                            .setImageUrl(Uri.parse(hotelModel!!.imageList[0].url))
                            .build())

                    .setGoogleAnalyticsParameters(
                            DynamicLink.GoogleAnalyticsParameters.Builder()
                                    .setSource("orkut")
                                    .setMedium("social")
                                    .setCampaign("example-promo")
                                    .build())

                    .buildShortDynamicLink()

                    .addOnSuccessListener { result ->

                        val shortLink = result.shortLink
                        val flowchartLink = result.previewLink
                        linkText = shortLink.toString()

                        ConstantFunction.saveDynamicLink(hotelModel!!.id, linkText)
                        Log.e(TAG, "LINK :: $shortLink  *** $flowchartLink")
                        shareLink()

                    }.addOnFailureListener {
                        Log.e(TAG, "LINK :: addOnFailureListener  *** ")
                        progress?.dismiss()
                    }
        } else {


            shareLink()

        }
    }

    private fun shareLink() {

        runOnUiThread {

            progress?.dismiss()

            val shareIntent = Intent()
            shareIntent.setAction(Intent.ACTION_SEND)
            shareIntent.type = "text/plain"
            shareIntent.putExtra(Intent.EXTRA_TEXT, linkText)
            shareIntent.putExtra(Intent.EXTRA_SUBJECT, linkText)

            startActivity(Intent.createChooser(shareIntent,"Select"))

        }


    }

    inner class SortSelectedDate : Comparator<Calendar> {
        override fun compare(o1: Calendar?, o2: Calendar?): Int {

            return if (o2!!.timeInMillis > o1!!.timeInMillis) -1 else 1
        }


    }
}
