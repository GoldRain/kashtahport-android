package com.itwhiz4u.q8mercato.kashtahport.activities

import android.app.Activity
import android.content.Intent
import android.graphics.Bitmap
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.support.annotation.RequiresApi
import android.util.Log
import android.view.View
import android.webkit.WebResourceRequest
import android.webkit.WebView
import android.webkit.WebViewClient
import com.itwhiz4u.q8mercato.kashtahport.Api.ApiResponseListener
import com.itwhiz4u.q8mercato.kashtahport.Api.ApiUrl
import com.itwhiz4u.q8mercato.kashtahport.Api.PaymentRequest
import com.itwhiz4u.q8mercato.kashtahport.Api.ServiceRequest
import com.itwhiz4u.q8mercato.kashtahport.R
import com.itwhiz4u.q8mercato.kashtahport.customDialog.CustomDialog
import com.itwhiz4u.q8mercato.kashtahport.customDialog.DialogListener
import com.itwhiz4u.q8mercato.kashtahport.helper.Constant
import com.itwhiz4u.q8mercato.kashtahport.model.HotelBookingModel
import com.itwhiz4u.q8mercato.kashtahport.model.TapPaymentModel
import kotlinx.android.synthetic.main.activity_payment.*
import org.json.JSONObject

class PaymentActivity : BaseActivity() {

    lateinit var tapPaymentModel: TapPaymentModel
    var pay = false
    var bookingModel:HotelBookingModel?= null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_payment)
        supportActionBar?.hide()

        bookingModel = intent.getParcelableExtra(Constant.KEY_BOOKING)

        back.setOnClickListener {
            super.onBackPressed()
        }
        Log.e("PaymentActivity", ApiUrl.ABOUT_US_WEB_VIEW)
        web_view.settings.javaScriptEnabled = true
        web_view.webViewClient = MyClient()
       // web_view.loadUrl(ApiUrl.ABOUT_US_WEB_VIEW)

        progress?.show()

        tapPaymentModel = TapPaymentModel()
        tapPaymentModel.firstName = prefManager.getString(Constant.KEY_USER_NAME)
        tapPaymentModel.receiptEmail = prefManager.getString(Constant.KEY_USER_EMAIL)
        tapPaymentModel.receiptSms = prefManager.getString(Constant.KEY_USER_PHONE)
        tapPaymentModel.amount = bookingModel?.price!!
        tapPaymentModel.currency = bookingModel!!.hotelModel!!.currency.toUpperCase()


        //GoSellSDK.init(this,Constant.TEST_TAP_SEC_KEY,packageName)
       /* val param = createChargeParam()
        PaymentRequest(object :ApiResponseListener{
            override fun onCompleted(`object`: Any) {
                progress?.dismiss()
                web_view.clearCache(true)
                web_view.loadUrl(`object`.toString())

            }

            override fun onError(errorMessage: String) {
                progress?.dismiss()
                errorDialog?.show("Error!!","$errorMessage")
            }
        }).getChargeRequestt(param)*/



        PaymentRequest(object :ApiResponseListener{
            override fun onCompleted(`object`: Any) {
                progress?.dismiss()
                web_view.clearCache(true)
                web_view.loadUrl(`object`.toString())

            }

            override fun onError(errorMessage: String) {
                progress?.dismiss()
                errorDialog?.show("Error!!","$errorMessage")
            }

        }).getChargeRequest(tapPaymentModel)
    }

    override fun onResume() {
        if(pay){
            onBackPressed()
        }

        super.onResume()

    }

    override fun onBackPressed() {
        if(pay){
            setResult(Activity.RESULT_OK)
        }
        super.onBackPressed()
    }
    /*{
  "amount": 1,
  "currency": "KWD",
  "threeDSecure": true,
  "save_card": false,
  "description": "Test Description",
  "statement_descriptor": "Sample",
  "metadata": {
    "udf1": "test 1",
    "udf2": "test 2"
  },
  "reference": {
    "transaction": "txn_0001",
    "order": "ord_0001"
  },
  "receipt": {
    "email": false,
    "sms": true
  },
  "customer": {
    "first_name": "test",
    "middle_name": "test",
    "last_name": "test",
    "email": "test@test.com",
    "phone": {
      "country_code": "965",
      "number": "50000000"
    }
  },
  "source": {
    "id": "src_kw.knet"
  },
  "post": {
    "url": "http://your_website.com/post_url"
  },
  "redirect": {
    "url": "http://your_website.com/redirect_url"
  }
}*/
    private fun createChargeParam(): JSONObject {
        val param = JSONObject()


        val paramPhone = JSONObject()
        paramPhone.put("country_code","876")
        paramPhone.put("number",prefManager.getString(Constant.KEY_USER_PHONE))

        val paramCustomer = JSONObject()
        paramCustomer.put("first_name",prefManager.getString(Constant.KEY_USER_NAME))
        paramCustomer.put("email",prefManager.getString(Constant.KEY_USER_EMAIL))
        paramCustomer.put("phone",paramPhone)


        val paramMetsData = JSONObject()
        paramMetsData.put("booking_id",bookingModel?.id)


        val paramSource = JSONObject()
        paramSource.put("id","src_kw.knet")

        val paramRef = JSONObject()
        paramRef.put("transaction","TXN_${System.currentTimeMillis()}")
        paramRef.put("order","ORD_${System.currentTimeMillis()}")

        val paramRed = JSONObject()
        paramRed.put("url",ApiUrl.getPaymetLoader)

        val paramPost = JSONObject()
        paramPost.put("url",ApiUrl.getPaymetLoader)


        val paramReceipt = JSONObject()
        paramReceipt.put("email",false)
        paramReceipt.put("phone",true)


        param.put("amount",bookingModel?.hotelModel?.rate)
        param.put("currency","KWD")
        param.put("description","This is Test Payment")
        param.put("statement_descriptor","This is Test Payment")
        param.put("threeDSecure",true)
        param.put("save_card",true)


        param.put("metadata",paramMetsData)
        param.put("reference",paramRef)
        param.put("customer",paramCustomer)
        param.put("source",paramSource)
        param.put("post",paramPost)
        param.put("redirect",paramRed)
        return param
    }


    inner class MyClient: WebViewClient() {

        override fun onPageStarted(view: WebView?, url: String?, favicon: Bitmap?) {
            Log.e("ABOUT_US"," onPageStarted")
            runOnUiThread {
                frame_loader.visibility = View.VISIBLE
            }
        }

        override fun onPageFinished(view: WebView?, url: String?) {
            Log.e("ABOUT_US"," onPageFinished  $url")
            runOnUiThread {
                frame_loader.visibility = View.GONE
            }

            val uri = Uri.parse(url)
            if(uri!= null){
                if (url!!.startsWith("${ApiUrl.getPaymetLoader}?chargeid=")) {
                    if (uri.getQueryParameter("result") == "SUCCESS"){
                        tapPaymentModel.chargeid = uri.getQueryParameter("chargeid")
                        tapPaymentModel.crd = uri.getQueryParameter("crd")
                        tapPaymentModel.crdtype = uri.getQueryParameter("crdtype")
                        tapPaymentModel.hash = uri.getQueryParameter("hash")
                        tapPaymentModel.payid = uri.getQueryParameter("payid")
                        tapPaymentModel.ref = uri.getQueryParameter("ref")
                        tapPaymentModel.result = uri.getQueryParameter("result")
                        tapPaymentModel.trackid = uri.getQueryParameter("trackid")
                       /* setResult(Activity.RESULT_OK, Intent().putExtra("model",tapPaymentModel))
                        finish()*/
                      if(!called){
                          called = true
                          verifyPayments(tapPaymentModel)
                      }
                    }
                }
            }
        }



        @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
        override fun shouldOverrideUrlLoading(view: WebView?, request: WebResourceRequest?): Boolean {
            Log.e("ABOUT_US"," shouldOverrideUrlLoading ")
            view!!.loadUrl(request!!.url.toString())
            return super.shouldOverrideUrlLoading(view, request)
        }
    }

    /*hotel_id
owner_id
buyer_id
booking_id
amount
payment_type
transaction_id
title
type*/
    var called = false
    private fun verifyPayments(tapPaymentModel: TapPaymentModel) {
        val param =  JSONObject()
        param.put("api_key",Constant.API_KEY)
        param.put("hotel_id",bookingModel?.hotelId)
        param.put("owner_id",bookingModel?.ownerId)
        param.put("buyer_id",bookingModel?.buyerId)
        param.put("booking_id",bookingModel?.id)
        param.put("amount",tapPaymentModel.amount)
        param.put("payment_type","Card")
        param.put("transaction_id",tapPaymentModel.ref)
        param.put("title","Paid For Booking ${bookingModel?.hotelModel!!.name}")
        param.put("display_number","${System.currentTimeMillis()}")
        param.put("type","Book")
        param.put("card_type",tapPaymentModel.crdtype)
        param.put("card",tapPaymentModel.crd)
        param.put("ref_no",tapPaymentModel.ref)
        param.put("currency",bookingModel?.hotelModel!!.currency.toLowerCase())


       runOnUiThread {
           progress?.show()
       }
        PaymentRequest(object :ApiResponseListener{
            override fun onCompleted(`object`: Any) {
                updateStatus("Paid")
                runOnUiThread {
                    progress?.dismiss()
                }
                pay = true
               // Toast.makeText(this@PaymentActivity,"Payment successfull",Toast.LENGTH_SHORT).show()
                val  intent = Intent(this@PaymentActivity,AllDoneActivity::class.java)
                intent.putExtra("fromPay",true)
                startActivity(intent)
            }

            override fun onError(errorMessage: String) {
                runOnUiThread {
                    CustomDialog(this@PaymentActivity).showPaymentFailedDialog(tapPaymentModel,object :DialogListener(){
                        override fun okClick() {
                            startActivity(Intent(this@PaymentActivity,SupportActivity::class.java))
                        }
                    })
                    progress?.dismiss()
                }

            }

        }).verifyPayments(param)
    }


    fun updateStatus( status: String) {
        val param = JSONObject()
        param.put("api_key", Constant.API_KEY)
        param.put("booking_id", bookingModel?.id)
        param.put("status", status)
        //progress?.show()

        ServiceRequest(object : ApiResponseListener {
            override fun onCompleted(`object`: Any) {

            }

            override fun onError(errorMessage: String) {

            }

        }).updateBooking(param)
    }
}
