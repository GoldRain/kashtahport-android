package com.lite.imagepickerlib


import android.app.Activity
import android.content.Intent
import android.media.MediaMetadataRetriever
import android.media.MediaPlayer
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.support.annotation.RequiresApi
import android.support.v4.app.Fragment
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.RecyclerView
import android.text.TextUtils
import android.util.Log
import android.util.TimeUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.MediaController
import android.widget.Toast
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.fragment_video.view.*
import java.io.File
import java.util.*
import java.util.concurrent.TimeUnit
import kotlin.collections.HashSet


/**
 * A simple [Fragment] subclass.
 */
class VideoFragment : Fragment() {


    private val TAG = "VideoFragment"
    private var imagesList = ArrayList<ImageFile>()
    private var selectedImagesList = ArrayList<String>()

    private var scale = true
    var multipleSelection = false
    private var MAX_SELECTION = 5
    private var noOfSelection = 0

    private var mPlayer = MediaPlayer()
    private var imagePickActivity: GalleryActivity? = null
    fun setImagePickActivity(imagePickActivity: GalleryActivity) {
        this.imagePickActivity = imagePickActivity
    }

    private var mediaController: MediaController? = null
    private lateinit var fragmentView: View

    private val failedPath = HashSet<String>()
    private var  curentPosition = 0

    override fun onPause() {
        if (mPlayer.isPlaying) {
            mPlayer.stop()
            mPlayer.reset()
        }
        super.onPause()
    }

    override fun onDestroy() {
        mPlayer.release()
        super.onDestroy()
    }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        fragmentView = inflater.inflate(R.layout.fragment_video, container, false)

        mediaController = MediaController(activity!!)
        fragmentView.list_view.layoutManager = GridLayoutManager(activity!!, 3) as RecyclerView.LayoutManager?
        fragmentView.list_view.addItemDecoration(EqualSpacingItemDecoration(3, 2, false, 0))
        fragmentView.list_view.adapter = GalleryVideoAdapter(activity!!, imagesList, ListItemClickListener(), 0)


        fragmentView.im_video.setOnClickListener {
            fragmentView.selected_image.visibility = View.GONE
            fragmentView.im_video.visibility = View.GONE

            playVideo()
        }
        return fragmentView
    }

    fun cropImage() {

        if (failedPath.contains(fileUrl)) {
            Toast.makeText(activity!!, "This video can't play. Select another", Toast.LENGTH_SHORT).show()
            return
        }

        val file = File(fileUrl)
        val fileSizeInMb = (file!!.length() / 1024) / 1024.toDouble()
        if (fileSizeInMb > 20) {
            Toast.makeText(activity!!, "File size should be less than 20 mb.", Toast.LENGTH_SHORT).show()
            return
        }

        val duration = getFileDuration()

        if (duration > 1) {
            Toast.makeText(activity!!, "File duration should be less than 1 minute.", Toast.LENGTH_SHORT).show()
            return
        }
        val intent = Intent()
        val arrayList = ArrayList<String>()
        if (selectedImagesList.isEmpty()) {
            arrayList.add(fileUrl!!)
        } else {
            for (path in selectedImagesList)
                arrayList.add(path)
        }
        intent.putExtra("images", arrayList)
        intent.putExtra("isVideo", true)

        imagePickActivity?.onFinishScreen(Activity.RESULT_OK, intent)       // comment this for edit images
    }


    private fun selectImage(position: Int) {

        if(curentPosition == position){
            return
        }
        curentPosition = position
        val tempfileUrl = if (selectedImagesList.isNotEmpty()) {
            selectedImagesList[selectedImagesList.size - 1]
        } else {
            imagesList[position].path
        }


        if(!TextUtils.isEmpty(fileUrl)){
            if (mPlayer.isPlaying) {
                mPlayer.stop()
                mPlayer.reset()
            }
        }

        if (failedPath.contains(tempfileUrl!!)) {
            fileUrl = tempfileUrl
            fragmentView.selected_image.visibility = View.VISIBLE
            fragmentView.im_video.visibility = View.VISIBLE
            Toast.makeText(activity!!, "This video can't play. Select another", Toast.LENGTH_SHORT).show()

        } else {

            fileUrl = tempfileUrl
            fragmentView.selected_image.visibility = View.GONE
            fragmentView.im_video.visibility = View.GONE
            playVideo()


        }
        Glide.with(activity!!).load(File(fileUrl)).into(fragmentView.selected_image)
    }


    private fun playVideo() {
        //    mPlayer =  MediaPlayer.create(activity!!, Uri.parse(fileUrl));

        fragmentView.videoView.setVideoPath(fileUrl);
        fragmentView.videoView.setMediaController(mediaController);


        fragmentView.videoView.start();


        fragmentView.videoView.setOnErrorListener(MediaPlayer.OnErrorListener { mediaPlayer, i, i1 ->
            false
        })
        fragmentView.videoView.setOnPreparedListener(MediaPlayer.OnPreparedListener {
            fragmentView.progress_bar.visibility = View.GONE

            Log.e(TAG, "isReady to play")
        })

        fragmentView.videoView.setOnErrorListener(object : MediaPlayer.OnErrorListener {
            override fun onError(p0: MediaPlayer?, p1: Int, p2: Int): Boolean {
                failedPath.add(fileUrl!!)
                fileUrl = ""
                fragmentView.selected_image.visibility = View.VISIBLE
                fragmentView.im_video.visibility = View.VISIBLE
                return false
            }

        })

        fragmentView.videoView.setOnInfoListener(object : MediaPlayer.OnInfoListener {
            override fun onInfo(p0: MediaPlayer?, p1: Int, p2: Int): Boolean {
                Log.e(TAG, "onInfo")
                mPlayer = p0!!
                return true
            }

        })

    }

    private var fileUrl: String? = null
    fun notifyList(list: ArrayList<ImageFile>) {
        for (imageFile in list) {
            imagesList.add(imageFile)
        }
        fragmentView.list_view.adapter!!.notifyDataSetChanged()
        //fragmentView.selected_image.setUri(Uri.fromFile(File(imagesList[0].path)))
        fileUrl = imagesList[0].path
        Glide.with(activity!!).load(File(imagesList[0].path)).into(fragmentView.selected_image)
        //  playVideo()
    }

    fun getFileDuration(): Long {
        val retriever = MediaMetadataRetriever();
//use one of overloaded setDataSource() functions to set your data source
        retriever.setDataSource(context, Uri.fromFile(File(fileUrl)))
        val time = retriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION);
        val timeInMillisec = time.toLong()
        val mn = TimeUnit.MILLISECONDS.toMinutes(timeInMillisec)
        val sc = TimeUnit.MILLISECONDS.toSeconds(timeInMillisec)
        Log.e(TAG, "DURATION $time $timeInMillisec  $mn $sc")
        retriever.release()
        return mn
    }

    private inner class ListItemClickListener : GalleryVideoAdapter.ItemClickListener {
        override fun onItemClick(position: Int) {
            //fragmentView.selected_image.setUri(Uri.fromFile(File(imageFile.path)))
            if (multipleSelection) {
                if (imagesList[position].checked == 0) {
                    if (noOfSelection == MAX_SELECTION) {
                        Toast.makeText(activity!!, "Max images selected.", Toast.LENGTH_SHORT).show()
                        return
                    }
                    imagesList[position].checked = 1
                    selectedImagesList.add(imagesList[position].path)
                    noOfSelection++
                } else {
                    imagesList[position].checked = 0
                    selectedImagesList.remove(imagesList[position].path)
                    noOfSelection--
                }
                fragmentView.list_view.adapter!!.notifyItemChanged(position)
            }
            selectImage(position)
            fragmentView.app_bar_view.setExpanded(true)
        }
    }


}// Required empty public constructor
